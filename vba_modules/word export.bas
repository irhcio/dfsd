Attribute VB_Name = "Word Export"
Option Compare Database

Private Const WD_DONOTSAVECHANGES = 0
Private Const WD_COLLAPSE_END = 0
Private Const WD_ALIGN_PARAGRAPH_RIGHT = 2
Private Const WD_ALIGN_PARAGRAPH_CENTER = 1
Private Const WD_ALIGN_PARAGRAPH_LEFT = 0
Dim f As String
Dim S As Integer
Dim B As Boolean
Dim i As Boolean
Dim U As Boolean
Dim SH As Boolean
Dim A As Integer
Dim II As Integer
Dim RR As Boolean
Dim strErrMessage As String
Option Explicit


Function word()

    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim CPath As String
    Dim TF As String
    Dim DF As String
    Dim objWord As Object
    Dim objWordDoc As Object
    Dim myrange As Object
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim strProgress As String
        
    Set dbs = CurrentDb
    Set objWord = CreateObject("Word.Application")
    CPath = Left(CurrentDb.Name, Len(CurrentDb.Name) - Len(Dir(CurrentDb.Name)))
    TF = Forms![FormSSV6]![Text0]
    DF = Forms![FormSSV6]![Text1]
    
    strProgress = Create_Msg([Forms]![MENU-NEW]![Text118], 4571)
    
    Set objWordDoc = objWord.Documents.Add
    objWordDoc.Content.InsertFile TF
    II = 1
    
    Call S01(myrange, objWordDoc)
    strSql = "SELECT * FROM [Word Export Template Temp] Order By [ID]"
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    Call Call_prog("")
    
    While Not rst.EOF
        If rst![Item] = "1" Then Call S02(myrange, objWordDoc, rst![ID])
        If rst![Item] = "2T" Then Call S03(myrange, objWordDoc, rst![ID])
        If rst![Item] = "2C" Then Call S04(myrange, objWordDoc, rst![ID])
        If rst![Item] = "3T" Then Call S05(myrange, objWordDoc, rst![ID])
        If rst![Item] = "3C" Then Call S06(myrange, objWordDoc, rst![ID])
        If rst![Item] = "4T" Then Call S07(myrange, objWordDoc, rst![ID])
        If rst![Item] = "4C" Then Call S08(myrange, objWordDoc, rst![ID])
        If rst![Item] = "5T" Then Call S09(myrange, objWordDoc, rst![ID])
        If rst![Item] = "5C" Then Call S10(myrange, objWordDoc, rst![ID])
        If rst![Item] = "6T" Then Call S11(myrange, objWordDoc, rst![ID])
        If rst![Item] = "6S" Then Call S12(myrange, objWordDoc, rst![ID])
        If rst![Item] = "6V" Then Call InterventionValue(myrange, objWordDoc, rst![ID])
        If rst![Item] = "6I" Then Call S13(myrange, objWordDoc, rst![ID])
        If rst![Item] = "6N" Then Call S14(myrange, objWordDoc, rst![ID])
        If rst![Item] = "7T" Then Call S15(myrange, objWordDoc, rst![ID])
        If rst![Item] = "7C" Then Call S16(myrange, objWordDoc, rst![ID])
        If rst![Item] = "8T" Then Call S17(myrange, objWordDoc, rst![ID])
        If rst![Item] = "8C" Then Call S18(myrange, objWordDoc, rst![ID])
        If rst![Item] = "9T" Then Call S19(myrange, objWordDoc, rst![ID])
        If rst![Item] = "9C" Then Call S20(myrange, objWordDoc, rst![ID])
        If rst![Item] = "9E" Then Call S21(myrange, objWordDoc, rst![ID])
        If rst![Item] = "9S" Then Call S22(myrange, objWordDoc, rst![ID])
        If rst![Item] = "B" Then Call S23(myrange, objWordDoc, rst![ID])
        
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), strProgress)
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    
    objWordDoc.SaveAs filename:=DF
    objWordDoc.Close (WD_DONOTSAVECHANGES)
    Set objWordDoc = Nothing
    objWord.Quit
    Set objWord = Nothing
    Set dbs = Nothing
    
    CloseProgress
    DoCmd.Close acForm, "FormSSV6"
    
End Function

Function BOOKMARK_EXPORT(Template As String, intCust As Integer)

    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim CPath As String
    Dim TF As String
    Dim DF As String
    Dim objWord As Object
    Dim objWordDoc As Object
    Dim objWordDocTemplate As Object 'DFSD-439
    Dim srcVBA As Variant 'DFSD-439
    Dim tgtVBA As Variant 'DFSD-439
    Dim srcModule As Variant 'DFSD-439
    Dim sDeskTop As String 'DFSD-439
    Dim myrange As Object
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim FN As Long
    Dim LN As Integer
    Dim ONM As String
    Dim NMN As String
    Dim i As Integer
    Dim CHK As String
    Dim CID As Long
    Dim strProgress As String
    
    strErrMessage = ""
    
    FN = Forms![MENU-NEW]![FirstNumber]
    CID = Forms![FormSSV7]![Combo43]
    strProgress = Create_Msg([Forms]![MENU-NEW]![Text118], 4571)
        
    Set dbs = CurrentDb
    Set objWord = CreateObject("Word.Application")
    CPath = Left(CurrentDb.Name, Len(CurrentDb.Name) - Len(Dir(CurrentDb.Name)))
    
    ONM = Forms![FormSSV7]![Text46]
    LN = Len(ONM)
    For i = 1 To LN
        CHK = MID(ONM, i, 1)
        If CHK = "/" Then
            NMN = NMN & "-"
        Else
            NMN = NMN & CHK
        End If
    Next i
    Forms![FormSSV7]![Text46] = NMN
    
    
    TF = Template
    DF = Forms![FormSSV7]![Text1] & Forms![FormSSV7]![Text46]
    
    Set objWordDoc = objWord.Documents.Add
    Set objWordDocTemplate = objWord.Documents.Open(TF) 'DFSD-439
    
    If Template = "" And Right(ONM, "4") = ".xls" Then
        Dim bookmarkCount As Long
        Dim maxBookmarks As Long
        maxBookmarks = DCount("[Include]", "[Word Export Bookmarks Temp]", "[BookmarkName]<>'????' AND [Include]=-1")
        objWord.ActiveDocument.Tables.Add Range:=objWord.Selection.Range, NumRows:=2, NumColumns:=maxBookmarks, DefaultTableBehavior:=1, AutoFitBehavior:=0
        With objWord.Selection.Tables(1)
            .Style = "Table List 4"
            .ApplyStyleHeadingRows = True
            .ApplyStyleLastRow = True
            .ApplyStyleFirstColumn = True
            .ApplyStyleLastColumn = True
        End With
        strSql = "SELECT * FROM [Word Export Bookmarks Temp] WHERE [BookmarkName]<>'????' AND [Include]=-1 ORDER BY [ID]"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
'        objWord.Selection.TypeText Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
'        objWord.Selection.MoveRight Unit:=12
'        objWord.Selection.TypeText Create_Msg([Forms]![MENU-NEW]![Text118], 22) 'Value
        bookmarkCount = 1
        While Not rst.EOF
            objWord.Selection.TypeText rst![Content].Value
            If bookmarkCount < maxBookmarks Then
                objWord.Selection.MoveRight Unit:=1
                objWord.Selection.MoveDown Unit:=5
                objWord.Selection.MoveLeft Unit:=1
            Else
                objWord.Selection.MoveDown Unit:=5
            End If
            objWordDoc.Bookmarks.Add Range:=objWord.Selection.Range, Name:=rst![Bookmarkname].Value
            objWord.Selection.MoveRight Unit:=12
            objWord.Selection.MoveUp Unit:=5
            bookmarkCount = bookmarkCount + 1
            rst.MoveNext
        Wend
        objWord.Selection.WholeStory
        objWord.Selection.Font.Name = "Arial"
        objWord.Selection.Font.Size = 10
        
        rst.Close
        Set rst = Nothing
    Else
        objWordDoc.Content.InsertFile TF
        'Start DFSD-439
        Set srcVBA = objWordDocTemplate.VBProject
        Set tgtVBA = objWordDoc.VBProject
        sDeskTop = CreateObject("WScript.Shell").specialfolders("Desktop")
        
        For Each srcModule In srcVBA.VBComponents
        If srcModule.Type = 1 Then 'vbext_ct_StdModule
            srcModule.Export sDeskTop & "\" & srcModule.Name
            tgtVBA.VBComponents.Import sDeskTop & "\" & srcModule.Name
            Kill sDeskTop & "\" & srcModule.Name
        End If
        Next srcModule
        objWordDocTemplate.Close
        'End DFSD-439
        
    End If
    
    strSql = "SELECT * FROM [Word Export Bookmarks Temp] WHERE [Include]=-1 Order By [ID]"
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    Dim isFrance As Boolean
    isFrance = False
    If FN = 24000000 Or FN = 46000000 Or FN = 48000000 Or FN = 50000000 Or FN = 52000000 Or FN = 54000000 Or FN = 56000000 Then
        isFrance = True
    End If
            
    Call Call_prog("")
    While Not rst.EOF
        'Changed these from IF, IF, IF... to IF, ELSEIF, ELSEIF...
        
        If intCust = 1 Then
            'AA->HH ||| CustomerName->BuildingName
            If rst![Item] = "AA" Then
                Call B08(myrange, objWordDoc, rst![Bookmarkname])
            'BB->II ||| CustomerStreet->BuildingStreet
            ElseIf rst![Item] = "BB" Then
                Call B09(myrange, objWordDoc, rst![Bookmarkname])
            'CC->JJ ||| CustomerCity->BuildingCity
            ElseIf rst![Item] = "CC" Then
                Call B10(myrange, objWordDoc, rst![Bookmarkname])
            'DD->KK ||| CustomerPostcode->BuildingPostcode
            ElseIf rst![Item] = "DD" Then
                Call B11(myrange, objWordDoc, rst![Bookmarkname])
            'FF->LL ||| CustomerNumber->BuildingNumber
            ElseIf rst![Item] = "FF" Then
                Call B12(myrange, objWordDoc, rst![Bookmarkname])
            'ZE->ZG ||| CustomerPhone->BuildingPhone
            ElseIf rst![Item] = "ZE" Then
                Call B49(myrange, objWordDoc, rst![Bookmarkname])
            'ZF->ZI |||CustomerFax->BuildingFax
            ElseIf rst![Item] = "ZF" Then
                Call B50(myrange, objWordDoc, rst![Bookmarkname])
            'BP->BO |||CustomerState->BuildingState
            ElseIf rst![Item] = "BP" Then
                Call B90(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "HH" Then
                Call B08(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "II" Then
                Call B09(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "JJ" Then
                Call B10(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "KK" Then
                Call B11(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "LL" Then
                Call B12(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "ZG" Then
                Call B49(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "ZI" Then
                Call B50(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "BO" Then
                Call B90(myrange, objWordDoc, rst![Bookmarkname])
            End If
        Else
            If rst![Item] = "AA" Then
                Call B01(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "BB" Then
                Call B02(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "CC" Then
                Call B03(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "DD" Then
                Call B04(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "FF" Then
                Call B06(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "ZE" Then
                Call B47(myrange, objWordDoc, rst![Bookmarkname], CID)
            ElseIf rst![Item] = "ZF" Then
                Call B48(myrange, objWordDoc, rst![Bookmarkname], CID)
            ElseIf rst![Item] = "BP" Then
                Call B89(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "HH" Then
                Call B08(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "II" Then
                Call B09(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "JJ" Then
                Call B10(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "KK" Then
                Call B11(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "LL" Then
                Call B12(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "ZG" Then
                Call B49(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "ZI" Then
                Call B50(myrange, objWordDoc, rst![Bookmarkname])
            ElseIf rst![Item] = "BO" Then
                Call B90(myrange, objWordDoc, rst![Bookmarkname])
            End If
        End If
        
        If rst![Item] = "TT" Then
            If isFrance = True Then
                Call B20_FRANCE(myrange, objWordDoc, rst![Bookmarkname])
            Else
                Call B20(myrange, objWordDoc, rst![Bookmarkname])
            End If
        ElseIf rst![Item] = "UU" Then
            If isFrance = True Then
                Call B21_FRANCE(myrange, objWordDoc, rst![Bookmarkname])
            Else
                Call B21(myrange, objWordDoc, rst![Bookmarkname])
            End If
        ElseIf rst![Item] = "UA" Then
            If isFrance = True Then
                Call B22_FRANCE(myrange, objWordDoc, rst![Bookmarkname])
            Else
                Call B22(myrange, objWordDoc, rst![Bookmarkname])
            End If
        ElseIf rst![Item] = "VV" Then
            If isFrance = True Then
                Call B23_FRANCE(myrange, objWordDoc, rst![Bookmarkname])
            Else
                Call B23(myrange, objWordDoc, rst![Bookmarkname])
            End If
        ElseIf rst![Item] = "XX" Then
            If isFrance = True Then
                Call B25_FRANCE(myrange, objWordDoc, rst![Bookmarkname])
            Else
                Call B25(myrange, objWordDoc, rst![Bookmarkname])
            End If
        ElseIf rst![Item] = "EE" Then
            Call B05(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "GG" Then
            Call B07(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "MM" Then
            Call B13(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "NN" Then
            Call B14(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "OO" Then
            Call B15(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "PP" Then
            Call B16(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "QQ" Then
            Call B17(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "RR" Then
            Call B18(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "SS" Then
            Call B19(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "WW" Then
            Call B24(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "YY" Then
            Call B26(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZZ" Then
            Call B27(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZA" Then
            Call B28(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZB" Then
            Call B29(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZC" Then
            Call B45(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZD" Then
            Call B46(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZH" Then
            Call B51(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZJ" Then
            Call B52(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZK" Then
            Call B52(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZL" Then
            Call B53(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZM" Then
            Call B54(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZN" Then
            Call B55(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZO" Then
            Call B46_GERM(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZP" Then
            Call B21_GERM(myrange, objWordDoc, rst![Bookmarkname])
        'Start:DFSD-167
        ElseIf rst![Item] = "ZPR12" Then
            Call B21_R12GERM(myrange, objWordDoc, rst![Bookmarkname])
        'End:DFSD-167
        ElseIf rst![Item] = "ZQ" Then
            Call B56(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZR" Then
            Call B05(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZS" Then
            Call B17(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZT" Then
            Call B57(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZU" Then
            Call B58(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZV" Then
            Call B58(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "AB" Then
            Call B70(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZW" Then
            Call B72(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "ZX" Then
            Call B74(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BA" Then
            Call B75(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BZ" Then
            Call B77(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BY" Then
            Call B80(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BX" Then
            Call B81(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BW" Then
            Call B82(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BV" Then
            Call B83(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BU" Then
            Call B84(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BT" Then
            Call B85(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BS" Then
            Call B86(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "BR" Then
            Call B87(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BQ" Then
            Call B88(myrange, objWordDoc, rst![Bookmarkname])
        ElseIf rst![Item] = "BP" Then
            Call B92(myrange, objWordDoc, rst![Bookmarkname])
        End If
       
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), strProgress)
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    
    If Right(ONM, "4") = ".xls" Then
        Call CopyFromWordToExcel(objWordDoc, DF)
    Else
        objWordDoc.SaveAs filename:=DF
    End If
    objWordDoc.Close (WD_DONOTSAVECHANGES)
    Set objWordDoc = Nothing
    objWord.Quit
    Set objWord = Nothing
    Set dbs = Nothing
    
    CloseProgress
    DoCmd.Close acForm, "FormSSV7"
    
    If strErrMessage <> "" Then
        MsgBox strErrMessage
    End If
End Function

Function CopyFromWordToExcel(objWordDoc As Object, pathToExcelDoc As String)
    Dim objExcel As Object
    Dim objExcelSheet As Object
    Dim sheetName As String
    sheetName = "DFSD Export"
    Set objExcel = CreateObject("Excel.Application")
    objExcel.SheetsInNewWorkbook = 1
    objExcel.DisplayAlerts = False
    objWordDoc.ActiveWindow.Selection.WholeStory
    objWordDoc.ActiveWindow.Selection.Copy
    If Len(Dir(pathToExcelDoc)) > 0 Then
        'File already exists, check if a sheet with the same name is already preset (update with new info)
        objExcel.Workbooks.Open pathToExcelDoc
        On Error Resume Next
        Set objExcelSheet = objExcel.ActiveWorkbook.Worksheets(sheetName)
        'Sheet does not exist, we need to create one
        If objExcelSheet Is Nothing Then
            Set objExcelSheet = objExcel.ActiveWorkbook.Worksheets.Add(After:=objExcel.ActiveWorkbook.Worksheets(objExcel.ActiveWorkbook.Worksheets.Count))
            objExcelSheet.Name = sheetName
        End If
        On Error GoTo 0
    Else
        objExcel.Workbooks.Add
        Set objExcelSheet = objExcel.ActiveWorkbook.Worksheets.Add(After:=objExcel.ActiveWorkbook.Worksheets(objExcel.ActiveWorkbook.Worksheets.Count))
        objExcel.ActiveWorkbook.Worksheets(1).Delete
        objExcelSheet.Name = sheetName
    End If
    objExcelSheet.Select
    objExcelSheet.Range("A1").Select
    objExcelSheet.Paste
    'Autosize all the columns on the new worksheet
    objExcelSheet.cells.WrapText = False
    objExcelSheet.Columns.Autofit
    objExcelSheet.Columns.VerticalAlignment = -4160
    objExcelSheet.Range("A1").Select
    objExcelSheet.SaveAs filename:=pathToExcelDoc
    objExcel.Workbooks.Close
    objExcel.Quit
    Set objExcelSheet = Nothing
    Set objExcel = Nothing
End Function

Function BOOKMARK_EXPORT_PARTS(Template As String, Optional PQID As Long)

    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim CPath As String
    Dim TF As String
    Dim DF As String
    Dim objWord As Object
    Dim objWordDoc As Object
    Dim myrange As Object
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim FN As Long
    Dim LN As Integer
    Dim ONM As String
    Dim NMN As String
    Dim i As Integer
    Dim CHK As String
    Dim CID As Long
    
    strErrMessage = ""
    
    'If PQID is 0, it means an ID wasn't passed, so if FormSales is open, get that ID
    'If it's not open, just keep the ID of 0
    If PQID = 0 And IsLoaded("FormSales") Then
        If Not IsNull(Forms![FormSales]![PQUOTEID]) Then
            PQID = Forms![FormSales]![PQUOTEID]
        End If
    End If
    'Get the Customer ID for the part quote
    If Not IsNull(DLookup("[CustomerID]", "Parts Quotation", "[PartQuotationID]= " & PQID)) Then
        CID = DLookup("[CustomerID]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    Else
        CID = 0
    End If
    
    FN = Forms![MENU-NEW]![FirstNumber]
        
    Set dbs = CurrentDb
    Set objWord = CreateObject("Word.Application")
    CPath = Left(CurrentDb.Name, Len(CurrentDb.Name) - Len(Dir(CurrentDb.Name)))

    ONM = Forms![FormPQP]![Text46]
    LN = Len(ONM)
    For i = 1 To LN
        CHK = MID(ONM, i, 1)
        If CHK = "/" Then
            NMN = NMN & "-"
        Else
            NMN = NMN & CHK
        End If
    Next i
    Forms![FormPQP]![Text46] = NMN
        
    TF = Template
    DF = Forms![FormPQP]![Text1] & Forms![FormPQP]![Text46]
    
    Set objWordDoc = objWord.Documents.Add
    objWordDoc.Content.InsertFile TF
    
    strSql = "SELECT * FROM [Word Export Bookmarks Temp] WHERE [Include]=-1 Order By [ID]"
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    Call Call_prog("")
    While Not rst.EOF
        'Changed these from IF, IF, IF... to IF, ELSEIF, ELSEIF...
        
        If rst![Item] = "AA" Then
            Call B32(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "AZ" Then
            Call B32(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "BB" Then
            Call B33(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "CC" Then
            Call B34(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "DD" Then
            Call B35(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "FF" Then
            Call B37(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "EE" Then
            Call B36(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WK" Then
            Call B36(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "ZE" Then
            Call B47(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "ZF" Then
            Call B48(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "GG" Then
            Call B38(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "ZB" Then
            Call B39(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WA" Then
            Call B39(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "OO" Then
            Call B40(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WB" Then
            Call B40(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "QQ" Then
            Call B41(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "RR" Then
            Call B38(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "SS" Then
            Call B42(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "UU" Then
            Call B43(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WN" Then
            Call B67(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "UA" Then
            Call B44(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "ZA" Then
            Call B31(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WC" Then
            Call B59(myrange, objWordDoc, rst![Bookmarkname], PQID, CID)
        ElseIf rst![Item] = "WD" Then
            Call B60(myrange, objWordDoc, rst![Bookmarkname], PQID, CID)
        ElseIf rst![Item] = "WE" Then
            Call B61(myrange, objWordDoc, rst![Bookmarkname], PQID, CID)
        ElseIf rst![Item] = "WF" Then
            Call B62(myrange, objWordDoc, rst![Bookmarkname], PQID, CID)
        ElseIf rst![Item] = "WG" Then
            Call B62(myrange, objWordDoc, rst![Bookmarkname], PQID, CID)
        ElseIf rst![Item] = "WH" Then
            Call B63_GERM(myrange, objWordDoc, rst![Bookmarkname], PQID)
        'Start:DFSD-167
        ElseIf rst![Item] = "WHR12" Then
            Call B63_R12GERM(myrange, objWordDoc, rst![Bookmarkname], PQID)
       'End:DFSD-167
        ElseIf rst![Item] = "WI" Then
            Call B64(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WL" Then
            Call B65(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WM" Then
            Call B66(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WO" Then
            Call B68(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "WJ" Then
            Call B69(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "AB" Then
            Call B71(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "ZG" Then
            Call B73(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "BA" Then
            Call B76(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "BD" Then
            Call B79(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "BC" Then
            Call B78(myrange, objWordDoc, rst![Bookmarkname], PQID)
        ElseIf rst![Item] = "BS" Then
            Call B86(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "BP" Then
            Call B91(myrange, objWordDoc, rst![Bookmarkname], CID)
        ElseIf rst![Item] = "CA" Then
            Call B43_mini(myrange, objWordDoc, rst![Bookmarkname], PQID)
        End If
                
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Creating Word Document")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing

    objWordDoc.SaveAs filename:=DF
    objWordDoc.Close (WD_DONOTSAVECHANGES)
    Set objWordDoc = Nothing
    objWord.Quit
    Set objWord = Nothing
    Set dbs = Nothing
    
    CloseProgress
    DoCmd.Close acForm, "FormPQP"
    
    If strErrMessage <> "" Then
        MsgBox strErrMessage
    End If
    
End Function

Function S01(myrange As Object, objWordDoc As Object)   'insert blank line
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, 0, 0, 0, 12, "Times New Roman", 0)
    myrange.Insertbefore (vbCrLf)
    II = II + 1
End Function

Function S02(myrange As Object, objWordDoc As Object, ID As Long) 'insert SSID, SSName, Date
    Dim Phrase As String
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = "ID:" & "   " & Find_SSID(Forms![FormSales]![SSID])
    myrange.Insertbefore (Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1
        
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1123) & "   " & DLookup("[Service Selection Name]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    myrange.Insertbefore (Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1
            
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1124) & "   " & Format(DLookup("[UpdatedDate]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID]), "d mmmm, yyyy")
    myrange.Insertbefore (Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1
End Function

Function S03(myrange As Object, objWordDoc As Object, ID As Long) 'insert Customer contact Title
    Dim Phrase As String
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1125)
    myrange.Insertbefore (Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1
End Function

Function S04(myrange As Object, objWordDoc As Object, ID As Long) 'insert Customer contact Details
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim CID As Long
    Dim BID As Long
    Dim CC As Boolean
    
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    CC = False

    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    CID = DLookup("[CustomerID]", "BD-Buildings", "[BuildingID]= " & BID)
    
    If CID > 0 Then
        strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] =" & CID
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            While Not rst.EOF
                
                Set myrange = objWordDoc.Paragraphs(II).Range
                
                If IsNull(rst![Name]) Then
                    Phrase = "-"
                Else
                    Phrase = rst![Name]
                End If
                myrange.Insertbefore (vbTab & Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
                
                Set myrange = objWordDoc.Paragraphs(II).Range
                If IsNull(rst![Phone]) Then
                    Phrase = "Ph : not recorded"
                Else
                    Phrase = "Ph : " & rst![Phone]
                End If
                myrange.Insertbefore (vbTab & Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
                    
                Set myrange = objWordDoc.Paragraphs(II).Range
                If IsNull(rst![Fax]) Then
                    Phrase = "Fax : not recorded"
                Else
                    Phrase = "Fax : " & rst![Fax]
                End If
                myrange.Insertbefore (vbTab & Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
                    
                rst.MoveNext
            Wend
            CC = True
        End If
        rst.Close
    End If
    
    If CID = 0 Or CC = False Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1134)
        myrange.Insertbefore (vbTab & Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
    
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S05(myrange As Object, objWordDoc As Object, ID As Long) 'insert Customer detail Title
    Dim Phrase As String
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1126)
    myrange.Insertbefore (Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1
End Function

Function S06(myrange As Object, objWordDoc As Object, ID As Long) 'insert Customer Details content
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim CID As Long
    Dim BID As Long
    Dim CC As Boolean
    
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    CC = False
    
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    CID = DLookup("[CustomerID]", "BD-Buildings", "[BuildingID]= " & BID)
    
    If CID > 0 Then
        strSql = "SELECT * FROM [BD-Customers] WHERE [CustomerID] =" & CID
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            
            Set myrange = objWordDoc.Paragraphs(II).Range
            Phrase = rst![CustomerName]
            myrange.Insertbefore (vbTab & Phrase & vbCrLf)
            Set myrange = objWordDoc.Paragraphs(II).Range
            Call FFF(myrange, B, i, U, S, f, A)
            II = II + 1
                    
            If Not IsNull(rst![Address]) Then
                Set myrange = objWordDoc.Paragraphs(II).Range
                Phrase = rst![Address]
                myrange.Insertbefore (vbTab & Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
            End If
                
            If Not IsNull(rst![City]) Then
                Set myrange = objWordDoc.Paragraphs(II).Range
                Phrase = rst![City]
                myrange.Insertbefore (vbTab & Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
            End If
                
            If Not IsNull(rst![PostalCode]) Then
                Set myrange = objWordDoc.Paragraphs(II).Range
                Phrase = rst![PostalCode]
                myrange.Insertbefore (vbTab & Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
            End If
                            
            If Not IsNull(rst![PhoneNumber]) Then
                Set myrange = objWordDoc.Paragraphs(II).Range
                Phrase = "Ph : " & rst![PhoneNumber]
                myrange.Insertbefore (vbTab & Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
            End If
                    
            If Not IsNull(rst![FaxNumber]) Then
                Set myrange = objWordDoc.Paragraphs(II).Range
                Phrase = "Fax : " & rst![FaxNumber]
                myrange.Insertbefore (vbTab & Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
            End If
        
            CC = True
        End If
        rst.Close
    End If
    
    If CID = 0 Or CC = False Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1134)
        myrange.Insertbefore (vbTab & Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
    
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S07(myrange As Object, objWordDoc As Object, ID As Long) 'insert Building detail Title
    Dim Phrase As String
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1127)
    myrange.Insertbefore (Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1
End Function

Function S08(myrange As Object, objWordDoc As Object, ID As Long) 'insert Building Details content
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim BID As Long
    Dim CC As Boolean
    
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    CC = False
    
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    strSql = "SELECT * FROM [BD-Buildings] WHERE [BuildingID] =" & BID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = rst![BuildingName]
    myrange.Insertbefore (vbTab & Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1
    
    If Not IsNull(rst![Address]) Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = rst![Address]
        myrange.Insertbefore (vbTab & Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
    
    If Not IsNull(rst![City]) Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = rst![City]
        myrange.Insertbefore (vbTab & Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
    
    If Not IsNull(rst![PostalCode]) Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = rst![PostalCode]
        myrange.Insertbefore (vbTab & Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
            
    If Not IsNull(rst![PhoneNumber]) Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = "Ph : " & rst![PhoneNumber]
        myrange.Insertbefore (vbTab & Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
                    
    If Not IsNull(rst![FaxNumber]) Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = "Fax : " & rst![FaxNumber]
        myrange.Insertbefore (vbTab & Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S09(myrange As Object, objWordDoc As Object, ID As Long) 'Equipment details title
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    strSql = "SELECT * FROM [Service Selection 4 Word Export] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
    
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1128)
        myrange.Insertbefore (Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S10(myrange As Object, objWordDoc As Object, ID As Long) 'Equipment details
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    strSql = "SELECT * FROM [Service Selection 4 Word Export] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=4)
        IRow = 2
                        
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 149)
        tbl.Cell(1, 1).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 151)
        tbl.Cell(1, 2).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 152)
        tbl.Cell(1, 3).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 606)
        tbl.Cell(1, 4).Range.Insertafter Phrase
        JJ = 4
        While Not rst.EOF
            strSql = "SELECT * FROM [BD-Equipments] WHERE [EquipmentID] =" & rst![EquipmentID]
            Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                If IsNull(rst1![Serial Number]) Then
                    tbl.Cell(IRow, 1).Range.Insertafter " "
                Else
                    tbl.Cell(IRow, 1).Range.Insertafter rst1![Serial Number]
                End If
                If IsNull(rst1![Make]) Then
                    tbl.Cell(IRow, 2).Range.Insertafter " "
                Else
                    tbl.Cell(IRow, 2).Range.Insertafter rst1![Make]
                End If
                If IsNull(rst1![Model]) Then
                    tbl.Cell(IRow, 3).Range.Insertafter " "
                Else
                    tbl.Cell(IRow, 3).Range.Insertafter rst1![Model]
                End If
                If IsNull(rst1![Equipment Tag]) Then
                    tbl.Cell(IRow, 4).Range.Insertafter " "
                Else
                    tbl.Cell(IRow, 4).Range.Insertafter rst1![Equipment Tag]
                End If
            End If
            JJ = JJ + 4
            IRow = IRow + 1
            rst1.Close
            rst.MoveNext
        Wend
        tbl.Columns.Autofit
        II = II + JJ + (IRow - 2) + 1
        Set tbl = Nothing
    End If
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
End Function

Function S11(myrange As Object, objWordDoc As Object, ID As Long) 'Service Interventions title
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then

        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1129)
        myrange.Insertbefore (Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S12(myrange As Object, objWordDoc As Object, ID As Long) 'intervention summary
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        
        RC = DCount("[Qty1]", "Z-SS Summary Cust1", "[Service Selection] =" & Forms![FormSales]![SSID])
        
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=5)
        IRow = 2
 
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 100) & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 1).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 28)
        tbl.Cell(1, 2).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 606)
        tbl.Cell(1, 3).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 62) & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 4).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 101)
        tbl.Cell(1, 5).Range.Insertafter Phrase
        JJ = 5
        strSql = "SELECT * FROM [Z-SS Summary Cust1] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
        Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst1.EOF
            If IsNull(rst1![Qty1]) Then
                tbl.Cell(IRow, 1).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 1).Range.Insertafter rst1![Qty1]
            End If
            If IsNull(rst1![Equipment Type]) Then
                tbl.Cell(IRow, 2).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 2).Range.Insertafter rst1![Equipment Type]
            End If
            If IsNull(rst1![Tag]) Then
                tbl.Cell(IRow, 3).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 3).Range.Insertafter rst1![Tag]
            End If
            If IsNull(rst1![Qty2]) Then
                tbl.Cell(IRow, 4).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 4).Range.Insertafter rst1![Qty2]
            End If
            If IsNull(rst1![TK]) Then
                tbl.Cell(IRow, 5).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 5).Range.Insertafter rst1![TK]
            End If
            JJ = JJ + 5
            IRow = IRow + 1
            rst1.MoveNext
        Wend
        rst1.Close
        On Error Resume Next
        tbl.Columns(1).Width = 65
        tbl.Columns(2).Width = 65
        tbl.Columns(3).Width = 65
        tbl.Columns(4).Width = 70
        tbl.Columns(5).Width = 200
        
        Set tbl = Nothing
        II = II + JJ + (IRow - 2) + 1
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function InterventionValue(myrange As Object, objWordDoc As Object, ID As Long) 'intervention value
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    Dim strFormat As String
    Dim columnCount As Integer
    
    columnCount = 6
    strFormat = GetDecimalFormat

    Set dbs = CurrentDb

    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)

    strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then

        RC = DCount("[Qty1]", "Z-SS Summary Cust2", "[Service Selection] =" & Forms![FormSales]![SSID])

        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=columnCount)
        IRow = 2

        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 100) & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 1).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 28)
        tbl.Cell(1, 2).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 606)
        tbl.Cell(1, 3).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 62) & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 4).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 101)
        tbl.Cell(1, 5).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 22)
        tbl.Cell(1, 6).Range.Insertafter Phrase
        JJ = columnCount
        strSql = "SELECT * FROM [Z-SS Summary Cust2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
        Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst1.EOF
            If IsNull(rst1![Qty1]) Then
                tbl.Cell(IRow, 1).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 1).Range.Insertafter rst1![Qty1]
            End If
            If IsNull(rst1![Equipment Type]) Then
                tbl.Cell(IRow, 2).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 2).Range.Insertafter rst1![Equipment Type]
            End If
            If IsNull(rst1![Tag]) Then
                tbl.Cell(IRow, 3).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 3).Range.Insertafter rst1![Tag]
            End If
            If IsNull(rst1![Qty2]) Then
                tbl.Cell(IRow, 4).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 4).Range.Insertafter rst1![Qty2]
            End If
            If IsNull(rst1![TK]) Then
                tbl.Cell(IRow, 5).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 5).Range.Insertafter rst1![TK]
            End If
            If IsNull(rst1![Price]) Then
                tbl.Cell(IRow, 6).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 6).Range.Insertafter Format(rst1![Price], strFormat)
            End If

            JJ = JJ + columnCount
            IRow = IRow + 1
            rst1.MoveNext
        Wend
        rst1.Close
        On Error Resume Next
        tbl.Columns(1).Width = 65
        tbl.Columns(2).Width = 65
        tbl.Columns(3).Width = 65
        tbl.Columns(4).Width = 70
        tbl.Columns(5).Width = 140
        tbl.Columns(6).Width = 60

        Set tbl = Nothing
        II = II + JJ + (IRow - 2) + 1
    End If

    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
End Function

Function S13(myrange As Object, objWordDoc As Object, ID As Long) 'intervention content
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    
    Set dbs = CurrentDb
    
    strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        While Not rst.EOF
            
            Call FF(ID)
            If RR = True Then
                
                Set myrange = objWordDoc.Paragraphs(II).Range
                Phrase = rst![Service Intervention]
                myrange.Insertbefore (Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
            
            End If
            
            Call FF(ID)
            If RR = True Then
                
                Set myrange = objWordDoc.Paragraphs(II).Range
                Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1504) & ": " & DLookup("[Tag]", "Tag", "[TagID]='" & rst![Tag] & "'")
                myrange.Insertbefore (Phrase & vbCrLf)
                Set myrange = objWordDoc.Paragraphs(II).Range
                Call FFF(myrange, B, i, U, S, f, A)
                II = II + 1
            
            End If

            Call Delete_Records("S_S_VSF_Temp")
            Call Delete_Records("VSF Customer")
            If rst![DFS] = 0 Then
                Call Write_Process_SSTemp(rst![ID3], 1, " ", 1, 2, 1, 90, 5, 0)
                Call Write_Extra_SSTemp(rst![ID3], 1, " ", 1)
            Else
                Call Write_Steps_SSTemp(rst![ID3])
            End If
            DoCmd.SetWarnings False
            DoCmd.OpenQuery "S_S_VSF_Temp Query1"
            DoCmd.SetWarnings True
            
            Set rst1 = dbs.OpenRecordset("VSF Customer", dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                Call FF(ID)
                If RR = True Then
                    rst1.MoveLast
                    rst1.MoveFirst
                    RC = rst1.RecordCount
                    Set myrange = objWordDoc.Paragraphs(II).Range
                    Call FFF(myrange, B, i, U, S, f, A)
                    Set myrange = objWordDoc.Paragraphs(II).Range
                    Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=1)
                    IRow = 2
                    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1130)
                    tbl.Cell(1, 1).Range.Insertafter Phrase
                    JJ = 1
                    While Not rst1.EOF
                        If IsNull(rst1![Text]) Then
                            tbl.Cell(IRow, 1).Range.Insertafter " "
                        Else
                            tbl.Cell(IRow, 1).Range.Insertafter rst1![Text]
                        End If
                        JJ = JJ + 1
                        IRow = IRow + 1
                        rst1.MoveNext
                    Wend
                    II = II + JJ + (IRow - 2) + 1
                    tbl.Columns(1).Width = 400
                    Set tbl = Nothing
                End If
            End If
            rst1.Close
            rst.MoveNext
            Call S01(myrange, objWordDoc)
        Wend
    End If
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
End Function

Function S14(myrange As Object, objWordDoc As Object, ID As Long) 'timetable
    Dim Phrase As String
    Dim dbs As Database
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    
    Set dbs = CurrentDb

    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)

    RC = DCount("[Qty]", "SS2A", "[Service Selection] =" & Forms![FormSales]![SSID] & " And [Category]=2")
    If RC > 0 Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 42)
        myrange.Insertbefore (Phrase & vbCrLf)
                
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=15)
        IRow = 2
                
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 101)
        tbl.Cell(1, 1).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 606)
        tbl.Cell(1, 2).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 3).Range.Insertafter Phrase
        tbl.Cell(1, 4).Range.Insertafter "J"
        tbl.Cell(1, 5).Range.Insertafter "F"
        tbl.Cell(1, 6).Range.Insertafter "M"
        tbl.Cell(1, 7).Range.Insertafter "A"
        tbl.Cell(1, 8).Range.Insertafter "M"
        tbl.Cell(1, 9).Range.Insertafter "J"
        tbl.Cell(1, 10).Range.Insertafter "J"
        tbl.Cell(1, 11).Range.Insertafter "A"
        tbl.Cell(1, 12).Range.Insertafter "S"
        tbl.Cell(1, 13).Range.Insertafter "O"
        tbl.Cell(1, 14).Range.Insertafter "N"
        tbl.Cell(1, 15).Range.Insertafter "D"
        JJ = 15
        
        strSql = "SELECT * FROM [SS2A] WHERE [Service Selection] =" & Forms![FormSales]![SSID] & " And [Category]= 2"
        Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst1.EOF
            If IsNull(rst1![TK]) Then
                tbl.Cell(IRow, 1).Range.Insertafter rst1![Service Intervention]
            Else
                tbl.Cell(IRow, 1).Range.Insertafter rst1![TK]
            End If
            If IsNull(rst1![Tag]) Then
                tbl.Cell(IRow, 2).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 2).Range.Insertafter rst1![Tag]
            End If
            If IsNull(rst1![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 3).Range.Insertafter rst1![Qty]
            End If
            If rst1![Jan] = False Then
                tbl.Cell(IRow, 4).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 4).Range.Insertafter "X"
            End If
            If rst1![Feb] = False Then
                tbl.Cell(IRow, 5).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 5).Range.Insertafter "X"
            End If
            If rst1![Mar] = False Then
                tbl.Cell(IRow, 6).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 6).Range.Insertafter "X"
            End If
            If rst1![Apr] = False Then
                tbl.Cell(IRow, 7).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 7).Range.Insertafter "X"
            End If
            If rst1![May] = False Then
                tbl.Cell(IRow, 8).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 8).Range.Insertafter "X"
            End If
            If rst1![Jun] = False Then
                tbl.Cell(IRow, 9).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 9).Range.Insertafter "X"
            End If
            If rst1![Jul] = False Then
                tbl.Cell(IRow, 10).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 10).Range.Insertafter "X"
            End If
            If rst1![Aug] = False Then
                tbl.Cell(IRow, 11).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 11).Range.Insertafter "X"
            End If
            If rst1![Sep] = False Then
                tbl.Cell(IRow, 12).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 12).Range.Insertafter "X"
            End If
            If rst1![Oct] = False Then
                tbl.Cell(IRow, 13).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 13).Range.Insertafter "X"
            End If
            If rst1![Nov] = False Then
                tbl.Cell(IRow, 14).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 14).Range.Insertafter "X"
            End If
            If rst1![Dec] = False Then
                tbl.Cell(IRow, 15).Range.Insertafter ""
            Else
                tbl.Cell(IRow, 15).Range.Insertafter "X"
            End If
            JJ = JJ + 15
            IRow = IRow + 1
            rst1.MoveNext
        Wend
        rst1.Close
        On Error Resume Next
        tbl.Columns(1).Width = 130
        tbl.Columns(2).Width = 60
        tbl.Columns(3).Width = 30
        tbl.Columns(4).Width = 20
        tbl.Columns(5).Width = 20
        tbl.Columns(6).Width = 20
        tbl.Columns(7).Width = 20
        tbl.Columns(8).Width = 20
        tbl.Columns(9).Width = 20
        tbl.Columns(10).Width = 20
        tbl.Columns(11).Width = 20
        tbl.Columns(12).Width = 20
        tbl.Columns(13).Width = 20
        tbl.Columns(14).Width = 20
        tbl.Columns(15).Width = 20
        II = II + JJ + (IRow - 1)
        
        Set tbl = Nothing
    End If
    Set rst1 = Nothing
    Set dbs = Nothing
End Function

Function S15(myrange As Object, objWordDoc As Object, ID As Long) 'notes title
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    strSql = "SELECT * FROM [Service Selection Notes] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1131)
        myrange.Insertbefore (Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S16(myrange As Object, objWordDoc As Object, ID As Long) 'notes detail
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    strSql = "SELECT * FROM [Service Selection Notes] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        While Not rst.EOF
            Set myrange = objWordDoc.Paragraphs(II).Range
            If IsNull(rst![TT]) Then
                Phrase = "-"
            Else
                 Phrase = rst![TT]
            End If
            myrange.Insertbefore (vbTab & Phrase & vbCrLf)
            Set myrange = objWordDoc.Paragraphs(II).Range
            Call FFF(myrange, B, i, U, S, f, A)
            II = II + 1
            rst.MoveNext
        Wend
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S17(myrange As Object, objWordDoc As Object, ID As Long) 'material title
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    strSql = "SELECT * FROM [Material Word Export] WHERE [Service Selection] = " & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        Set myrange = objWordDoc.Paragraphs(II).Range
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1132)
        myrange.Insertbefore (Phrase & vbCrLf)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        II = II + 1
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S18(myrange As Object, objWordDoc As Object, ID As Long) 'material content
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
            
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
    
    strSql = "SELECT * FROM [Material Word Export] WHERE [Service Selection] = " & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        
        Set myrange = objWordDoc.Paragraphs(II).Range
        myrange.Insertbefore (" ")
                
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=3)
        IRow = 2
            
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 158)
        tbl.Cell(1, 1).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1011)
        tbl.Cell(1, 2).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 29)
        tbl.Cell(1, 3).Range.Insertafter Phrase
        JJ = 3
    
        While Not rst.EOF
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 1).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 1).Range.Insertafter rst![PartNumber]
            End If
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 2).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 2).Range.Insertafter rst![Description]
            End If
            If IsNull(rst![QT]) Then
                tbl.Cell(IRow, 3).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 3).Range.Insertafter rst![QT]
            End If
            JJ = JJ + 3
            IRow = IRow + 1
            rst.MoveNext
        Wend
        tbl.Columns.Autofit
        Set tbl = Nothing
        II = II + JJ + (IRow - 1)
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S19(myrange As Object, objWordDoc As Object, ID As Long) 'Price Title
    Dim Phrase As String
        
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
        
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1133)
    myrange.Insertbefore (Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1
End Function

Function S20(myrange As Object, objWordDoc As Object, ID As Long) 'price content
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    Dim Mat As Single
    Dim LAB As Single
    Dim TAO As Single
    Dim MIS As Single
    Dim DIS As Single
    Dim TOT As Single
    Dim decimals As Integer
    Dim strFormat As String
    Dim iCnt As Integer
    
    strFormat = GetDecimalFormat
   
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
        
    Mat = CSng(Forms![FormSales]![Child3].Form![FormSSV4].Form![Psell])
    LAB = CSng(Forms![FormSales]![Child3].Form![FormSSV1].Form![Text97])
    TAO = CSng(Forms![FormSales]![Child3].Form![FormSSV2].Form![Text1006] + Forms![FormSales]![Child3].Form![FormSSV2].Form![Text2006] + Forms![FormSales]![Child3].Form![FormSSV2].Form![Text3006])
    MIS = CSng(Forms![FormSales]![Child3].Form![Text3006])
    DIS = CSng(Forms![FormSales]![Child3].Form![Text244])
    TOT = Mat + LAB + TAO + MIS
    
    RC = 0
    If Mat > 0 Then RC = 1
    If LAB > 0 Then RC = RC + 1
    If TAO > 0 Then RC = RC + 1
    If MIS > 0 Then RC = RC + 1
    If DIS > 0 Then RC = RC + 2
    
    Set myrange = objWordDoc.Paragraphs(II).Range
    myrange.Insertbefore (" ")
    
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    Set myrange = objWordDoc.Paragraphs(II).Range
    
    Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=2)
    IRow = 1
    JJ = 0
    If Mat > 0 Then
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 113)
        tbl.Cell(IRow, 1).Range.Insertafter Phrase
        tbl.Cell(IRow, 2).Range.Insertafter Format(Mat, strFormat)
        IRow = IRow + 1
        JJ = JJ + 3
    End If
    If LAB > 0 Then
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 682)
        tbl.Cell(IRow, 1).Range.Insertafter Phrase
        tbl.Cell(IRow, 2).Range.Insertafter Format(LAB, strFormat)
        IRow = IRow + 1
        JJ = JJ + 3
    End If
    If TAO > 0 Then
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1136)
        tbl.Cell(IRow, 1).Range.Insertafter Phrase
        tbl.Cell(IRow, 2).Range.Insertafter Format(TAO, strFormat)
        IRow = IRow + 1
        JJ = JJ + 3
    End If
    If MIS > 0 Then
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1034)
        tbl.Cell(IRow, 1).Range.Insertafter Phrase
        tbl.Cell(IRow, 2).Range.Insertafter Format(MIS, strFormat)
        IRow = IRow + 1
        JJ = JJ + 3
    End If
    If TOT > 0 Then
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56)
        tbl.Cell(IRow, 1).Range.Insertafter Phrase
        tbl.Cell(IRow, 2).Range.Insertafter Format(TOT, strFormat)
        IRow = IRow + 1
        JJ = JJ + 3
    End If
    If DIS > 0 Then
        Phrase = DIS & Create_Msg([Forms]![MENU-NEW]![Text118], 1598)
        tbl.Cell(IRow, 1).Range.Insertafter Phrase
        tbl.Cell(IRow, 2).Range.Insertafter Format((Mat + LAB + TAO + MIS) * (DIS / 100), "-" + strFormat) 'NegativeFormat "-0.00"
        IRow = IRow + 1
        JJ = JJ + 3
    End If
    If DIS > 0 Then
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1599)
        tbl.Cell(IRow, 1).Range.Insertafter Phrase
        tbl.Cell(IRow, 2).Range.Insertafter Format((Mat + LAB + TAO + MIS) * ((100 - DIS) / 100), strFormat)
        IRow = IRow + 1
        JJ = JJ + 3
    End If
        
    tbl.Columns.Autofit
    Set tbl = Nothing
    II = II + JJ
    Set dbs = Nothing
End Function

Function S21(myrange As Object, objWordDoc As Object, ID As Long) 'extended pricing
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    Dim strFormat As String
    strFormat = GetDecimalFormat
    
    Set dbs = CurrentDb
    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)

    Call S21_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
                
        Set myrange = objWordDoc.Paragraphs(II).Range
        Call FFF(myrange, B, i, U, S, f, A)
        Set myrange = objWordDoc.Paragraphs(II).Range
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=6)
        IRow = 2
 
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
        tbl.Cell(1, 1).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 158) 'Part Number
        tbl.Cell(1, 2).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54) 'Qty
        tbl.Cell(1, 3).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1014) 'Price
        tbl.Cell(1, 4).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1595) 'Discount
        tbl.Cell(1, 5).Range.Insertafter Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) 'Total
        tbl.Cell(1, 6).Range.Insertafter Phrase
        
        JJ = 6
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 1).Range.Insertafter rst![Description]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 2).Range.Insertafter rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 3).Range.Insertafter rst![Qty]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 4).Range.Insertafter Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Discount]) Then
                tbl.Cell(IRow, 5).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 5).Range.Insertafter rst![Discount]
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 6).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 6).Range.Insertafter Format(rst![Total], strFormat)
            End If
            
            JJ = JJ + 6
            IRow = IRow + 1
            rst.MoveNext
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 200
        tbl.Columns(2).Width = 75
        tbl.Columns(3).Width = 50
        tbl.Columns(4).Width = 50
        tbl.Columns(5).Width = 50
        tbl.Columns(6).Width = 50
        
        Set tbl = Nothing
        II = II + JJ + IRow - 1
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function S22(myrange As Object, objWordDoc As Object, ID As Long) 'extended pricing summary
    Dim Phrase As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    Dim TaxRate As Single
    Dim strFormat As String
    strFormat = GetDecimalFormat
        
    Call FF(ID)
    If RR = False Then Exit Function
    TaxRate = DLookup("[TaxRate]", "Office")
    Call S01(myrange, objWordDoc)

    RC = 1
                
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=6)
    IRow = 2
 
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) 'total
    tbl.Cell(1, 1).Range.Insertafter Phrase
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1595) 'discount
    tbl.Cell(1, 2).Range.Insertafter Phrase
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1036) 'sell
    tbl.Cell(1, 3).Range.Insertafter Phrase
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1604) 'Tax
    tbl.Cell(1, 4).Range.Insertafter Phrase
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1605) 'Tax Rate
    tbl.Cell(1, 5).Range.Insertafter Phrase
    Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1014) 'Price
    tbl.Cell(1, 6).Range.Insertafter Phrase
        
    JJ = 6
    Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![Text205]), "###" + strFormat) '"###0.00"
    tbl.Cell(2, 1).Range.Insertafter Phrase
    Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![Text244]), "###0.00")
    tbl.Cell(2, 2).Range.Insertafter Phrase
    Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![Text245]), "###" + strFormat) '"###0.00"
    tbl.Cell(2, 3).Range.Insertafter Phrase
    Phrase = Format(((CDbl(Forms![FormSales]![Child3].Form![Text245]) * TaxRate) / 100), strFormat)
    tbl.Cell(2, 4).Range.Insertafter Phrase
    Phrase = Format(TaxRate, "###0.00")
    tbl.Cell(2, 5).Range.Insertafter Phrase
    Phrase = Format((CDbl(Forms![FormSales]![Child3].Form![Text245]) * TaxRate) / 100 + CDbl(Forms![FormSales]![Child3].Form![Text245]), "###" + strFormat) '"###0.00"
    tbl.Cell(2, 6).Range.Insertafter Phrase
            
    JJ = JJ + 6
    IRow = IRow + 1
        
    On Error Resume Next
    tbl.Columns(1).Width = 80
    tbl.Columns(2).Width = 80
    tbl.Columns(3).Width = 80
    tbl.Columns(4).Width = 80
    tbl.Columns(5).Width = 75
    tbl.Columns(6).Width = 80
        
    Set tbl = Nothing
    II = II + JJ + IRow - 1

End Function

Function S23(myrange As Object, objWordDoc As Object, ID As Long) 'insert block
    Dim Phrase As String
                    
    Call FF(ID)
    If RR = False Then Exit Function
    Call S01(myrange, objWordDoc)
                    
    Set myrange = objWordDoc.Paragraphs(II).Range
    Phrase = DLookup("[Description]", "Word Export Template Temp", "[ID]= " & ID)
    myrange.Insertbefore (Phrase & vbCrLf)
    Set myrange = objWordDoc.Paragraphs(II).Range
    Call FFF(myrange, B, i, U, S, f, A)
    II = II + 1

End Function

Function FF(ID)
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String

    Set dbs = CurrentDb
    strSql = "SELECT * FROM [Word Export Template Temp] WHERE [ID] = " & ID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    f = rst![TFont]
    S = rst![TSize]
    B = rst![TBold]
    i = rst![TItalic]
    U = rst![TUnderline]
    SH = rst![TShadow]
    If rst![TAlignment] = "L" Then A = WD_ALIGN_PARAGRAPH_LEFT
    If rst![TAlignment] = "C" Then A = WD_ALIGN_PARAGRAPH_CENTER
    If rst![TAlignment] = "R" Then A = WD_ALIGN_PARAGRAPH_RIGHT
    If rst![Include] = -1 Then
        RR = True
    Else
        RR = False
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function FFF(myrange As Object, B As Boolean, i As Boolean, U As Boolean, S As Integer, f As String, A As Integer)
    myrange.Font.Bold = B
    myrange.Font.Italic = i
    myrange.Font.Underline = U
    myrange.Font.Size = S
    myrange.Font.Name = f
    myrange.Paragraphs.Alignment = A
End Function

Function S21_base()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

'1 Intervention Labour
    strSql = "SELECT * FROM [Service Selection Value Temp] WHERE [SSID] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Intervention]
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![TT]
        rst![Price] = rst1![LR]
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close

'2 Travel Time, Other Time, Distance
    Set rst1 = dbs.OpenRecordset("Service Selection Value Temp1", dbOpenDynaset, dbSeeChanges)
    If rst1![TravelQty] > 0 Then
        rst.AddNew
        rst![Description] = Create_Msg([Forms]![MENU-NEW]![Text118], 625)
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![TravelQty]
        rst![Price] = rst1![TravelRate]
        rst![Discount] = rst1![DiscountT]
        rst![Total] = rst1![TravelPrice]
        rst.Update
    End If
    If rst1![OtherQty] > 0 Then
        rst.AddNew
        rst![Description] = Create_Msg([Forms]![MENU-NEW]![Text118], 967)
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![OtherQty]
        rst![Price] = rst1![OtherRate]
        rst![Discount] = rst1![DiscountO]
        rst![Total] = rst1![OtherPrice]
        rst.Update
    End If
    If rst1![kmQty] > 0 Then
        rst.AddNew
        rst![Description] = Create_Msg([Forms]![MENU-NEW]![Text118], 961)
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![kmQty]
        rst![Price] = rst1![kmRate]
        rst![Discount] = rst1![DiscountD]
        rst![Total] = rst1![kmPrice]
        rst.Update
    End If
    rst1.Close
    
'3 Material
    strSql = "SELECT * FROM [Service Selection 3M Temp] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Price] / rst![Qty]
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    
'4 Miscellanous
    strSql = "SELECT * FROM [Service Selection B] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = "****"
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Sell]
        rst![Discount] = 0
        rst![Total] = rst1![Sell] * rst1![Qty]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function

Function B01(myrange As Object, objWordDoc As Object, Bookmarkname As String)    'customername
    Dim Phrase As String
    Dim CID As Long
    Dim BID As Long
    
    On Error GoTo B01ERR
'    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![Formsales]![SSID])
'    CID = DLookup("[CustomerID]", "BD-Buildings", "[BuildingID]= " & BID)
    CID = Forms![FormSSV7]![Combo43]
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = DLookup("[CustomerName]", "BD-Customers", "[CustomerID]= " & CID)
    myrange.Insertbefore (Phrase)
    Exit Function
B01ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B02(myrange As Object, objWordDoc As Object, Bookmarkname As String)    'customer address
    Dim Phrase As String
    Dim CID As Long
    Dim BID As Long
    
    On Error GoTo B02ERR
    CID = Forms![FormSSV7]![Combo43]
        
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Address]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer address"
    Else
        Phrase = DLookup("[Address]", "BD-Customers", "[CustomerID]= " & CID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B02ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B03(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'customer city
    Dim Phrase As String
    Dim CID As Long
    Dim BID As Long
    
    On Error GoTo B03ERR
    CID = Forms![FormSSV7]![Combo43]
                
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[City]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer city"
    Else
        Phrase = DLookup("[City]", "BD-Customers", "[CustomerID]= " & CID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B03ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B04(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'customer postcode
    Dim Phrase As String
    Dim CID As Long
    Dim BID As Long
    
    On Error GoTo B04ERR
    CID = Forms![FormSSV7]![Combo43]
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[PostalCode]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer postcode"
    Else
        Phrase = DLookup("[PostalCode]", "BD-Customers", "[CustomerID]= " & CID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B04ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B05(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'contact name
    Dim Phrase As String
            
    On Error GoTo B05ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])) Then
        Phrase = "no contact"
    Else
        Phrase = DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    End If
    myrange.Insertbefore (Phrase)
    
    Exit Function
B05ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B06(myrange As Object, objWordDoc As Object, Bookmarkname As String)    'customernumber
    Dim Phrase As String
    Dim CID As Long
    Dim BID As Long
    
    On Error GoTo B06ERR
    CID = Forms![FormSSV7]![Combo43]
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[CustomerNumber]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer number"
    Else
        Phrase = DLookup("[CustomerNumber]", "BD-Customers", "[CustomerID]= " & CID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B06ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B07(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'region
    Dim Phrase As String
            
    On Error GoTo B07ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Region]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])) Then
        Phrase = "no region"
    Else
        Phrase = DLookup("[Region]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B07ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B08(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'building name
    Dim Phrase As String
    Dim BID As Long
            
    On Error GoTo B08ERR
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[BuildingName]", "BD-Buildings", "[BuildingID]= " & BID)) Then
        Phrase = "no building name"
    Else
        Phrase = DLookup("[BuildingName]", "BD-Buildings", "[BuildingID]= " & BID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B08ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B09(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'building street
    Dim Phrase As String
    Dim BID As Long
            
    On Error GoTo B09ERR
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Address]", "BD-Buildings", "[BuildingID]= " & BID)) Then
        Phrase = "no building street"
    Else
        Phrase = DLookup("[Address]", "BD-Buildings", "[BuildingID]= " & BID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B09ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B10(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'building city
    Dim Phrase As String
    Dim BID As Long
            
    On Error GoTo B10ERR
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[City]", "BD-Buildings", "[BuildingID]= " & BID)) Then
        Phrase = "no building city"
    Else
        Phrase = DLookup("[City]", "BD-Buildings", "[BuildingID]= " & BID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B10ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B11(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'building post code
    Dim Phrase As String
    Dim BID As Long
            
    On Error GoTo B11ERR
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[PostalCode]", "BD-Buildings", "[BuildingID]= " & BID)) Then
        Phrase = "no building postcode"
    Else
        Phrase = DLookup("[PostalCode]", "BD-Buildings", "[BuildingID]= " & BID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B11ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B12(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'building number
    Dim Phrase As String
    Dim BID As Long
            
    On Error GoTo B12ERR
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[BuildingName]", "BD-Buildings", "[BuildingID]= " & BID)) Then
        Phrase = "no building number"
    Else
        Phrase = DLookup("[BuildingNumber]", "BD-Buildings", "[BuildingID]= " & BID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B12ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B13(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'equipment model number
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim i As Integer
    
    Set dbs = CurrentDb
    
    On Error GoTo B13ERR
    strSql = "SELECT * FROM [Service Selection 4 Word Export] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        While Not rst.EOF
            strSql = "SELECT * FROM [BD-Equipments] WHERE [EquipmentID] =" & rst![EquipmentID]
            Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
                If IsNull(rst1![Model]) Then
                    If i = 0 Then
                        Phrase = rst1![Model]
                    Else
                        Phrase = Phrase & " , " & "no model number"
                    End If
                Else
                    If i = 0 Then
                        Phrase = rst1![Model]
                    Else
                        Phrase = Phrase & " , " & rst1![Model]
                    End If
                End If
            End If
            rst1.Close
            rst.MoveNext
            i = i + 1
        Wend
    End If
    myrange.Insertbefore (Phrase)
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    Exit Function
B13ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B14(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'equipment serial number
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim i As Integer
    
    Set dbs = CurrentDb
    
    On Error GoTo B14ERR
    strSql = "SELECT * FROM [Service Selection 4 Word Export] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        While Not rst.EOF
            strSql = "SELECT * FROM [BD-Equipments] WHERE [EquipmentID] =" & rst![EquipmentID]
            Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
                If IsNull(rst1![Model]) Then
                    If i = 0 Then
                        Phrase = rst1![Serial Number]
                    Else
                        Phrase = Phrase & " , " & "no serial number"
                    End If
                Else
                    If i = 0 Then
                        Phrase = rst1![Serial Number]
                    Else
                        Phrase = Phrase & " , " & rst1![Serial Number]
                    End If
                End If
            End If
            rst1.Close
            rst.MoveNext
            i = i + 1
        Wend
    End If
    myrange.Insertbefore (Phrase)
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    Exit Function
B14ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B15(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'service selection name
    Dim Phrase As String
            
    On Error GoTo B15ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = DLookup("[Service Selection Name]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    myrange.Insertbefore (Phrase)
    Exit Function
B15ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B16(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'service selection name
    Dim Phrase As String
            
    On Error GoTo B16ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = DLookup("[Service Selection Name]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    myrange.Insertbefore (Phrase)
    Exit Function
B16ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B17(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'user name
    Dim Phrase As String
    Dim SALESID As Long
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    On Error GoTo B17ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Set rst = dbs.OpenRecordset("SalesEngineers", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        SALESID = DLookup("[SEID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    Else
        Phrase = "no sales engineer"
        GoTo BYPASS_NAME
    End If
    rst.Close
    
    strSql = "SELECT * FROM [SalesEngineers] WHERE [SalesEngineerID] =" & SALESID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        Phrase = rst![SalesFirstName] & " " & rst![SalesLastName]
    Else
        Phrase = "no sales engineer"
    End If
    
BYPASS_NAME:
    myrange.Insertbefore (Phrase)
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B17ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B18(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'region
    Dim Phrase As String
            
    On Error GoTo B18ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Region]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])) Then
        Phrase = "no region"
    Else
        Phrase = DLookup("[Region]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B18ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B19(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'sales engineer phone
    Dim Phrase As String
    Dim SALESID As Long
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    On Error GoTo B19ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Set rst = dbs.OpenRecordset("SalesEngineers", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        SALESID = DLookup("[SEID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    Else
        Phrase = "no sales engineer phonenumber"
        GoTo BYPASS_NAME
    End If
    rst.Close
    
    strSql = "SELECT * FROM [SalesEngineers] WHERE [SalesEngineerID] =" & SALESID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    
    If rst.RecordCount > 0 Then
        If IsNull(rst![PhoneNumber]) Then
            Phrase = "no sales engineer phonenumber"
        Else
            Phrase = rst![PhoneNumber]
        End If
    Else
            Phrase = "no sales engineer phonenumber"
    End If
        
BYPASS_NAME:
    myrange.Insertbefore (Phrase)
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B19ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B20(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'total labour and selling price
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B20ERR
    Call B20_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
       
       TPP = TPP + rst![Total]
       rst.MoveNext
    Wend
    rst.Close
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B20ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B20_base()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

'1 Intervention Labour
    strSql = "SELECT * FROM [Service Selection Value Temp] WHERE [SSID] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Intervention]
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![TT]
        rst![Price] = rst1![LR]
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close

'2 Travel Time, Other Time, Distance
    Set rst1 = dbs.OpenRecordset("Service Selection Value Temp1", dbOpenDynaset, dbSeeChanges)
    If rst1![TravelQty] > 0 Then
        rst.AddNew
        rst![Description] = Create_Msg([Forms]![MENU-NEW]![Text118], 625)
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![TravelQty]
        rst![Price] = rst1![TravelRate]
        rst![Discount] = rst1![DiscountT]
        rst![Total] = rst1![TravelPrice]
        rst.Update
    End If
    If rst1![OtherQty] > 0 Then
        rst.AddNew
        rst![Description] = Create_Msg([Forms]![MENU-NEW]![Text118], 967)
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![OtherQty]
        rst![Price] = rst1![OtherRate]
        rst![Discount] = rst1![DiscountO]
        rst![Total] = rst1![OtherPrice]
        rst.Update
    End If
    If rst1![kmQty] > 0 Then
        rst.AddNew
        rst![Description] = Create_Msg([Forms]![MENU-NEW]![Text118], 961)
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![kmQty]
        rst![Price] = rst1![kmRate]
        rst![Discount] = rst1![DiscountD]
        rst![Total] = rst1![kmPrice]
        rst.Update
    End If
    rst1.Close
    
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function

Function B20_FRANCE(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'SPECIAL PARTS ONLY
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    Call B21_base
'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    On Error GoTo B20FRANCEERR
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

    While Not rst.EOF
       If MID(rst![PartNumber], 1, 3) = "SP-" Then
            TPP = TPP + rst![Total]
       End If
       rst.MoveNext
    Wend
    rst.Close
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B20FRANCEERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B21(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'parts table
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B21ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B21_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 2
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=5)
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
        tbl.Cell(1, 1).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 158) 'Part Number
        tbl.Cell(1, 2).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54) 'Qty
        tbl.Cell(1, 3).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1014) 'Price
        tbl.Cell(1, 4).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) 'Total
        tbl.Cell(1, 5).Range.Insertbefore Phrase
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 250
        tbl.Columns(2).Width = 80
        tbl.Columns(3).Width = 50
        tbl.Columns(4).Width = 50
        tbl.Columns(5).Width = 50
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B21ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B21_FRANCE(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'parts table
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B21FRANCEERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B21_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=4)
'        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
'        tbl.cell(1, 1).Range.Insertbefore Phrase
'        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 158) 'Part Number
'        tbl.cell(1, 2).Range.Insertbefore Phrase
'        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54) 'Qty
'        tbl.cell(1, 2).Range.Insertbefore Phrase
'        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1014) 'Price
'        tbl.cell(1, 3).Range.Insertbefore Phrase
'        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) 'Total
'        tbl.cell(1, 4).Range.Insertbefore Phrase
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
 '           If IsNull(rst![PartNumber]) Then
 '               tbl.cell(IRow, 2).Range.Insertbefore " "
 '           Else
 '               tbl.cell(IRow, 2).Range.Insertbefore rst![PartNumber]
 '           End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        
        On Error Resume Next
        tbl.Columns(1).Width = 290
        tbl.Columns(2).Width = 50
        tbl.Columns(3).Width = 70
        tbl.Columns(4).Width = 70
'        tbl.columns(5).Width = 50
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B21FRANCEERR:
Call createErrorMessage(Bookmarkname)
End Function
'Start:DFSD-167
Function B21_R12GERM(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'parts table
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B21_R12GERMERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B21_R12GERM_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp Query", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=6)
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 3).Range.Insertbefore rst![Qty]
                End If
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Price], strFormat)
                End If
            End If
            If IsNull(rst![Discount]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Discount], strFormat)
                End If
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 6).Range.Insertbefore Format(rst![Total], strFormat)
                End If
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        
        On Error Resume Next
        tbl.Columns(1).Width = 190
        tbl.Columns(2).Width = 80
        tbl.Columns(3).Width = 30
        tbl.Columns(4).Width = 65
        tbl.Columns(5).Width = 50
        tbl.Columns(6).Width = 75
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B21_R12GERMERR:
Call createErrorMessage(Bookmarkname)
End Function
'End:DFSD-167

Function B21_GERM(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'parts table
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B21_GERMERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B21GERM_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp Query", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=6)
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 3).Range.Insertbefore rst![Qty]
                End If
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Price], strFormat)
                End If
            End If
            If IsNull(rst![Discount]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Discount], strFormat)
                End If
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 6).Range.Insertbefore Format(rst![Total], strFormat)
                End If
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        
        On Error Resume Next
        tbl.Columns(1).Width = 190
        tbl.Columns(2).Width = 80
        tbl.Columns(3).Width = 30
        tbl.Columns(4).Width = 65
        tbl.Columns(5).Width = 50
        tbl.Columns(6).Width = 75
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B21_GERMERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B21_base()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

    strSql = "SELECT * FROM [Service Selection 3M Temp] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        If rst![Qty] = 0 Then
            rst![Price] = 0
        Else
            rst![Price] = rst1![Price] / rst![Qty]
        End If
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function
Function B21_base_TIS()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp2")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)

    strSql = "SELECT * FROM [Service Selection 3M Temp] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        rst![order] = rst1![Listprice]
        If rst![Qty] = 0 Then
            rst![Price] = 0
        Else
            rst![Price] = rst1![Price] / rst1![Qty]
        End If
        rst![Discount] = (1 - rst1![Qty2]) * 100
        rst![Total] = rst1![Price]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function
'Start:DFSD-167
Function B21_R12GERM_base()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim CHK1 As Integer
   
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    Call Delete_Records("Word Export Template Temp2")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

    strSql = "SELECT * FROM [Service Selection 3M] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Cost] * rst1![Margin]
        'If rst![Qty] = 0 Then
         '   rst![Price] = 0
        'Else
            'rst![Price] = rst1![ListPrice] * rst1![Qty2]
        'End If
        rst![Discount] = rst1![Discount]
        rst![Total] = ((rst1![Cost] * rst1![Margin]) * rst![Qty] * ((100 - rst1![Discount]) / 100))
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    Set rst1 = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        CHK1 = 3
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Price]
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Total]
        'If rst![Qty] = 0 Then
         '   rst![Price] = 0
        'Else
         '   rst![Price] = rst1![Price]
        'End If
        'rst![Discount] = rst1![Discount]
        'rst![Total] = rst1![Price] * rst![Qty] * ((100 - rst1![Discount]) / 100)
        
        If MID(rst![PartNumber], 1, 1) = "C" Then
            If MID(rst![PartNumber], 2, 1) = "0" Then
                CHK1 = 0
            End If
        End If
        rst![order] = CHK1
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
        
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        rst.AddNew
        If rst![order] = 0 Then
            rst.AddNew
            rst![order] = 2
            rst.Update
            GoTo ZZZ
        End If
        rst.MoveNext
    Wend
ZZZ:
    rst.Close
        
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function
'End:DFSD-167

Function B21GERM_base()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim CHK1 As Integer
   
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    Call Delete_Records("Word Export Template Temp2")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

    strSql = "SELECT * FROM [Service Selection 3M Temp] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        If rst![Qty] = 0 Then
            rst![Price] = 0
        Else
            rst![Price] = rst1![Listprice] * rst1![Qty2]
        End If
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price] * rst![Qty]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    Set rst1 = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        CHK1 = 3
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        If rst![Qty] = 0 Then
            rst![Price] = 0
        Else
            rst![Price] = rst1![Price]
        End If
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price] * rst![Qty] * ((100 - rst1![Discount]) / 100)
        
        If MID(rst![PartNumber], 1, 1) = "C" Then
            If MID(rst![PartNumber], 2, 1) = "0" Then
                CHK1 = 0
            End If
        End If
        rst![order] = CHK1
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
        
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        rst.AddNew
        If rst![order] = 0 Then
            rst.AddNew
            rst![order] = 2
            rst.Update
            GoTo ZZZ
        End If
        rst.MoveNext
    Wend
ZZZ:
    rst.Close
        
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function
Function B22(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'total parts price
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim strFormat As String
   
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B22ERR
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

    While Not rst.EOF
       
       TPP = TPP + rst![Total]
       rst.MoveNext
    Wend
    rst.Close
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B22ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B22_FRANCE(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'NO SPECIAL PARTS
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B22FRANCEERR
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

    While Not rst.EOF
       
       If MID(rst![PartNumber], 1, 3) <> "SP-" Then
            TPP = TPP + rst![Total]
       End If
       rst.MoveNext
    Wend
    rst.Close
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B22FRANCEERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B23(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'total labour and total parts
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B23ERR
    Call B23_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
       
       TPP = TPP + rst![Total]
       rst.MoveNext
    Wend
    rst.Close
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B23ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B23_FRANCE(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'total labour and total parts
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B23FRANCEERR
    Call B23_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
       If MID(rst![PartNumber], 1, 5) <> "*****" Then
            TPP = TPP + rst![Total]
       End If
       rst.MoveNext
    Wend
    rst.Close
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B23FRANCEERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B23_base()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

'1 Intervention Labour
    strSql = "SELECT * FROM [Service Selection Value Temp] WHERE [SSID] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Intervention]
        rst![PartNumber] = "*****"
        rst![Qty] = rst1![TT]
        rst![Price] = rst1![LR]
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close

'parts
    strSql = "SELECT * FROM [Service Selection 3M Temp] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Price] / rst![Qty]
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
        
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function

Function B24(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'total labour and total parts
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B24ERR
    Call B24_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
       
       TPP = TPP + rst![Total]
       rst.MoveNext
    Wend
    rst.Close
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B24ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B24_base()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    
'4 Miscellanous
    strSql = "SELECT * FROM [Service Selection B] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = "****"
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Sell]
        rst![Discount] = 0
        rst![Total] = rst1![Sell] * rst1![Qty]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function

Function B25(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'TOTAL
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim DCT As Single
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B25ERR
    Call S21_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
       
       TPP = TPP + rst![Total]
       rst.MoveNext
    Wend
    rst.Close
            
    DCT = Forms![FormSales]![Child3]![Text244]
    TPP = TPP * (100 - DCT) / 100
                
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B25ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B25_FRANCE(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'TOTAL
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim TPP As Single
    Dim DCT As Single
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B25FRANCEERR
    Call S21_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
       If MID(rst![PartNumber], 1, 5) <> "*****" Then
            TPP = TPP + rst![Total]
       End If
       rst.MoveNext
    Wend
    rst.Close
    
    DCT = Forms![FormSales]![Child3]![Text244]
    TPP = TPP * (100 - DCT) / 100
    
            
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(TPP, strFormat)
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B25FRANCEERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B26(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'intervention summary
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    
    Set dbs = CurrentDb
    On Error GoTo B26ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        
        RC = DCount("[Qty1]", "Z-SS Summary Cust1", "[Service Selection] =" & Forms![FormSales]![SSID])
        
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=5)
        IRow = 2
 
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 100) & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 1).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 28)
        tbl.Cell(1, 2).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 606)
        tbl.Cell(1, 3).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 62) & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 4).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 101)
        tbl.Cell(1, 5).Range.Insertbefore Phrase
        JJ = 5
        strSql = "SELECT * FROM [Z-SS Summary Cust1] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
        Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst1.EOF
            If IsNull(rst1![Qty1]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst1![Qty1]
            End If
            If IsNull(rst1![Equipment Type]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst1![Equipment Type]
            End If
            If IsNull(rst1![Tag]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore rst1![Tag]
            End If
            If IsNull(rst1![Qty2]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore rst1![Qty2]
            End If
            If IsNull(rst1![TK]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore rst1![TK]
            End If
            JJ = JJ + 5
            IRow = IRow + 1
            rst1.MoveNext
        Wend
        rst1.Close
        
        On Error Resume Next
        tbl.Columns(1).Width = 65
        tbl.Columns(2).Width = 65
        tbl.Columns(3).Width = 65
        tbl.Columns(4).Width = 70
        tbl.Columns(5).Width = 200
        
        Set tbl = Nothing
        II = II + JJ + (IRow - 2) + 1
    End If
    
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    Exit Function
B26ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B27(myrange As Object, objWordDoc As Object, Bookmarkname As String)   'Equipment details
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    
    Set dbs = CurrentDb
    On Error GoTo B27ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    strSql = "SELECT * FROM [Service Selection 4 Word Export] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=4)
        IRow = 2
                        
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 149)
        tbl.Cell(1, 1).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 151)
        tbl.Cell(1, 2).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 152)
        tbl.Cell(1, 3).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 606)
        tbl.Cell(1, 4).Range.Insertbefore Phrase
        JJ = 4
        While Not rst.EOF
            strSql = "SELECT * FROM [BD-Equipments] WHERE [EquipmentID] =" & rst![EquipmentID]
            Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                If IsNull(rst1![Serial Number]) Then
                    tbl.Cell(IRow, 1).Range.Insertbefore " "
                Else
                    tbl.Cell(IRow, 1).Range.Insertbefore rst1![Serial Number]
                End If
                If IsNull(rst1![Make]) Then
                    tbl.Cell(IRow, 2).Range.Insertbefore " "
                Else
                    tbl.Cell(IRow, 2).Range.Insertbefore rst1![Make]
                End If
                If IsNull(rst1![Model]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore " "
                Else
                    tbl.Cell(IRow, 3).Range.Insertbefore rst1![Model]
                End If
                If IsNull(rst1![Equipment Tag]) Then
                    tbl.Cell(IRow, 4).Range.Insertbefore " "
                Else
                    tbl.Cell(IRow, 4).Range.Insertbefore rst1![Equipment Tag]
                End If
            End If
            JJ = JJ + 4
            IRow = IRow + 1
            rst1.Close
            rst.MoveNext
        Wend
        tbl.Columns.Autofit
        II = II + JJ + (IRow - 2) + 1
        Set tbl = Nothing
    End If
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    Exit Function
B27ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B28(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'intervention content
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim rst2 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim JJ As Integer
    Dim tbl As Object
    
    Set dbs = CurrentDb
    On Error GoTo B28ERR
     
    Call Delete_Records("Word Export Bookmarks Interventions Temp")
    Set rst2 = dbs.OpenRecordset("Word Export Bookmarks Interventions Temp", dbOpenDynaset, dbSeeChanges)
    
    strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        While Not rst.EOF
                        
            Phrase = " "
            rst2.AddNew
            rst2![Temp] = Phrase
            rst2.Update
                        
            Phrase = rst![Service Intervention]
            rst2.AddNew
            rst2![Temp] = Phrase
            rst2.Update
                                    
            Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1504) & ": " & DLookup("[Tag]", "Tag", "[TagID]='" & rst![Tag] & "'")
            rst2.AddNew
            rst2![Temp] = MID(Phrase, 1, 250) & ":"
            rst2.Update
            
'add blank line
            rst2.AddNew
            rst2![Temp] = " "
            rst2.Update
            
            Call Delete_Records("S_S_VSF_Temp")
            Call Delete_Records("VSF Customer")
            If rst![DFS] = 0 Then
                If rst![InterventionID] <> 0 Then
                    Call Write_Process_SSTemp_IL(rst![ID3], 1, " ", 1, 90, 5, 0)
                Else
                    Call Write_Process_SSTemp(rst![ID3], 1, " ", 1, 2, 1, 90, 5, 0)
                End If
                Call Write_Extra_SSTemp(rst![ID3], 1, " ", 1)
            Else
                Call Write_Steps_SSTemp(rst![ID3])
            End If
            If rst![InterventionID] <> 0 Then
                DoCmd.SetWarnings False
                DoCmd.OpenQuery "S_S_VSF_Temp Query1 IL"
                DoCmd.SetWarnings True
            Else
                DoCmd.SetWarnings False
                DoCmd.OpenQuery "S_S_VSF_Temp Query1"
                DoCmd.SetWarnings True
            End If
            
            Set rst1 = dbs.OpenRecordset("VSF Customer", dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                rst1.MoveLast
                rst1.MoveFirst
                Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1130)
                rst2.AddNew
                rst2![Temp] = Phrase
                rst2.Update
                While Not rst1.EOF
                    If IsNull(rst1![Text]) Then
                        Phrase = " "
                        rst2.AddNew
                        rst2![Temp] = Phrase
                        rst2.Update
                    Else
                        Phrase = rst1![Text]
                        rst2.AddNew
                        rst2![Temp] = "-" & Phrase
                        rst2.Update
                    End If
                    rst1.MoveNext
                Wend
            End If
            rst1.Close
            rst.MoveNext
        Wend
    End If
    rst.Close
    rst2.Close

    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Set rst2 = dbs.OpenRecordset("Word Export Bookmarks Interventions Temp", dbOpenDynaset, dbSeeChanges)
    While Not rst2.EOF
        rst2.MoveLast
        rst2.MoveFirst
        RC = rst2.RecordCount
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=1)
        IRow = 2
        JJ = 1
        While Not rst2.EOF
            If IsNull(rst2![Temp]) Then
                tbl.Cell(IRow, 1).Range.Insertafter " "
            Else
                tbl.Cell(IRow, 1).Range.Insertafter rst2![Temp]
            End If
            JJ = JJ + 1
            IRow = IRow + 1
            rst2.MoveNext
        Wend
        II = II + JJ + (IRow - 2) + 1
        tbl.Columns(1).Width = 400
        Set tbl = Nothing
    Wend
    rst2.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set dbs = Nothing
    Exit Function
B28ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B29(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'service selection ID
    Dim Phrase As String
    
    On Error GoTo B29ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Find_SSID(Forms![FormSales]![SSID])
    myrange.Insertbefore (Phrase)
    Exit Function
    
B29ERR:
Call createErrorMessage(Bookmarkname)
End Function


Function B31(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'discount
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
        
    On Error GoTo B31ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    strSql = "SELECT * FROM [Parts Quotation] WHERE [PartQuotationID] =" & PQID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    Phrase = Format(rst![Discount], strFormat)
    rst.Close
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
    
B31ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B32(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID As Long)    'customername
    Dim Phrase As String
    
    On Error GoTo B32ERR
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = DLookup("[CustomerName]", "BD-Customers", "[CustomerID]= " & CID)
    myrange.Insertbefore (Phrase)
    Exit Function
B32ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B33(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID As Long)    'customer address
    Dim Phrase As String
    
    On Error GoTo B33ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Address]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer address"
    Else
        Phrase = DLookup("[Address]", "BD-Customers", "[CustomerID]= " & CID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B33ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B34(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID As Long) 'customer city
    Dim Phrase As String
    
    On Error GoTo B34ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[City]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer city"
    Else
        Phrase = DLookup("[City]", "BD-Customers", "[CustomerID]= " & CID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B34ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B35(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID As Long) 'customer postcode
    Dim Phrase As String
    Dim BID As Long
    
    On Error GoTo B35ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[PostalCode]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer postcode"
    Else
        Phrase = DLookup("[PostalCode]", "BD-Customers", "[CustomerID]= " & CID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B35ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B36(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'contact name
    Dim Phrase As String
            
    On Error GoTo B36ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)) Then
        Phrase = "no contact"
    Else
        Phrase = DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B36ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B37(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID As Long)    'customernumber
    Dim Phrase As String
    
    On Error GoTo B37ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = DLookup("[CustomerNumber]", "BD-Customers", "[CustomerID]= " & CID)
    myrange.Insertbefore (Phrase)
    Exit Function
B37ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B38(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'region
    Dim Phrase As String
            
    On Error GoTo B38ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Region]", "Parts Quotation", "[PartQuotationID]= " & PQID)) Then
        Phrase = "no region"
    Else
        Phrase = DLookup("[Region]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B38ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B39(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'service selection ID
    Dim Phrase As String
    
    On Error GoTo B39ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Find_PSID(PQID)
    myrange.Insertbefore (Phrase)
    Exit Function
    
B39ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B40(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'service selection name
    Dim Phrase As String
            
    On Error GoTo B40ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = DLookup("[PartQuotationName]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    myrange.Insertbefore (Phrase)
    Exit Function
B40ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B41(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'user name
    Dim Phrase As String
    Dim SALESID As Long
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    On Error GoTo B41ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Set rst = dbs.OpenRecordset("SalesEngineers", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        SALESID = DLookup("[SEID]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    Else
        Phrase = "no sales engineer"
        GoTo BYPASS_NAME
    End If
    rst.Close
    
    strSql = "SELECT * FROM [SalesEngineers] WHERE [SalesEngineerID] =" & SALESID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        Phrase = rst![SalesFirstName] & " " & rst![SalesLastName]
    Else
        Phrase = "no sales engineer"
    End If
    
BYPASS_NAME:
    myrange.Insertbefore (Phrase)
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B41ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B42(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'sales engineer phone
    Dim Phrase As String
    Dim SALESID As Long
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    On Error GoTo B42ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Set rst = dbs.OpenRecordset("SalesEngineers", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        SALESID = DLookup("[SEID]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    Else
        Phrase = "no sales engineer phonenumber"
        GoTo BYPASS_NAME
    End If
    rst.Close
    
    strSql = "SELECT * FROM [SalesEngineers] WHERE [SalesEngineerID] =" & SALESID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    
    If rst.RecordCount > 0 Then
        If IsNull(rst![PhoneNumber]) Then
            Phrase = "no sales engineer phonenumber"
        Else
            Phrase = rst![PhoneNumber]
        End If
    Else
            Phrase = "no sales engineer phonenumber"
    End If
    
BYPASS_NAME:
    myrange.Insertbefore (Phrase)
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B42ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B43(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts table
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B43ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B43_base(PQID)
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 2
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=5)
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
        tbl.Cell(1, 1).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 158) 'Part Number
        tbl.Cell(1, 2).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54) 'Qty
        tbl.Cell(1, 3).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1014) 'Price
        tbl.Cell(1, 4).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) 'Total
        tbl.Cell(1, 5).Range.Insertbefore Phrase
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 250
        tbl.Columns(2).Width = 75
        tbl.Columns(3).Width = 50
        tbl.Columns(4).Width = 50
        tbl.Columns(5).Width = 50
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B43ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B43_mini(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts table
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B43ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B43_base(PQID)
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 2
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=4)
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
        tbl.Cell(1, 1).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54) 'Qty
        tbl.Cell(1, 2).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1014) 'Price
        tbl.Cell(1, 3).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) 'Total
        tbl.Cell(1, 4).Range.Insertbefore Phrase
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 325
        tbl.Columns(2).Width = 50
        tbl.Columns(3).Width = 50
        tbl.Columns(4).Width = 50
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B43ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B43_base(PQID As Long)

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim pricingmodel As Integer
    
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    pricingmodel = getPricingModel
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

'parts
    strSql = "SELECT * FROM [Parts Quotation 1] WHERE [PartQuotationID] =" & PQID
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        If pricingmodel = 1 Then
            rst.AddNew
            rst![Description] = rst1![Description]
            rst![PartNumber] = rst1![PartNumber]
            rst![Qty] = rst1![Qty]
            rst![Price] = rst1![Cost] * rst1![Multiplier]
            rst![Discount] = rst1![Discount]
            rst![Total] = (rst1![Cost] * rst1![Multiplier] * (1 - (rst1![Discount] / 100))) * rst1![Qty]
            rst.Update
            rst1.MoveNext
        Else
            rst.AddNew
            rst![Description] = rst1![Description]
            rst![PartNumber] = rst1![PartNumber]
            rst![Qty] = rst1![Qty]
            rst![Price] = rst1![Listprice] * rst1![Multiplier]
            rst![Discount] = rst1![Discount]
            rst![Total] = (rst1![Listprice] * rst1![Multiplier] * (1 - (rst1![Discount] / 100))) * rst1![Qty]
            rst.Update
            rst1.MoveNext
        
        End If
    Wend
    rst1.Close
        
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function
Function B43_base_TIS(PQID As Long)

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim pricingmodel As Integer
    
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word EXport Template Temp2")
    pricingmodel = getPricingModel
    
    Set rst = dbs.OpenRecordset("Word EXport Template Temp2", dbOpenDynaset, dbSeeChanges)

'parts
    strSql = "SELECT * FROM [Parts Quotation 1] WHERE [PartQuotationID] =" & PQID
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        If pricingmodel = 1 Then
            rst.AddNew
            rst![Description] = rst1![Description]
            rst![PartNumber] = rst1![PartNumber]
            rst![Qty] = rst1![Qty]
            rst![order] = rst1![Cost]
            rst![Price] = rst1![Cost] * rst1![Multiplier]
            rst![Discount] = rst1![Discount]
            rst![Total] = (rst1![Cost] * rst1![Multiplier] * (1 - (rst1![Discount] / 100))) * rst1![Qty]
            rst.Update
            rst1.MoveNext
        Else
            rst.AddNew
            rst![Description] = rst1![Description]
            rst![PartNumber] = rst1![PartNumber]
            rst![Qty] = rst1![Qty]
            rst![order] = rst1![Listprice]
            rst![Price] = rst1![Listprice] * rst1![Multiplier]
            rst![Discount] = (1 - rst1![Multiplier]) * 100
            rst![Total] = (rst1![Listprice] * rst1![Multiplier] * (1 - (rst1![Discount] / 100))) * rst1![Qty]
            rst.Update
            rst1.MoveNext
        
        End If
    Wend
    rst1.Close
        
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function

Function B44(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts price total
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strFormat As String
    
    Set dbs = CurrentDb
        
    On Error GoTo B44ERR
    strFormat = GetDecimalFormat
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    strSql = "SELECT * FROM [Parts Quotation] WHERE [PartQuotationID] =" & PQID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    Phrase = Format(rst![Price], strFormat)
    rst.Close
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
    
B44ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B45(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'discount
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
        
    On Error GoTo B45ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    strSql = "SELECT * FROM [Service Selection] WHERE [Service SElection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    Phrase = Format(rst![Discount], strFormat)
    rst.Close
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
    
B45ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B46(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'interventions price
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim tbl As Object
    Dim strFormat As String
    strFormat = GetDecimalFormat

    Set dbs = CurrentDb

    On Error GoTo B46ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        
        RC = rst.RecordCount

        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=6)
        IRow = 2

        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 100) & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 1).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 28)
        tbl.Cell(1, 2).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 606)
        tbl.Cell(1, 3).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 62) & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 54)
        tbl.Cell(1, 4).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 101)
        tbl.Cell(1, 5).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 22)
        tbl.Cell(1, 6).Range.Insertbefore Phrase
        strSql = "SELECT * FROM [Z-SS Summary Cust2] WHERE [Service Selection] =" & Forms![FormSales]![SSID] & " AND [ID2] > " & 99
        Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst1.EOF
            If IsNull(rst1![Qty1]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst1![Qty1]
            End If
            If IsNull(rst1![Equipment Type]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst1![Equipment Type]
            End If
            If IsNull(rst1![Tag]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore rst1![Tag]
            End If
            If IsNull(rst1![Qty2]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore rst1![Qty2]
            End If
            If IsNull(rst1![TK]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore rst1![TK]
            End If
            If IsNull(rst1![Price]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 6).Range.Insertbefore Format(rst1![Price], strFormat)
            End If

            IRow = IRow + 1
            rst1.MoveNext
        Wend
        rst1.Close
        On Error Resume Next
        tbl.Columns(1).Width = 65
        tbl.Columns(2).Width = 65
        tbl.Columns(3).Width = 65
        tbl.Columns(4).Width = 70
        tbl.Columns(5).Width = 140
        tbl.Columns(6).Width = 60

        Set tbl = Nothing
    End If

    rst.Close
    Exit Function  'DFSD-167 Added
B46ERR:
Call createErrorMessage(Bookmarkname)
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
End Function
Function B46_GERM(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'interventions price
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim RC As String
    Dim IRow As Integer
    Dim tbl As Object
    Dim strFormat As String
    strFormat = GetDecimalFormat

    Set dbs = CurrentDb

    On Error GoTo B46GERMERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        
        RC = rst.RecordCount

        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=5)
        IRow = 1

        strSql = "SELECT * FROM [Z-SS Summary Cust2] WHERE [Service Selection] =" & Forms![FormSales]![SSID] & " AND [ID2] > " & 99
        Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst1.EOF
            If IsNull(rst1![TK]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst1![TK] & " (" & rst1![Time] * rst1![Q1] * rst1![Q2] & " " & Create_Msg([Forms]![MENU-NEW]![Text118], 421) & ")"
            End If
            If IsNull(rst1![Qty2]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst1![Qty2] * rst1![Qty1]
            End If
            If IsNull(rst1![Price]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore Format((rst1![Price]) / (rst1![Qty2] * rst1![Qty1]), strFormat)
            End If
            If IsNull(rst1![Discount]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore Format(rst1![Discount], strFormat)
            End If
            If IsNull(rst1![Price]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst1![Price], strFormat)
            End If

            IRow = IRow + 1
            rst1.MoveNext
        Wend
        rst1.Close
        On Error Resume Next
        tbl.Columns(1).Width = 270
        tbl.Columns(2).Width = 30
        tbl.Columns(3).Width = 65
        tbl.Columns(4).Width = 50
        tbl.Columns(5).Width = 75
        Set tbl = Nothing
    End If
    rst.Close
    Exit Function
B46GERMERR:
Call createErrorMessage(Bookmarkname)
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
End Function


Function B47(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID As Long)    'customerphonenumber
    Dim Phrase As String
    
    On Error GoTo B47ERR
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[CustomerNumber]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer phone number"
    Else
        If Not IsNull(DLookup("[PhoneNumber]", "BD-Customers", "[CustomerID]= " & CID)) Then
            Phrase = DLookup("[PhoneNumber]", "BD-Customers", "[CustomerID]= " & CID)
        Else
            Phrase = "no customer phone number"
        End If
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B47ERR:
Call createErrorMessage(Bookmarkname)
       
End Function

Function B48(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID As Long)    'customerfaxnumber
    Dim Phrase As String
    
    On Error GoTo B48ERR
        
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[CustomerNumber]", "BD-Customers", "[CustomerID]= " & CID)) Then
        Phrase = "no customer fax number"
    Else
        If Not IsNull(DLookup("[FaxNumber]", "BD-Customers", "[CustomerID]= " & CID)) Then
            Phrase = DLookup("[FaxNumber]", "BD-Customers", "[CustomerID]= " & CID)
        Else
            Phrase = "no customer fax number"
        End If
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B48ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B49(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'buildingphone number
    Dim Phrase As String
    Dim BID As Long
            
    On Error GoTo B49ERR
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[PhoneNumber]", "BD-Buildings", "[BuildingID]= " & BID)) Then
        Phrase = "no building phone number"
    Else
        Phrase = DLookup("[PhoneNumber]", "BD-Buildings", "[BuildingID]= " & BID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B49ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B50(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'building fax number
    Dim Phrase As String
    Dim BID As Long
            
    On Error GoTo B50ERR
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[FaxNumber]", "BD-Buildings", "[BuildingID]= " & BID)) Then
        Phrase = "no building fax number"
    Else
        Phrase = DLookup("[FaxNumber]", "BD-Buildings", "[BuildingID]= " & BID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B50ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B51(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'user name email
    Dim Phrase As String
    Dim SALESID As Long
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    On Error GoTo B51ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Set rst = dbs.OpenRecordset("SalesEngineers", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        SALESID = DLookup("[SEID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    Else
        Phrase = "no sales engineer"
        GoTo BYPASS_NAME
    End If
    rst.Close
    
    strSql = "SELECT * FROM [SalesEngineers] WHERE [SalesEngineerID] =" & SALESID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        Phrase = rst![email]
    Else
        Phrase = "no sales engineer email"
    End If
    
BYPASS_NAME:
    myrange.Insertbefore (Phrase)
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B51ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B52(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'service selection id
    Dim Phrase As String
            
    On Error GoTo B52ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Find_SSID(Forms![FormSales]![SSID])
    myrange.Insertbefore (Phrase)
    Exit Function
B52ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B53(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'contact phone number
       
    Dim Phrase As String
    Dim Search As String
    Dim BID As Long
    Dim CID As Long
    Dim CHK As Integer
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    On Error GoTo B53ERR
    Set dbs = CurrentDb

    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])) Then
        Search = "----"
        CHK = DLookup("[ContactLocation]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    Else
        Search = DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
        CHK = DLookup("[ContactLocation]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    End If
    
    BID = Forms![FormSales]![BuildingID]
'    CID = Forms![FormSales]![CustomerID]
    CID = DLookup("[CustomerID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    If CHK = 2 Then
        strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] = " & CID & " And [Name] = '" & Search & "'"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            If Not IsNull(rst![Phone]) Then
                Phrase = rst![Phone]
            Else
                Phrase = "no contact phone number"
            End If
        Else
            Phrase = "no contact phone number"
        End If
        rst.Close
    ElseIf CHK = 1 Then
        strSql = "SELECT * FROM [BD-Buildings Contact] WHERE [BuildingID] = " & BID & " And [Name] = '" & Search & "'"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            If Not IsNull(rst![Phone]) Then
                Phrase = rst![Phone]
            Else
                Phrase = "no contact phone number"
            End If
        Else
            Phrase = "no contact phone number"
        End If
        rst.Close
    End If
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B53ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B54(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'contact email
    
    Dim Phrase As String
    Dim Search As String
    Dim BID As Long
    Dim CID As Long
    Dim CHK As Integer
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    On Error GoTo B54ERR
    Set dbs = CurrentDb

    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])) Then
        Search = "----"
        CHK = DLookup("[ContactLocation]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    Else
        Search = DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
        CHK = DLookup("[ContactLocation]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    End If
    
    BID = Forms![FormSales]![BuildingID]
'    CID = Forms![FormSales]![CustomerID]
    CID = DLookup("[CustomerID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    If CHK = 2 Then
        strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] = " & CID & " And [Name] = '" & Search & "'"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            If Not IsNull(rst![email]) Then
                Phrase = rst![email]
            Else
                Phrase = "no contact email"
            End If
        Else
            Phrase = "no contact email"
        End If
        rst.Close
    ElseIf CHK = 1 Then
        strSql = "SELECT * FROM [BD-Buildings Contact] WHERE [BuildingID] = " & BID & " And [Name] = '" & Search & "'"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            If Not IsNull(rst![email]) Then
                Phrase = rst![email]
            Else
                Phrase = "no contact email"
            End If
        Else
            Phrase = "no contact email"
        End If
        rst.Close
    End If
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B54ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B55(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'contact email
    Dim Phrase As String
    Dim Search As String
    Dim BID As Long
    Dim CID As Long
    Dim CHK As Integer
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    On Error GoTo B55ERR
    Set dbs = CurrentDb

    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])) Then
        Search = "----"
        CHK = DLookup("[ContactLocation]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    Else
        Search = DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
        CHK = DLookup("[ContactLocation]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    End If
    
    BID = Forms![FormSales]![BuildingID]
'    CID = Forms![FormSales]![CustomerID]
    CID = DLookup("[CustomerID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])

    If CHK = 2 Then
        strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] = " & CID & " And [Name] = '" & Search & "'"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            If Not IsNull(rst![Fax]) Then
                Phrase = rst![Fax]
            Else
                Phrase = "no contact fax"
            End If
        Else
            Phrase = "no contact fax"
        End If
        rst.Close
    ElseIf CHK = 1 Then
        strSql = "SELECT * FROM [BD-Buildings Contact] WHERE [BuildingID] = " & BID & " And [Name] = '" & Search & "'"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            If Not IsNull(rst![Fax]) Then
                Phrase = rst![Fax]
            Else
                Phrase = "no contact fax"
            End If
        Else
            Phrase = "no contact fax"
        End If
        rst.Close
    End If
    
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B55ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B56(myrange As Object, objWordDoc As Object, Bookmarkname As String)  'German pricing total
    Dim Phrase As String
    Dim tbl As Object
    Dim TaxRate As Single
    Dim strFormat As String
    strFormat = GetDecimalFormat
        
    On Error GoTo B56ERR
        
    TaxRate = DLookup("[TaxRate]", "Office")

                
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=1, NumColumns:=6)
         
    Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![Text205]), "###" + strFormat) '"###0.00"
    tbl.Cell(2, 1).Range.Insertbefore Phrase
    Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![Text244]), "###0.00")
    tbl.Cell(2, 2).Range.Insertbefore Phrase
    Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![Text245]), "###" + strFormat) '"###0.00"
    tbl.Cell(2, 3).Range.Insertbefore Phrase
    Phrase = Format(((CDbl(Forms![FormSales]![Child3].Form![Text245]) * TaxRate) / 100), strFormat)
    tbl.Cell(2, 4).Range.Insertbefore Phrase
    Phrase = Format(TaxRate, "###0.00")
    tbl.Cell(2, 5).Range.Insertbefore Phrase
    Phrase = Format((CDbl(Forms![FormSales]![Child3].Form![Text245]) * TaxRate) / 100 + CDbl(Forms![FormSales]![Child3].Form![Text245]), "###" + strFormat) '"###0.00"
    tbl.Cell(2, 6).Range.Insertbefore Phrase
        
    On Error Resume Next
    tbl.Columns(1).Width = 80
    tbl.Columns(2).Width = 80
    tbl.Columns(3).Width = 80
    tbl.Columns(4).Width = 80
    tbl.Columns(5).Width = 75
    tbl.Columns(6).Width = 80
        
    Set tbl = Nothing
    Exit Function

B56ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B57(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'travel, other,distance
    Dim Phrase As String
    Dim dbs As Database
    Dim RC As String
    Dim IRow As Integer
    Dim tbl As Object
    Dim strFormat As String
    Dim rst As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    On Error GoTo B57ERR
    
    Call Delete_Records("Word Export Temp")
    strFormat = GetDecimalFormat
    
    strSql = "Word Export Temp"
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text1004])) <> 0 Then
        rst.AddNew
        rst![C1] = Create_Msg([Forms]![MENU-NEW]![Text118], 1045)
        rst![C2] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text1004]))
        rst![C3] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text1005]))
        rst![C4] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text1007]))
        rst![C5] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text1006]))
        rst.Update
    End If
    If CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text2004])) <> 0 Then
        rst.AddNew
        rst![C1] = Create_Msg([Forms]![MENU-NEW]![Text118], 1044)
        rst![C2] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text2004]))
        rst![C3] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text2005]))
        rst![C4] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text2007]))
        rst![C5] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text2006]))
        rst.Update
    End If
    If CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text3004])) <> 0 Then
        rst.AddNew
        rst![C1] = Create_Msg([Forms]![MENU-NEW]![Text118], 961)
        rst![C2] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text3004]))
        rst![C3] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text3005]))
        rst![C4] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text3007]))
        rst![C5] = CDbl((Forms![FormSales]![Child3].Form![FormSSV2]![Text3006]))
        rst.Update
    End If
    rst.Close
    
    strSql = "Word Export Temp"
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=5)
        IRow = 1
    
        While Not rst.EOF
            
            If IsNull(rst![C1]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![C1]
            End If
            If IsNull(rst![C2]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![C2]
            End If
            If IsNull(rst![C3]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore Format(rst![C3], strFormat)
            End If
            If IsNull(rst![C4]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![C4], strFormat)
            End If
            If IsNull(rst![C5]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![C5], strFormat)
            End If
        
            IRow = IRow + 1
            rst.MoveNext
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 270
        tbl.Columns(2).Width = 30
        tbl.Columns(3).Width = 65
        tbl.Columns(4).Width = 50
        tbl.Columns(5).Width = 75
    
        Set tbl = Nothing
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B57ERR:
Call createErrorMessage(Bookmarkname)
End Function


Function B58(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'contact title
    Dim Phrase As String
    Dim Search As String
    Dim BID As Long
    Dim CID As Long
    Dim CHK As Integer
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    On Error GoTo B58ERR
    Set dbs = CurrentDb

    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])) Then
        Search = "----"
        CHK = DLookup("[ContactLocation]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    Else
        Search = DLookup("[Contact]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
        CHK = DLookup("[ContactLocation]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    End If
    
    BID = Forms![FormSales]![BuildingID]
'    CID = Forms![FormSales]![CustomerID]
    CID = DLookup("[CustomerID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    If CHK = 2 Then
        strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] = " & CID & " And [Name] = '" & Search & "'"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            If Not IsNull(rst![Title]) Then
                Phrase = rst![Title]
            Else
                Phrase = "no contact title"
            End If
        Else
            Phrase = "no contact title"
        End If
        rst.Close
    ElseIf CHK = 1 Then
        strSql = "SELECT * FROM [BD-Buildings Contact] WHERE [BuildingID] = " & BID & " And [Name] = '" & Search & "'"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            If Not IsNull(rst![Title]) Then
                Phrase = rst![Title]
            Else
                Phrase = "no contact title"
            End If
        Else
            Phrase = "no contact title"
        End If
        rst.Close
    End If
    
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B58ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B59(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long, CID As Long) 'contact phone number PARTS
    Dim Phrase As String
    Dim Search As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    On Error GoTo B59ERR
    Set dbs = CurrentDb

    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    If IsNull(DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)) Then
        Search = "----"
    Else
        Search = DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    End If

    strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] = " & CID & " And [Name] = '" & Search & "'"
    
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        If Not IsNull(rst![Phone]) Then
            Phrase = rst![Phone]
        Else
            Phrase = "no contact phone number"
        End If
    Else
        Phrase = "no contact phone number"
    End If
    rst.Close
    
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B59ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B60(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long, CID As Long) 'contact email number PARTS
    Dim Phrase As String
    Dim Search As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    On Error GoTo B60ERR
    Set dbs = CurrentDb

    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    If IsNull(DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)) Then
        Search = "----"
    Else
        Search = DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    End If

    strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] = " & CID & " And [Email] = '" & Search & "'"
    
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        If Not IsNull(rst![Phone]) Then
            Phrase = rst![Phone]
        Else
            Phrase = "no contact email"
        End If
    Else
        Phrase = "no contact email"
    End If
    rst.Close
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B60ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B61(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long, CID As Long) 'contact fax
    Dim Phrase As String
    Dim Search As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    On Error GoTo B61ERR
    Set dbs = CurrentDb

    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)) Then
        Search = "----"
    Else
        Search = DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    End If

    strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] = " & CID & " And [Name] = '" & Search & "'"
    
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        If Not IsNull(rst![Fax]) Then
            Phrase = rst![Fax]
        Else
            Phrase = "no contact fax"
        End If
    Else
        Phrase = "no contact fax"
    End If
    rst.Close
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B61ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B62(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long, CID As Long) 'contact title
    Dim Phrase As String
    Dim Search As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    
    On Error GoTo B62ERR
    Set dbs = CurrentDb

    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)) Then
        Search = "----"
    Else
        Search = DLookup("[Contact]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    End If

    strSql = "SELECT * FROM [BD-Customers Contact] WHERE [CustomerID] = " & CID & " And [Name] = '" & Search & "'"
    
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        If Not IsNull(rst![Title]) Then
            Phrase = rst![Title]
        Else
            Phrase = "no contact title"
        End If
    Else
        Phrase = "no contact title"
    End If
    rst.Close
    myrange.Insertbefore (Phrase)
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B62ERR:
Call createErrorMessage(Bookmarkname)
End Function
'Start:DFSD-167
Function B63_R12GERM(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts table
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B63_R12GERMERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B63R12GERM_base(PQID)
    Set rst = dbs.OpenRecordset("Word Export Template Temp Query", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=6)
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 3).Range.Insertbefore rst![Qty]
                End If
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Price], strFormat)
                End If
            End If
            If IsNull(rst![Discount]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Discount], strFormat)
                End If
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 6).Range.Insertbefore Format(rst![Total], strFormat)
                End If
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 190
        tbl.Columns(2).Width = 80
        tbl.Columns(3).Width = 30
        tbl.Columns(4).Width = 65
        tbl.Columns(5).Width = 50
        tbl.Columns(6).Width = 75
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B63_R12GERMERR:
Call createErrorMessage(Bookmarkname)
End Function
'End:DFSD-167

Function B63_GERM(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts table
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B63_GERMERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B63GERM_base(PQID)
    Set rst = dbs.OpenRecordset("Word Export Template Temp Query", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=6)
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 3).Range.Insertbefore rst![Qty]
                End If
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Price], strFormat)
                End If
            End If
            If IsNull(rst![Discount]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Discount], strFormat)
                End If
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                If IsNull(rst![Description]) Then
                    tbl.Cell(IRow, 3).Range.Insertbefore ""
                Else
                    tbl.Cell(IRow, 6).Range.Insertbefore Format(rst![Total], strFormat)
                End If
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 190
        tbl.Columns(2).Width = 80
        tbl.Columns(3).Width = 30
        tbl.Columns(4).Width = 65
        tbl.Columns(5).Width = 50
        tbl.Columns(6).Width = 75
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B63_GERMERR:
Call createErrorMessage(Bookmarkname)
End Function
'Start:DFSD-167
Function B63R12GERM_base(PQID As Long)

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim CHK1 As Integer
   
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    Call Delete_Records("Word Export Template Temp2")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

    strSql = "SELECT * FROM [Parts Quotation 1] WHERE [PartQuotationID] =" & PQID
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Cost] * rst1![Multiplier]
        'If rst![Qty] = 0 Then
         '   rst![Price] = 0
        'Else
         '   rst![Price] = rst1![ListPrice] * rst1![Multiplier]
        'End If
        rst![Discount] = rst1![Discount]
        rst![Total] = ((rst1![Cost] * rst1![Multiplier]) * ((100 - rst1![Discount]) / 100)) * rst![Qty]
        'rst![Total] = rst1![SellPrice]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    Set rst1 = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        CHK1 = 3
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Price]
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Total]
        'If rst![Qty] = 0 Then
         '   rst![Price] = 0
        'Else
         '   rst![Price] = rst1![Price]
        'End If
        'rst![Discount] = rst1![Discount]
        'rst![Total] = rst1![Price] * rst![Qty] * ((100 - rst1![Discount]) / 100)
        
        If MID(rst![PartNumber], 1, 1) = "C" Then
            If MID(rst![PartNumber], 2, 1) = "0" Then
                CHK1 = 0
            End If
        End If
        rst![order] = CHK1
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
        
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        rst.AddNew
        If rst![order] = 0 Then
            rst.AddNew
            rst![order] = 2
            rst.Update
            GoTo ZZZ
        End If
        rst.MoveNext
    Wend
ZZZ:
    rst.Close
        
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function
'End:DFSD-167

Function B63GERM_base(PQID As Long)

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim CHK1 As Integer
   
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    Call Delete_Records("Word Export Template Temp2")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

    strSql = "SELECT * FROM [Parts Quotation 1] WHERE [PartQuotationID] =" & PQID
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        If rst![Qty] = 0 Then
            rst![Price] = 0
        Else
            rst![Price] = rst1![Listprice] * rst1![Multiplier]
        End If
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![SellPrice]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    Set rst1 = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        CHK1 = 3
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = rst1![PartNumber]
        rst![Qty] = rst1![Qty]
        If rst![Qty] = 0 Then
            rst![Price] = 0
        Else
            rst![Price] = rst1![Price]
        End If
        rst![Discount] = rst1![Discount]
        rst![Total] = rst1![Price] * rst![Qty] * ((100 - rst1![Discount]) / 100)
        
        If MID(rst![PartNumber], 1, 1) = "C" Then
            If MID(rst![PartNumber], 2, 1) = "0" Then
                CHK1 = 0
            End If
        End If
        rst![order] = CHK1
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
        
    Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        rst.AddNew
        If rst![order] = 0 Then
            rst.AddNew
            rst![order] = 2
            rst.Update
            GoTo ZZZ
        End If
        rst.MoveNext
    Wend
ZZZ:
    rst.Close
        
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function

Function B64(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long)  'German pricing total
    Dim Phrase As String
    Dim tbl As Object
    Dim TaxRate As Single
    Dim strFormat As String
    strFormat = GetDecimalFormat
        
    On Error GoTo B64ERR
        
    TaxRate = DLookup("[TaxRate]", "Office")

                
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=1, NumColumns:=6)
         
    'Old "Phrase=" were commented out and new ones added so the function could also work with quick part quotations
    'The old ones are left as a reference to what the values should be
         
    ''''''Total selling price of all parts on the quotation'''''''
    'Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![Psell]), "###" + strFormat) '"###0.00"
    If IsNull(DSum("[SellPrice]", "[Parts Quotation 1]", "[PartQuotationID] = " & PQID)) Then
        Phrase = "0.00"
    Else
        Phrase = Format(CDbl(DSum("[SellPrice]", "[Parts Quotation 1]", "[PartQuotationID] = " & PQID)), "###" + strFormat)
    End If
    tbl.Cell(2, 1).Range.Insertbefore Phrase
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    ''''''Discount for the part quotation'''''''''''''''''''''''''
    'Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![FormPQD2].Form![Text244]), "###0.00")
    If IsNull(DLookup("[Discount]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)) Then
        Phrase = "0.00"
    Else
        Phrase = Format(CDbl(DLookup("[Discount]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)), "###0.00")
    End If
    tbl.Cell(2, 2).Range.Insertbefore Phrase
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    ''''''Final Sell price of the quotation'''''''''''''''''''''''
    'Phrase = Format(CDbl(Forms![FormSales]![Child3].Form![FormPQD2].Form![Text245]), "###" + strFormat) '"###0.00"
    If IsNull(DLookup("[Price]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)) Then
        Phrase = "0.00"
    Else
        Phrase = Format(CDbl(DLookup("[Price]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)), "###0.00")
    End If
    tbl.Cell(2, 3).Range.Insertbefore Phrase
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    ''''''Amount of Tax = Total Sell Price * Tax Rate'''''''''''''
    'Phrase = Format(((CDbl(Forms![FormSales]![Child3].Form![FormPQD2].Form![Text245]) * TaxRate) / 100), strFormat)
    If IsNull(DLookup("[Price]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)) Then
        Phrase = "0.00"
    Else
        Phrase = Format(((CDbl(DLookup("[Price]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)) * TaxRate) / 100), "###0.00")
    End If
    tbl.Cell(2, 4).Range.Insertbefore Phrase
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    '''''''''''Tax Rate'''''''''''''''''''''''''''''''''''''''''''
    Phrase = Format(TaxRate, "###0.00")
    tbl.Cell(2, 5).Range.Insertbefore Phrase
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    ''''''''''Total Sell Price + Amount of Tax''''''''''''''''''''
    'Phrase = Format((CDbl(Forms![FormSales]![Child3].Form![FormPQD2].Form![Text245]) * TaxRate) / 100 + CDbl(Forms![FormSales]![Child3].Form![FormPQD2].Form![Text245]), "###" + strFormat) '"###0.00"
    If IsNull(DLookup("[Price]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)) Then
        Phrase = "0.00"
    Else
        Phrase = Format(((CDbl(DLookup("[Price]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)) * TaxRate) / 100) + CDbl(DLookup("[Price]", "[Parts Quotation]", "[PartQuotationID] = " & PQID)), "###0.00")
    End If
    tbl.Cell(2, 6).Range.Insertbefore Phrase
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    On Error Resume Next
    tbl.Columns(1).Width = 80
    tbl.Columns(2).Width = 80
    tbl.Columns(3).Width = 80
    tbl.Columns(4).Width = 80
    tbl.Columns(5).Width = 75
    tbl.Columns(6).Width = 80
        
    Set tbl = Nothing
    Exit Function

B64ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B65(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts quotation transport amount
    Dim Phrase As String
    Dim strFormat As String
    
            
    On Error GoTo B65ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(CDbl(DLookup("[TransportAmount]", "Parts Quotation", "[PartQuotationID]= " & PQID)), "###" + strFormat)
    myrange.Insertbefore (Phrase)
    Exit Function
B65ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B66(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'leadgeneration
    Dim Phrase As String
    Dim ID As Long
    
    On Error GoTo B66ERR
    
    ID = DLookup("[TechID]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    If IsNull(DLookup("[TechLastName]", "technicians", "[technicianID]= " & ID)) Then
        Phrase = "No technician selected"
    Else
        Phrase = DLookup("[TechLastName]", "technicians", "[technicianID]= " & ID)
    End If
    myrange.Insertbefore (Phrase)
    Exit Function
B66ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B67(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts table part number first
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B67ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B43_base(PQID)
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 2
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=5)
 '       Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
 '       tbl.cell(1, 1).Range.Insertbefore Phrase
 '       Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 158) 'Part Number
 '       tbl.cell(1, 2).Range.Insertbefore Phrase
 '       Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54) 'Qty
 '       tbl.cell(1, 3).Range.Insertbefore Phrase
 '       Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1014) 'Price
 '       tbl.cell(1, 4).Range.Insertbefore Phrase
 '       Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) 'Total
 '       tbl.cell(1, 5).Range.Insertbefore Phrase
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 70
        tbl.Columns(2).Width = 250
        tbl.Columns(3).Width = 35
        tbl.Columns(4).Width = 65
        tbl.Columns(5).Width = 65
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B67ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B68(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts quotation discount
    Dim Phrase As String
    Dim strFormat As String
    
            
    On Error GoTo B68ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(CDbl(DLookup("[Discount]", "Parts Quotation", "[PartQuotationID]= " & PQID)), "###" + strFormat)
    myrange.Insertbefore (Phrase)
    Exit Function
B68ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B69(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'user name email
    Dim Phrase As String
    Dim SALESID As Long
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
            
    Set dbs = CurrentDb
    
    On Error GoTo B69ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Set rst = dbs.OpenRecordset("SalesEngineers", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        SALESID = DLookup("[SEID]", "Parts Quotation", "[PartQuotationID]= " & PQID)
    Else
        Phrase = "no sales engineer"
        GoTo BYPASS_NAME
    End If
    rst.Close
    
    strSql = "SELECT * FROM [SalesEngineers] WHERE [SalesEngineerID] =" & SALESID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        Phrase = rst![email]
    Else
        Phrase = "no sales engineer email"
    End If
    
BYPASS_NAME:
    myrange.Insertbefore (Phrase)
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B69ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B70(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'service selection creation date
    Dim Phrase As String
            
    On Error GoTo B70ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(DLookup("[RecordedDate]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID]), "Short Date")
    myrange.Insertbefore (Phrase)
    Exit Function
B70ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B71(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts quotation creation date
    Dim Phrase As String
            
    On Error GoTo B71ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(DLookup("[RecordedDate]", "Parts Quotation", "[PartQuotationID]= " & PQID), "Short Date")
    myrange.Insertbefore (Phrase)
    Exit Function
B71ERR:
Call createErrorMessage(Bookmarkname)
End Function
Function B72(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'service selection notes
    Dim Phrase As String
    Dim strSql As String
    Dim dbs As Database
    Dim rst As Recordset
            
    Set dbs = CurrentDb
           
    On Error GoTo B72ERR
    
    strSql = "SELECT * FROM [Service Selection Notes] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
        If Not IsNull(rst![TT]) Then
            Phrase = rst![TT]
            myrange.Insertbefore (Phrase)
        End If
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B72ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B73(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts table(RUSSIA)
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B73ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B73_base(PQID)
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 2
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=7)
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
        tbl.Cell(1, 1).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 158) 'Part Number
        tbl.Cell(1, 2).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54) 'Qty
        tbl.Cell(1, 3).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1014) 'Price
        tbl.Cell(1, 4).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) 'Total
        tbl.Cell(1, 5).Range.Insertbefore Phrase
        Phrase = "VAT"
        tbl.Cell(1, 6).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 56) & "+VAT" 'Total + VAT
        tbl.Cell(1, 7).Range.Insertbefore Phrase
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 6).Range.Insertbefore Format(rst![Total] * 0.18, strFormat)
            End If
             If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 7).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 7).Range.Insertbefore Format(rst![Total] * 1.18, strFormat)
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 180
        tbl.Columns(2).Width = 60
        tbl.Columns(3).Width = 30
        tbl.Columns(4).Width = 50
        tbl.Columns(5).Width = 50
        tbl.Columns(6).Width = 50
        tbl.Columns(7).Width = 60
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B73ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B73_base(PQID As Long)

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim pricingmodel As Integer
    
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    pricingmodel = getPricingModel
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

'parts
    strSql = "SELECT * FROM [Parts Quotation 1] WHERE [PartQuotationID] =" & PQID
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        If pricingmodel = 1 Then
            rst.AddNew
            rst![Description] = rst1![Description]
            rst![PartNumber] = rst1![PartNumber]
            rst![Qty] = rst1![Qty]
            rst![Price] = rst1![Cost] * rst1![Multiplier]
            rst![Discount] = rst1![Discount]
            rst![Total] = (rst1![Cost] * rst1![Multiplier] * (1 - (rst1![Discount] / 100))) * rst1![Qty]
            rst.Update
            rst1.MoveNext
        Else
            rst.AddNew
            rst![Description] = rst1![Description]
            rst![PartNumber] = rst1![PartNumber]
            rst![Qty] = rst1![Qty]
            rst![Price] = rst1![Listprice] * rst1![Multiplier]
            rst![Discount] = rst1![Discount]
            rst![Total] = (rst1![Listprice] * rst1![Multiplier] * (1 - (rst1![Discount] / 100))) * rst1![Qty]
            rst.Update
            rst1.MoveNext
        
        End If
    Wend
    rst1.Close
        
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function

Function B74(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'miscellanous amount
    Dim Phrase As String
    Dim strFormat As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim Pr As Single
    
    Set dbs = CurrentDb
         
    On Error GoTo B74ERR
    Call B74_base
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        Pr = Pr + rst![Price] * rst![Qty]
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = Format(Pr, "###" + strFormat)
    myrange.Insertbefore (Phrase)
    Exit Function
B74ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B74_base()

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    
'4 Miscellanous
    strSql = "SELECT * FROM [Service Selection B] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = "****"
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Sell]
        rst![Discount] = 0
        rst![Total] = rst1![Sell] * rst1![Qty]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function

Function B75(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'parts table TIS
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    Dim i As Integer
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B75ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B21_base_TIS
     Set rst = dbs.OpenRecordset("Word Export Template Temp2", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
        i = 1
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC, NumColumns:=8)
        
        While Not rst.EOF
            tbl.Cell(IRow, 1).Range.Insertbefore i
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![order]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![order], strFormat)
            End If
            If IsNull(rst![Discount]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 6).Range.Insertbefore Format(rst![Discount], strFormat)
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 7).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 7).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 8).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 8).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            rst.MoveNext
            i = i + 1
            IRow = IRow + 1
        Wend
        
        On Error Resume Next
        tbl.Columns(1).Width = 15
        tbl.Columns(2).Width = 70
        tbl.Columns(3).Width = 190
        tbl.Columns(4).Width = 30
        tbl.Columns(5).Width = 45
        tbl.Columns(6).Width = 40
        tbl.Columns(7).Width = 45
        tbl.Columns(8).Width = 50
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B75ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B76(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts table TIS
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    Dim i As Integer
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B76ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B43_base_TIS(PQID)
    Set rst = dbs.OpenRecordset("Word EXport Template Temp2", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
        i = 1
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC, NumColumns:=8)
        
        While Not rst.EOF
            tbl.Cell(IRow, 1).Range.Insertbefore i
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![order]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![order], strFormat)
            End If
            If IsNull(rst![Discount]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 6).Range.Insertbefore Format(rst![Discount], strFormat)
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 7).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 7).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 8).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 8).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            rst.MoveNext
            i = i + 1
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 15
        tbl.Columns(2).Width = 70
        tbl.Columns(3).Width = 190
        tbl.Columns(4).Width = 30
        tbl.Columns(5).Width = 45
        tbl.Columns(6).Width = 40
        tbl.Columns(7).Width = 45
        tbl.Columns(8).Width = 50
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B76ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B77(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'Miscellanous details

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Set dbs = CurrentDb
    Dim Phrase As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    Dim i As Integer
    
    On Error GoTo B77ERR
    strFormat = GetDecimalFormat
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call Delete_Records("Word Export Template Temp1")
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    
'4 Miscellanous
    strSql = "SELECT * FROM [Service Selection B] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        rst.AddNew
        rst![Description] = rst1![Description]
        rst![PartNumber] = "****"
        rst![Qty] = rst1![Qty]
        rst![Price] = rst1![Sell]
        rst![Discount] = 0
        rst![Total] = rst1![Sell] * rst1![Qty]
        rst.Update
        rst1.MoveNext
    Wend
    rst1.Close
    rst.Close
       
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
        i = 1
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC, NumColumns:=2)
        
        While Not rst.EOF
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            
            rst.MoveNext
            i = i + 1
            IRow = IRow + 1
        Wend
        
        On Error Resume Next
        tbl.Columns(1).Width = 300
        tbl.Columns(2).Width = 50
        
        Set tbl = Nothing
    End If
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    Exit Function
B77ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B78(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts table Iberia
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strFormat As String
    Dim i As Integer
    
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    On Error GoTo B78ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Call B78_base(PQID)
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 1
        i = 1
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC, NumColumns:=7)
        
        While Not rst.EOF
            If IsNull(rst![PartNumber]) Then
                tbl.Cell(IRow, 1).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore rst![Description]
            End If
            If IsNull(rst![Description]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 2).Range.Insertbefore rst![PartNumber]
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 4).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 4).Range.Insertbefore rst![Qty]
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore Format(rst![Price] / (1 - (rst![Discount]) / 100), strFormat)
            End If
            If IsNull(rst![Discount]) Then
                tbl.Cell(IRow, 5).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 5).Range.Insertbefore Format(rst![Discount], strFormat)
            End If
            If IsNull(rst![Price]) Then
                tbl.Cell(IRow, 6).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 6).Range.Insertbefore Format(rst![Price], strFormat)
            End If
            If IsNull(rst![Total]) Then
                tbl.Cell(IRow, 7).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 7).Range.Insertbefore Format(rst![Total], strFormat)
            End If
            rst.MoveNext
            i = i + 1
            IRow = IRow + 1
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 190
        tbl.Columns(2).Width = 70
        tbl.Columns(4).Width = 30
        tbl.Columns(3).Width = 45
        tbl.Columns(5).Width = 40
        tbl.Columns(6).Width = 45
        tbl.Columns(7).Width = 50
        
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B78ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B78_base(PQID As Long)

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim pricingmodel As Integer
    
    
    Set dbs = CurrentDb
    
    Call Delete_Records("Word Export Template Temp1")
    pricingmodel = getPricingModel
    
    Set rst = dbs.OpenRecordset("Word Export Template Temp1", dbOpenDynaset, dbSeeChanges)

'parts
    strSql = "SELECT * FROM [Parts Quotation 1] WHERE [PartQuotationID] =" & PQID
    Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst1.EOF
        If pricingmodel = 1 Then
            rst.AddNew
            rst![Description] = rst1![Description]
            rst![PartNumber] = rst1![PartNumber]
            rst![Qty] = rst1![Qty]
            rst![Price] = rst1![Cost] * rst1![Multiplier]
            rst![Discount] = (1 - rst1![Multiplier]) * 100
            rst![Total] = (rst1![Cost] * rst1![Multiplier] * (1 - (rst1![Discount] / 100))) * rst1![Qty]
            rst.Update
            rst1.MoveNext
        Else
            rst.AddNew
            rst![Description] = rst1![Description]
            rst![PartNumber] = rst1![PartNumber]
            rst![Qty] = rst1![Qty]
            rst![Price] = rst1![Listprice] * rst1![Multiplier]
            rst![Discount] = (1 - rst1![Multiplier]) * 100
            rst![Total] = (rst1![Listprice] * rst1![Multiplier] * (1 - (rst1![Discount] / 100))) * rst1![Qty]
            rst.Update
            rst1.MoveNext
        
        End If
    Wend
    rst1.Close
        
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    
End Function
Function B79(myrange As Object, objWordDoc As Object, Bookmarkname As String, PQID As Long) 'parts notes
    Dim Phrase As String
            
    On Error GoTo B79ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    Phrase = DLookup("[TT]", "Parts Quotation Notes", "[PartQuotationID]= " & PQID)
    myrange.Insertbefore (Phrase)
    Exit Function
B79ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B80(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'SCW

    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strFormat As String
    Dim Sell As Single
    Dim i As Integer
    
    On Error GoTo B80ERR
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    'SCW
    Sell = 0
    strSql = "SELECT [Sell] FROM [Service Selection B] Where [Service Selection]= " & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        Sell = Sell + rst![Sell]
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    
    'Undocumented business rule
    Sell = Sell * 0.02
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (Format(Sell, strFormat))
    
    Exit Function
B80ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B81(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'Direct

    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strFormat As String
    Dim Sell As Single
    Dim miscSell As Single
    Dim Discount As Single
    Dim i As Integer
    
    Set dbs = CurrentDb
    On Error GoTo B81ERR
    strFormat = GetDecimalFormat
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Sell = 0
    miscSell = 0
    Discount = 0
    
    'Direct
    strSql = "SELECT [Dollar] FROM [Service Selection 4] Where [Service Selection]= " & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        Sell = Sell + rst![Dollar]
        rst.MoveNext
    Wend
    rst.Close
    
    'Miscellaneous
    strSql = "SELECT [Sell] FROM [Service Selection B] Where [Service Selection]= " & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        miscSell = miscSell + rst![Sell]
        rst.MoveNext
    Wend
    rst.Close
    
    'Discount to get calculated sell
    strSql = "SELECT [Discount] FROM [Service Selection] Where [Service Selection]= " & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        If Not IsNull(rst![Discount]) Then
            Discount = rst![Discount]
        End If
        rst.MoveNext
    Wend
    rst.Close
    
    Set rst = Nothing
    Set dbs = Nothing
    
    'Undocumented business rule
    Sell = ((Sell / (1 - (Discount / 100))) - miscSell) * 0.05
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (Format(Sell, strFormat))
    
    Exit Function
B81ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B82(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'maintenance intervention timetable
    Dim Phrase As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim tbl As Object
    Dim IRow As Integer
    Dim RC As Integer
    Dim strQty As String
    Dim visitDetails As String
    
    Set dbs = CurrentDb
    
    On Error GoTo B82ERR
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    
    Delete_Records "Word Export Bookmarks Maint Int Timetable Temp"
    DoCmd.SetWarnings False
    DoCmd.OpenQuery "Z-SS2 Append"
    DoCmd.SetWarnings True
    Set rst = dbs.OpenRecordset("Word Export Bookmarks Maint Int Timetable Temp", dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        RC = rst.RecordCount
        IRow = 2
                 
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC + 1, NumColumns:=3)
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 40) 'Description
        tbl.Cell(1, 1).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 54) 'Qty
        tbl.Cell(1, 2).Range.Insertbefore Phrase
        Phrase = Create_Msg([Forms]![MENU-NEW]![Text118], 1153) 'Visit Date
        tbl.Cell(1, 3).Range.Insertbefore Phrase
        
        While Not rst.EOF
            visitDetails = ""
            If rst![DFS] = 0 And Nz(rst![InterventionID], 0) = 0 Then
                tbl.Cell(IRow, 1).Range.Insertbefore Nz(DLookup("[TT]", "Service Interventions Temp", "[Service Intervention]= '" & rst![Service Intervention] & "'"), "")
            Else
                tbl.Cell(IRow, 1).Range.Insertbefore Nz(rst![Tk1], "")
            End If
            If IsNull(rst![Qty]) Then
                tbl.Cell(IRow, 2).Range.Insertbefore " "
            Else
                'without this line you cannot insert the qty without it silently failing
                strQty = rst![Qty]
                tbl.Cell(IRow, 2).Range.Insertbefore strQty
            End If
            If rst![Jan] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 70)
            End If
            If rst![Feb] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 71)
            End If
            If rst![Mar] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 72)
            End If
            If rst![Apr] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 73)
            End If
            If rst![May] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 74)
            End If
            If rst![Jun] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 75)
            End If
            If rst![Jul] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 76)
            End If
            If rst![Aug] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 77)
            End If
            If rst![Sep] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 78)
            End If
            If rst![Oct] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 79)
            End If
            If rst![Nov] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 80)
            End If
            If rst![Dec] = -1 Then
                If visitDetails <> "" Then
                    visitDetails = visitDetails + ", "
                End If
                visitDetails = visitDetails + Create_Msg([Forms]![MENU-NEW]![Text118], 81)
            End If
            If visitDetails = "" Then
                tbl.Cell(IRow, 3).Range.Insertbefore " "
            Else
                tbl.Cell(IRow, 3).Range.Insertbefore visitDetails
            End If
            rst.MoveNext
            IRow = IRow + 1
        Wend
        
        On Error Resume Next
        tbl.Columns(1).Width = 240
        tbl.Columns(2).Width = 40
        tbl.Columns(3).Width = 170
            
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    
    Exit Function
B82ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B83(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'maintenance intervention billing
    Dim dbs As Database
    Dim rst As Recordset
    Dim tbl As Object
    Dim IRow As Long
    Dim RC As Integer
    Dim strSql As String
    Dim strFormat As String
    Dim textRange As Object
    
    Set dbs = CurrentDb
    
    strFormat = GetDecimalFormat
    
    On Error GoTo B83ERR
    strSql = "SELECT * FROM [Service Selection Invoice] WHERE [Service Selection]=" & [Forms]![FormSales]![SSID] & " AND [ID3]<>0 AND [ID2]<>0"
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        If rst.RecordCount = 1 Then
            RC = 14
        Else
            RC = (rst.RecordCount * 15) - 1
        End If
        IRow = 1
        Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
        Set tbl = objWordDoc.Tables.Add(Range:=myrange, NumRows:=RC, NumColumns:=2)
 
        While Not rst.EOF
            'Add a line break after each group for easy readability
            If IRow <> 1 Then
                tbl.Cell(IRow, 1).Range.Insertafter " "
                IRow = IRow + 1
            End If
            tbl.Cell(IRow, 1).Range.Insertafter Nz(rst![Service Intervention Description], rst![Service Intervention])
            IRow = IRow + 1
            tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 1219) 'Month
            tbl.Cell(IRow, 2).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 161)  'Amount
            IRow = IRow + 1
            If Not IsNull(rst![JanD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 70)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![JanD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![FebD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 71)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![FebD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![MarD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 72)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![MarD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![AprD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 73)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![AprD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![MayD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 74)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![MayD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![JunD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 75)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![JunD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![JulD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 76)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![JulD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![AugD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 77)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![AugD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![SepD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 78)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![SepD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![OctD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 79)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![OctD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![NovD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 80)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![NovD], strFormat))
                IRow = IRow + 1
            End If
            If Not IsNull(rst![DecD]) Then
                tbl.Cell(IRow, 1).Range.Insertafter Create_Msg([Forms]![MENU-NEW]![Text118], 81)
                tbl.Cell(IRow, 2).Range.Insertafter (Format(rst![DecD], strFormat))
                IRow = IRow + 1
            End If
            rst.MoveNext
        Wend
        On Error Resume Next
        tbl.Columns(1).Width = 75
        tbl.Columns(2).Width = 125
        Set tbl = Nothing
    End If
    
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
B83ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B84(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'Price Less Parts Sell
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strFormat As String
    Dim Price As Single
    Dim PartsSell As Single
    Dim PriceMinusPartsSell As Single
    
    On Error GoTo B84ERR
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    'Get Price
    Price = 0
    strSql = "SELECT [Dollar] FROM [Service Selection 4] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        Price = Price + rst![Dollar]
        rst.MoveNext
    Wend
    rst.Close
    
    'Get Parts Sell
    PartsSell = 0
    strSql = "SELECT [Price] FROM [Service Selection 3M Temp] Where [Service Selection]= " & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        PartsSell = PartsSell + rst![Price]
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    
    PriceMinusPartsSell = Price - PartsSell
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (Format(PriceMinusPartsSell, strFormat))
    
    Exit Function
B84ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B85(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'Distance Sell Plus Miscellaneous Sell
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strFormat As String
    Dim DistanceSell As Single
    Dim miscSell As Single
    Dim DistanceSellPlusMiscSell As Single
    
    On Error GoTo B85ERR
    Set dbs = CurrentDb
    strFormat = GetDecimalFormat
    
    'Get Distance Sell
    DistanceSell = 0
    strSql = "SELECT [kmPrice] FROM [Service Selection Value Temp1]"
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        DistanceSell = DistanceSell + rst![kmPrice]
        rst.MoveNext
    Wend
    rst.Close
    
    'Get Misc Sell
    miscSell = 0
    strSql = "SELECT [Sell], [Qty] FROM [Service Selection B] Where [Service Selection]= " & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        miscSell = miscSell + (rst![Sell] * rst![Qty])
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    
    DistanceSellPlusMiscSell = DistanceSell + miscSell
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (Format(DistanceSellPlusMiscSell, strFormat))
    
    Exit Function
B85ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B86(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID) 'Customer Payment Terms
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim PayTerm As String
    
    On Error GoTo B86ERR
    Set dbs = CurrentDb
    
    'Get Customer Payment Terms
    strSql = "SELECT [PayTerm] FROM [BD-Customers] WHERE [CustomerID]=" & CID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        PayTerm = Nz(rst![PayTerm], "")
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore PayTerm
    
    Exit Function
B86ERR:
Call createErrorMessage(Bookmarkname)
End Function


Function B87(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'FinSysID
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strFinSysID As String
    
    On Error GoTo B87ERR
    Set dbs = CurrentDb
    
    strSql = "SELECT [Service Order Number] FROM [Service Selection] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        strFinSysID = Nz(rst![Service Order Number], "")
        rst.MoveNext
    Wend
    rst.Close
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (strFinSysID)
    
    Exit Function
B87ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B88(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'Purchase #
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strPurchaseNumber As String
    
    On Error GoTo B88ERR
    Set dbs = CurrentDb
    
    strSql = "SELECT [PurchaseNumber] FROM [Service Selection] WHERE [Service Selection] =" & Forms![FormSales]![SSID]
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        strPurchaseNumber = Nz(rst![PurchaseNumber], "")
        rst.MoveNext
    Wend
    rst.Close
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (strPurchaseNumber)
    
    Exit Function
B88ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B89(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'CustomerState
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strState As String
    Dim CID As Long
            
    On Error GoTo B89ERR
    CID = Forms![FormSSV7]![Combo43]
    
    Set dbs = CurrentDb
    
    strSql = "SELECT [State] FROM [BD-Customers] WHERE [CustomerID] =" & CID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        strState = Nz(rst![State], "")
        rst.MoveNext
    Wend
    rst.Close
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (strState)
    
    Exit Function
B89ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B90(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'BuildingState
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strState As String
    Dim BID As Long
            
    On Error GoTo B90ERR
    BID = DLookup("[BuildingID]", "Service Selection", "[Service Selection]= " & Forms![FormSales]![SSID])
    
    Set dbs = CurrentDb
    
    strSql = "SELECT [State] FROM [BD-Buildings] WHERE [BuildingID] =" & BID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        strState = Nz(rst![State], "")
        rst.MoveNext
    Wend
    rst.Close
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (strState)
    
    Exit Function
B90ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B91(myrange As Object, objWordDoc As Object, Bookmarkname As String, CID As Long) 'PQ CustomerState
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strState As String
            
    On Error GoTo B91ERR
    
    Set dbs = CurrentDb
    
    strSql = "SELECT [State] FROM [BD-Customers] WHERE [CustomerID] =" & CID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        strState = Nz(rst![State], "")
        rst.MoveNext
    Wend
    rst.Close
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (strState)
    
    Exit Function
B91ERR:
Call createErrorMessage(Bookmarkname)
End Function

Function B92(myrange As Object, objWordDoc As Object, Bookmarkname As String) 'Sales Engineer Mobile Number
    Dim strSalesEngineerMobileNumber As String
    Dim SEID As Long
    
    On Error GoTo B92ERR
    
    SEID = Nz(DLookup("[SEID]", "[Service Selection]", "[Service Selection] =" & Forms![FormSales]![SSID]), 0)
    strSalesEngineerMobileNumber = Nz(DLookup("[MobileNumber]", "[SalesEngineers]", "[SalesEngineerID]=" & SEID), "")
    
    Set myrange = objWordDoc.Bookmarks(Bookmarkname).Range
    myrange.Insertbefore (strSalesEngineerMobileNumber)
    
    Exit Function
B92ERR:
Call createErrorMessage(Bookmarkname)
End Function


Private Sub createErrorMessage(strBookmarkName As String)
    If strErrMessage <> "" Then
        strErrMessage = strErrMessage & ", " & strBookmarkName
    Else
        strErrMessage = Create_Msg([Forms]![MENU-NEW]![Text118], 4758) & " " & strBookmarkName
    End If
End Sub
