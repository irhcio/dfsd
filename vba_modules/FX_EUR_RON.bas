Option Compare Database
Dim SS(14) As String
Dim KK(14) As Integer

Public Sub Convert_EUR_to_RON(Optional SSID As Long = -1)
    
    Dim pricingmodel As Integer
    Dim FN As Long
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim strSqlSelections As String
    Dim OEX As Single
    Dim EX As Single
    Dim qdfZSS3MAddForValue As queryDef
    Dim qdfZSS3MAddForValueLP As queryDef
    Dim qdfZInterventionValue As queryDef
    Dim qdfZSSV As queryDef
    Dim strMode As String
    
        
        
    EX = 0.0378
    
    Set dbs = CurrentDb
    
    If SSID = -1 Then
        strSqlSelections = "SELECT [Service Selection],[BuildingID],[CustomerID] FROM [Service Selection] WHERE FEX = 1 " _
                            & " and [active] in (1,2,3,13)"
        strMode = "Batch"
    Else
        strSqlSelections = "SELECT [Service Selection],[BuildingID],[CustomerID] FROM [Service Selection] WHERE FEX = 1 " _
                            & " and [active] in (1,2,3,13) and [Service Selection] = " & SSID
        strMode = "Single"
    End If
    
        ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "EUR-RON Conversion", "Start Program " & strMode, Null, LogIDType.ServiceSelection
    
    Set rst = dbs.OpenRecordset(strSqlSelections, dbOpenDynaset, dbSeeChanges)
    
    OEX = 1 'Original FX Rate compared to EUR
    While Not rst.EOF
        ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "EUR-RON Conversion", "Start EUR-RON conversion", rst![Service Selection], LogIDType.ServiceSelection
        Call FOREX(rst![Service Selection])
        FN = 162000000 'Forms![MENU-NEW]![FirstNumber]
        'Determine the pricing model selected for the office
        pricingmodel = getPricingModel
        Call Calc_Values_EUR_RON(rst![Service Selection], -1)
        Call steps_EUR_RON(rst![Service Selection], rst![BuildingID], rst![CustomerID]) 'Got This Far 20160112
        DoCmd.SetWarnings False
        DoCmd.OpenQuery "FS_Delete1"
        DoCmd.OpenQuery "FS_Delete2"
        DoCmd.OpenQuery "FS_Delete3"
        DoCmd.OpenQuery "FS_Delete4"
        Call OtherValues(rst![Service Selection])
        DoCmd.SetWarnings False
            
        If FN = 24000000 Or FN = 46000000 Or FN = 48000000 Or FN = 50000000 Or FN = 52000000 Or FN = 54000000 Or FN = 56000000 Then
        
            Set qdfZSS3MAddForValueLP = dbs.QueryDefs("Z-SS3M Add For Value LP")
            qdfZSS3MAddForValueLP![[Forms]![FormSales]![SSID]] = SSID
            qdfZSS3MAddForValueLP.Execute
            Set qdfZSS3MAddForValue = Nothing
        Else
           'Run the appropriate query depending on pricingModel
            '1-Cost, 2-List Price
            If pricingmodel = 2 Then
                Set qdfZSS3MAddForValueLP = dbs.QueryDefs("Z-SS3M Add For Value LP")
                qdfZSS3MAddForValueLP![[Forms]![FormSales]![SSID]] = SSID
                qdfZSS3MAddForValueLP.Execute
                Set qdfZSS3MAddForValue = Nothing

            Else
                Set qdfZSS3MAddForValue = dbs.QueryDefs("Z-SS3M Add For Value")
                qdfZSS3MAddForValue![[Forms]![FormSales]![SSID]] = SSID
                qdfZSS3MAddForValue.Execute
                Set qdfZSS3MAddForValue = Nothing
            End If
        End If
        'DoCmd.OpenQuery "Z-Intervention Value"
        Set qdfZInterventionValue = dbs.QueryDefs("Z-Intervention Value")
        qdfZInterventionValue![[Forms]![FormSales]![SSID]] = SSID
        qdfZInterventionValue.Execute
        Set qdfZInterventionValue = Nothing
        
        'DoCmd.OpenQuery "Z-SSV"
        Set qdfZSSV = dbs.QueryDefs("Z-SSV")
        qdfZSSV![[Forms]![FormSales]![SSID]] = SSID
        qdfZSSV.Execute
        Set qdfZSSV = Nothing
        
        DoCmd.SetWarnings True
        ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "EUR-RON Conversion", "End EUR-RON conversion", rst![Service Selection], LogIDType.ServiceSelection
        rst.MoveNext
    Wend
    rst.Close
    Set dbs = Nothing
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "EUR-RON Conversion", "End Program " & strMode, Null, LogIDType.ServiceSelection

   
End Sub

Function FOREX(SSID As String)
    
    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim EX As Single
    Dim CurrencyCode As String
        
    
    EX = 0.0378 'Conversion rate to apply, Convert from CZK to EUR
    CurrencyCode = "EUR" 'Currency to convert to
    
    Set dbs = CurrentDb
        
    strSql = "SELECT [Cost], [Listprice] FROM [Service Selection 3M] WHERE [Service Selection] = " & SSID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                
    While Not rst.EOF
        rst.Edit
        rst![Cost] = rst![Cost] * EX
        rst![Listprice] = rst![Listprice] * EX
        rst.Update
        
        rst.MoveNext
    Wend
    rst.Close
    
    strSql = "SELECT * FROM [Service Selection 4] WHERE [Service Selection] = " & SSID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                
    While Not rst.EOF
        rst.Edit
        rst![Dollar] = rst![Dollar] * EX
        rst![LabRate] = rst![LabRate] * EX
        rst![TravelRate] = rst![TravelRate] * EX
        rst![OtherRate] = rst![OtherRate] * EX
        rst![kmRate] = rst![kmRate] * EX
        rst![CDollar] = rst![CDollar] * EX
        rst![CLabRate] = rst![CLabRate] * EX
        rst![CTravelRate] = rst![CTravelRate] * EX
        rst![COtherRate] = rst![COtherRate] * EX
        rst![CkmRate] = rst![CkmRate] * EX
        rst.Update
        
        rst.MoveNext
    Wend
    rst.Close
    
    strSql = "SELECT [Cost], [Sell] FROM [Service Selection B] WHERE [Service Selection] = " & SSID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                
    While Not rst.EOF
        rst.Edit
        rst![Cost] = rst![Cost] * EX
        rst![Sell] = rst![Sell] * EX
        rst.Update
            
        rst.MoveNext
    Wend
    rst.Close
    
    strSql = "SELECT [FEX], [CurrencyCode] FROM [Service Selection] WHERE [Service Selection] = " & SSID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                
    While Not rst.EOF
        rst.Edit
        rst![FEX] = EX
        rst![CurrencyCode] = CurrencyCode
        rst.Update
            
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
End Function

Function Calc_Values_EUR_RON(SSID As String, CHECK As Integer)

    Dim db As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim strSql As String
    Dim LRate As Single
    Dim TRate As Single
    Dim ORate As Single
    Dim kmRate As Single
    Dim Dollar As Single
    Dim CLRate As Single
    Dim CTRate As Single
    Dim CORate As Single
    Dim CkmRate As Single
    Dim CDollar As Single
    Dim DollarM As Single
    Dim QQ As Single
    Dim TRA As Single
    Dim TOA As Single
    Dim TKA As Single
    Dim PCost As Single
    Dim cnt As Integer
    Dim Pcnt As Integer
    Dim CC As Integer
    Dim SPF As Single
    Dim SPF_Check As Integer
    Dim qdfCalcPadQuery1B As queryDef
    
    Set db = CurrentDb
    
    Set rst = db.OpenRecordset("DefaultRates3", dbOpenDynaset, dbSeeChanges)
    If Not rst.EOF Then
        SPF = rst.Fields(Month(Date) + 1)
    Else
        SPF = 1
    End If
    rst.Close
    
    'added to speed up processing service selections with lots of equipment
    Delete_Records ("Calculation Pad Query1B Temp")
    DoCmd.SetWarnings False
    Set qdfCalcPadQuery1B = db.QueryDefs("Calculation Pad Query1B Write")
    qdfCalcPadQuery1B![SSID] = SSID
    qdfCalcPadQuery1B.Execute
    Set qdfCalcPadQuery1B = Nothing
    DoCmd.SetWarnings True
    
    If CHECK = -1 Then
        strSql = "SELECT * FROM [Service Selection 4] WHERE [Service Selection] = " & SSID & " AND [Dollar]=-1 AND [CDollar]=-1"
    Else
        strSql = "SELECT * FROM [Service Selection 4] WHERE [Service Selection] = " & SSID
    End If
    Set rst = db.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
    End If
    cnt = rst.RecordCount
    CC = 0
    
    While Not rst.EOF

        CC = CC + 1
        If (rst![Dollar] = -1 And rst![CDollar] = -1) Or CHECK = 1 Then
            
            If IsNull((DLookup("[Category]", "Service Selection 2", "[ID2]=" & rst![ID2] & " And [ID3] =" & rst![ID3]))) Then
                SPF_Check = 2
            Else
                SPF_Check = (DLookup("[Category]", "Service Selection 2", "[ID2]=" & rst![ID2] & " And [ID3] =" & rst![ID3]))
            End If
            
            If SPF_Check <> 2 Then
                rst.Edit
                rst![CLabRate] = rst![CLabRate] * SPF
                rst![CTravelRate] = rst![CTravelRate] * SPF
                rst![COtherRate] = rst![COtherRate] * SPF
                
                rst![LabRate] = rst![LabRate] * SPF
                rst![TravelRate] = rst![TravelRate] * SPF
                rst![OtherRate] = rst![OtherRate] * SPF
                rst.Update
            End If
            
            
            Dollar = 0
            DollarM = 0
            QQ = 0
            strSql = "SELECT [Q], [TR], [TO], [TK], [Time] FROM [Calculation Pad Query1B Temp] WHERE [Service Selection] = " & SSID & " And [ID3]= " & rst![ID3] & " And [EquipmentID]= " & rst![EquipmentID]
            Set rst1 = db.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                QQ = rst1![q]
                TRA = 0
                TOA = 0
                TKA = 0
                LRate = rst![LabRate]
                TRate = rst![TravelRate]
                ORate = rst![OtherRate]
                kmRate = Nz(rst![kmRate], 0)
                CLRate = rst![CLabRate]
                CTRate = rst![CTravelRate]
                CORate = rst![COtherRate]
                CkmRate = rst![CkmRate]
                
                If Not IsNull(rst1![tr]) Then TRA = rst1![tr]
                If Not IsNull(rst1![TO]) Then TOA = rst1![TO]
                If Not IsNull(rst1![TK]) Then TKA = rst1![TK]
                
                Dollar = QQ * rst1![Time] * LRate + TRA * TRate + TOA * ORate + TKA * kmRate
                CDollar = QQ * rst1![Time] * CLRate + TRA * CTRate + TOA * CORate + TKA * CkmRate
            End If
            rst1.Close
            
            strSql = "SELECT [PartCost], [Qty] FROM [Calculation Pad Query2A] WHERE [Service Selection] = " & SSID & " And [ID3]= " & rst![ID3] & " And [EquipmentID]= " & rst![EquipmentID]
            Set rst1 = db.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            While Not rst1.EOF
                If IsNull(rst1![PartCost]) Then
                    PCost = 0
                Else
                    PCost = rst1![PartCost]
                End If
                DollarM = DollarM + QQ * rst1![Qty] * PCost
                rst1.MoveNext
            Wend
            rst1.Close
            
        End If
        rst.MoveNext

        
    Wend
        
    rst.Close
    
        
    strSql = "SELECT * FROM [Service Selection A] WHERE [Service Selection] = " & SSID
    Set rst = db.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    
    If rst.RecordCount = 0 Then
        rst.AddNew
        rst![Service Selection] = SSID
        rst.Update
    End If
    rst.Close
    CloseProgress
    Set rst = Nothing
    Set rst1 = Nothing
    Set db = Nothing

End Function

Function steps_EUR_RON(SSID As Long, BuildingID As String, CustomerID As String)

    Dim dbs As Database
    Dim rst As Recordset
    Dim rst1 As Recordset
    Dim k As Integer
    Dim J As Integer
    Dim CHK_INT As Integer
    Dim CC(14) As Integer
    Dim RR(14) As Integer
    Dim CID As Long
    Dim BID As Long
    Dim EID As Long
    Dim SID As Long
    
    
    Set dbs = CurrentDb

    If IsNumeric(CustomerID) Then
        CID = CustomerID
    Else
        CID = -1
    End If
    BID = BuildingID
    EID = 0
    SID = SSID

    Delete_Records ("SS_CHK")
    Set rst = dbs.OpenRecordset("SS_CHK", dbOpenDynaset, dbSeeChanges)
    Set rst1 = dbs.OpenRecordset("SS_CHK1", dbOpenDynaset, dbSeeChanges)

    For k = 1 To 14
        KK(k) = 0
        CC(k) = 0
        RR(k) = 0
    Next k

    k = 14
    J = 4
    Call Get_SS_EUR_RON
    CHK_INT = 0

    If KK(4) = 0 And KK(7) = 0 Then
        KK(4) = 1
        KK(5) = 1
        KK(7) = 1
    End If

'MsgBox ("BEFORE KK1=" & KK(1) & "  KK2=" & KK(2) & "  KK3=" & KK(3) & "  KK4=" & KK(4) & "  KK5=" & KK(5) & "  KK6=" & KK(6) & "  KK7=" & KK(7) & "  KK8=" & KK(8) & "  KK9=" & KK(9) & "  KK10=" & KK(10) & "  KK11=" & KK(11) & "  KK12=" & KK(12) & "  KK13=" & KK(13) & "  KK14=" & KK(14))
    
    For k = 1 To 14
        rst.AddNew
        rst![ID] = k
        rst![NN] = SS(k)  'item Name
        rst![CC] = get_Signe_EUR_RON(SSID, BID, k)  'done or not 'MMG Here 20170112
        rst![RR] = KK(k)  'required or not
        rst.Update
        
    Next k
    
'fixing for no DFS and NDFS
    If KK(4) = 0 And KK(7) = 0 Then
        rst.MoveFirst
        rst.Move (4)
        rst.Edit
        rst![RR] = 1
        rst.Update
    End If
    
'fixing for DFS complete and no NDFS
    rst.MoveFirst
    For k = 1 To 14
        CC(k) = rst![CC]
        RR(k) = rst![RR]
        rst.MoveNext
    Next k
    If CC(3) = 1 And CC(4) = 1 And RR(6) = 0 And RR(7) = 0 Then
        rst.MoveFirst
        rst.Move (6)
        rst.Edit
        rst![CC] = 1
        rst.Update
        rst.MoveFirst
        rst.Move (7)
        rst.Edit
        rst![CC] = 1
        rst.Update
    End If

'MsgBox ("AFTER KK1=" & KK(1) & "  KK2=" & KK(2) & "  KK3=" & KK(3) & "  KK4=" & KK(4) & "  KK5=" & KK(5) & "  KK6=" & KK(6) & "  KK7=" & KK(7) & "  KK8=" & KK(8) & "  KK9=" & KK(9) & "  KK10=" & KK(10) & "  KK11=" & KK(11) & "  KK12=" & KK(12) & "  KK13=" & KK(13) & "  KK14=" & KK(14))
    rst.MoveFirst
    For k = 1 To 14
        If k > 2 And k < 9 Then
            If CHK_INT = 1 Then GoTo SKIP_THE_REST
            If rst![CC] = 1 Then CHK_INT = 27
            If rst![CC] = 0 And rst![RR] = 0 Then CHK_INT = 2
            If rst![CC] = 0 And rst![RR] = 1 Then CHK_INT = 1
        End If

        If k > 8 Then
            rst1.Edit
            If k = 9 Then rst1![T1] = CHK_INT
            If rst![CC] = 1 Then rst1(k - 8) = 27
            If rst![CC] = 0 And rst![RR] = 0 Then rst1(k - 8) = 2
            If rst![CC] = 0 And rst![RR] = 1 Then rst1(k - 8) = 1
            rst1.Update
        End If
        
SKIP_THE_REST:
        rst.MoveNext
    Next k
    
    rst1.Close
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
End Function

Function Get_SS_EUR_RON()
    Dim i As Long
    Dim L As Integer
    Dim M As Integer
    
    i = 1
    
    SS(1) = Create_Msg(i, 19) 'Building Name
    SS(2) = Create_Msg(i, 362) 'Service Selection Name
    SS(3) = Create_Msg(i, 100) 'Equipment
    SS(4) = Create_Msg(i, 62) & " (DFS)" 'Intervention DFS
    SS(5) = Create_Msg(i, 52) & " (DFS)" 'Processes DFS
    SS(6) = Create_Msg(i, 53) & " (DFS)" 'Extra DFS
    SS(7) = Create_Msg(i, 62) & " (NDFS)"  'Intervention NDFS
    SS(8) = Create_Msg(i, 97)  'NDFS Intervention Steps
    SS(9) = Create_Msg(i, 155) 'Notes
    SS(10) = Create_Msg(i, 42) 'Time Table
    SS(11) = Create_Msg(i, 964) 'Other Monthly Information
    SS(12) = Create_Msg(i, 113) 'Material
    SS(13) = Create_Msg(i, 22) 'Value
    SS(14) = Create_Msg(i, 754) 'Invoice Time Table
    
    KK(1) = 1
    KK(2) = 1
    KK(3) = 1
    KK(10) = 1
    KK(11) = 1
    KK(12) = 0
    KK(13) = 1
    KK(14) = 1
    
End Function

Private Function get_Signe_EUR_RON(SSID As Long, BID As Long, k As Integer) As Integer
    
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim IDD3(300) As Long
    Dim IDD3S(300) As Integer
    Dim i As Integer
    Dim ID3Count As Integer
    Dim GG As Integer
    Dim GK As Integer
    
    Set dbs = CurrentDb
    i = 0
    
    If k = 5 Or k = 6 Or k = 8 Or k = 10 Or k = 12 Then
        strSql = "SELECT [ID3],[DFS] FROM [Service selection 2] WHERE [Service selection] = " & SSID
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst.EOF
            i = i + 1
            IDD3(i) = rst![ID3]
            IDD3S(i) = rst![DFS]
            rst.MoveNext
        Wend
        rst.Close
    End If
    
    ID3Count = i
    If k = 1 Then get_Signe_EUR_RON = Get_N("BD-Buildings", "[BuildingName]", "[BuildingID]", BID)
    If k = 2 Then get_Signe_EUR_RON = Get_N("Service selection", "[Service Selection]", "[Service Selection]", SSID)
    If k = 3 Then get_Signe_EUR_RON = Get_N("Service selection 1", "[Service Selection]", "[Service Selection]", SSID)
    If k = 4 Then
        get_Signe_EUR_RON = Get_N("Z-Count_Int_DFS Query", "[Service Selection]", "[Service Selection]", SSID)
        If get_Signe_EUR_RON = 1 Then
            KK(4) = 1
            KK(5) = 1
'****************
        Else
            KK(4) = 0
            KK(5) = 0
'***************
        End If
    End If
    
    If k = 5 Then
        GG = 0
        For i = 1 To ID3Count
            If IDD3S(i) = 0 Then
                GG = Get_N("Service Selection 3", "[ID3]", "[ID3]", IDD3(i))
                If GG = 0 Then Exit For
            End If
        Next i
        get_Signe_EUR_RON = GG
    End If
    
    If k = 6 Then get_Signe_EUR_RON = 1
'code below checks for DFS extra may not be practical
'   If K = 6 Then
 '       GG = 0
 '       For i = 1 To ID3Count
 '           If IDD3S(i) = 0 Then
 '               GG = Get_N("Service Selection Extra", "[ID3]", "[ID3]", IDD3(i))
 '               If GG = 0 Then Exit For
 '           End If
 '       Next i
 '       get_Signe = GG
 '   End If

           
     If k = 7 Then
        get_Signe_EUR_RON = Get_N("Z-Count_Int_NDFS Query", "[Service Selection]", "[Service Selection]", SSID)
        If get_Signe_EUR_RON = 1 Then
            KK(7) = 1
'*****************************
        Else
            KK(7) = 0
'****************************
        End If
    End If
            
    If k = 8 Then
        GG = 0
        For i = 1 To ID3Count
            If IDD3S(i) = 1 Then
                GG = Get_N("Service Selection NDFS Steps", "[ID3]", "[ID3]", IDD3(i))
                If GG = 0 Then Exit For
            End If
        Next i
        get_Signe_EUR_RON = GG
    End If
    
    If k = 9 Then get_Signe_EUR_RON = Get_N("Service selection Notes", "[Service Selection]", "[Service Selection]", SSID)
    
    If k = 10 Then
        GG = 0
        For i = 1 To ID3Count
            GG = Get_FN("Z-SS2 Timetable Query", "[KK]", "[ID3]", IDD3(i))
            If GG = 0 Then Exit For
        Next i
        get_Signe_EUR_RON = GG
    End If
    
    If k = 11 Then get_Signe_EUR_RON = Get_FN("Z-Count Others info Query", "[Tot]", "[Service Selection]", SSID)
    
    If k = 12 Then get_Signe_EUR_RON = Get_N("Service Selection 3M", "[Service Selection]", "[Service Selection]", SSID)

'code below checks for every equipment every intervention may not be practical
'    If K = 12 Then
'        GG = 0
'        For i = 1 To ID3Count
'            GG = Get_FN("Z-Count Mat Query", "[KK]", "[ID3]", IDD3(i))
'            If GG = 0 Then Exit For
'        Next i
'        get_Signe = GG
'    End If
    
    If k = 13 Then get_Signe_EUR_RON = Get_FN("Z-Count Value Query", "[MinofDollar]", "[Service Selection]", SSID)
    
'check for value and DDate
    If k = 14 Then
        get_Signe_EUR_RON = Get_FN("Z-Count Invoice Query", "[KK]", "[Service Selection]", SSID)
        get_Signe_EUR_RON = Get_FN("Z-Count Invoice Query", "[DDate]", "[Service Selection]", SSID)
        
    End If
    Set rst = Nothing
    Set dbs = Nothing

End Function
Public Sub ut_Convert_EUR_to_RON()
Call Convert_EUR_to_RON
End Sub

