Attribute VB_Name = "InvoiceRevenueSwap"
Option Compare Database
Public Sub InvoiceRevenueSwap(Optional ByVal p_SSID As String = "")
Dim dbs As Database
Dim rstSerSel As Recordset
Dim strSQLSerSel As String
Dim arrDate() As Date
Dim cutoverDate As Date

cutoverDate = "10/1/2017"

Set dbs = CurrentDb
strSQLSerSel = "SELECT a.* FROM [Service Selection] as a " _
            & "where a.[contract start] < #10/1/2017# " _
            & "and a.[contract end] > #10/1/2017# " _
            & "and a.active = 4 " _
            & "and exists " _
            & "(" _
                & "SELECT " _
                & "b.Category " _
                & "FROM [service selection 2] as b " _
                & "where b.Category = 2 " _
                & "and b.[service selection] = a.[service selection] " _
            & ")"
If p_SSID <> "" Then
    strSQLSerSel = strSQLSerSel & " and a.[Service Selection] = " & p_SSID
End If
            
Set rstSerSel = dbs.OpenRecordset(strSQLSerSel)

While Not rstSerSel.EOF

    arrDate() = getContractMonths(rstSerSel.Fields("Contract Start"), rstSerSel.Fields("Contract End"))
    Call getSerSelInvoice(rstSerSel.Fields("Service Selection"), arrDate, rstSerSel.Fields("Contract Start"), rstSerSel.Fields("Contract End"))
    rstSerSel.MoveNext

Wend
End Sub

Public Function getSerSelInvoice(p_SSID, p_arrDate() As Date, p_contract_start_dt, p_contract_end_dt)
    Dim dbs As Database
    Dim rstSerInvoice As Recordset
    Dim strSQLSerInvoice As String
    Dim residualAmt As Double
    Dim numResidualDays As Integer
    Dim strSQLUpdSerSelRevenue As String
    Dim strSQLInsSerSelRevenue As String
    
    Dim JanD As Double
    Dim FebD As Double
    Dim MarD As Double
    Dim AprD As Double
    Dim MayD As Double
    Dim JunD As Double
    Dim JulD As Double
    Dim AugD As Double
    Dim SepD As Double
    Dim OctD As Double
    Dim NovD As Double
    Dim DecD As Double
      
    Set dbs = CurrentDb
    residualAmt = 0
    
    strSQLSerInvoice = "Select * from [Service Selection Invoice] where ID3 = 0 and ID2 = 0" & _
                        " and [service selection] = " & p_SSID
                        
    
    Set rstSerInvoice = dbs.OpenRecordset(strSQLSerInvoice)
    
    If Year(p_contract_end_dt) > 2018 And Month(p_contract_end_dt) > 10 Then
        p_contract_end_dt = Day(p_contract_end_dt) & "/" & Month(p_contract_end_dt) & "/" & "2017"
    'Else if leap year
        ElseIf Year(p_contract_end_dt) > 2018 And Month(p_contract_end_dt) = 2 And Day(p_contract_end_dt) = 29 Then
        p_contract_end_dt = Day(p_contract_end_dt) - 1 & "/" & Month(p_contract_end_dt) & "/" & "2018"
    ElseIf Year(p_contract_end_dt) > 2018 And Month(p_contract_end_dt) < 10 Then
        p_contract_end_dt = Day(p_contract_end_dt) & "/" & Month(p_contract_end_dt) & "/" & "2018"
    End If
    
    While Not rstSerInvoice.EOF
        Debug.Print "Pre-Update Section"
        'Pre R12 Golive months
        If CDate(p_contract_end_dt) >= CDate("1/31/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("JanD")
            numResidualDays = 31
        Else
            Debug.Print "copy this amount to revenue table JanD: " & rstSerInvoice.Fields("JanD")
        End If
        If CDate(p_contract_end_dt) >= CDate("2/28/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("FebD")
            numResidualDays = numResidualDays + 28
        Else
            Debug.Print "copy this amount to revenue table FebD: " & rstSerInvoice.Fields("FebD")
        End If
        If CDate(p_contract_end_dt) >= CDate("3/31/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("MarD")
            numResidualDays = numResidualDays + 31
        Else
            Debug.Print "copy this amount to revenue table MarD: " & rstSerInvoice.Fields("MarD")
        End If
        If CDate(p_contract_end_dt) >= CDate("4/30/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("AprD")
            numResidualDays = numResidualDays + 30
        Else
            Debug.Print "copy this amount to revenue table AprD: " & rstSerInvoice.Fields("AprD")
        End If
        If CDate(p_contract_end_dt) >= CDate("5/31/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("MayD")
            numResidualDays = numResidualDays + 31
        Else
            Debug.Print "copy this amount to revenue table MayD: " & rstSerInvoice.Fields("MayD")
        End If
        If CDate(p_contract_end_dt) >= CDate("6/30/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("JunD")
            numResidualDays = numResidualDays + 30
        Else
            Debug.Print "copy this amount to revenue table JunD: " & rstSerInvoice.Fields("JunD")
        End If
        If CDate(p_contract_end_dt) >= CDate("7/31/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("JulD")
            numResidualDays = numResidualDays + 31
        Else
            Debug.Print "copy this amount to revenue table JulD: " & rstSerInvoice.Fields("JulD")
        End If
        If CDate(p_contract_end_dt) >= CDate("8/31/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("AugD")
            numResidualDays = numResidualDays + 31
        Else
            Debug.Print "copy this amount to revenue table AugD: " & rstSerInvoice.Fields("AugD")
        End If
       If CDate(p_contract_end_dt) >= CDate("9/30/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("SepD")
            numResidualDays = numResidualDays + 30
        Else
            Debug.Print "copy this amount to revenue table SepD: " & rstSerInvoice.Fields("SepD")
        End If
        'Post R12 Go Live Months
         If CDate(p_contract_end_dt) < CDate("10/31/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("OctD")
            numResidualDays = numResidualDays + 31
        Else
            Debug.Print "copy this amount to revenue table OctD: " & rstSerInvoice.Fields("OctD")
        End If
        If CDate(p_contract_end_dt) < CDate("11/30/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("NovD")
            numResidualDays = numResidualDays + 30
        Else
            Debug.Print "copy this amount to revenue table NovD: " & rstSerInvoice.Fields("NovD")
        End If
        If CDate(p_contract_end_dt) < CDate("12/31/2018") Then
            residualAmt = residualAmt + rstSerInvoice.Fields("DecD")
            numResidualDays = numResidualDays + 31
        Else
            Debug.Print "copy this amount to revenue table DecD: " & rstSerInvoice.Fields("DecD")
        End If
      
        'Divide residualAmt between the eligible months
        Debug.Print "Update Section"
        
         'Pre R12 Golive months
        If CDate(p_contract_end_dt) >= CDate("1/31/2018") Then
            Debug.Print "Jand: " & (residualAmt / numResidualDays) * 31
            JanD = (residualAmt / numResidualDays) * 31
        Else
            Debug.Print "copy this amount to revenue table JanD: " & rstSerInvoice.Fields("JanD")
            JanD = rstSerInvoice.Fields("JanD")
        End If
        If CDate(p_contract_end_dt) >= CDate("2/28/2018") Then
            Debug.Print "FebD: " & (residualAmt / numResidualDays) * 28
            FebD = (residualAmt / numResidualDays) * 28
        Else
            Debug.Print "copy this amount to revenue table FebD: " & rstSerInvoice.Fields("FebD")
            FebD = rstSerInvoice.Fields("FebD")
        End If
        If CDate(p_contract_end_dt) >= CDate("3/31/2018") Then
            Debug.Print "MarD: " & (residualAmt / numResidualDays) * 31
            MarD = (residualAmt / numResidualDays) * 31
         Else
            Debug.Print "copy this amount to revenue table MarD: " & rstSerInvoice.Fields("MarD")
            MarD = rstSerInvoice.Fields("MarD")
        End If
        If CDate(p_contract_end_dt) >= CDate("4/30/2018") Then
            Debug.Print "AprD: " & (residualAmt / numResidualDays) * 30
            AprD = (residualAmt / numResidualDays) * 30
        Else
            Debug.Print "copy this amount to revenue table AprD: " & rstSerInvoice.Fields("AprD")
            AprD = rstSerInvoice.Fields("AprD")
        End If
        If CDate(p_contract_end_dt) >= CDate("5/31/2018") Then
            Debug.Print "MayD: " & (residualAmt / numResidualDays) * 31
            MayD = (residualAmt / numResidualDays) * 31
        Else
            Debug.Print "copy this amount to revenue table MayD: " & rstSerInvoice.Fields("MayD")
            MayD = rstSerInvoice.Fields("MayD")
        End If
        If CDate(p_contract_end_dt) >= CDate("6/30/2018") Then
            Debug.Print "JunD: " & (residualAmt / numResidualDays) * 30
            JunD = (residualAmt / numResidualDays) * 30
        Else
            Debug.Print "copy this amount to revenue table JunD: " & rstSerInvoice.Fields("JunD")
            JunD = rstSerInvoice.Fields("JunD")
        End If
        If CDate(p_contract_end_dt) >= CDate("7/31/2018") Then
            Debug.Print "JulD: " & (residualAmt / numResidualDays) * 31
            JulD = (residualAmt / numResidualDays) * 31
        Else
            Debug.Print "copy this amount to revenue table JulD: " & rstSerInvoice.Fields("JulD")
            JulD = rstSerInvoice.Fields("JulD")
        End If
        If CDate(p_contract_end_dt) >= CDate("8/31/2018") Then
            Debug.Print "AugD: " & (residualAmt / numResidualDays) * 31
            AugD = (residualAmt / numResidualDays) * 31
        Else
            Debug.Print "copy this amount to revenue table AugD: " & rstSerInvoice.Fields("AugD")
            AugD = rstSerInvoice.Fields("AugD")
        End If
       If CDate(p_contract_end_dt) >= CDate("9/30/2018") Then
            Debug.Print "SepD: " & (residualAmt / numResidualDays) * 30
            SepD = (residualAmt / numResidualDays) * 30
        Else
            Debug.Print "copy this amount to revenue table SepD: " & rstSerInvoice.Fields("SepD")
            SepD = rstSerInvoice.Fields("SepD")
        End If
        'Post R12 Go Live Months
         If CDate(p_contract_end_dt) < CDate("10/31/2018") Then
            Debug.Print "OctD: " & (residualAmt / numResidualDays) * 31
            OctD = (residualAmt / numResidualDays) * 31
        Else
            Debug.Print "copy this amount to revenue table OctD: " & rstSerInvoice.Fields("OctD")
            OctD = rstSerInvoice.Fields("OctD")
        End If
        If CDate(p_contract_end_dt) < CDate("11/30/2018") Then
            Debug.Print "NovD: " & (residualAmt / numResidualDays) * 30
            NovD = (residualAmt / numResidualDays) * 30
        Else
            Debug.Print "copy this amount to revenue table NovD: " & rstSerInvoice.Fields("NovD")
            NovD = rstSerInvoice.Fields("NovD")
        End If
        If CDate(p_contract_end_dt) < CDate("12/31/2018") Then
            Debug.Print "DecD: " & (residualAmt / numResidualDays) * 31
            DecD = (residualAmt / numResidualDays) * 31
        Else
            Debug.Print "copy this amount to revenue table DecD:" & rstSerInvoice.Fields("DecD")
            DecD = rstSerInvoice.Fields("DecD")
        End If
        
        strSQLUpdSerSelRevenue = "UPDATE [Service Selection Revenue] set Jan = " & JanD & "," & _
                              "Feb = " & FebD & "," & _
                              "Mar = " & MarD & "," & _
                              "Apr = " & AprD & "," & _
                              "May = " & MayD & "," & _
                              "Jun = " & JunD & "," & _
                              "Jul = " & JulD & "," & _
                              "Aug = " & AugD & "," & _
                              "Sep = " & SepD & "," & _
                              "Oct = " & OctD & "," & _
                              "Nov = " & NovD & "," & _
                              "Dec = " & DecD & " " & _
                              "where [Service Selection] = " & p_SSID
                              
    
        dbs.Execute strSQLUpdSerSelRevenue, dbFailOnError
        
        If dbs.RecordsAffected = 0 Then
            strSQLInsSerSelRevenue = "insert into [Service Selection Revenue] " & _
                                     "([service selection],Jan ,Feb ,Mar ,Apr ,May ,Jun ,Jul ,Aug ,Sep ,Oct ,Nov ,Dec ) VALUES " & _
                                     "(" & p_SSID & "," & JanD & "," & FebD & "," & MarD & "," & AprD & "," & MayD & "," & JunD & _
                                     "," & JulD & "," & AugD & "," & SepD & "," & OctD & "," & NovD & "," & DecD & ")"
            
            dbs.Execute strSQLInsSerSelRevenue, dbFailOnError

        End If
        
        rstSerInvoice.MoveNext
       
    Wend

    dbs.Close
    Set dbs = Nothing

End Function

Public Function getContractMonths(p_contract_start_dt, p_contract_end_dt) As Date()
 Dim arrDate() As Date
 Dim vdate As Date
 Dim i As Integer
 
 i = 0
 vdate = p_contract_start_dt
 While DateSerial(Year(p_contract_end_dt), Month(p_contract_end_dt), 0) >= DateSerial(Year(vdate), Month(vdate), 0)
    ReDim Preserve arrDate(0 To i)
    arrDate(i) = DateSerial(Year(vdate), Month(vdate) + 1, 0)
    vdate = DateSerial(Year(vdate), Month(vdate) + 1, 1)
    i = i + 1
 Wend
 
 getContractMonths = arrDate()

End Function


Public Sub test_getContractMonths()
Dim arrDate() As Date
arrDate() = getContractMonths("1/1/2017", "31/12/2017")
End Sub

Public Sub test_InvoiceRevenueSwap()
    'Call InvoiceRevenueSwap(34023150)
    Call InvoiceRevenueSwap
End Sub






