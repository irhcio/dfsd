Option Compare Database

Private Sub Convert_EUR_to_RON_PQ(Optional PQID As Long = -1)
    
    Dim pricingmodel As Integer
    Dim FN As Long
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim OEX As Single
    Dim strMode As String
    
    EX = 0.0378
    
    Set dbs = CurrentDb
    If PQID = -1 Then
        strSqlPQ = "SELECT [PartQuotationID] FROM [Parts Quotation] WHERE FEX = 1 and [active] in (1,2,3,13)"
        strMode = "Batch"
    Else
        strSqlPQ = "SELECT [PartQuotationID] FROM [Parts Quotation] WHERE FEX = 1 and [active] in (1,2,3,13)" _
                    & " and PartQuotationID = " & PQID
        strMode = "Single"
    End If

    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "PQ EUR-RON Conversion", "Start Program " & strMode, Null, LogIDType.PartsQuotation
    
    Set rst = dbs.OpenRecordset(strSqlPQ, dbOpenDynaset, dbSeeChanges)
    OEX = 1 'Original FX Rate compared to EUR
        
    While Not rst.EOF
        ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "PQ EUR-RON Conversion", "Start PQ EUR-RON conversion", rst![PartQuotationID], LogIDType.PartsQuotation
    
        Call FOREX_PQ(rst![PartQuotationID])

        ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "PQ EUR-RON Conversion", "End PQ EUR-RON conversion", rst![PartQuotationID], LogIDType.PartsQuotation
          
        rst.MoveNext
    Wend
    rst.Close
    Set dbs = Nothing
  
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "PQ EUR-RON Conversion", "End Program " & strMode, Null, LogIDType.PartsQuotation
            
End Sub


Function FOREX_PQ(PQID As Long)
    
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim EX As Single
    Dim CurrencyCode As String
            
    EX = 0.0378 'Conversion rate to apply, Convert from CZK to EUR
       
    CurrencyCode = "EUR"  'Currency to convert to
    
    Set dbs = CurrentDb
        
    strSql = "SELECT [Cost], [Listprice], [SellPrice] FROM [Parts Quotation 1] WHERE [PartQuotationID] = " & PQID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                
    While Not rst.EOF
        rst.Edit
        rst![Cost] = rst![Cost] * EX
        rst![Listprice] = rst![Listprice] * EX
        rst![SellPrice] = rst![SellPrice] * EX
        rst.Update
        
        rst.MoveNext
    Wend
    rst.Close
        
    strSql = "SELECT [FEX], [Price], [CurrencyCode], [TransportAmount] FROM [Parts Quotation] WHERE [PartQuotationID] = " & PQID  '1460
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                
    While Not rst.EOF
        rst.Edit
        rst![FEX] = EX
        rst![Price] = rst![Price] * EX
        rst![CurrencyCode] = CurrencyCode  ' 1460
        rst![TransportAmount] = rst![TransportAmount] * EX  '1460
        rst.Update
         
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing

End Function

Public Sub ut_Convert_EUR_to_RON_PQ()
    Call Convert_EUR_to_RON_PQ
End Sub

Public Sub Change_Base_Currency(strOldBaseCurr As String, strNewBaseCurr As String)


    Dim dbs As Database
    Dim rst As Recordset
    Dim strSqlEURSS As String
    Dim strSqlRONSS As String
    Dim strSqlEURPQ As String
    Dim strSqlRONPQ As String
    Dim strSqlCurrRON As String
    Dim strSqlCurrEUR As String
    Dim strSqlPL As String
    
    Dim lngRowsAffected As Long
    
    On Error GoTo ErrHandler:
    
    Set dbs = CurrentDb
    
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Change Base Currency", "Start Program: " & strOldBaseCurr & " -> " & strNewBaseCurr, Null, LogIDType.ServiceSelection
    
    BeginTrans
    
    
    'EUR is now base currency, so set FEX = (1/0.0378) for all those still in CZK
    strSqlSSEUR = "update [service selection] as a left join 1_select_actionlog_EUR_RON_conversion as b on a.[service selection] = b.id1" _
                & " set a.FEX = (1/0.0378)" _
                & " where b.id1 is null and a.fex =1"
    
    dbs.Execute strSqlSSEUR, dbFailOnError
    
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Change Base Currency", "strSqlSSEUR Recordsaffected: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection
    
    'EUR is now base currency, so set FEX = 1 for all SS that have been converted
    strSqlSSRon = "update [service selection] as a inner join 1_select_actionlog_EUR_RON_conversion as b on a.[service selection] = b.id1" _
                & " set a.FEX = 1"
            
    dbs.Execute strSqlSSRon, dbFailOnError
    
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Change Base Currency", "strSqlSSRON Recordsaffected: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection
    
    'EUR is now base currency, so set FEX = (1/0.0378) for all those still in CZK
    strSqlPQEUR = "Update [Parts Quotation]  a left join 1_select_actionlog_PQ_EUR_RON_CONVERSION as b on a.[partquotationid] = b.id1" _
                 & " set  a.FEX = (1/0.0378) " _
                 & " where FEX = 1 and b.id1 is null"
    
    dbs.Execute strSqlPQEUR, dbFailOnError
    
    ActionLog LogAction.UpdateRecord, LogObjectType.PartsQuotation, "Change Base Currency", "strSqlPQEUR Recordsaffected: " & dbs.RecordsAffected, Null, LogIDType.PartsQuotation
    
    'EUR is now base currency, so set FEX = 1 for all PQ that have been converted
    strSqlPQRon = "Update [Parts Quotation]  a inner join 1_select_actionlog_PQ_EUR_RON_CONVERSION as b on a.[partquotationid] = b.id1" _
                 & " set  a.FEX = 1"
                             
    dbs.Execute strSqlPQRon, dbFailOnError
    
    ActionLog LogAction.UpdateRecord, LogObjectType.PartsQuotation, "Change Base Currency", "strSqlPQRON Recordsaffected: " & dbs.RecordsAffected, Null, LogIDType.PartsQuotation
    
    'Update Currency table set base currency to EUR
    strSqlCurrRON = "Update [Currency] set FX = 1 where currencycode = ""EUR"""
            
    dbs.Execute strSqlCurrRON, dbFailOnError
    
    ActionLog LogAction.UpdateRecord, LogObjectType.OtherPriceSettings, "Change Base Currency", "strSqlCurrRON Recordsaffected: " & dbs.RecordsAffected, Null, Null
    
    'Update Currency table set new FEX rate for CZK
    strSqlCurrEUR = "Update [Currency] set FX = (1/0.0378) where currencycode = ""CZK"""
            
    dbs.Execute strSqlCurrEUR, dbFailOnError
    
    ActionLog LogAction.UpdateRecord, LogObjectType.OtherPriceSettings, "Change Base Currency", "strSqlCurrEUR Recordsaffected: " & dbs.RecordsAffected, Null, Null
    
    
    'Update [BD-Parts List] table convert PartCost and SC to RON
    strSqlPL = "Update [BD-Parts List] a set a.partcost = a.partcost * 0.0378, a.SC = a.SC * 0.0378"
            
    dbs.Execute strSqlPL, dbFailOnError
    
    ActionLog LogAction.UpdateRecord, LogObjectType.OtherPriceSettings, "Change Base Currency", "strSqlPL Recordsaffected: " & dbs.RecordsAffected, Null, Null
    
    
    'Rollback
    CommitTrans
    
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Change Base Currency", "End Program: " & strOldBaseCurr & " -> " & strNewBaseCurr, Null, LogIDType.ServiceSelection
    
Exit Sub

ErrHandler:
    Rollback
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Change Base Currency", Err.Description, Null, LogIDType.PartsQuotation
    
End Sub
Public Sub ut_Change_Base_Currency()
    Call Change_Base_Currency("CZK", "EUR")
End Sub


Public Sub Reset_Order_Timetable(strCountry As String, Optional SSID As Long = -1)

    Dim RomaniaCountryName As String
    Dim LoginUserInfo As Variant
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSqlInvoiceMaintenance As String
    Dim strSqlInvoiceNonMaintenance As String
    Dim lngRowsAffected As Long
    
    On Error GoTo ErrHandler:

    RomaniaCountryName = strCountry
    LoginUserInfo = Split(Forms![MENU-NEW]![Text122], " - ")
    
    If SSID = -1 Then
        strSqlInvoiceMaintenance = "Update [service selection invoice] set [JanD] =0, [FebD]= 0, [MarD]=0, [AprD]=0, [MayD]=0, [JunD]=0, [JulD]=0, [AugD]=0, " _
                      & "[SepD]=0, [OctD]=0, [NovD]=0, [DecD]=0, dolar =0 where [service selection] in" _
                      & " (SELECT DISTINCT id1 FROM actionlog WHERE moduleandfunctionname = ""EUR-RON Conversion""" _
                      & " and  id1 is not null) and category = 2"
                      
        strSqlInvoiceNonMaintenance = "Update [service selection invoice] set [JanD] =0, [FebD]= 0, [MarD]=0, [AprD]=0, [MayD]=0, [JunD]=0, [JulD]=0, [AugD]=0, " _
                      & "[SepD]=0, [OctD]=0, [NovD]=0, [DecD]=0, dolar =0.1 where [service selection] in" _
                      & " (SELECT DISTINCT id1 FROM actionlog WHERE moduleandfunctionname = ""EUR-RON Conversion""" _
                      & " and  id1 is not null) and category <> 2"
        
        strMode = "Batch"
    Else
        strSqlInvoiceMaintenance = "Update [service selection invoice] set [JanD] =0, [FebD]= 0, [MarD]=0, [AprD]=0, [MayD]=0, [JunD]=0, [JulD]=0, [AugD]=0, " _
                      & "[SepD]=0, [OctD]=0, [NovD]=0, [DecD]=0, dolar = 0 where [service selection] in" _
                      & " (SELECT DISTINCT id1 FROM actionlog WHERE moduleandfunctionname = ""EUR-RON Conversion""" _
                      & " and  id1 is not null) category = 2 and [Service Selection] = " & SSID
                      
        strSqlInvoiceNonMaintenance = "Update [service selection invoice] set [JanD] =0, [FebD]= 0, [MarD]=0, [AprD]=0, [MayD]=0, [JunD]=0, [JulD]=0, [AugD]=0, " _
                      & "[SepD]=0, [OctD]=0, [NovD]=0, [DecD]=0, dolar = 0.1 where [service selection] in" _
                      & " (SELECT DISTINCT id1 FROM actionlog WHERE moduleandfunctionname = ""EUR-RON Conversion""" _
                      & " and  id1 is not null) category <> 2 and [Service Selection] = " & SSID
                      
        strMode = "Single"
    End If
    
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Reset Order Timetable", "Start Program: " & strMode & " " & LoginUserInfo(1), Null, LogIDType.ServiceSelection

    If LoginUserInfo(1) = RomaniaCountryName Then
        
        Set dbs = CurrentDb
        
        BeginTrans
        
        dbs.Execute strSqlInvoiceMaintenance, dbFailOnError
        
        ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Reset Order Timetable", "strSqlInvoiceMaintenance Recordsaffected: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection
        
        dbs.Execute strSqlInvoiceNonMaintenance, dbFailOnError
        
        ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Reset Order Timetable", "strSqlInvoiceNonMaintenance Recordsaffected: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection
        
        'Rollback
        CommitTrans
    
    End If
    
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Reset Order Timetable", "End Program: " & strMode & " " & strCountry, Null, LogIDType.ServiceSelection
    
Exit Sub

ErrHandler:
    Rollback
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Reset Order Timetable", Err.Description, Null, LogIDType.PartsQuotation
    
End Sub

Public Sub ut_Reset_Order_Timetable()
    Call Reset_Order_Timetable("Romania")
End Sub