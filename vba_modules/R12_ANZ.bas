Option Compare Database

Sub R12_Project_Conversions()

Dim dbs As Database
'REMEMBER TO UPDATE
' 1. AGREEMENT CURRENCY - AUD or NZD
' 2. AGREEMENT SOURCE SYSTEM TRANE_AU_PRONTO or TRANE_NZ_PRONTO
' 3. PROJ_CUST_LEG CURRENCY - AUD or NZD
' 4. PROJ_CUST_LEGSOURCE SYSTEM TRANE_AU_PRONTO or TRANE_NZ_PRONTO
' 5. R12_Orgs table -  use correct country version
' 6  R12_Templates - use correct country version
' 7. EXPORT_PROJECTS() - update filename

insert_r12_projects
upd_R12_Projects_fcid_category
upd_R12_Projects_contract_dates
upd_r12_Projects_type
output_c_creation
export_projects

End Sub

Sub insert_r12_projects()

Dim dbs As Database
Dim sql_insert_non_maint_not_invoiced As String
Dim sql_insert_maint_not_invoiced As String
Dim sql_insert_maint_invoiced_not_complete As String

Set dbs = CurrentDb

Delete_Records "R12_Projects"
Delete_Records "C_creation"
Delete_Records "C_Task"
Delete_Records "C_keymbr"

'Non Maintenance Projects that are active/won and not fully invoiced
sql_insert_non_maint_not_invoiced = "INSERT INTO R12_Projects (SSID, Fully_Invoiced_Flag, active_won_date, contract_start_date, contract_term, contract_end_date) " _
                                & "SELECT [service selection], 'N', [ActiveWonDate], [Contract Start], [Contract Term], [Contract End] FROM [SERVICE SELECTION] as A " _
                                & "where active = 4" _
                                & "and not exists (SELECT 1 from [SERVICE SELECTION 2] as b " _
                                & "                where b.[service selection]= a.[service selection] " _
                                & "                and category = 2) " _
                                & "and exists (SELECT 1 from [SERVICE SELECTION INVOICE] as c " _
                                & "                where  c.[service selection]= a.[service selection] " _
                                & "                and c.othern = 0 " _
                                & "                and c.[service intervention] <> '(ALL)') " _
                                & " and not exists (SELECT 1 from [SERVICE SELECTION INVOICE] as c " _
                                & "                where  c.[service selection]= a.[service selection] " _
                                & "                and c.fcat in (0, 1001,855)) " 'No FCAT and Callouts, Parts
                                
dbs.Execute sql_insert_non_maint_not_invoiced, dbFailOnError

'Maintenance Projects that are active/won and not fully invoiced

sql_insert_maint_not_invoiced = "INSERT INTO R12_Projects (SSID, Fully_Invoiced_Flag, active_won_date, contract_start_date, contract_term, contract_end_date) " _
                                & "SELECT [service selection], 'N', [ActiveWonDate], [Contract Start], [Contract Term], [Contract End] FROM [SERVICE SELECTION] as A " _
                                & "where active = 4 " _
                                & "and exists (SELECT 1 from [SERVICE SELECTION 2] as b " _
                                & "                where b.[service selection]= a.[service selection] " _
                                & "                and category = 2) " _
                                & "and exists (SELECT 1 from [SERVICE SELECTION INVOICE] as c " _
                                & "                where  c.[service selection]= a.[service selection] " _
                                & "                and c.othern = 0 " _
                                & "                and c.[service intervention] <> '(ALL)') " _
                                & "and not exists (SELECT 1 from [SERVICE SELECTION INVOICE] as c " _
                                & "                where  c.[service selection]= a.[service selection] " _
                                & "                and c.fcat in (0,1001,855)) " 'No FCAT and Callouts, Parts
                                
dbs.Execute sql_insert_maint_not_invoiced, dbFailOnError

'Maintenance jobs that that are active/won and fully invoiced and have end date > SYSDATE
sql_insert_maint_invoiced_not_complete = "INSERT INTO R12_Projects (SSID, Fully_Invoiced_Flag, active_won_date, contract_start_date, contract_term, contract_end_date) " _
                                & "SELECT [service selection], 'N', [ActiveWonDate], [Contract Start], [Contract Term], [Contract End] FROM [SERVICE SELECTION] as A " _
                                        & "where active = 4 " _
                                        & "and [contract end] > date() " _
                                        & "and exists (SELECT 1 from [SERVICE SELECTION 2] as b " _
                                        & "                where b.[service selection]= a.[service selection] " _
                                        & "                and category = 2) " _
                                        & "and not exists (SELECT 1 from [SERVICE SELECTION INVOICE] as c " _
                                        & "                where  c.[service selection]= a.[service selection] " _
                                        & "                and c.othern = 0 " _
                                        & "                and c.[service intervention] <> '(ALL)') " _
                                        & "and not exists (SELECT 1 from [SERVICE SELECTION INVOICE] as c " _
                                        & "                where  c.[service selection]= a.[service selection] " _
                                        & "                and c.fcat  in (0,1001,855)) "  'No FCAT and Callouts, Parts
                                        
dbs.Execute sql_insert_maint_invoiced_not_complete, dbFailOnError
                                
End Sub

Sub upd_R12_Projects_fcid_category()

Dim dbs As Database
Dim upd_R12_Projects_fcid_category As String

Set dbs = CurrentDb

    sql_upd_R12_Projects_fcid_category = "update r12_projects as a " _
                                    & "inner join  [service selection invoice] as b on a.ssid = b.[service selection] " _
                                    & "set a.fcid = b.fcat, a.category = b.category " _
                                    & "where b.[service intervention] = '(ALL)' "
                                
    dbs.Execute sql_upd_R12_Projects_fcid_category, dbFailOnError

End Sub
Sub upd_R12_Projects_contract_dates()

    Dim dbs As Database
    Dim sql_upd_R12_Projects_contract_dates As String
    
    Set dbs = CurrentDb

    sql_upd_R12_Projects_contract_dates = "update r12_projects as a inner join [service selection] as b on a.[ssid] = b.[service selection] " _
                                       & "set a.[contract_start_date] = format(nz(iif(a.category = 2, a.[contract_start_date], nz(a.active_won_date,b.recordeddate)),nz(a.[contract_start_date],b.recordeddate)), 'mm/dd/yyyy'), " _
                                       & "a.[contract_end_date]  = format(nz(iif(a.category = 2, a.[contract_end_date], dateadd('YYYY', 1, nz(a.active_won_date,recordeddate))), dateadd('YYYY', 1, nz(a.[contract_start_date],b.recordeddate))), 'mm/dd/yyyy')"
                                       
    dbs.Execute sql_upd_R12_Projects_contract_dates, dbFailOnError

End Sub

Sub upd_r12_Projects_type()

Dim dbs As Database
Dim sql_upd_r12_Projects_type As String

Set dbs = CurrentDb


sql_upd_r12_Projects_type = "Update  ([R12_Projects] as a inner join   [service selection] as b " _
                            & "on a.ssid = b.[service selection]) " _
                            & "Inner join  fcat as c on a.fcid = c.fcid " _
                            & "set a.project_type   = iif(c.R12ProjectType = 'Service Agreement' and (Date() between a.[contract_start_date] and a.[contract_end_date]) AND a.fully_invoiced_flag = 'Y', 'No Charge - Service', " _
                            & "                            iif(c.R12ProjectType = 'Service Agreement' and  a.[contract_start_date] < Date() AND a.fully_invoiced_flag = 'N', 'On Demand' , " _
                            & "                                iif(c.R12ProjectType = 'Service Agreement' and  a.[contract_start_date] > Date() AND a.fully_invoiced_flag = 'N', 'Service Agreement' , " _
                            & "                                    iif(c.R12ProjectType <> 'Service Agreement' and a.fully_invoiced_flag = 'N', c.R12ProjectType, c.R12ProjectType)))) "
                                
 dbs.Execute sql_upd_r12_Projects_type, dbFailOnError

End Sub

Sub output_c_creation()

    Dim dbs As Database
    Dim rst As Recordset
    Dim sql_select_c_creation As String
    Dim sql_ins_c_creation As String
    
    
    Set dbs = CurrentDb
    
    sql_select_c_creation = "select 'DFSD_EMEA' as Source_System, " _
                            & "        a.[service selection] as Source_System_Reference, " _
                            & "        c.Template_Name as orig_Project_Number, " _
                            & "        a.[service selection] as Project_Number, " _
                            & "        a.[service selection] as Project_Name, " _
                            & "        g.inv_org_name as Organization, " _
                            & "        iif(nz(iif(b.category = 2, a.[contract start], a.activewondate), nz(a.[contract start], a.recordeddate)) < #1/26/2021#, #1/26/2021#, format(nz(iif(b.category = 2, a.[contract start], a.activewondate), nz(a.[contract start], a.recordeddate)), 'mm/dd/yyyy')) AS start_date, " _
                            & "        'APPROVED' as Project_Status, " _
                            & "        a.[service selection name] as Orig_Project_Name, " _
                            & "        a.[service selection name] as Description, " _
                            & "        format(nz(iif(b.category = 2, a.[contract end], dateadd('YYYY', 1, nz(a.activewondate,a.recordeddate))), dateadd('YYYY', 1, nz(a.[contract start],a.recordeddate))), 'mm/dd/yyyy')  as Completion_Date, " _
                            & "        a.[contract term] as contract_term, " _
                            & "        nz(f.[salesnumber] , get_default_Pm()) as deployer_employee_number " _
                            & "from ((((([service selection] as a inner join [R12_Projects] as b " _
                            & "on a.[service selection] = b.ssid) " _
                            & "inner join R12_Templates as c on b.project_type = c.project_type) " _
                            & "inner join [bd-buildings] as d on a.buildingid = d.buildingid ) " _
                            & "left join [salesoffice] as e on d.officeid = e.officeid) " _
                            & "left join [fulfillment] as f on a.deployerid = f.fulfillmentid) " _
                            & "left join [R12_orgs] as g on e.orgid = g.inv_org_id "
                            '& "where b.ssid = 210006043"
                        
                      
    Set rst = dbs.OpenRecordset(sql_select_c_creation)
    While Not rst.EOF
        
        sql_ins_c_creation = "insert into c_creation (Source_System, Source_System_Reference, orig_Project_Number, Project_Number, Project_Name, Organization, Start_Date, Project_Status, Orig_Project_Name, Description, Completion_Date) " _
                                & "values ( """ & rst!Source_System & """," & rst!Source_System_Reference & ",""" & rst!orig_Project_Number & """," & rst!Project_Number & ",""" & rst!project_name & """,""" & rst!Organization & """,#" & rst!Start_Date & "#,""" & rst!Project_Status & """,""" & Replace(rst!Orig_Project_Name, """", "'") & """,""" & Replace(rst!Description, """", "'") & """,#" & rst!Completion_Date & "#)"
        
        dbs.Execute sql_ins_c_creation, dbFailOnError
        Call output_c_task(rst![Project_Number], rst![Start_Date], rst![contract_term], rst![Completion_Date], rst![project_name])
        Call output_c_KeyMbr(rst![Project_Number], Nz(rst!deployer_employee_number, ""), rst![Start_Date])
        rst.MoveNext
    
        
    Wend
    rst.Close
    
    Call output_c_proj_Cust_Leg
    Call output_c_class
    Call output_Agreement_r12
    Call output_funding
    Call output_c_Cred_Rec
    



End Sub

Sub output_c_task(p_ssid As Long, p_contractStartDate As Date, p_contractTerm As Integer, p_contractEndDate As Date, p_project_name As String)
        Dim months() As Date
        Dim monthsNeed() As Date
        Dim monthCount As Integer
        Dim monthArrIndex As Integer
        Dim monthIndex As Integer
        Dim taskno As Integer
        Dim rst As Recordset
        Set dbs = CurrentDb
        
        Dim contractStartMonth As Integer
        Dim monthsPerTerm As Integer
        
        Dim skipFutureNonMaintenance As Boolean
        Dim sql_ins_c_task As String
        contractStartMonth = Month(p_contractStartDate)
        skipFutureNonMaintenance = False
        monthsPerTerm = 12
        taskno = 10
        
        strSql = "SELECT [ID2], [ID3], [Category], [Jan], [Feb], [Mar], [Apr], [May], [Jun], [Jul], [Aug], [Sep], [Oct], [Nov], [Dec] FROM [Service Selection 2] WHERE [Service Selection]=" & p_ssid
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst.EOF
            ' Build up a list of applicable task visit dates.
            monthIndex = contractStartMonth
            monthArrIndex = 0
            monthCount = 0
            While monthCount < monthsPerTerm * p_contractTerm
                If rst(monthIndex + 2) = -1 Then
                    ReDim Preserve months(0 To monthArrIndex)
                    months(monthArrIndex) = DateAdd("m", monthCount, p_contractStartDate)
                    monthArrIndex = monthArrIndex + 1
                End If
                monthIndex = monthIndex + 1
                If monthIndex > 12 Then
                    monthIndex = 1
                End If
                monthCount = monthCount + 1
            Wend
            
            ' Reset the month index and then iterate through each one.
            monthArrIndex = 0
            While monthArrIndex <= UBound(months)
                strSql = "SELECT [InvoiceTo], [FCat] FROM [Service Selection Invoice] WHERE [Service Selection]=" & p_ssid & " AND [ID2]=" & rst![ID2] & " AND [ID3]=" & rst![ID3]
                Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                While Not rst1.EOF
                    If (Nz(rst1![Fcat], "") <> "") Then
                        strSql = "SELECT [R12FcatCode], [R12FcatDescr], [R12Account] FROM [FCat] WHERE [FCID]=" & CInt(Nz(rst1![Fcat], 0))
                        Set rst2 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                        While Not rst2.EOF
                            Dim monthChars As String
                            Dim taskName As String
                            ' Ensure a 0 appears in front of any month less than 10.
                            monthChars = Month(months(monthArrIndex))
                            If Len(monthChars) = 1 Then
                                monthChars = "0" & monthChars
                            End If
                            ' Use task name also within description.
                            taskName = Year(months(monthArrIndex)) & " " & monthChars & " " & Nz(rst2![R12FcatCode], "")
                            
                            sql_ins_c_task = "insert into c_task (T_Project_Number, T_Task_Name, T_Task_Number, T_Task_Reference, T_Start_Date, Chargeable_Flag, Billable_Flag, T_Parent_Task_Ref, T_Description, T_Finish_Date, T_Product_Code, T_Cost_Center) " _
                                            & "values (" & p_ssid & ", """ & taskName & """, " & taskno & ", " _
                                            & p_ssid & ", #" & p_contractStartDate & "#, " & """Yes""" & ", " & """Yes""" & ", " & "NULL" & ", """ & p_project_name & " " & p_ssid & """, #" & p_contractEndDate & "#,  " & Nz(rst2![R12Account], "") & ", " & """00000""" & ")"
                            
                            'Debug.Print taskName & " | " & taskName & " " & Nz(rst2![R12FcatDescr], "") & " | " & Nz(rst2![R12Account], "") & " | " & taskno
                            
                            dbs.Execute sql_ins_c_task, dbFailOnError
                            
                            taskno = taskno + 10

                            rst2.MoveNext
                        Wend
                        rst2.Close
                    End If
                    rst1.MoveNext
                Wend
                rst1.Close
                monthArrIndex = monthArrIndex + 1
            Wend
            
            rst.MoveNext
        Wend
        rst.Close

End Sub

Sub output_c_KeyMbr(p_ssid As Long, p_emp_number As String, p_start_date As Date)

 Dim rst As Recordset
 Set dbs = CurrentDb
 Dim sql_ins_c_task As String
 
 
 
 sql_ins_c_keymbr = "insert into c_keymbr (K_Project_Number, K_Emp_Number, K_Role, K_From_Date) values ( " _
                    & p_ssid & ", """ & p_emp_number & """," & """Project Manager""" & ", #" & Format(IIf(p_start_date > Date, Date, p_start_date), "dd-mmm-yyyy") & "#)"
                    
 dbs.Execute sql_ins_c_keymbr, dbFailOnError

End Sub

Sub output_c_proj_Cust_Leg()

    Set dbs = CurrentDb
    Dim sql_c_proj_Cust_Leg As String

    'REMEMBER TO CHANGE CURRENCY
    
    'sql_c_proj_Cust_Leg = "SELECT 'TRANE_AU_PRONTO' as C_Orig_System, a.[service selection] as C_Project_Number, d.customernumber as C_Customer_Number, " _

    sql_c_proj_Cust_Leg = "SELECT 'TRANEN_AU_PRONTO' as C_Orig_System, a.[service selection] as C_Project_Number, d.customernumber as C_Customer_Number, " _
                            & "        'Primary' as C_Relationship, " _
                            & "        100 as C_Contribution, " _
                            & "        d.[CustomerNumber] as Customer_Bill, " _
                            & "        d.[CustomerNumber] as Customer_Ship, " _
                            & "        d.[CustomerNumber] as C_Bill_Address, " _
                            & "        d.[CustomerNumber] & '_' & e.[BuildingID] as C_Ship_Address, " _
                            & "        'AUD' as C_Currency " _
                            & "INTO  c_proj_Cust_Leg " _
                            & "FROM ((([service selection] as a " _
                            & "        inner join R12_Projects as b on a.[service selection] = b.[ssid]) " _
                            & "            inner join [service selection invoice] as c on a.[service selection] = c.[service selection] and b.[category] = c.[category] ) " _
                            & "                inner join [bd-customers] as d on d.customerid = c.InvoiceTo) " _
                            & "                    inner join [bd-buildings] as e on a.BuildingId = e.BuildingId " _
                            & "WHERE c.[service intervention] = '(ALL)' and b.project_type is not null"
                            
    dbs.Execute "DROP TABLE [c_proj_Cust_Leg];"
    
    dbs.Execute sql_c_proj_Cust_Leg, dbFailOnError

End Sub

Sub output_c_class()

    Set dbs = CurrentDb
    Dim sql_c_class As String
    
    On Error GoTo Errhandler

    sql_c_class = "select * INTO c_class from " _
                    & "(SELECT a.[service selection] as CL_Project_Number, 'Business Offering' as CL_Category, c.R12BusOffering as CL_Class_Code " _
                    & "FROM ([service selection invoice] as a " _
                    & "        inner join R12_Projects as b on a.[service selection] = b.[ssid]) " _
                    & "            inner join [fcat] as c on a.[fcat] = c.[fcid] " _
                    & "WHERE a.[service intervention] = '(ALL)' and b.project_type is not null " _
                    & "UNION " _
                    & "SELECT a.[service selection] as CL_Project_Number, 'Sales Channel' as CL_Category, 'Direct Sales Channel' as CL_Class_Code " _
                    & "FROM ([service selection invoice] as a " _
                    & "        inner join R12_Projects as b on a.[service selection] = b.[ssid]) " _
                    & "            inner join [fcat] as c on a.[fcat] = c.[fcid] " _
                    & "WHERE a.[service intervention] = '(ALL)' and b.project_type is not null " _
                    & "UNION " _
                    & "SELECT a.[service selection] as CL_Project_Number, 'FinCat' as CL_Category, c.R12FcatCode as CL_Class_Code " _
                    & "FROM ([service selection invoice] as a " _
                    & "        inner join R12_Projects as b on a.[service selection] = b.[ssid] and a.[category] = b.[category]) " _
                    & "            inner join [fcat] as c on a.[fcat] = c.[fcid] " _
                    & "WHERE a.[service intervention] = '(ALL)' and b.project_type is not null) "
                            
    dbs.Execute "DROP TABLE [c_class];"
    
    dbs.Execute sql_c_class, dbFailOnError
    
Errhandler:
    If Err.Number = 3376 Then
        dbs.Execute "create table c_class (dummy char)"
        Resume
    End If
    Debug.Print Err.Description
    

End Sub
Sub output_Agreement_r12()

    Set dbs = CurrentDb
    Dim sql_Agreement_r12 As String
    
    On Error GoTo Errhandler
    'REMEMBER TO CHANGE CURRENCY
    sql_Agreement_r12 = "SELECT 'TRANE_AU_PRONTO' as C_Orig_System, " _
                    & "        a.[service selection] as Agreement_Number, " _
                    & "        'Conversion' as Agreement_Type, " _
                    & "        d.customernumber as Customer_Number, " _
                    & "        'AUD' as Agreement_currency, " _
                    & "        format(iif(c.category = 2, c.[JanD]+ c.[FebD]+c.[MarD]+c.[AprD]+c.[MayD]+c.[JunD]+c.[JulD]+c.[AugD]+c.[SepD]+c.[OctD]+c.[NovD]+c.[DecD], " _
                    & "                c.Dolar) * a.[contract term], ""###0.00"") as Amount, " _
                    & "        'DFSD_EMEA' as Product_Source, " _
                    & "        d.payterm as Terms, " _
                    & "        nz(e.[salesnumber] , get_default_Pm()) as Agreement_Owned_By, " _
                    & "        a.[PurchaseNumber] as Customer_Order_Number, " _
                    & "        format(iif(b.category = 2, b.contract_start_date, b.active_won_date),'dd-mmm-yyyy')   as Agreement_Start_Date, " _
                    & "        'YES' as Revenue_Hard_Limit, " _
                    & "        'YES' as Invoice_Hard_Limit, " _
                    & "        a.[service selection name] as Description, " _
                    & "        h.inv_org_name as Organization " _
                    & "INTO Agreement_r12 " _
                    & "FROM (((((([service selection] as a " _
                    & "        inner join R12_Projects as b on a.[service selection] = b.[ssid]) " _
                    & "            inner join [service selection invoice] as c on a.[service selection] = c.[service selection] ) " _
                    & "                inner join [bd-customers] as d on d.customerid = c.InvoiceTo) " _
                    & "                    left join [fulfillment] as e on a.deployerid = e.fulfillmentid) " _
                    & "                        inner join [bd-buildings] as f on a.buildingid = f.buildingid ) " _
                    & "                            left join [salesoffice] as g on f.officeid = g.officeid) left join [R12_orgs] as h on g.orgid = h.inv_org_id " _
                    & "WHERE c.[ID2]=0 AND c.[ID3]=0 and b.project_type is not null"
                    
    dbs.Execute "DROP TABLE [Agreement_r12];"
    
    dbs.Execute sql_Agreement_r12, dbFailOnError
    
Errhandler:
    If Err.Number = 3376 Then
        dbs.Execute "create table Agreement_r12 (dummy char)"
        Resume
    End If
    Debug.Print Err.Description
    

End Sub

Sub output_funding()

    Set dbs = CurrentDb
    Dim sql_funding As String
    
    On Error GoTo Errhandler

    sql_funding = "SELECT  a.[service selection] as Agreement_Number, " _
                & "        a.[service selection] as Project_Number, " _
                & "        format(b.active_won_date, 'dd-mmm-yyyy') as Allocated_Date, " _
                & "        format(iif(c.category = 2, c.[JanD]+ c.[FebD]+c.[MarD]+c.[AprD]+c.[MayD]+c.[JunD]+c.[JulD]+c.[AugD]+c.[SepD]+c.[OctD]+c.[NovD]+c.[DecD], " _
                & "                c.Dolar) * a.[contract term], ""###0.00"") as Amount, " _
                & "        'TRANSFER' as Classification " _
                & "         INTO funding " _
                & "FROM ([service selection] as a " _
                & "        inner join R12_Projects as b on a.[service selection] = b.[ssid]) " _
                & "           inner join [service selection invoice] as c on a.[service selection] = c.[service selection] " _
                & "WHERE c.[ID2]=0 AND c.[ID3]=0 and b.project_type is not null"
                    
    dbs.Execute "DROP TABLE [funding];"
    
    dbs.Execute sql_funding, dbFailOnError
    
Errhandler:
    If Err.Number = 3376 Then
        dbs.Execute "create table funding (dummy char)"
        Resume
    End If
    Debug.Print Err.Description
    

End Sub

Sub output_c_Cred_Rec()

    Set dbs = CurrentDb
    Dim sql_c_Cred_Rec As String
    
    On Error GoTo Errhandler

    sql_c_Cred_Rec = "select a.[service selection] as PROJECT_NUMBER, " _
                    & "         'QUOTA CREDIT' as CR_CREDIT_TYPE, " _
                    & "        c.SalesNumber as CR_EMPLOYEE_NUMBER, " _
                    & "        '' as CR_EMPLOYEE_NAME, " _
                    & "        100 as CR_CREDIT_PERCENT, " _
                    & "        'N' as CR_INTERFACE_TO_AR, " _
                    & "        format(b.contract_Start_Date, 'dd-mmm-yyyy') as CR_START_DATE " _
                    & "into c_Cred_Rec " _
                    & "from ([service selection] as a " _
                    & "        inner join R12_Projects as b on a.[service selection] = b.[ssid]) " _
                    & "        left join Salesengineers as c on a.seid = c.salesengineerid " _
                    & "where B.project_type Is Not Null "
                    
    dbs.Execute "DROP TABLE [c_Cred_Rec];"
    
    dbs.Execute sql_c_Cred_Rec, dbFailOnError
    
Errhandler:
    If Err.Number = 3376 Then
        dbs.Execute "create table c_Cred_Rec (dummy char)"
        Resume
    End If
    Debug.Print Err.Description
    

End Sub
   


Function get_default_Pm() As Long

    get_default_Pm = DLookup("employee_number", "R12_Default_Project_Manager")

End Function

Function query_to_xls(p_query_name As String, p_output_path As String, p_filename As String, p_file_ext As String, p_sheet_name As String, p_sheet_no As Integer)
    
    DoCmd.TransferSpreadsheet acExport, acSpreadsheetTypeExcel12Xml, p_query_name, p_output_path & p_filename & p_batch_no & p_file_ext, True


End Function

Public Sub export_projects()
    
    
     Dim output_path As String
     Dim filename As String
     Dim file_ext As String
     
    output_path = Application.CurrentProject.Path & "\"
     
    filename = "DFSD_AUSTRALIA_PROJECT_CONVERSIONS"
    'filename = "DFSD_NZ_PROJECT_CONVERSIONS"
    file_ext = ".xlsx"
     
    'Initiate progress bar
    Call Call_prog("")
    
    'Update Progress Br
    Call UpdateProgress((1 / 8) * 100, "Exporting Sheet:" & 1)
    'C_Creation
    Call query_to_xls("c_creation", output_path, filename, file_ext, "c_creation", 1)
    
    'Update Progress Br
    Call UpdateProgress((2 / 8) * 100, "Exporting Sheet:" & 1)
    'c_task
    Call query_to_xls("c_task", output_path, filename, file_ext, "c_task", 1)
    
    'Update Progress Br
    Call UpdateProgress((3 / 8) * 100, "Exporting Sheet:" & 1)
    'c_KeyMbr
    Call query_to_xls("c_KeyMbr", output_path, filename, file_ext, "c_KeyMbr", 1)
    
    'Update Progress Br
    Call UpdateProgress((4 / 8) * 100, "Exporting Sheet:" & 1)
    'c_proj_Cust_Leg
    Call query_to_xls("c_proj_Cust_Leg", output_path, filename, file_ext, "c_proj_Cust_Leg", 1)
    
    'Update Progress Br
    Call UpdateProgress((5 / 8) * 100, "Exporting Sheet:" & 1)
    'c_Class
    Call query_to_xls("c_Class", output_path, filename, file_ext, "c_Class", 1)
    
    'Update Progress Br
    Call UpdateProgress((6 / 8) * 100, "Exporting Sheet:" & 1)
    'Agreement_r12
    Call query_to_xls("Agreement_r12", output_path, filename, file_ext, "Agreement_r12", 1)
    
    'Update Progress Br
    Call UpdateProgress((7 / 8) * 100, "Exporting Sheet:" & 1)
    'Funding
    Call query_to_xls("Funding", output_path, filename, file_ext, "Funding", 1)
    
    'Update Progress Br
    Call UpdateProgress((8 / 8) * 100, "Exporting Sheet:" & 1)
    'c_Cred Rec
    Call query_to_xls("c_Cred_Rec", output_path, filename, file_ext, "c_Cred_Rec", 1)
    
     
    'Close Progress Bar
    CloseProgress
     
End Sub



'Sub ut_output_c_task()
'Call output_c_task(210006043, "1/15/2021", 1)
'End Sub
