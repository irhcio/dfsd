Option Compare Database
Option Explicit

Public Sub IntegrateProject(SSID As Long, shipSalesOfficeOrgCode As String, shipName As String, shipAddress1 As String, shipAddress2 As String, shipAddress3 As String, shipCity As String, shipPostalCode As String, shipCountry As String, Note As String, needByDate As Date, projectBillingEvent_Flag As Boolean)
    Dim pathToIntegrationQueue As String
    Dim pathToStatusIntegrationQueue As String
    Dim pathToMaterialIntegrationQueue As String
    Dim pathToMaterialStatusIntegrationQueue As String
    Dim submitIntReqs As Boolean
    Dim rstFCAT_Lumpsum_XREF As Recordset 'DFSD-156
    Dim rstServiceSelectInvoice As Recordset 'DFSD-156
    Dim strSQLServiceSelectInvoice As String
    Dim pathtoProjectEventsIntegrationQueue As String 'DFSD-156

    pathToIntegrationQueue = Nz(DLookup("R12IntegrationQueuePath", "DocumentPaths"), "")
    pathToStatusIntegrationQueue = Nz(DLookup("R12StatusIntegrationQueuePath", "DocumentPaths"), "")
    pathToMaterialIntegrationQueue = Nz(DLookup("R12MaterialIntegrationQueuePath", "DocumentPaths"), "")
    pathToMaterialStatusIntegrationQueue = Nz(DLookup("R12MaterialStatusIntegrationQueuePath", "DocumentPaths"), "")
    submitIntReqs = (Nz(DLookup("R12IntReqSubmission", "Office"), 0) = -1)
    pathtoProjectEventsIntegrationQueue = Nz(DLookup("R12ProjectEventsIntegrationQueuePath", "DocumentPaths"), "") 'DFSD-156
    
    ' If there is a path to the queue, then we can assume they want to run the integration.
    If pathToIntegrationQueue <> "" And pathToStatusIntegrationQueue <> "" And pathToMaterialIntegrationQueue <> "" And pathToMaterialStatusIntegrationQueue <> "" Then
        Dim DFSDSmartIntegrator As New DFSDComIntegrator.OutboundDatabaseIntegrator
        Dim dbs As Database
        Dim rst As Recordset
        Dim rst1 As Recordset
        Dim rst2 As Recordset
        Dim rst3 As Recordset
        Dim strSql As String
    
        Dim BID As Long
        Dim CID As Long
        Dim AgreementNumber As String
        Dim projectId As String
        Dim R12CustomerNumber As String
        Dim r12CustomerBillToSiteId As String
        Dim PayTerm As String
        Dim BuildingID As String
        Dim BuildingName As String
        Dim buildingAddress As String
        Dim BuildingCity As String
        Dim buildingCountry As String
        Dim buildingPostalCode As String
        Dim organizationId As String
        Dim ServiceSelectionName As String
        Dim PurchaseNumber As String
        Dim SalesEngineerSalesNumber As String
        Dim fullfillmentSalesNumber As String
        Dim serviceSelectionAllowR12PoUpdate As String
        Dim serviceSelectionStartDate As String
        Dim serviceSelectionEndDate As String
        Dim serviceSelectionActiveWonDate As String
        Dim dateActiveWonDate As Date
        Dim serviceSelectionInvoiceDollar As String
        Dim ServiceSelectionCurrencyCode As String
        Dim serviceSelectionFCat As String
        Dim fcatCatCode As String
        Dim fcatProjectType As String
        Dim fcatBusOffering As String
        Dim partialShip As String
        Dim hasMaterials As Boolean
        Dim PreferredShipping As String
        Dim contractTerm As Integer
        Dim invoiceValue As Double
        
        BID = 0
        CID = 0
        AgreementNumber = ""
        projectId = CStr(SSID)
        R12CustomerNumber = ""
        r12CustomerBillToSiteId = ""
        PayTerm = "NET 30"
        BuildingID = ""
        buildingAddress = ""
        BuildingCity = ""
        buildingCountry = ""
        buildingPostalCode = ""
        organizationId = ""
        ServiceSelectionName = ""
        PurchaseNumber = ""
        SalesEngineerSalesNumber = ""
        fullfillmentSalesNumber = ""
        serviceSelectionAllowR12PoUpdate = ""
        serviceSelectionStartDate = Year(Date) & "-" & Month(Date) & "-" & Day(Date)
        serviceSelectionEndDate = (1 + Year(Date)) & "-" & Month(Date) & "-" & Day(Date)
        serviceSelectionInvoiceDollar = ""
        ServiceSelectionCurrencyCode = "EUR"
        serviceSelectionFCat = ""
        fcatProjectType = ""
        fcatCatCode = ""
        fcatBusOffering = ""
        partialShip = ""
        hasMaterials = False
        PreferredShipping = ""
        contractTerm = Nz(DLookup("[Contract Term]", "[Service Selection]", "[Service Selection]=" & SSID), 1)
        invoiceValue = 0
        
        Set dbs = CurrentDb
        
        ' Get data from Service Selection.
        strSql = "SELECT [BuildingID], [CustomerID], [Service Selection Name], [PurchaseNumber], [AgreementNumber], [SalesOfficeID], [AddValueToAgreementPO], [SEID], [DeployerID], [ShippingPreferenceID], [PreferredShippingID], [ActiveWonDate], [CurrencyCode] FROM [Service Selection] WHERE [Service Selection]=" & SSID
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst.EOF
            BID = rst![BuildingID]
            BuildingID = CStr(BID)
            CID = Nz(rst![CustomerID], 0)
            ServiceSelectionName = Nz(rst![Service Selection Name], "")
            PurchaseNumber = Nz(rst![PurchaseNumber], "")
            AgreementNumber = Nz(rst![AgreementNumber], "")
            serviceSelectionAllowR12PoUpdate = Nz(rst![AddValueToAgreementPO], "0")
            ServiceSelectionCurrencyCode = Nz(rst![CurrencyCode], Nz(DLookup("[CurrencyCode]", "[Currency]", "[FX]=1"), "EUR"))
            If serviceSelectionAllowR12PoUpdate = "-1" Then
                serviceSelectionAllowR12PoUpdate = "Y"
            Else
                serviceSelectionAllowR12PoUpdate = "N"
            End If
            organizationId = Nz(DLookup("[OrgID]", "[SalesOffice]", "[OfficeID]=" & Nz(rst![SalesOfficeID], 0)), "")
            SalesEngineerSalesNumber = Nz(DLookup("[SalesNumber]", "[SalesEngineers]", "[SalesEngineerID]=" & Nz(rst![SEID], 0)), "")
            fullfillmentSalesNumber = Nz(DLookup("[SalesNumber]", "[Fulfillment]", "[FulfillmentID]=" & Nz(rst![DeployerID], 0)), "")
            PreferredShipping = Nz(DLookup("[Name]", "[Preferred Shipping]", "[ID]=" & Nz(rst![PreferredShippingID], 0)), "Standard Priority")
            If PreferredShipping = "Priority Customer" Then
                PreferredShipping = "100"
            ElseIf PreferredShipping = "Emergency" Then
                PreferredShipping = "200"
            ElseIf PreferredShipping = "Standard Priority" Then
                PreferredShipping = "300"
            ElseIf PreferredShipping = "Stock" Then
                PreferredShipping = "400"
            End If
            If Nz(rst![ShippingPreferenceID], 0) = 0 Then
                partialShip = "N"
            Else
                partialShip = "Y"
            End If
            
            dateActiveWonDate = rst![ActiveWonDate]
            serviceSelectionActiveWonDate = Year(rst![ActiveWonDate]) & "-" & Month(rst![ActiveWonDate]) & "-" & Day(rst![ActiveWonDate])
            rst.MoveNext
        Wend
        rst.Close
        
        ' Used for start date (for maintenance) and also visit gathering.
        Dim contractStartDate As Date
        contractStartDate = Nz(DLookup("[Contract Start]", "[Service Selection]", "[Service Selection]=" & SSID), Date)
        
        ' Get data from Interventions.
        Dim Category As Integer
        Category = Nz(DLookup("[Category]", "[Service Selection 2]", "[Service Selection]=" & SSID), 2)
        If Category <> 2 Then
            If Category = 3 Then
                'Callout start = ActiveWon date and end = ActiveWon date + 1 year
                serviceSelectionStartDate = Year(dateActiveWonDate) & "-" & Month(dateActiveWonDate) & "-" & Day(dateActiveWonDate)
                serviceSelectionEndDate = (1 + Year(dateActiveWonDate)) & "-" & Month(dateActiveWonDate) & "-" & Day(dateActiveWonDate)
            Else
                'Non-maintenance, non-callout start = current date and end = 1 year in future
                serviceSelectionStartDate = Year(Date) & "-" & Month(Date) & "-" & Day(Date)
                serviceSelectionEndDate = (1 + Year(Date)) & "-" & Month(Date) & "-" & Day(Date)
            End If
        Else
            'Maintenance = Contract Start & End dates from DB
            Dim contractEndDate As Date
            contractEndDate = Nz(DLookup("[Contract End]", "[Service Selection]", "[Service Selection]=" & SSID), DateAdd("yyyy", 1, Date))
            serviceSelectionStartDate = Year(contractStartDate) & "-" & Month(contractStartDate) & "-" & Day(contractStartDate)
            'Contract End Date is the 'end date' for the integration.
            serviceSelectionEndDate = Year(contractEndDate) & "-" & Month(contractEndDate) & "-" & Day(contractEndDate)
        End If
        
        ' Get value data from Invoice (from the group invoice).
        strSql = "SELECT IIF([Category]=2,([JanD]+[FebD]+[MarD]+[AprD]+[MayD]+[JunD]+[JulD]+[AugD]+[SepD]+[OctD]+[NovD]+[DecD]),[Dolar]) AS InvoiceAmount, [FCat], [DDate] FROM [Service Selection Invoice] WHERE [Service Selection]=" & SSID & " AND [ID2]=0 AND [ID3]=0"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount = 0 Then
            rst.Close
            strSql = "SELECT IIF([Category]=2,([JanD]+[FebD]+[MarD]+[AprD]+[MayD]+[JunD]+[JulD]+[AugD]+[SepD]+[OctD]+[NovD]+[DecD]),[Dolar]) AS InvoiceAmount, [FCat], [DDate] FROM [Service Selection Invoice] WHERE [Service Selection]=" & SSID & " AND [ID2]=8"
            Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        End If
        While Not rst.EOF
            invoiceValue = Nz(rst![InvoiceAmount], 0)
            serviceSelectionInvoiceDollar = Nz(Format(invoiceValue * contractTerm, "###0.00"), "")
            serviceSelectionFCat = Nz(rst![Fcat], "")
            rst.MoveNext
        Wend
        rst.Close
        
        ' Get data from SS FCat (if available).
        If (Nz(serviceSelectionFCat, "") <> "") Then
            Dim FCID As Long
            FCID = CInt(serviceSelectionFCat)
            strSql = "SELECT [R12FcatCode], [R12ProjectType], [R12BusOffering] FROM [FCat] WHERE [FCID]=" & FCID
            Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            While Not rst.EOF
                fcatProjectType = Nz(rst![R12ProjectType], "")
                fcatCatCode = Nz(rst![R12FcatCode], "")
                ' Warranty work should have a value of $1.
                If Len(fcatCatCode) >= 4 And Left(fcatCatCode, 4) = "D890" Then
                    serviceSelectionInvoiceDollar = "1.00"
                End If
                fcatBusOffering = Nz(rst![R12BusOffering], "")
                rst.MoveNext
            Wend
            rst.Close
        End If
        
        ' Get data from Buildings.
        strSql = "SELECT [BuildingName], [Address], [City], [CountryCode], [PostalCode] FROM [BD-Buildings] WHERE [BuildingID]=" & BID
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst.EOF
            BuildingName = Nz(rst![BuildingName], "")
            buildingAddress = Nz(rst![Address], "")
            BuildingCity = Nz(rst![City], "")
            buildingCountry = Nz(rst![CountryCode], "")
            buildingPostalCode = Nz(rst![PostalCode], "")
            rst.MoveNext
        Wend
        rst.Close
        
        Dim invoiceToCID As Long
        invoiceToCID = Nz(DLookup("[InvoiceTo]", "[Service Selection Invoice]", "[Service Selection]=" & SSID & " AND [ID2]=0 AND [ID3]=0"), 0)
        If invoiceToCID = 0 Then
            invoiceToCID = Nz(DLookup("[InvoiceTo]", "[Service Selection Invoice]", "[Service Selection]=" & SSID & " AND [ID2]=8"), 0)
        End If
        R12CustomerNumber = Nz(DLookup("[R12CustomerNumber]", "[BD-Customers]", "[CustomerID]=" & Nz(invoiceToCID, 0)), "")
        r12CustomerBillToSiteId = Nz(DLookup("[R12CustBillToSiteID]", "[BD-Customers]", "[CustomerID]=" & Nz(invoiceToCID, 0)), "")
        PayTerm = Nz(DLookup("[PayTerm]", "[BD-Customers]", "[CustomerID]=" & Nz(invoiceToCID, 0)), "")
        
        Call DFSDSmartIntegrator.StartProject(AgreementNumber, projectId, R12CustomerNumber, r12CustomerBillToSiteId, PayTerm, BuildingID, BuildingName, buildingAddress, BuildingCity, buildingPostalCode, buildingCountry, organizationId, ServiceSelectionName, PurchaseNumber, SalesEngineerSalesNumber, fullfillmentSalesNumber, serviceSelectionAllowR12PoUpdate, serviceSelectionStartDate, serviceSelectionEndDate, serviceSelectionActiveWonDate, serviceSelectionInvoiceDollar, ServiceSelectionCurrencyCode, fcatProjectType, fcatCatCode, fcatBusOffering)
        
        Dim months() As Date
        Dim monthsNeed() As Date
        Dim monthCount As Integer
        Dim monthArrIndex As Integer
        Dim monthIndex As Integer
        
        Dim contractStartMonth As Integer
        Dim monthsPerTerm As Integer
        
        Dim skipFutureNonMaintenance As Boolean
        contractStartMonth = Month(contractStartDate)
        skipFutureNonMaintenance = False
        monthsPerTerm = 12
        
        strSql = "SELECT [ID2], [ID3], [Category], [Jan], [Feb], [Mar], [Apr], [May], [Jun], [Jul], [Aug], [Sep], [Oct], [Nov], [Dec] FROM [Service Selection 2] WHERE [Service Selection]=" & SSID
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst.EOF
            ' Build up a list of applicable task visit dates.
            monthIndex = contractStartMonth
            monthArrIndex = 0
            monthCount = 0
            While monthCount < monthsPerTerm * contractTerm
                If rst(monthIndex + 2) = -1 Then
                    ReDim Preserve months(0 To monthArrIndex)
                    months(monthArrIndex) = DateAdd("m", monthCount, contractStartDate)
                    monthArrIndex = monthArrIndex + 1
                End If
                monthIndex = monthIndex + 1
                If monthIndex > 12 Then
                    monthIndex = 1
                End If
                monthCount = monthCount + 1
            Wend
            
            ' Reset the month index and then iterate through each one.
            monthArrIndex = 0
            While monthArrIndex <= UBound(months)
                strSql = "SELECT [InvoiceTo], [FCat] FROM [Service Selection Invoice] WHERE [Service Selection]=" & SSID & " AND [ID2]=" & rst![ID2] & " AND [ID3]=" & rst![ID3]
                Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                While Not rst1.EOF
                    If (Nz(rst1![Fcat], "") <> "") Then
                        strSql = "SELECT [R12FcatCode], [R12FcatDescr], [R12Account] FROM [FCat] WHERE [FCID]=" & CInt(Nz(rst1![Fcat], 0))
                        Set rst2 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                        While Not rst2.EOF
                            Dim monthChars As String
                            Dim taskName As String
                            ' Ensure a 0 appears in front of any month less than 10.
                            monthChars = Month(months(monthArrIndex))
                            If Len(monthChars) = 1 Then
                                monthChars = "0" & monthChars
                            End If
                            ' Use task name also within description.
                            taskName = Year(months(monthArrIndex)) & " " & monthChars & " " & Nz(rst2![R12FcatCode], "")
                            Call DFSDSmartIntegrator.AddTaskToProject(taskName, taskName & " " & Nz(rst2![R12FcatDescr], ""), Nz(rst2![R12Account], ""))
                            
                            ' Only submit IntReq's if the office is configured for it (this is separate from the Project, R12Enabled flag).
                            If submitIntReqs Then
                                ' Add material information to the integration.
                                strSql = "SELECT [PartNumber], Sum([Qty]) AS SumQty FROM [Service Selection 3M] WHERE [Service Selection]=" & SSID & " AND [ID3]=" & rst![ID3] & " AND [R12Inclusion]=-1 AND [Qty]>0 GROUP BY [PartNumber]"
                                Set rst3 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                                If Not rst3.EOF Then
                                    rst3.MoveLast
                                    rst3.MoveFirst
                                    ' Ensure the material integration only gets instantiated once per SS AND has R12 materials to send.
                                    If rst3.RecordCount > 0 And hasMaterials = False Then
                                        Call DFSDSmartIntegrator.StartInternalRequisition(projectId, PreferredShipping, partialShip, organizationId, shipSalesOfficeOrgCode, shipName, shipAddress1, shipAddress2, shipAddress3, shipCity, shipPostalCode, shipCountry)
                                        hasMaterials = True
                                    End If
                                End If
                                While Not rst3.EOF
                                    Call DFSDSmartIntegrator.AddLineItemToInternalRequisition(rst3![PartNumber], Year(needByDate) & "-" & Month(needByDate) & "-" & Day(needByDate), rst3![SumQty], Note)
                                    rst3.MoveNext
                                Wend
                                rst3.Close
                            End If

                            rst2.MoveNext
                        Wend
                        rst2.Close
                    End If
                    rst1.MoveNext
                Wend
                rst1.Close
                monthArrIndex = monthArrIndex + 1
            Wend
            
            rst.MoveNext
        Wend
        rst.Close

        ' Finish adding the SS to the integration queues.
        Call DFSDSmartIntegrator.AddProjectToIntegrationQueue(pathToIntegrationQueue, pathToStatusIntegrationQueue)
        If hasMaterials = True Then
            Call DFSDSmartIntegrator.AddInternalRequisitionToIntegrationQueue(pathToMaterialIntegrationQueue, pathToMaterialStatusIntegrationQueue)
        End If
                
        
       'Start DFSD-156
         If (projectBillingEvent_Flag) Then
               If pathtoProjectEventsIntegrationQueue <> "" Then
                      Call IntegrateProjectEvents(SSID, organizationId, ServiceSelectionCurrencyCode, pathtoProjectEventsIntegrationQueue)
               End If
          End If
        'End DFSD-156
        
        Set dbs = Nothing
        
    End If
End Sub

Function IsCheckCategory(SSID As Long) As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Set dbs = CurrentDb
    Dim strSql As String
    Dim Flag As Boolean
    Flag = False
    
        strSql = "SELECT * FROM [Service Selection Invoice] WHERE [Service Selection] = " & SSID & " AND [Category]= 2"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            rst.Close
            Set rst = Nothing
            Set dbs = Nothing
            If (ValidateLumpsum(SSID) <> True) Then
                  Flag = True
                  Call MsgBox1(0, 5070, 0, 0, 0, 1, 0)
            End If
        Else
            strSql = "SELECT * FROM [Service Selection Invoice] WHERE [Service Selection] = " & SSID & " AND [Category]<> 2 AND Lumpsum=Yes"
            Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            If rst.RecordCount > 0 Then
                Flag = False
                rst.Close
                Set rst = Nothing
                Set dbs = Nothing
            Else
                 If (ValidateVisitDetails(SSID)) Then
                        If (ValidateDiscount(SSID)) Then
                              If (ValidateMisc(SSID)) Then
                                   Flag = False
                              Else
                                Flag = True
                                Call MsgBox1(0, 5069, 0, 0, 0, 1, 0)
                              End If
                        Else
                            Flag = True
                            Call MsgBox1(0, 5068, 0, 0, 0, 1, 0)
                        End If
                 Else
                     Flag = True
                     Call MsgBox1(0, 5067, 0, 0, 0, 1, 0)
                 End If
            End If
       End If
       IsCheckCategory = Flag
End Function

Function ValidateLumpsum(SSID As Long) As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Set dbs = CurrentDb
    Dim strSql As String
    Dim Flag As Boolean
    Flag = False
    
        strSql = "SELECT * FROM [Service Selection Invoice] WHERE [Service Selection] = " & SSID & "AND Lumpsum=Yes"
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            Flag = True
            rst.Close
            Set rst = Nothing
            Set dbs = Nothing
        End If
        ValidateLumpsum = Flag
End Function

Function ValidateVisitDetails(SSID As Long) As Boolean
    Dim dbs_T As Database
    Dim rst_T As Recordset
    Set dbs_T = CurrentDb
    
    Dim dbs_K As Database
    Dim rst_K As Recordset
    Set dbs_K = CurrentDb
    
    Dim dbs_O As Database
    Dim rst_O As Recordset
    Set dbs_O = CurrentDb
    
    Dim strSqlTH As String
    Dim StrSqlKM As String
    Dim StrSqlOther As String
       
    Dim SumT As Integer
    Dim Sumk As Integer
    Dim SumO As Integer
    Dim Flag As Boolean
    Flag = True
    
    strSqlTH = "SELECT SUM([O1T]+[O2T]+[O3T]+[O4T]+[O5T]+[O6T]+[O7T]+[O8T]+[O9T]+[O10T]+[O11T]+[O12T]) As TotalT FROM [Service Selection A] WHERE [Service Selection] = " & SSID
    StrSqlKM = "SELECT SUM([O1K]+[O2K]+[O3K]+[O4K]+[O5K]+[O6K]+[O7K]+[O8K]+[O9K]+[O10K]+[O11K]+[O12K]) As TotalK FROM [Service Selection A] WHERE [Service Selection] = " & SSID
    StrSqlOther = "SELECT SUM([O1O]+[O2O]+[O3O]+[O4O]+[O5O]+[O6O]+[O7O]+[O8O]+[O9O]+[O10O]+[O11O]+[O12O]) As TotalO FROM [Service Selection A] WHERE [Service Selection] = " & SSID
    
        
        Set rst_T = dbs_T.OpenRecordset(strSqlTH, dbOpenDynaset, dbSeeChanges)
        While Not rst_T.EOF
            SumT = Nz(rst_T![TotalT], 0)
            rst_T.MoveNext
        Wend
        rst_T.Close
        Set rst_T = Nothing
        Set dbs_T = Nothing
        
        Set rst_K = dbs_K.OpenRecordset(StrSqlKM, dbOpenDynaset, dbSeeChanges)
        While Not rst_K.EOF
            Sumk = Nz(rst_K![TotalK], 0)
            rst_K.MoveNext
        Wend
        rst_K.Close
        Set rst_K = Nothing
        Set dbs_K = Nothing
        
        Set rst_O = dbs_O.OpenRecordset(StrSqlOther, dbOpenDynaset, dbSeeChanges)
        While Not rst_O.EOF
            SumO = Nz(rst_O![TotalO], 0)
            rst_O.MoveNext
        Wend
        rst_O.Close
        Set rst_O = Nothing
        Set dbs_O = Nothing
       
        
        If SumT > 0 Or Sumk > 0 Or SumO > 0 Then
            Flag = False
        End If
        
        ValidateVisitDetails = Flag
End Function

Function ValidateDiscount(SSID As Long) As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Set dbs = CurrentDb
    Dim strSql As String
    Dim Discount As Integer
    Dim Flag As Boolean
    Flag = True
    
    strSql = "SELECT [Discount] FROM [Service Selection] WHERE [Service Selection] = " & SSID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        Discount = Nz(rst![Discount], 0)
        rst.MoveNext
    Wend
      rst.Close
      Set rst = Nothing
      Set dbs = Nothing
         
      If Discount > 0 Then
            Flag = False
      End If
      
        ValidateDiscount = Flag
End Function

Function ValidateMisc(SSID As Long) As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Set dbs = CurrentDb
    Dim strSql As String
    Dim MCost As Integer
    Dim MSell As Integer
    Dim Flag As Boolean
    Flag = True
    
    strSql = "SELECT SUM([Cost]) As SCost,SUM([Sell]) As SSell FROM [Service Selection B] WHERE [Service Selection] = " & SSID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    While Not rst.EOF
        MCost = Nz(rst![SCost], 0)
        MSell = Nz(rst![SSell], 0)
        rst.MoveNext
    Wend
        rst.Close
        Set rst = Nothing
        Set dbs = Nothing
        
        If MCost > 0 Or MSell > 0 Then
            Flag = False
        End If
        
        ValidateMisc = Flag
End Function

Public Function GetProjectIntegrationStatus(SSID As Long) As String
    Dim projectStatus As String
    Dim pathToStatusIntegrationQueue As String
    
    ' All non-R12 enabled offices will return APPROVED and all SS'es in R12 enabled offices pre-R12 enabling will still work.
    projectStatus = "APPROVED"
    pathToStatusIntegrationQueue = Nz(DLookup("R12StatusIntegrationQueuePath", "DocumentPaths"), "")
    
    ' If there is a path to the queue, then we can assume they want to run the integration and check the status from the queue.
    If pathToStatusIntegrationQueue <> "" Then
        Dim DFSDSmartIntegrator As New DFSDComIntegrator.OutboundDatabaseIntegrator
        projectStatus = DFSDSmartIntegrator.GetProjectStatus(CStr(SSID), pathToStatusIntegrationQueue)
    End If
    
    If InStr(1, UCase(projectStatus), "APPROVED") <> 0 Then
        If Nz(DLookup("R12ProjectStatus", "[Service Selection]", "[Service Selection]=" & SSID), "") = "" Then
            projectStatus = "USER APPROVED, NOT INTERFACED"
        Else
            projectStatus = "APPROVED"
        End If
    End If
    
    GetProjectIntegrationStatus = projectStatus
End Function

Public Function GetInternalRequisitionIntegrationStatus(SSID As Long) As String
    Dim internalRequisitionStatus As String
    Dim pathToStatusIntegrationQueue As String
    
    ' All non-R12 enabled offices will return APPROVED and all SS'es in R12 enabled offices pre-R12 enabling will still work.
    internalRequisitionStatus = "APPROVED"
    pathToStatusIntegrationQueue = Nz(DLookup("R12MaterialStatusIntegrationQueuePath", "DocumentPaths"), "")
    
    ' If there is a path to the queue, then we can assume they want to run the integration and check the status from the queue.
    If pathToStatusIntegrationQueue <> "" Then
        Dim DFSDSmartIntegrator As New DFSDComIntegrator.OutboundDatabaseIntegrator
        internalRequisitionStatus = DFSDSmartIntegrator.GetInternalRequisitionStatus(CStr(SSID), pathToStatusIntegrationQueue)
    End If
    
    If InStr(1, UCase(internalRequisitionStatus), "APPROVED") <> 0 Then
        If Nz(DLookup("R12ProjectStatus", "[Service Selection]", "[Service Selection]=" & SSID), "") = "" Then
            internalRequisitionStatus = "USER APPROVED, NOT INTERFACED"
        Else
            internalRequisitionStatus = "APPROVED"
        End If
    End If
    
    GetInternalRequisitionIntegrationStatus = internalRequisitionStatus
End Function

'---------------------------------------------------------------------------------------
' Procedure : TestIntegratePartSalesToR12
' Author    : irhcio
' Date      : 02/04/2017
' Purpose   : Test changes made as part of JIRA DFSD-58
'---------------------------------------------------------------------------------------
'
Public Function TestIntegratePartSalesToR12(PartQtId As Long, shipPartySiteId As String, shipName As String, shipAddress1 As String, shipAddress2 As String, shipAddress3 As String, shipCity As String, shipPostalCode As String, shipCountry As String, Note As String, needByDate As Date, p_pre_payment_pct As Variant, p_pre_payment_local_amt As Variant, p_ship_from_org As Variant, p_pre_payment_invoice_linetype As Variant, p_pre_payment_credit_linetype As Variant) As String

   Call IntegratePartSalesToR12(PartQtId, shipPartySiteId, shipName, shipAddress1, shipAddress2, shipAddress3, shipCity, shipPostalCode, shipCountry, Note, needByDate, "N", p_pre_payment_pct, p_pre_payment_local_amt, p_ship_from_org, p_pre_payment_invoice_linetype, p_pre_payment_credit_linetype)

   TestIntegratePartSalesToR12 = "done"
   
End Function

 Public Sub IntegratePartSalesToR12(PartQtId As Long, shipPartySiteId As String, shipName As String, shipAddress1 As String, shipAddress2 As String, shipAddress3 As String, shipCity As String, shipPostalCode As String, shipCountry As String, Note As String, needByDate As Date, bookedFlag As String, p_pre_payment_pct As Variant, p_pre_payment_local_amt As Variant, p_ship_from_org As Variant, p_pre_payment_invoice_linetype As Variant, p_pre_payment_credit_linetype As Variant)
    
    Dim pathToPartSalesntegrationQueue As String
    Dim pathToPartSalesStatusIntegrationQueue As String
    Dim partSalesToR12Enabled As Boolean
    
    pathToPartSalesntegrationQueue = Nz(DLookup("R12PartSalesIntegrationQueuePath", "DocumentPaths"), "")
    pathToPartSalesStatusIntegrationQueue = Nz(DLookup("R12PartSalesStatusIntegrationQueuePath", "DocumentPaths"), "")
    partSalesToR12Enabled = (Nz(DLookup("R12EnablePartSalesIntf", "Office"), 0) = -1)
    
    ' If part sales are enabled and there is a path to the queue, then we can assume they want to run the integration.
    If partSalesToR12Enabled And pathToPartSalesntegrationQueue <> "" And pathToPartSalesStatusIntegrationQueue <> "" Then
        
        Dim DFSDSmartIntegrator As New DFSDComIntegrator.OutboundDatabaseIntegrator
        Dim dbs As Database
        Dim rst As Recordset
        Dim rst1 As Recordset
        Dim strSql As String
        Dim DirLocSR As String
        Dim dbs_sr As Database
        Dim rst_sr As Recordset
    
        Dim CID As Long
        Dim PQID As String
        Dim OUCode As String
        Dim OUId As String
        Dim CountryId As String
        Dim ListPriceListName As String
        Dim R12CustomerNumber As String
        Dim R12InvoicePartySiteId As String
        Dim PayTerm As String
        Dim PurchaseNumber As String
        Dim SalesEngSalesNumber As String
        Dim PqCurrencyCode As String
        Dim PreferredShipping As String
        Dim PreferredShippingName As String
        Dim RequestedDate As String
        Dim OrderEmail As String
        Dim Freight As String
        Dim BranchLocationNum As String
        Dim SalesEngOfficeId As Integer
        Dim hasMaterials As Boolean
               
        CID = 0
        PQID = CStr(PartQtId)
        OUCode = ""                             ' from Office
        OUId = ""
        CountryId = ""
        R12CustomerNumber = ""                  ' from Customer by part quote customer ID
        R12InvoicePartySiteId = ""              ' same
        PayTerm = "NET 30"                      ' same
        PurchaseNumber = ""                      ' from part quote
        SalesEngSalesNumber = ""                ' from SalesEngineer by ID from part quote
        PqCurrencyCode = "EUR"                  ' translate from FEX
        PreferredShipping = "300"               ' from PreferredShipping by id in part quote
        RequestedDate = Year(needByDate) & "-" & Month(needByDate) & "-" & Day(needByDate)
        BranchLocationNum = ""
        SalesEngOfficeId = 0
        hasMaterials = False

        
        Set dbs = CurrentDb
        
        ' Get data from Part Quote
        strSql = "SELECT [CustomerID], [SEID], [PONumber], [CurrencyCode], [RequiredDate], [Transport], Nz([TransportAmount], 0) AS TransportAmt FROM [Parts Quotation] WHERE [PartQuotationID]=" & PartQtId
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst.EOF
            CID = Nz(rst![CustomerID], 0)
            PurchaseNumber = Nz(rst![PONumber], "")
            PqCurrencyCode = Nz(rst![CurrencyCode], Nz(DLookup("[CurrencyCode]", "[Currency]", "[FX]=1"), "EUR"))
            SalesEngSalesNumber = Nz(DLookup("[SalesNumber]", "[SalesEngineers]", "[SalesEngineerID]=" & Nz(rst![SEID], 0)), "")
            SalesEngOfficeId = Nz(DLookup("[OfficeID]", "[SalesEngineers]", "[SalesEngineerID]=" & Nz(rst![SEID], 0)), 0)
            OrderEmail = Nz(DLookup("[Email]", "[SalesEngineers]", "[SalesEngineerID]=" & Nz(rst![SEID], 0)), "")
            Freight = CStr(rst![TransportAmt])
            PreferredShippingName = Nz(DLookup("[Name]", "[Preferred Shipping]", "[ID]=" & Nz(rst![Transport], 0)), "Standard Priority")
            If PreferredShippingName = "Priority Customer" Then
                PreferredShipping = "100"
            ElseIf PreferredShippingName = "Emergency" Then
                PreferredShipping = "200"
            ElseIf PreferredShippingName = "Standard Priority" Then
                PreferredShipping = "300"
            ElseIf PreferredShippingName = "Stock" Then
                PreferredShipping = "400"
            End If
            rst.MoveNext
        Wend
        rst.Close
        
        ' if email is still empty or the default of '-', get the default entry from the Office table
        If OrderEmail = "" Or OrderEmail = "-" Then
            OrderEmail = Nz(DLookup("[R12OrderEmail]", "[Office]", "[R12EnablePartSalesIntf]=-1"), "")
        End If

        Dim invoiceToCID As Long
        invoiceToCID = Nz(DLookup("[InvoiceTo]", "[Parts Quotation Invoice]", "[PartQuotationID]=" & PartQtId), 0)
        If invoiceToCID = 0 Then
            invoiceToCID = CID
        End If
        
        R12CustomerNumber = Nz(DLookup("[R12CustomerNumber]", "[BD-Customers]", "[CustomerID]=" & Nz(invoiceToCID, 0)), "")
        R12InvoicePartySiteId = Nz(DLookup("[R12PartySiteID]", "[BD-Customers]", "[CustomerID]=" & Nz(invoiceToCID, 0)), "")
        PayTerm = Nz(DLookup("[PayTerm]", "[BD-Customers]", "[CustomerID]=" & Nz(invoiceToCID, 0)), "")
        
        ' get data from SR db
        OUCode = Nz(DLookup("[R12OUCode]", "[Office]", "[R12EnablePartSalesIntf]=-1"), "")
        DirLocSR = Preferences.GetSRFileLocation
        Set dbs_sr = DBEngine.Workspaces(0).OpenDatabase(DirLocSR & "dfsd_sr.mdb", False, False, ";PWD=dfsadmin")
        
        ' get Operating Unit ID and Price List name using Office setting for the Operating Unit Code
        strSql = "SELECT [Countries].[CountryPartOraclePriceListName], [Country Operating Unit Codes].[UnitId] FROM [Countries], [Country Operating Unit Codes] WHERE [Countries].[CountryID]=[Country Operating Unit Codes].[CountryID] AND [Country Operating Unit Codes].[UnitCode]='" & OUCode & "'"
        Set rst_sr = dbs_sr.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst_sr.EOF
            OUId = rst_sr![UnitId]  ' operating unit id
            ListPriceListName = rst_sr![CountryPartOraclePriceListName] ' list-rice list name
            rst_sr.MoveNext
        Wend
        rst_sr.Close
        
        ' get Branch Location using the Org Code associated with the sales office of the Sales Engineer (SEID) for the order
        ' Branch Location could end up being empty if no sales eng, sales eng not assiged to office, no sales offices set up
        ' from SaleEngOfficeID, look up the OrgId for office then look up the branch
        Dim salesengOrgCd As String
        salesengOrgCd = Nz(DLookup("[OrgID]", "[SalesOffice]", "[OfficeID]=" & SalesEngOfficeId), "")
        ' if the org code is null, get any org code for any office in the sales office tabel something
        If salesengOrgCd = "" Then
            salesengOrgCd = Nz(DLookup("[OrgID]", "[SalesOffice]"), "")
        End If
        strSql = "SELECT [Country Organization Codes].[BranchLocNum] FROM [Country Organization Codes] WHERE [Country Organization Codes].[OrganizationCode]='" & salesengOrgCd & "'"
        Set rst_sr = dbs_sr.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If Not rst_sr.EOF Then
            ' should never be more than one
            BranchLocationNum = Nz(rst_sr![BranchLocNum], "")
        End If
        rst_sr.Close

        Set dbs_sr = Nothing

      
        ' after verifying that there are parts to order, create the order, then add the part order lines
        ' this sql will combine lines with identical part numbers, ie, R12 will only receive one line per part number
        'DFSD-154
        strSql = "SELECT [PartNumber], " & _
                        "Sum(Nz([Qty],0)) AS SumQty, " & _
                        "Sum(Nz([SellPrice],0)) AS SumSellingPrice, " & _
                        "Sum(Nz([listPrice],0)* Nz([Qty],0)) AS SumlistPrice, " & _
                        "Nz([Unit],'') AS Uom, " & _
                        "[WH], " & _
                        "avg(Nz([discount],0)) AS avgDiscount " & _
                        "FROM [Parts Quotation 1] " & _
                        "WHERE [PartQuotationID] =" & PQID & " " & _
                        "AND [Qty]>0 " & _
                        "GROUP BY [PartNumber],[Unit],[WH] "

        Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If Not rst1.EOF Then
            rst1.MoveLast
            rst1.MoveFirst
            ' Ensure the material integration only gets instantiated once per SS AND has R12 materials to send.
            If rst1.RecordCount > 0 And hasMaterials = False Then
                Call DFSDSmartIntegrator.StartPartSalesOrder(PQID, OUId, R12CustomerNumber, R12InvoicePartySiteId, PayTerm, shipName, shipAddress1, shipAddress2, shipAddress3, shipCity, shipPostalCode, shipCountry, shipPartySiteId, PreferredShipping, PurchaseNumber, SalesEngSalesNumber, RequestedDate, _
                                                                PqCurrencyCode, Note, ListPriceListName, OrderEmail, Freight, BranchLocationNum, bookedFlag, p_pre_payment_pct, p_pre_payment_local_amt, p_ship_from_org, p_pre_payment_invoice_linetype, p_pre_payment_credit_linetype)
                hasMaterials = True
            End If
        End If
        
        ' add the individual order lines to sales order
        Dim strUnitSellingPrice As String
        Dim UnitSellingPrice As Double
        Dim strUnitListPrice As String
        Dim UnitListPrice As Double
        
        While Not rst1.EOF
            'Calculate Unit Selling Price
            If (rst1![SumSellingPrice] = 0) Or (rst1![SumQty] = 0) Then
                UnitSellingPrice = 0
            ElseIf rst1![SumQty] = 1 Then
                UnitSellingPrice = rst1![SumSellingPrice]
            Else
                UnitSellingPrice = rst1![SumSellingPrice] / rst1![SumQty]
            End If
                       
            strUnitSellingPrice = Replace(CStr(UnitSellingPrice), ",", ".")
                        
            'DFSD-154 Calculate Unit List Price
            If (rst1![SumListPrice] = 0) Or (rst1![SumQty] = 0) Then
                UnitListPrice = 0
            ElseIf rst1![SumQty] = 1 Then
                UnitListPrice = rst1![SumListPrice]
            Else
                UnitListPrice = rst1![SumListPrice] / rst1![SumQty]
            End If
                       
            strUnitListPrice = Replace(CStr(UnitListPrice), ",", ".")
                        
                        
            'AddLineToInternalReq
            Call DFSDSmartIntegrator.AddLineItemToPartSalesOrder(rst1![PartNumber], CStr(rst1![SumQty]), rst1![Uom], strUnitSellingPrice, Nz(rst1!WH, ""), _
                                                                strUnitListPrice, ((strUnitListPrice - strUnitSellingPrice) / strUnitListPrice) * 100)
            rst1.MoveNext
        Wend
        rst1.Close


        ' Finish adding the parts order info to the integration queue.
        If hasMaterials = True Then
            Call DFSDSmartIntegrator.AddPartSalesOrderToIntegrationQueue(pathToPartSalesntegrationQueue, pathToPartSalesStatusIntegrationQueue)
        End If
        
        Set dbs = Nothing
        
    End If  ' partSales enabled and all paths have values
    
End Sub

Public Function GetPartSalesIntegrationStatus(PQID As Long) As String

    Dim partSalesOrderStatus As String
    Dim pathToStatusIntegrationQueue As String
    
    ' All non-R12 enabled offices will return APPROVED and all SS'es in R12 enabled offices pre-R12 enabling will still work.
    'partSalesOrderStatus = "APPROVED"
    partSalesOrderStatus = "INTERFACED"
    pathToStatusIntegrationQueue = Nz(DLookup("R12PartSalesStatusIntegrationQueuePath", "DocumentPaths"), "")
    
    ' If there is a path to the queue, then we can assume they want to run the integration and check the status from the queue.
    If pathToStatusIntegrationQueue <> "" Then
        Dim DFSDSmartIntegrator As New DFSDComIntegrator.OutboundDatabaseIntegrator
        partSalesOrderStatus = DFSDSmartIntegrator.GetPartOrderStatus(CStr(PQID), pathToStatusIntegrationQueue)
    End If
    
    ' BOOKED, CLOSED or ENTERED will all signify that the Parts Sales Order has been interfaced to R12
    If (InStr(1, UCase(partSalesOrderStatus), "BOOKED") <> 0) Or (InStr(1, UCase(partSalesOrderStatus), "CLOSED") <> 0) Or (InStr(1, UCase(partSalesOrderStatus), "ENTERED") <> 0) Then
        If Nz(DLookup("R12PartOrderStatus", "[Parts Quotation]", "[PartQuotationID]=" & PQID), "") = "" Then
            partSalesOrderStatus = "USER APPROVED, NOT INTERFACED"
        Else
            partSalesOrderStatus = "INTERFACED"
        End If
    End If
    
    GetPartSalesIntegrationStatus = partSalesOrderStatus
    
End Function

Public Function IsSequenceable(SSID As Long) As Boolean
    Dim canBeSequenced As Boolean
    Dim projectStatus As String
    Dim intReqStatus As String
    
    canBeSequenced = True
    projectStatus = GetProjectIntegrationStatus(SSID)
    If InStr(1, UCase(projectStatus), "APPROVED") = 0 Then
        canBeSequenced = False
    Else
        intReqStatus = GetInternalRequisitionIntegrationStatus(SSID)
        If InStr(1, UCase(intReqStatus), "APPROVED") = 0 Then
            canBeSequenced = False
        End If
    End If
    
    IsSequenceable = canBeSequenced
End Function

Public Sub IntegrateProjectEvents(p_SSID As Long, p_organizationId As String, p_currency_code As String, p_json_path As String)

    Dim DFSDSmartIntegrator As New DFSDComIntegrator.OutboundDatabaseIntegrator
    Dim dbs As Database
    Dim strSQL_Ser_Sel_lumpsum As String
    Dim rstSQL_Ser_Sel_lumpsum As DAO.Recordset
    Dim strSQL_labor As String
    Dim strSQL_parts As String
    Dim rstFCAT_Lumpsum_XREF As DAO.Recordset
    Dim strSQL_Summary As String
    Dim rstSQL_Summary As DAO.Recordset
    Dim strSQL_Maintenance_Summary As String
    Dim rstSQL_Maintenance_Summary As DAO.Recordset
    Dim arrDate() As Date
    Dim i As Integer
    Dim j As Integer
    Dim strMonthNameEng As String
    Dim strSQL_labor_and_parts As String
    Dim rstSQL_labor_and_parts As DAO.Recordset
    Dim intPriceBy As Integer
    Dim strSQL_InterventionNames As String
    Dim rstSQL_InterventionNames As DAO.Recordset
    Dim strInterventionNames As String

    Set dbs = CurrentDb
    
    strSQL_Ser_Sel_lumpsum = "SELECT [fcat],[lumpsum],[category] FROM [service selection invoice] where [service selection] = " _
                                & p_SSID & " AND [ID2]=0 AND [ID3]=0"
    
    Set rstSQL_Ser_Sel_lumpsum = dbs.OpenRecordset(strSQL_Ser_Sel_lumpsum)

    Set rstFCAT_Lumpsum_XREF = fGet_FCAT_Lumpsum_XREF(rstSQL_Ser_Sel_lumpsum.Fields("Fcat"), rstSQL_Ser_Sel_lumpsum.Fields("LumpSum"))
    
    If Not rstFCAT_Lumpsum_XREF.EOF Then
        If rstFCAT_Lumpsum_XREF.Fields("linetype") = "SUMMARY" Then 'Summary payload
    
            Call DFSDSmartIntegrator.StartProjectBillingEvent(p_SSID, _
                                                         p_organizationId, _
                                                         p_currency_code)
    
            If rstSQL_Ser_Sel_lumpsum.Fields("category") = 2 Then ' Maintenance
                                
                strSQL_Maintenance_Summary = "SELECT " & _
                                            "e.[contract start], " & _
                                            "e.[contract end], " & _
                                            "e.[contract term], " & _
                                            "d.event_type, " & _
                                            "a.[Service Intervention] as description, " & _
                                            "a.[Service Selection], " & _
                                            "format(a.ddate,""mm/dd/yyyy"") as completion_date, " & _
                                            "1 AS quantity_billed, " & _
                                            "c.CurrencyCode, " & _
                                            "a.janD as Jan, a.febD as Feb, a.marD as Mar, a.aprD as Apr, a.mayD as May, a.junD as Jun, " & _
                                            "a.julD as Jul, a.augD as Aug, a.sepD as Sep, a.octD as Oct, a.novD as Nov, A.DecD as Dec " & _
                                            "FROM [Currency] AS c,fcat_lumpsum_xref as d " & _
                                            "INNER Join " & _
                                            "( " & _
                                            "   [service selection Invoice] AS a " & _
                                            "   INNER Join " & _
                                            "   ( " & _
                                            "      [service selection] as e ) ON e.[Service Selection] = a.[Service Selection] " & _
                                            ") " & _
                                            "ON d.FCID = a.Fcat " & _
                                            "WHERE C.FX = 1 " & _
                                            "And d.linetype = ""SUMMARY"" " & _
                                            "and a.id2 =0 and id3 = 0 " & _
                                            "and A.[Service Selection] = " & p_SSID
                                            
                                            
                Set rstSQL_Maintenance_Summary = dbs.OpenRecordset(strSQL_Maintenance_Summary)
                
                'For Maintenance jobs Summary events the requirment is to send a seperate billing event for each month that
                'invoicing takes place, spanning the entire length of the contract. So if no of interventions = 2 each with
                '2 visits per year and contract term = 2 that means 8 billing events
                
                'Generate array of dates spanning contract term
                arrDate() = getContractMonths(rstSQL_Maintenance_Summary.Fields("Contract Start"), rstSQL_Maintenance_Summary.Fields("Contract End"))
                
                While Not rstSQL_Maintenance_Summary.EOF
                
                    'Validate Order Timetable. Warn users if no values are entered
                    With rstSQL_Maintenance_Summary
                        If .Fields("Jan") + .Fields("Feb") + .Fields("Mar") + .Fields("Apr") + .Fields("May") + .Fields("Jun") _
                           + .Fields("Jul") + .Fields("Aug") + .Fields("Sep") + .Fields("Oct") + .Fields("Nov") + .Fields("Dec") = 0 Then
                            MsgBox "Order Timetable does not contain any billing amounts. No Billing Events will be sent to R12", vbOKOnly
                        End If
                    End With
                    
                    'Loop thru months of contract
                    For i = 0 To UBound(arrDate())
                        'Loop through fields of recordset searching for corresponding month
                        For j = 0 To rstSQL_Maintenance_Summary.Fields.Count - 1
                            'Use Choose() function to derive monthname in English. Using MonthName() function not safe as it employes user locale
                            strMonthNameEng = Choose(Month(arrDate(i)), "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
                            'If a match is found with value > 0 then create billing event for that month
                            If strMonthNameEng = rstSQL_Maintenance_Summary.Fields(j).Name And rstSQL_Maintenance_Summary.Fields(j).Value > 0 Then
                        
                                'Get list of intervention names included in that month
                                strSQL_InterventionNames = "SELECT [Service Intervention] " & _
                                                          "from [service selection 2] as a " & _
                                                          "where a.[Service Selection] = " & p_SSID & " " & _
                                                          "and a." & strMonthNameEng & " = -1"
                                
                                Set rstSQL_InterventionNames = dbs.OpenRecordset(strSQL_InterventionNames)
                                
                                'Wipe contents of strInterventionNames
                                strInterventionNames = ""
                                
                                'Loop through intervention names and concatenate together
                                While Not rstSQL_InterventionNames.EOF
                                    strInterventionNames = Trim(strInterventionNames & " | " & rstSQL_InterventionNames.Fields("Service Intervention"))
                                    rstSQL_InterventionNames.MoveNext
                                Wend
                                                          
                                'Trim Leading pipe
                                If InStr(strInterventionNames, "|") = 1 Then
                                    strInterventionNames = Right(strInterventionNames, Len(strInterventionNames) - 1)
                                End If
                                
                                'Truncate strInterventionNames to 250 chars
                                strInterventionNames = Trim(Left(strInterventionNames, 250))
                            
                                'Add Billing Event
                                If rstFCAT_Lumpsum_XREF.Fields("billing_line") = "SUMMARY" Then
                                    Debug.Print arrDate(i) & " " & strMonthNameEng & " " & Year(arrDate(i)) & " " & rstSQL_Maintenance_Summary.Fields(j).Value
                            
                                    Call DFSDSmartIntegrator.AddEventToProjectBilling(p_SSID, rstSQL_Maintenance_Summary.Fields("event_type"), strInterventionNames, _
                                                                                       "Y", "", arrDate(i), rstSQL_Maintenance_Summary.Fields("quantity_billed"), rstSQL_Maintenance_Summary.Fields(j).Value, _
                                                                                        rstSQL_Maintenance_Summary.Fields(j).Value, 0, "")
                                End If
                                
                                'Add Revenue Event
                                If rstFCAT_Lumpsum_XREF.Fields("revenue_line") = "SUMMARY" Then
                            
                                    Call DFSDSmartIntegrator.AddEventToProjectBilling(p_SSID, rstSQL_Maintenance_Summary.Fields("event_type"), strMonthNameEng & " " & Year(arrDate(i)), _
                                                                                    "", "Y", arrDate(i), 0, 0, _
                                                                                    0, rstSQL_Maintenance_Summary.Fields(j).Value, "")
                                End If
                            
                            End If
                        Next j
                    Next i
                    
                    rstSQL_Maintenance_Summary.MoveNext
                Wend
                                
            Else
                strSQL_Summary = "SELECT " & _
                                 "d.event_type, " & _
                                 "a.[Service Intervention Description] as description, " & _
                                 "a.[Service Selection], " & _
                                 "format(a.ddate,""mm/dd/yyyy"") as completion_date, " & _
                                 "1 AS quantity_billed, " & _
                                 "c.CurrencyCode, " & _
                                 "a.Dolar AS unit_price, " & _
                                 "a.Dolar AS bill_trans_bill_amount " & _
                                 "FROM [Currency] AS c,fcat_lumpsum_xref as d " & _
                                 "INNER Join " & _
                                 "( " & _
                                 "   [service selection Invoice] AS a " & _
                                 "   INNER JOIN [service selection 2] AS b ON a.[Service Selection] = b.[Service Selection] " & _
                                 "   AND a.ID3 = b.ID3 " & _
                                 ") " & _
                                 "on  d.FCID = a.Fcat " & _
                                 "WHERE C.FX = 1 " & _
                                 "And d.linetype = ""SUMMARY"" " & _
                                 "and A.[Service Selection] = " & p_SSID & " " & _
                                 "order by a.ddate"
    
                Set rstSQL_Summary = dbs.OpenRecordset(strSQL_Summary)
                While Not rstSQL_Summary.EOF
                'Add Billing Event
                    If rstFCAT_Lumpsum_XREF.Fields("billing_line") = "SUMMARY" Then
                
                        Call DFSDSmartIntegrator.AddEventToProjectBilling(p_SSID, rstSQL_Summary.Fields("event_type"), rstSQL_Summary.Fields("description"), _
                                                                        "Y", "", rstSQL_Summary.Fields("completion_date"), rstSQL_Summary.Fields("quantity_billed"), rstSQL_Summary.Fields("unit_price"), _
                                                                        rstSQL_Summary.Fields("bill_trans_bill_amount"), 0, "")
                    End If
                'Add Revenue Event
                    If rstFCAT_Lumpsum_XREF.Fields("revenue_line") = "SUMMARY" Then
                
                        Call DFSDSmartIntegrator.AddEventToProjectBilling(p_SSID, rstSQL_Summary.Fields("event_type"), rstSQL_Summary.Fields("description"), _
                                                                        "", "Y", rstSQL_Summary.Fields("completion_date"), 0, 0, _
                                                                        0, rstSQL_Summary.Fields("bill_trans_bill_amount"), "")
                    End If
        
                    rstSQL_Summary.MoveNext
                Wend
                
            End If
          
            Call DFSDSmartIntegrator.AddProjectBillingToQueue(p_json_path)

        ElseIf (rstFCAT_Lumpsum_XREF.Fields("linetype") = "OTHER" Or rstFCAT_Lumpsum_XREF.Fields("linetype") = "PARTS") And _
        rstSQL_Ser_Sel_lumpsum.Fields("category") <> 2 Then
    
        'Not a Maintenance Job (Category = 2) so we CAN send detailed billing events
            
            Call DFSDSmartIntegrator.StartProjectBillingEvent(p_SSID, _
                                                         p_organizationId, _
                                                         p_currency_code)
                                                         
            strSQL_labor = " SELECT " & _
                            "a.[service intervention], " & _
                            "a.[service intervention description] as description, " & _
                            "a.id3, " & _
                            "format(a.ddate,'mm/dd/yyyy') as completion_date, " & _
                            "iif(b.[Time] <1, 1, b.[Time]) as quantity_billed, " & _
                            "iif(b.[Time] < 1, iif(b.others1value > 0, (1-(b.others1value/100))*[Time] * b.[labrate],[Time] * b.[labrate]), iif(b.others1value > 0,  (1-(b.others1value/100))*b.[labrate],b.[labrate]))  as unit_price, " & _
                            "iif(b.others1value > 0,(1-(b.others1value/100))*b.[Time] * b.[labrate], b.[Time] * b.[labrate]) as bill_trans_bill_amount, " & _
                            "b.[labrate] as attribute14, " & _
                            "C.CurrencyCode, " & _
                            "d.event_type " & _
                            "from [Currency] AS c,fcat_lumpsum_xref as d " & _
                            "INNER Join " & _
                            "(" & _
                            "   [service selection invoice] as A " & _
                            "   INNER JOIN [calculation Pad Query1B] as B ON a.[service selection] = b.[service selection] " & _
                            "   and a.ID3 = b.ID3 " & _
                            ")" & _
                            "ON d.Lumpsum = a.Lumpsum " & _
                            "AND d.FCID = a.Fcat " & _
                            "where C.FX = 1 " & _
                            "and d.linetype = ""OTHER"" " & _
                            "and a.[Service selection] = " & p_SSID

            intPriceBy = DLookup("PriceBy", "Office")
            
            strSQL_parts = "SELECT " & _
                            "a.[service selection], " & _
                            "b.partnumber & "" - "" & b.description as description, " & _
                            "a.id3, " & _
                            "format(a.ddate,'mm/dd/yyyy') as completion_date, " & _
                            "b.qty as  quantity_billed, " & _
                            "iif(" & intPriceBy & " = 1,iif(b.discount > 0,((1 - (b.discount/100))*b.cost*b.margin),b.cost*b.margin),iif(b.discount > 0,((1 - (b.discount/100))*b.listprice),b.listprice)) as unit_price," & _
                            "iif(" & intPriceBy & " = 1,iif(b.discount > 0,((1 - (b.discount/100))*b.cost*b.margin)*b.qty,b.qty*b.cost*b.margin),iif(b.discount > 0,((1 - (b.discount/100))*b.listprice)*b.qty,b.qty*b.listprice)) as bill_trans_bill_amount," & _
                            "iif(" & intPriceBy & " = 1,b.cost*b.margin , b.listprice) as attribute14, " & _
                            "c.CurrencyCode, " & _
                            "d.event_type " & _
                            "from [Currency] AS c,fcat_lumpsum_xref as d " & _
                            "INNER Join " & _
                            "(" & _
                            "   [service selection invoice] as A " & _
                            "   INNER JOIN [Service Selection 3M] as B ON a.[service selection] = b.[service selection] " & _
                            "   and a.ID3 = b.ID3 " & _
                            ") " & _
                            "ON d.Lumpsum = a.Lumpsum " & _
                            "AND d.FCID = a.Fcat " & _
                            "where C.FX = 1 " & _
                            "and d.linetype = ""PARTS"" " & _
                            "and a.[Service selection] =" & p_SSID

                
            strSQL_labor_and_parts = "SELECT * FROM (" & strSQL_labor & " UNION " & strSQL_parts & ") " & _
                                     "order by completion_date, id3"
                
            Set rstSQL_labor_and_parts = dbs.OpenRecordset(strSQL_labor_and_parts)
                    
            While Not rstSQL_labor_and_parts.EOF
            'Add Billing Event
                If rstFCAT_Lumpsum_XREF.Fields("billing_line") = "DETAILED" Then
            
                    Call DFSDSmartIntegrator.AddEventToProjectBilling(p_SSID, rstSQL_labor_and_parts.Fields("event_type"), rstSQL_labor_and_parts.Fields("description"), _
                                                                    "Y", "", rstSQL_labor_and_parts.Fields("completion_date"), rstSQL_labor_and_parts.Fields("quantity_billed"), _
                                                                    rstSQL_labor_and_parts.Fields("unit_price"), rstSQL_labor_and_parts.Fields("bill_trans_bill_amount"), _
                                                                    0, rstSQL_labor_and_parts.Fields("attribute14"))
                End If
            'Add Revenue Event
                If rstFCAT_Lumpsum_XREF.Fields("revenue_line") = "DETAILED" Then
            
                    Call DFSDSmartIntegrator.AddEventToProjectBilling(p_SSID, rstSQL_labor_and_parts.Fields("event_type"), rstSQL_labor_and_parts.Fields("description"), _
                                                                    "", "Y", rstSQL_labor_and_parts.Fields("completion_date"), 0, 0, _
                                                                    0, rstSQL_labor_and_parts.Fields("bill_trans_bill_amount"), "")
                End If
    
                rstSQL_labor_and_parts.MoveNext
            Wend
            
            Call DFSDSmartIntegrator.AddProjectBillingToQueue(p_json_path)
        
        ElseIf (rstFCAT_Lumpsum_XREF.Fields("linetype") = "OTHER" Or rstFCAT_Lumpsum_XREF.Fields("linetype") = "PARTS") And _
        rstSQL_Ser_Sel_lumpsum.Fields("category") = 2 Then

        'Maintenance Job (Category = 2) so we CANNOT send detailed billing events
            
            MsgBox "Detailed Project Billing Events will not be sent for Maintenance Jobs", vbOKOnly
            
            
                
        End If
    
    End If

End Sub

Public Function fGet_FCAT_Lumpsum_XREF(p_fcid As Variant, p_LumpSum As Variant) As DAO.Recordset
Dim MyDB As Database
Dim MyRS As Recordset
Dim strSql As String
 

strSql = "Select * From FCAT_Lumpsum_XREF where FCID = " & p_fcid & " and lumpsum = " & p_LumpSum
 
Set MyDB = CurrentDb
Set MyRS = MyDB.OpenRecordset(strSql, dbOpenSnapshot)
 
'Set the return value of the Function = a DAO Recordset
Set fGet_FCAT_Lumpsum_XREF = MyRS
End Function
Public Function getContractMonths(p_contract_start_dt, p_contract_end_dt) As Date()
 Dim arrDate() As Date
 Dim vdate As Date
 Dim i As Integer
 
 i = 0
 vdate = p_contract_start_dt
 While DateSerial(Year(p_contract_end_dt), Month(p_contract_end_dt), 0) > DateSerial(Year(vdate), Month(vdate), 0)
    ReDim Preserve arrDate(0 To i)
    arrDate(i) = DateSerial(Year(vdate), Month(vdate) + 1, 0)
    vdate = DateSerial(Year(vdate), Month(vdate) + 1, 1)
    i = i + 1
 Wend
 
 getContractMonths = arrDate()

End Function

Public Sub TestIntegrateProjectEvents()
'Call IntegrateProjectEvents(142101796, "OFD", "EUR","E:\DEV\Database\DFSD_R12\DFSDIntegrationQueue_ProjectEvents.json")
'Call IntegrateProjectEvents(142101781, "OFD", "EUR","E:\DEV\Database\DFSD_R12\DFSDIntegrationQueue_ProjectEvents.json")
Call IntegrateProjectEvents(156014116, "OFD", "EUR", "E:\DEV\Database\DFSD_R12\DFSDIntegrationQueue_ProjectEvents.json")
End Sub









