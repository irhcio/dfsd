Option Compare Database
Option Explicit

Public Sub PruneServiceSelections(p_CutoffDate As Date)

    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "PruneServiceSelections", "Start PruneServiceSelections ", Null, LogIDType.ServiceSelection
    'Delete Temp Tables
    Delete_Temp_Tables ("MMG_Completed_Interventions")
    Delete_Temp_Tables ("MMG_Intervention_Timetable")
    Delete_Temp_Tables ("MMG_Full_Intervention_backlog")
    Delete_Temp_Tables ("MMG_SSID_FOR_PRUNING")
    Delete_Temp_Tables ("MMG_SSID_COMPLETED_AFTER_CUTOFF")
  
    'Populate Temp Tables
    Populate_Temp_Tables (p_CutoffDate)
    
    'Prune tables that require join to [Service Selection 1]
    Prune_SerSel_1_Tables ("[Service Selection 1A]")
    
    'Prune tables that require join to [Service Selection 2]
    Prune_SerSel_2_Tables ("[Service Selection 2A]")
    Prune_SerSel_2_Tables ("[Service Selection 3]")
    Prune_SerSel_2_Tables ("[Service Selection 3MB]")
    Prune_SerSel_2_Tables ("[Service Selection Extra]")
    Prune_SerSel_2_Tables ("[Deployment Notes]")
    Prune_SerSel_2_Tables ("[DeploymentDiary]")
    Prune_SerSel_2_Tables ("[DeploymentDiaryArchive]")
    
    'Prune tables that require join to [Service Selection 2] on TagID
    Prune_Tag_Tables ("[Tag]")
    
    'Prune tables that require join to [Service Selection]
    Prune_SerSel_Tables ("[Service Selection Backup]")
    Prune_SerSel_Tables ("[Service Selection 5]")
    Prune_SerSel_Tables ("[Service Selection 6]")
    Prune_SerSel_Tables ("[Service Selection 1]")
    Prune_SerSel_Tables ("[Service Selection 2]")
    Prune_SerSel_Tables ("[Service Selection 2 Backup]")
    Prune_SerSel_Tables ("[Service Selection 3M]")
    Prune_SerSel_Tables ("[Service Selection 3MA]")
    Prune_SerSel_Tables ("[Service Selection 4]")
    Prune_SerSel_Tables ("[Service Selection A]")
    Prune_SerSel_Tables ("[Service Selection B]")
    Prune_SerSel_Tables ("[Service Selection Invoice]")
    Prune_SerSel_Tables ("[Service Selection Invoice Backup]")
    Prune_SerSel_Tables ("[Service Selection Invoice Proposed]")
    Prune_SerSel_Tables ("[Service Selection Notes]")
    Prune_SerSel_Tables ("[Service Selection Other Value]")
    Prune_SerSel_Tables ("[Service Selection NDFS Steps]")
    Prune_SerSel_Tables ("[Service Selection]")

    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "PruneServiceSelections", "Start PruneServiceSelections ", Null, LogIDType.ServiceSelection


End Sub

Public Sub Delete_Temp_Tables(p_tablename As String)

Dim dbs As Database
Dim sql_Del_Temp_Tables As String

Set dbs = CurrentDb

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Delete_Temp_Tables", "Start Delete_Temp_Tables ", Null, LogIDType.ServiceSelection

sql_Del_Temp_Tables = "DELETE * FROM " & p_tablename

dbs.Execute sql_Del_Temp_Tables, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Delete_Temp_Tables", "sql_Del_Temp_Tables " & p_tablename & ": " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Delete_Temp_Tables", "End Delete_Temp_Tables ", Null, LogIDType.ServiceSelection


End Sub
Public Sub Populate_Temp_Tables(p_CutoffDate)

Dim dbs As Database
Dim sql_Ins_MMG_Completed_Interventions As String
Dim sql_ins_MMG_Intervention_Timetable As String
Dim sql_ins_MMG_Full_Intervention_backlog As String
Dim sql_ins_MMG_SSID_FOR_PRUNING As String
Dim sql_ins_MMG_SSID_COMPLETED_AFTER_CUTOFF As String
Dim sql_del_MMG_SSID_FOR_PRUNING

Set dbs = CurrentDb

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Populate_Temp_Tables", "Start Populate_Temp_Tables ", Null, LogIDType.ServiceSelection

sql_Ins_MMG_Completed_Interventions = "insert into MMG_Completed_Interventions ([service selection],id3,completedate) " & _
                                      "SELECT a.[Service Selection] , a.ID3, a.CompleteDate " & _
                                      "from [service selection 6] as a " & _
                                      "where a.completedate is not null " & _
                                      "and A.CompleteDate < #" & p_CutoffDate & "#"
                                      
dbs.Execute sql_Ins_MMG_Completed_Interventions, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Populate_Temp_Tables", "sql_Ins_MMG_Completed_Interventions: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection

sql_ins_MMG_Intervention_Timetable = "INSERT INTO  MMG_Intervention_Timetable ([service selection], SS1_ID2,SS2_ID2, ID3) " & _
                                     "select a.[service selection],a.[id2],b.[id2], b.[id3] " & _
                                     "from [service selection 1] as a " & _
                                     "left outer join [service selection 2] as b on " & _
                                     "( " & _
                                     "  A.[Service Selection] = B.[Service Selection] " & _
                                     "  and a.id2 = b.id2 " & _
                                     ")"
                                     
dbs.Execute sql_ins_MMG_Intervention_Timetable, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Populate_Temp_Tables", "sql_ins_MMG_Intervention_Timetable: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection
                                    
sql_ins_MMG_Full_Intervention_backlog = "INSERT INTO MMG_Full_Intervention_backlog " & _
                                        "(" & _
                                        "    [Service Selection] , SS1_ID2, SS2_ID2, SS2_ID3, SS6_ID3, CompleteDate " & _
                                        ") " & _
                                        "SELECT " & _
                                        "A.[Service Selection] , A.SS1_ID2, A.SS2_ID2, A.ID3, B.ID3, B.CompleteDate " & _
                                        "from MMG_Intervention_Timetable as a " & _
                                        "left outer join MMG_Completed_Interventions as b on a.[service selection] = b.[service selection] " & _
                                        "and a.id3 = b.id3 "
                                        
                                     
dbs.Execute sql_ins_MMG_Full_Intervention_backlog, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Populate_Temp_Tables", "sql_ins_MMG_Full_Intervention_backlog: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection
    
sql_ins_MMG_SSID_FOR_PRUNING = "INSERT INTO MMG_SSID_FOR_PRUNING " & _
                                        "(" & _
                                        "    [Service Selection] " & _
                                        ") " & _
                                        "SELECT " & _
                                        "distinct [service selection]" & _
                                        "from MMG_Full_Intervention_backlog as a " & _
                                        "where Not exists " & _
                                        "( " & _
                                        "   select " & _
                                        "   * " & _
                                        "   from MMG_Full_Intervention_backlog as b " & _
                                        "   where A.[Service Selection] = B.[Service Selection] " & _
                                        "   and a.SS1_ID2 = b.SS1_ID2" & _
                                        "   and a.SS6_ID3 is null" & _
                                        ")"
                                     
dbs.Execute sql_ins_MMG_SSID_FOR_PRUNING, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Populate_Temp_Tables", "sql_ins_MMG_SSID_FOR_PRUNING: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection

sql_ins_MMG_SSID_COMPLETED_AFTER_CUTOFF = "INSERT INTO MMG_SSID_COMPLETED_AFTER_CUTOFF " & _
                                          "([service selection]) " & _
                                          "SELECT distinct  b.[service selection]  " & _
                                          "from MMG_completed_Interventions as a " & _
                                          "inner join [service selection 6] as b " & _
                                          "on a.[service selection] = b.[service selection]" & _
                                          "where B.CompleteDate >= #" & p_CutoffDate & "#"
                                          
dbs.Execute sql_ins_MMG_SSID_COMPLETED_AFTER_CUTOFF, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Populate_Temp_Tables", "sql_ins_MMG_SSID_COMPLETED_AFTER_CUTOFF: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection

sql_del_MMG_SSID_FOR_PRUNING = "DELETE * from MMG_SSID_FOR_PRUNING where [Service Selection] in " & _
                               "(SELECT * FROM  MMG_SSID_COMPLETED_AFTER_CUTOFF)"
                               
dbs.Execute sql_del_MMG_SSID_FOR_PRUNING, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Populate_Temp_Tables", "sql_del_MMG_SSID_FOR_PRUNING: " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection
                              
ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Populate_Temp_Tables", "End Populate_Temp_Tables", Null, LogIDType.ServiceSelection


End Sub

Public Sub Prune_SerSel_Tables(p_tablename As String)

Dim dbs As Database
Dim sql_Prune_SerSel_Tables As String

Set dbs = CurrentDb

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_Tables", "Start Prune_SerSel_Tables ", Null, LogIDType.ServiceSelection

sql_Prune_SerSel_Tables = "DELETE * FROM " & p_tablename & " " & _
                      "WHERE [Service Selection] in " & _
                      "(SELECT [Service Selection] " & _
                      "FROM MMG_SSID_FOR_PRUNING)"

dbs.Execute sql_Prune_SerSel_Tables, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_Tables", "PRUNING " & p_tablename & ": " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_Tables", "End Prune_SerSel_Tables ", Null, LogIDType.ServiceSelection

End Sub

Public Sub Prune_SerSel_1_Tables(p_tablename As String)

Dim dbs As Database
Dim sql_Prune_SerSel_1_Tables As String

Set dbs = CurrentDb

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_1_Tables", "Start Prune_SerSel_1_Tables ", Null, LogIDType.ServiceSelection

sql_Prune_SerSel_1_Tables = "DELETE * FROM " & p_tablename & " " & _
                      "WHERE [ID2] in " & _
                      "(SELECT [ID2] from [Service Selection 1] as a INNER JOIN " & _
                      " MMG_SSID_FOR_PRUNING as b on a.[service selection] = b.[service selection] )"

dbs.Execute sql_Prune_SerSel_1_Tables, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_1_Tables", "PRUNING " & p_tablename & ": " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_1_Tables", "End Prune_SerSel_1_Tables ", Null, LogIDType.ServiceSelection

End Sub

Public Sub Prune_SerSel_2_Tables(p_tablename As String)

Dim dbs As Database
Dim sql_Prune_SerSel_2_Tables As String

Set dbs = CurrentDb

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_2_Tables", "Start Prune_SerSel_2_Tables ", Null, LogIDType.ServiceSelection

sql_Prune_SerSel_2_Tables = "DELETE * FROM " & p_tablename & " " & _
                      "WHERE [ID3] in " & _
                      "(SELECT [ID3] from [Service Selection 2] as a INNER JOIN " & _
                      " MMG_SSID_FOR_PRUNING as b on a.[service selection] = b.[service selection] )"

dbs.Execute sql_Prune_SerSel_2_Tables, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_2_Tables", "PRUNING " & p_tablename & ": " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_SerSel_2_Tables", "End Prune_SerSel_2_Tables ", Null, LogIDType.ServiceSelection

End Sub
Public Sub Prune_Tag_Tables(p_tablename As String)

Dim dbs As Database
Dim sql_Prune_Tag_Tables As String

Set dbs = CurrentDb

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_Tag_Tables", "Start Prune_Tag_Tables ", Null, LogIDType.ServiceSelection

sql_Prune_Tag_Tables = "DELETE * FROM " & p_tablename & " " & _
                      "WHERE [TagID] in " & _
                      "(SELECT [Tag] from [Service Selection 2] as a INNER JOIN " & _
                      " MMG_SSID_FOR_PRUNING as b on a.[service selection] = b.[service selection] )"

dbs.Execute sql_Prune_Tag_Tables, dbFailOnError

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_Tag_Tables", "PRUNING " & p_tablename & ": " & dbs.RecordsAffected, Null, LogIDType.ServiceSelection

ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "Prune_Tag_Tables", "End Prune_Tag_Tables ", Null, LogIDType.ServiceSelection

End Sub

Public Sub Print_Num_Rows(p_tablename As String)

Dim dbs As Database
Dim sql_Print_Num_Rows As String

Set dbs = CurrentDb
Dim rst As Recordset

sql_Print_Num_Rows = "SELECT count(1) as Num_Rows from " & p_tablename

Set rst = dbs.OpenRecordset(sql_Print_Num_Rows, dbOpenDynaset, dbSeeChanges)

 While Not rst.EOF
    Debug.Print p_tablename & " : " & rst.Fields("Num_Rows")
    rst.MoveNext
 Wend
 rst.Close

End Sub

Public Sub ut_Print_Num_Rows()

    'Prune tables that require join to [Service Selection 1]
    Print_Num_Rows ("[Service Selection 1A]")
    
    'Prune tables that require join to [Service Selection 2]
    Print_Num_Rows ("[Service Selection 2A]")
    Print_Num_Rows ("[Service Selection 3]")
    Print_Num_Rows ("[Service Selection 3MB]")
    Print_Num_Rows ("[Service Selection Extra]")
    Print_Num_Rows ("[Deployment Notes]")
    Print_Num_Rows ("[DeploymentDiary]")
    Print_Num_Rows ("[DeploymentDiaryArchive]")
    
    'Prune tables that require join to [Service Selection 2] on TagID
    Print_Num_Rows ("[Tag]")
    
    'Prune tables that require join to [Service Selection]
    Print_Num_Rows ("[Service Selection Backup]")
    Print_Num_Rows ("[Service Selection 5]")
    Print_Num_Rows ("[Service Selection 6]")
    Print_Num_Rows ("[Service Selection 1]")
    Print_Num_Rows ("[Service Selection 2]")
    Print_Num_Rows ("[Service Selection 2 Backup]")
    Print_Num_Rows ("[Service Selection 3M]")
    Print_Num_Rows ("[Service Selection 3MA]")
    Print_Num_Rows ("[Service Selection 4]")
    Print_Num_Rows ("[Service Selection A]")
    Print_Num_Rows ("[Service Selection B]")
    Print_Num_Rows ("[Service Selection Invoice]")
    Print_Num_Rows ("[Service Selection Invoice Backup]")
    Print_Num_Rows ("[Service Selection Invoice Proposed]")
    Print_Num_Rows ("[Service Selection Notes]")
    Print_Num_Rows ("[Service Selection Other Value]")
    Print_Num_Rows ("[Service Selection NDFS Steps]")
    Print_Num_Rows ("[Service Selection]")


End Sub
Public Sub PruneLostCancelledServiceSelections(p_CutoffDate As Date)

    Dim sql_ins_MMG_SSID_FOR_PRUNING As String
    Dim dbs As Database
    
    Set dbs = CurrentDb
    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "PruneLostCancelledServiceSelections", "Start PruneLostCancelledServiceSelections ", Null, LogIDType.ServiceSelection
    'Delete Temp Tables

    Delete_Temp_Tables ("MMG_SSID_FOR_PRUNING")
    
    sql_ins_MMG_SSID_FOR_PRUNING = "INSERT INTO MMG_SSID_FOR_PRUNING " & _
                                        "(" & _
                                        "    [Service Selection] " & _
                                        ") " & _
                                        "SELECT " & _
                                        "[service selection] " & _
                                        "from [Service selection] as a " & _
                                        "where active in (5,6) " & _
                                        "and updateddate < #" & p_CutoffDate & "#"
    
    dbs.Execute sql_ins_MMG_SSID_FOR_PRUNING, dbFailOnError
    
    'Prune tables that require join to [Service Selection 1]
    Prune_SerSel_1_Tables ("[Service Selection 1A]")
    
    'Prune tables that require join to [Service Selection 2]
    Prune_SerSel_2_Tables ("[Service Selection 2A]")
    Prune_SerSel_2_Tables ("[Service Selection 3]")
    Prune_SerSel_2_Tables ("[Service Selection 3MB]")
    Prune_SerSel_2_Tables ("[Service Selection Extra]")
    Prune_SerSel_2_Tables ("[Deployment Notes]")
    Prune_SerSel_2_Tables ("[DeploymentDiary]")
    Prune_SerSel_2_Tables ("[DeploymentDiaryArchive]")
    
    'Prune tables that require join to [Service Selection 2] on TagID
    Prune_Tag_Tables ("[Tag]")
    
    'Prune tables that require join to [Service Selection]
    Prune_SerSel_Tables ("[Service Selection Backup]")
    Prune_SerSel_Tables ("[Service Selection 5]")
    Prune_SerSel_Tables ("[Service Selection 6]")
    Prune_SerSel_Tables ("[Service Selection 1]")
    Prune_SerSel_Tables ("[Service Selection 2]")
    Prune_SerSel_Tables ("[Service Selection 2 Backup]")
    Prune_SerSel_Tables ("[Service Selection 3M]")
    Prune_SerSel_Tables ("[Service Selection 3MA]")
    Prune_SerSel_Tables ("[Service Selection 4]")
    Prune_SerSel_Tables ("[Service Selection A]")
    Prune_SerSel_Tables ("[Service Selection B]")
    Prune_SerSel_Tables ("[Service Selection Invoice]")
    Prune_SerSel_Tables ("[Service Selection Invoice Backup]")
    Prune_SerSel_Tables ("[Service Selection Invoice Proposed]")
    Prune_SerSel_Tables ("[Service Selection Notes]")
    Prune_SerSel_Tables ("[Service Selection Other Value]")
    Prune_SerSel_Tables ("[Service Selection NDFS Steps]")
    Prune_SerSel_Tables ("[Service Selection]")

    ActionLog LogAction.UpdateRecord, LogObjectType.ServiceSelection, "PruneLostCancelledServiceSelections", "Start PruneLostCancelledServiceSelections ", Null, LogIDType.ServiceSelection

    

End Sub

Public Sub ut_Prune()

    PruneServiceSelections (#1/1/2016#)

End Sub

Public Sub ut_PruneLost()

    PruneLostCancelledServiceSelections (#1/1/2017#)

End Sub



