Attribute VB_Name = "Integration"
Option Compare Database
Dim CID As Long
Dim BID As Long
Dim BN As String
Dim Xstr As String
Option Explicit

Function ITG(X As Integer, Path As String, OfficeFlag As String, SName As String)
    
    Dim Msg As String
    Dim simp As String
    Dim retVal As String
    Dim LOCATION_MSACCESS As String
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim PauseTime, Start, Finish As Single
    Dim strSql As String
    Dim SName1 As String
    Dim FN As Long
    
    SName1 = Left(SName, Len(SName) - 3) & "ldb"
    
    Msg = "List of errors, Note this and inform DFS admin"
    Select Case X
        Case 1
            Xstr = "Scala Interface"
            fix_dash 'this was added to correct problem seen in Italy with ' and symbol �
        Case 2
            Xstr = "SIMP Interface"
        Case 3
            Xstr = "Online Interface"
        Case 4
            Xstr = "SAS Interface"
            
    End Select
        
    FN = DLookup("[FirstNumber]", "General Office Info")
    
    If X = 1 Or X = 3 Then
        
        If X = 3 Then
            LOCATION_MSACCESS = SysCmd(acSysCmdAccessDir) & "Msaccess.exe"
            simp = LOCATION_MSACCESS & " " & Path & "\" & SName
            retVal = Shell(simp, vbMinimizedNoFocus)
            
            Do
                If Dir(Path & "\" & SName1) = "" Then Exit Do
            Loop
        End If
    
        'Fix The issue of TEMP vs TMP
        If DLookup("[Table3]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            DoCmd.SetWarnings False
            DoCmd.OpenQuery "TMP Query"
            DoCmd.OpenQuery "Delete INT SL01"
            DoCmd.SetWarnings True
        End If
        Set dbs = CurrentDb
        If OfficeFlag <> "-" Then
            strSql = "SELECT * FROM [Synch03] WHERE [SL01010] = 'TMP' and [SL01017] <> '" & OfficeFlag & "'"
            Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            While Not rst.EOF
                rst.Delete
                rst.MoveNext
            Wend
            rst.Close
            Set rst = Nothing
            Set dbs = Nothing
        End If
        
        If DLookup("[Table3]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch03", "[ENTRY_TYPE] = 'I' " & " and [SL01010] <> 'TMP'") > 0 Then
                If SCALA_CUST_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Customers"
            End If
            If DCount("[ENTRY_TYPE]", "Synch03", "[ENTRY_TYPE] = 'U' " & " and [SL01010] <> 'TMP'") > 0 Then
                If SCALA_CUST_SYNC_U = False Then Msg = Msg & "  -   Problem with updating Customers"
            End If
            If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If DCount("[ENTRY_TYPE]", "Synch03", "[ENTRY_TYPE] = 'I' " & " and [SL01010] <> 'TMP'") > 0 Then
                    If SCALA_BUIL_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Buildings"
                End If
                If DCount("[ENTRY_TYPE]", "Synch03", "[ENTRY_TYPE] = 'U' " & " and [SL01010] <> 'TMP'") > 0 Then
                    If SCALA_BUIL_SYNC_U = False Then Msg = Msg & "  -   Problem with updating Buildings"
                End If
            Else
                If DCount("[ENTRY_TYPE]", "Synch03", "[ENTRY_TYPE] = 'I' " & " and [SL01010] = 'TMP'") > 0 Then
                    If SCALA_BUIL_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Buildings"
                End If
                If DCount("[ENTRY_TYPE]", "Synch03", "[ENTRY_TYPE] = 'U' " & " and [SL01010] = 'TMP'") > 0 Then
                    If SCALA_BUIL_SYNC_U = False Then Msg = Msg & "  -   Problem with updating Buildings"
                End If
            End If
                
        End If
            
        If DLookup("[Table2]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch02") > 0 Then
                If Correct_Eq_Table = False Then Msg = Msg & "  -  Problem with inserting Building Number in Equipment file"
            End If
            If DCount("[ENTRY_TYPE]", "Synch02", "[ENTRY_TYPE] = 'I' ") > 0 Then
                If SCALA_EQUI_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Equipment"
            End If
            If DCount("[ENTRY_TYPE]", "Synch02", "[ENTRY_TYPE] = 'U' ") > 0 Then
                If SCALA_EQUI_SYNC_U = False Then Msg = Msg & "  -   Problem with updating Equipment"
            End If
        End If
            
        If DLookup("[Table1]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch01", "[ENTRY_TYPE] = 'I' ") > 0 Then
                If SCALA_TECH_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Technicians"
            End If
            If DCount("[ENTRY_TYPE]", "Synch01", "[ENTRY_TYPE] = 'U' ") > 0 Then
                If SCALA_TECH_SYNC_U = False Then Msg = Msg & "  -  Problem with updating Technicians"
            End If
        End If
        If DLookup("[Table4]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch04", "[ENTRY_TYPE] = 'I' ") > 0 Then
                If SCALA_PART_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Parts"
            End If
            If DCount("[ENTRY_TYPE]", "Synch04", "[ENTRY_TYPE] = 'U' ") > 0 Then
                If SCALA_PART_SYNC_U = False Then Msg = Msg & "  -  Problem with updating Parts"
            End If
        End If
        
        If DLookup("[Table6]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch06") > 0 Then
                If SCALA_PART_SYNC_M = False Then Msg = Msg & "  -  Problem with updating Mercury Data"
            End If
        End If
        
    ElseIf X = 2 Then
    
        Set dbs = CurrentDb
        source = "Synch06"
        Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
        If rst![ls] < Date Then
            rst.Edit
            rst![ls] = Date
            rst.Update
            rst.Close
            LOCATION_MSACCESS = SysCmd(acSysCmdAccessDir) & "Msaccess.exe"
            simp = LOCATION_MSACCESS & " " & Path & "\" & SName
            retVal = Shell(simp, vbMinimizedNoFocus)
            
            PauseTime = 5
            Start = Timer
            Do While Timer < Start + PauseTime
                DoEvents
            Loop
            Finish = Timer
            If DCount("[Clave_Clie]", "Synch03") > 0 Then
                If SIMP_CUST_SYNC = False Then Msg = Msg & "  -  Problem Synchronising Customers"
            End If
            If DCount("[Clave_Art]", "Synch04") > 0 Then
                If SIMP_PART_SYNC = False Then Msg = Msg & "  -  Problem Synchronising Parts List"
            End If
        Else
' do nothing
        End If
        Set rst = Nothing
        Set dbs = Nothing
    
    ElseIf X = 4 Then
        If DLookup("[Table3]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch03", "[ENTRY_TYPE] = 'I' ") > 0 Then
                If SAS_CUST_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Customers"
            End If
            If DCount("[ENTRY_TYPE]", "Synch03", "[ENTRY_TYPE] = 'U' ") > 0 Then
                If SAS_CUST_SYNC_U = False Then Msg = Msg & "  -  Problem with updating Customers"
            End If
        End If
        If DLookup("[Table1]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch01", "[ENTRY_TYPE] = 'I' ") > 0 Then
                If SAS_BUIL_SYNC_I() = False Then Msg = Msg & "  -  Problem with inserting Buildings"
            End If
            If DCount("[ENTRY_TYPE]", "Synch01", "[ENTRY_TYPE] = 'U' ") > 0 Then
                If SAS_BUIL_SYNC_U = False Then Msg = Msg & "  -   Problem with updating Buildings"
            End If
        End If
        If DLookup("[Table2]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch02", "[ENTRY_TYPE] = 'I' ") > 0 Then
                If SAS_EQUI_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Equipment"
            End If
            If DCount("[ENTRY_TYPE]", "Synch02", "[ENTRY_TYPE] = 'U' ") > 0 Then
                If SAS_EQUI_SYNC_U = False Then Msg = Msg & "  -   Problem with updating Equipment"
            End If
        End If
        If DLookup("[Table5]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch05", "[ENTRY_TYPE] = 'I' ") > 0 Then
                If SAS_PART_SYNC_I = False Then Msg = Msg & "  -  Problem with inserting Parts"
            End If
            If DCount("[ENTRY_TYPE]", "Synch05", "[ENTRY_TYPE] = 'U' ") > 0 Then
                If SAS_PART_SYNC_U = False Then Msg = Msg & "  -   Problem with updating Parts"
            End If
        End If
        If DLookup("[Table6]", "TRN", "[FirstNumber]=" & FN) <> "-" Then
            If DCount("[ENTRY_TYPE]", "Synch06") > 0 Then
                If SCALA_PART_SYNC_M = False Then Msg = Msg & "  -  Problem with updating Mercury Data"
            End If
        End If
      
    End If
    
    If Msg <> "List of errors, Note this and inform DFS admin" Then MsgBox Msg

    
End Function
Function FIND_BNO(X As Long)
    BN = ""
    If DCount("[BuildingNumber]", "BD-Buildings", "[BuildingID]=" & X) > 0 Then
        BN = DLookup("[BuildingNumber]", "BD-Buildings", "[BuildingID]=" & X)
    End If
End Function
Function FIND_CID(X As String)
    CID = 0
    If DCount("[CustomerID]", "BD-Customers", "[CustomerNumber]='" & X & "'") > 0 Then
        CID = DLookup("[CustomerID]", "BD-Customers", "[CustomerNumber]='" & X & "'")
    End If
End Function
Function FIND_BID(X As String)
    BID = 0
    If DCount("[BuildingID]", "BD-Buildings", "[BuildingNumber]='" & X & "'") > 0 Then
        BID = DLookup("[BuildingID]", "BD-Buildings", "[BuildingNumber]='" & X & "'")
    End If
End Function

Function SCALA_CUST_SYNC_I() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_CUST As String
    Dim FN As Long
    
    FN = DLookup("[FirstNumber]", "General Office Info")
        
    On Error GoTo endd
    SCALA_CUST_SYNC_I = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")
    source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'I' " & " And [SL01010] <> 'TMP' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Customers"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    While Not rst.EOF
        If DCount("[CustomerNumber]", "[BD-Customers]", "[CustomerNumber] Like '" & Convert_SS(rst![SL01001]) & "'") = 0 Then
            rst1.AddNew
            If DCount("[CustomerID]", "BD-Customers") > 0 Then
                If DMax("[CustomerID]", "BD-Customers", "") > 0 Then
                    rst1![CustomerID] = DMax("[CustomerID]", "BD-Customers", "") + 1
                Else
                    rst1![CustomerID] = DLookup("[FirstNumber]", "General Office Info")
                End If
            Else
                rst1![CustomerID] = DLookup("[FirstNumber]", "General Office Info")
            End If
                
            TMP_CUST = rst![SL01002]
            i = Dupl_CheckI("BD-Customers", "CustomerName", rst![SL01002], "CustomerID", rst1![CustomerID])
            If i > 0 Then
                TMP_CUST = TMP_CUST & "-" & i
            End If
            If Nz(rst![SL01002], "") <> "" Then rst1![CustomerName] = TMP_CUST
            If Nz(rst![SL01001], "") <> "" Then rst1![CustomerNumber] = Left(rst![SL01001], 50)
'ITALY
            If FN = 58000000 Or FN = 60000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & Chr(160) & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'IBERIA
            ElseIf FN = 44000000 Or FN = 128000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'NL
            ElseIf FN = 40000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)

'GSA
            ElseIf FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'CEEBR
            ElseIf FN = 132000000 Or FN = 134000000 Or FN = 160000000 Or FN = 162000000 Or FN = 164000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'MAIR
            ElseIf FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'UK
            ElseIf FN = 38000000 Or FN = 36000000 Or FN = 34000000 Or FN = 26000000 Or FN = 18000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
            End If
            
            
            If Nz(rst![SL01011], "") <> "" Then rst1![PhoneNumber] = Left(rst![SL01011], 30)
            If Nz(rst![SL01013], "") <> "" Then rst1![FaxNumber] = Left(rst![SL01013], 30)
            If Nz(rst![SL01010], "") <> "" Then rst1![NationalAcc] = Left(rst![SL01010], 4)
            
            If FN = 170000000 Or FN = 172000000 Or FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Or FN = 166000000 Or FN = 168000000 Then
            Else
                If Nz(rst![SL01024]) <> "" Then rst1![PayTerm] = Left(rst![SL01024], 50)
            End If
            rst1![RecordedDate] = Date
            rst1![UpdatedDate] = Date
            rst1![Creator] = Xstr
            rst1.Update
            If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
' do not delete for Germans as it may also need to be added as a building
            Else
                rst.Delete
            End If
        Else
            rst.Edit
            rst![ENTRY_TYPE] = "U"
            rst.Update
        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising new Customers From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    
    
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_CUST_SYNC_I = False
    CloseProgress
End Function

Function SCALA_CUST_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_CUST As String
    Dim FN As Long
    Dim CNBR As String
    Dim EXIST_CNBR As String
    Dim ZZ As Integer
    
    FN = DLookup("[FirstNumber]", "General Office Info")
    
    On Error GoTo endd
    SCALA_CUST_SYNC_U = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")
    source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'U' " & " and [SL01010] <> 'TMP' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        
        CNBR = Convert_SS(rst![SL01001])
        source1 = "SELECT * FROM [BD-Customers] WHERE [CustomerNumber] Like '" & CNBR & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        
        If rst1.RecordCount = 0 Then
            ZZ = InStr(1, rst![SL01001], " ")
            If ZZ = 0 Then
                EXIST_CNBR = CNBR
            Else
                EXIST_CNBR = MID(rst![SL01001], 1, (ZZ - 1))
            End If
            rst1.Close
            source1 = "SELECT * FROM [BD-Customers] WHERE [CustomerNumber] Like '" & EXIST_CNBR & "'"
            Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                rst1.Edit
                rst1![CustomerNumber] = CNBR
                rst1.Update
            End If
        End If
        rst1.Close
        
        source1 = "SELECT * FROM [BD-Customers] WHERE [CustomerNumber] Like '" & CNBR & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
                
        If rst1.RecordCount > 0 Then
            rst1.Edit
            TMP_CUST = rst![SL01002]
            i = Dupl_CheckI("BD-Customers", "CustomerName", rst![SL01002], "CustomerID", rst1![CustomerID])
            If i > 0 Then
                TMP_CUST = TMP_CUST & "-" & i
            End If
            If Nz(rst![SL01002], "") <> "" Then rst1![CustomerName] = TMP_CUST
            If Nz(rst![SL01001], "") <> "" Then rst1![CustomerNumber] = Left(rst![SL01001], 50)
'ITALY
            If FN = 58000000 Or FN = 60000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & Chr(160) & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'IBERIA
            ElseIf FN = 44000000 Or FN = 128000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'GSA
            ElseIf FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
'NL
            ElseIf FN = 40000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)

'CEEBR
            ElseIf FN = 132000000 Or FN = 134000000 Or FN = 160000000 Or FN = 162000000 Or FN = 164000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
'MAIR
            ElseIf FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
'UK
            ElseIf FN = 38000000 Or FN = 36000000 Or FN = 34000000 Or FN = 26000000 Or FN = 18000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
            End If
            
            If Nz(rst![SL01011], "") <> "" Then rst1![PhoneNumber] = Left(rst![SL01011], 30)
            If Nz(rst![SL01013], "") <> "" Then rst1![FaxNumber] = Left(rst![SL01013], 30)
'            If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
            If Nz(rst![SL01010], "") <> "" Then rst1![NationalAcc] = Left(rst![SL01010], 4)
            If FN = 170000000 Or FN = 172000000 Or FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Or FN = 166000000 Or FN = 168000000 Then
            Else
                If Nz(rst![SL01024], "") <> "" Then rst1![PayTerm] = Left(rst![SL01024], 50)
            End If
            rst1![UpdatedDate] = Date
            rst1.Update
        Else
'customer does not exist in DFSD it must be added (SAME CODE AS CUSTOMER ADD BELOW)
            rst1.AddNew
            
            If DCount("[CustomerID]", "BD-Customers") > 0 Then
                If DMax("[CustomerID]", "BD-Customers", "") > 0 Then
                    rst1![CustomerID] = DMax("[CustomerID]", "BD-Customers", "") + 1
                Else
                    rst1![CustomerID] = DLookup("[FirstNumber]", "General Office Info")
                End If
            Else
                rst1![CustomerID] = DLookup("[FirstNumber]", "General Office Info")
            End If
            TMP_CUST = rst![SL01002]
            i = Dupl_CheckI("BD-Customers", "CustomerName", rst![SL01002], "CustomerID", rst1![CustomerID])
            If i > 0 Then
                TMP_CUST = TMP_CUST & "-" & i
            End If
            If Nz(rst![SL01002], "") <> "" Then rst1![CustomerName] = TMP_CUST
            If Nz(rst![SL01001], "") <> "" Then rst1![CustomerNumber] = Left(rst![SL01001], 50)
'ITALY
            If FN = 58000000 Or FN = 60000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & Chr(160) & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'IBERIA
            ElseIf FN = 44000000 Or FN = 128000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'NL
            ElseIf FN = 40000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)

'GSA
            ElseIf FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'CEEBR
            ElseIf FN = 132000000 Or FN = 134000000 Or FN = 160000000 Or FN = 162000000 Or FN = 164000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'MAIR
            ElseIf FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'UK
            ElseIf FN = 38000000 Or FN = 36000000 Or FN = 34000000 Or FN = 26000000 Or FN = 18000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
            End If
            
            If Nz(rst![SL01011], "") <> "" Then rst1![PhoneNumber] = Left(rst![SL01011], 30)
            If Nz(rst![SL01013], "") <> "" Then rst1![FaxNumber] = Left(rst![SL01013], 30)
            If Nz(rst![SL01010], "") <> "" Then rst1![NationalAcc] = Left(rst![SL01010], 4)
            If FN = 170000000 Or FN = 172000000 Or FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Or FN = 166000000 Or FN = 168000000 Then
            Else
                If Nz(rst![SL01024], "") <> "" Then rst1![PayTerm] = Left(rst![SL01024], 50)
            End If
            rst1![RecordedDate] = Date
            rst1![UpdatedDate] = Date
            rst1![Creator] = Xstr
            rst1.Update
        End If
        rst1.Close
        If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
' do not delete for Germans as it may also be a building
        Else
            rst.Delete
        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Customers From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
       
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_CUST_SYNC_U = False
    CloseProgress

End Function

Function SCALA_BUIL_SYNC_I() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_BUIL As String
    Dim FN As Long
    
    FN = DLookup("[FirstNumber]", "General Office Info")
    
    On Error GoTo endd
    SCALA_BUIL_SYNC_I = True
        
    Set dbs = CurrentDb
    
    Call Call_prog("")
    If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
        source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'I' ORDER BY [ENTRY_DATE]"
    Else
        source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'I' " & " and [SL01010] = 'TMP' ORDER BY [ENTRY_DATE]"
    End If
    
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Buildings"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        If DCount("[BuildingNumber]", "[BD-Buildings]", "[BuildingNumber] Like '" & Convert_SS(rst![SL01001]) & "'") = 0 Then
            rst1.AddNew
            If DCount("[BuildingID]", "BD-Buildings") > 0 Then
                rst1![BuildingID] = DMax("[BuildingID]", "BD-Buildings", "") + 1
            Else
                rst1![BuildingID] = DLookup("[FirstNumber]", "General Office Info")
            End If
            TMP_BUIL = rst![SL01002]
            i = Dupl_CheckI("BD-Buildings", "BuildingName", rst![SL01002], "BuildingID", rst1![BuildingID])
            If i > 0 Then
                TMP_BUIL = TMP_BUIL & "-" & i
            End If
            If Nz(rst![SL01002], "") <> "" Then rst1![BuildingName] = TMP_BUIL
            If Nz(rst![SL01001], "") <> "" Then rst1![BuildingNumber] = Left(rst![SL01001], 50)
'ITALY
            If FN = 58000000 Or FN = 60000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & Chr(160) & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
                If Nz(rst![SL01104], "") <> "" Then rst1![CountryCode] = Left(rst![SL01104], 10)
'IBERIA
            ElseIf FN = 44000000 Or FN = 128000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'NL
            ElseIf FN = 40000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)

'GSA
            ElseIf FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'CEEBR
            ElseIf FN = 132000000 Or FN = 134000000 Or FN = 160000000 Or FN = 162000000 Or FN = 164000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'MAIR
            ElseIf FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'UK
            ElseIf FN = 38000000 Or FN = 36000000 Or FN = 34000000 Or FN = 26000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                rst1![State] = Left(rst![SL01099], 20)
                If Nz(rst![SL01104], "") <> "" Then rst1![CountryCode] = Left(rst![SL01104], 10)
            End If

            If Nz(rst![SL01011], "") <> "" Then rst1![PhoneNumber] = Left(rst![SL01011], 30)
            If Nz(rst![SL01013], "") <> "" Then rst1![FaxNumber] = Left(rst![SL01013], 30)
            CID = 0
            If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
' the relationship between Customer and Site is not valid for German Scala implementation
'                rst1![CustomerID] = CID
            Else
                If Nz(rst![SL01056], "") <> "" Then Call FIND_CID(CStr(rst![SL01056]))
                If Nz(rst![SL01056], "") <> "" Then rst1![CustomerID] = CID
            End If
            If FN <> 40000000 Then
                If Nz(rst![SL01010], "") <> "" Then rst1![NationalAcc] = Left(rst![SL01010], 4)
            End If
            rst1![RecordedDate] = Date
            rst1![UpdatedDate] = Date
            rst1![Creator] = Xstr & "-NEW"
            rst1.Update
            rst.Delete
        Else
            rst.Edit
            rst![ENTRY_TYPE] = "U"
            rst.Update
        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising new Buildngs From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_BUIL_SYNC_I = False
    CloseProgress

End Function

Function SCALA_BUIL_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_BUIL As String
    Dim AddCheck As String
    Dim CityCheck As String
    Dim StateCheck As String
    Dim PCodeCheck As String
    Dim PNumber As String
    Dim AAtext As String
    Dim FN As Long
    Dim BNBR As String
    Dim EXIST_BNBR As String
    Dim ZZ As Integer
    
    FN = DLookup("[FirstNumber]", "General Office Info")

    On Error GoTo endd
    SCALA_BUIL_SYNC_U = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")
    If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
        source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Else
        source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'U' " & " and [SL01010] = 'TMP' ORDER BY [ENTRY_DATE]"
    End If
    
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        BNBR = Convert_SS(rst![SL01001])
'MsgBox ("SL01001=" & rst![SL01001] & "! Building Number=" & DLookup("[BuildingNumber]", "BD-Buildings", rst![SL01001]) & " Convert_SS (SL01001)=" & BNBR & "!")
        source1 = "SELECT * FROM [BD-Buildings] WHERE [BuildingNumber] Like '" & BNBR & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        If rst1.RecordCount = 0 Then
            ZZ = InStr(1, rst![SL01001], " ")
            If ZZ = 0 Then
                EXIST_BNBR = BNBR
            Else
                EXIST_BNBR = MID(rst![SL01001], 1, (ZZ - 1))
            End If
            rst1.Close
            source1 = "SELECT * FROM [BD-Buildings] WHERE [BuildingNumber] Like '" & EXIST_BNBR & "'"
            Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
'MsgBox ("BNBR=" & BNBR & "!   EXIST_BNBR=" & EXIST_BNBR & "!")
            If rst1.RecordCount > 0 Then
                rst1.Edit
                rst1![BuildingNumber] = BNBR
                rst1.Update
            End If
        End If
        rst1.Close
        
        source1 = "SELECT * FROM [BD-Buildings] WHERE [BuildingNumber] Like '" & BNBR & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        If rst1.RecordCount > 0 Then
            rst1.Edit
            TMP_BUIL = rst![SL01002]
            i = Dupl_CheckI("BD-Buildings", "BuildingName", rst![SL01002], "BuildingID", rst1![BuildingID])
            If i > 0 Then
                TMP_BUIL = TMP_BUIL & "-" & i
            End If
            If Nz(rst![SL01002], "") <> "" Then rst1![BuildingName] = TMP_BUIL
            If Nz(rst![SL01001], "") <> "" Then rst1![BuildingNumber] = Left(rst![SL01001], 50)
'ITALY
            If FN = 58000000 Or FN = 60000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & Chr(160) & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" And rst![SL01005] <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
                If Nz(rst![SL01104], "") <> "" Then rst1![CountryCode] = Left(rst![SL01104], 10)
'IBERIA
            ElseIf FN = 44000000 Or FN = 128000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" And rst![SL01005] <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'NL
            ElseIf FN = 40000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)

'GSA
            ElseIf FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'CEEBR
            ElseIf FN = 132000000 Or FN = 134000000 Or FN = 160000000 Or FN = 162000000 Or FN = 164000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'MAIR
            ElseIf FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'UK
            ElseIf FN = 38000000 Or FN = 36000000 Or FN = 34000000 Or FN = 26000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                rst1![State] = Left(rst![SL01099], 20)
                If Nz(rst![SL01104], "") <> "" Then rst1![CountryCode] = Left(rst![SL01104], 10)
            End If
            
            If Nz(rst![SL01011], "") <> "" Then rst1![PhoneNumber] = Left(rst![SL01011], 30)
            If Nz(rst![SL01013], "") <> "" Then rst1![FaxNumber] = Left(rst![SL01013], 30)
            If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
' the relationship between Customer and Site is not valid for German Scala implementation
'                rst1![CustomerID] = CID
            Else
                If Nz(rst![SL01056], "") <> "" Then Call FIND_CID(CStr(rst![SL01056]))
                If CID <> 0 Then
                    If Nz(rst![SL01056], "") <> "" Then rst1![CustomerID] = CID
                End If
            End If
            If FN <> 40000000 Then
                If Nz(rst![SL01010], "") <> "" Then rst1![NationalAcc] = Left(rst![SL01010], 4)
            End If
            rst1![UpdatedDate] = Date
            rst1.Update
        Else
' Building does not exist in DFSD it must be added (SAME CODES AS BUILDING ADD BELOW)
'IS CAUSING DUPLICATION WHEN THE BUILDING NUMBER IN DFSD HAS SPACES AFTER IT AND THE BUILDING NUMBER IN SCALA DOES NOT NEEDS TO BE CORRECTED!
            
            rst1.AddNew
            If DCount("[BuildingID]", "BD-Buildings") > 0 Then
                rst1![BuildingID] = DMax("[BuildingID]", "BD-Buildings", "") + 1
            Else
                rst1![BuildingID] = DLookup("[FirstNumber]", "General Office Info")
            End If
            TMP_BUIL = rst![SL01002]
            i = Dupl_CheckI("BD-Buildings", "BuildingName", rst![SL01002], "BuildingID", rst1![BuildingID])
            If i > 0 Then
                TMP_BUIL = TMP_BUIL & "-" & i
            End If
            If Nz(rst![SL01002], "") <> "" Then rst1![BuildingName] = TMP_BUIL
            If Nz(rst![SL01001], "") <> "" Then rst1![BuildingNumber] = Left(rst![SL01001], 50)
'ITALY
            If FN = 58000000 Or FN = 60000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & Chr(160) & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
                If Nz(rst![SL01104], "") <> "" Then rst1![CountryCode] = Left(rst![SL01104], 10)
'IBERIA
            ElseIf FN = 44000000 Or FN = 128000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![State] = Left(rst![SL01099], 20)
'NL
            ElseIf FN = 40000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)

'GSA
            ElseIf FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'CEEBR
            ElseIf FN = 132000000 Or FN = 134000000 Or FN = 160000000 Or FN = 162000000 Or FN = 164000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'MAIR
            ElseIf FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01005]), 250)
                If Nz(rst![SL01099], "") <> "" Then rst1![City] = Left(rst![SL01099], 100)
                If Nz(rst![SL01104], "") <> "" Then rst1![State] = Left(rst![SL01104], 20)
'UK
            ElseIf FN = 38000000 Or FN = 36000000 Or FN = 34000000 Or FN = 26000000 Then
                If Nz(rst![SL01003], "") <> "" Then rst1![Address] = Left(rst![SL01003], 250)
                If Nz(rst![SL01004], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![SL01004]), 250)
                If Nz(rst![SL01005], "") <> "" Then rst1![City] = Left(rst![SL01005], 100)
                If Nz(rst![SL01083], "") <> "" Then rst1![PostalCode] = Left(rst![SL01083], 20)
                rst1![State] = Left(rst![SL01099], 20)
                If Nz(rst![SL01104], "") <> "" Then rst1![CountryCode] = Left(rst![SL01104], 10)
            End If
                        
            If Nz(rst![SL01011], "") <> "" Then rst1![PhoneNumber] = Left(rst![SL01011], 30)
            If Nz(rst![SL01013], "") <> "" Then rst1![FaxNumber] = Left(rst![SL01013], 30)
            If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
' the relationship between Customer and Site is not valid for German Scala implementation
'                rst1![CustomerID] = CID
            Else
                If Nz(rst![SL01056], "") <> "" Then
                    Call FIND_CID(CStr(rst![SL01056]))
                    rst1![CustomerID] = CID
                End If
            End If
            If FN <> 40000000 Then
                If Nz(rst![SL01010], "") <> "" Then rst1![NationalAcc] = Left(rst![SL01010], 4)
            End If
            rst1![RecordedDate] = Date
            rst1![UpdatedDate] = Date
            rst1![Creator] = Xstr & "_TEST FOR DUPLICATION"
            
            rst1.Update
        End If
        rst1.Close
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Buildings From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    rst.Close
    CloseProgress
    
' UPDATE SERVICE SELECTION 5 TABLE
   Call Call_prog("")
   Call UpdateProgress(1, "Updating changed Buildings From Scala into DFSD Service Backlog")

    If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
        source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Else
        source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'U' " & " and [SL01010] = 'TMP' ORDER BY [ENTRY_DATE]"
    End If
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    While Not rst.EOF
        source1 = "SELECT * FROM [Service Selection 5] WHERE [BuildingNumber] Like '" & Convert_SS(rst![SL01001]) & "'"
        Set rst1 = dbs.OpenRecordset(source1)
        While Not rst1.EOF
            rst1.Edit
            If Nz(rst![SL01002], "") <> "" Then rst1![BuildingName] = rst![SL01002]
            AddCheck = "-"
            CityCheck = "-"
            StateCheck = "-"
            PCodeCheck = "-"
            PNumber = "-"
'ITALY
            If FN = 58000000 Or FN = 60000000 Then
                If Nz(rst![SL01003], "") <> "" Then AddCheck = rst![SL01003]
                If Nz(rst![SL01004], "") <> "" Then AddCheck = AddCheck & Chr(160) & rst![SL01004]
                If Nz(rst![SL01005], "") <> "" Then CityCheck = rst![SL01005]
                If Nz(rst![SL01083], "") <> "" Then PCodeCheck = MID(rst![SL01083], 1, 20)
                If Nz(rst![SL01099], "") <> "" Then StateCheck = Left(rst![SL01099], 20)
'IBERIA
            ElseIf FN = 44000000 Or FN = 128000000 Then
                If Nz(rst![SL01003], "") <> "" Then AddCheck = rst![SL01003]
                If Nz(rst![SL01004], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01004]
                If Nz(rst![SL01005], "") <> "" Then CityCheck = rst![SL01005]
                If Nz(rst![SL01083], "") <> "" Then PCodeCheck = MID(rst![SL01083], 1, 20)
                If Nz(rst![SL01099], "") <> "" Then StateCheck = Left(rst![SL01099], 20)
'NL
            ElseIf FN = 40000000 Then
                If Nz(rst![SL01003], "") <> "" Then AddCheck = rst![SL01003]
                If Nz(rst![SL01004], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01004]
                If Nz(rst![SL01005], "") <> "" Then CityCheck = rst![SL01005]
                If Nz(rst![SL01083], "") <> "" Then PCodeCheck = MID(rst![SL01083], 1, 20)
                If Nz(rst![SL01099], "") <> "" Then CityCheck = rst![SL01099]
                If Nz(rst![SL01104], "") <> "" Then StateCheck = rst![SL01104]

'GSA
            ElseIf FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SL01003], "") <> "" Then AddCheck = rst![SL01003]
                If Nz(rst![SL01004], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01004]
                If Nz(rst![SL01005], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01005]
                If Nz(rst![SL01099], "") <> "" Then CityCheck = rst![SL01099]
                If Nz(rst![SL01083], "") <> "" Then PCodeCheck = MID(rst![SL01083], 1, 20)
                If Nz(rst![SL01104], "") <> "" Then StateCheck = rst![SL01104]
'CEEBR
            ElseIf FN = 132000000 Or FN = 134000000 Or FN = 160000000 Or FN = 162000000 Or FN = 164000000 Then
                If Nz(rst![SL01003], "") <> "" Then AddCheck = rst![SL01003]
                If Nz(rst![SL01004], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01004]
                If Nz(rst![SL01005], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01005]
                If Nz(rst![SL01099], "") <> "" Then CityCheck = rst![SL01099]
                If Nz(rst![SL01083], "") <> "" Then PCodeCheck = MID(rst![SL01083], 1, 20)
                If Nz(rst![SL01104], "") <> "" Then StateCheck = rst![SL01104]
' MAIR
            ElseIf FN = 146000000 Or FN = 143000000 Or FN = 150000000 Or FN = 154000000 Then
                If Nz(rst![SL01003], "") <> "" Then AddCheck = rst![SL01003]
                If Nz(rst![SL01004], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01004]
                If Nz(rst![SL01005], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01005]
                If Nz(rst![SL01005], "") <> "" Then CityCheck = rst![SL01005]
                If Nz(rst![SL01104], "") <> "" Then StateCheck = rst![SL01104]
'UK
            ElseIf FN = 36000000 Or FN = 38000000 Or FN = 34000000 Or FN = 26000000 Then
                If Nz(rst![SL01003], "") <> "" Then AddCheck = rst![SL01003]
                If Nz(rst![SL01004], "") <> "" Then AddCheck = AddCheck & " " & rst![SL01004]
                If Nz(rst![SL01005], "") <> "" Then CityCheck = rst![SL01005]
                If Nz(rst![SL01083], "") <> "" Then PCodeCheck = rst![SL01083]
                If Nz(rst![SL01099], "") <> "" Then StateCheck = rst![SL01099]
            End If
            
            AAtext = AddCheck & ", " & CityCheck & ", " & StateCheck & ", " & PCodeCheck
            rst1![AA] = Left(AAtext, 250)
            If Nz(rst![SL01011], "") <> "" Then PNumber = rst![SL01011]
            If PNumber <> "" Then
                rst1![PhoneNumber] = Left(PNumber, 30)
            End If
            rst1.Update
            rst1.MoveNext
        Wend
        rst1.Close

        rst.Delete

        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Buildings From Scala into DFSD SB")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_BUIL_SYNC_U = False
    CloseProgress

End Function

Function SCALA_EQUI_SYNC_I() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim rst2 As Recordset
    Dim rst3 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim Eqt As String
    Dim i As Integer
    Dim TMP_BUIL As String
    Dim TMP_MODEL As String
    Dim LL As Integer
    Dim FN As Long
    Dim BNBR As String
    Dim EXIST_BNBR As String
    Dim ZZ As Integer
    
    FN = DLookup("[FirstNumber]", "General Office Info")
    
    On Error GoTo endd
    SCALA_EQUI_SYNC_I = True
        
    Set dbs = CurrentDb
    Call Call_prog("")
    source = "SELECT * FROM [Synch02] WHERE [ENTRY_TYPE] = 'I' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Equipments"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        BNBR = Convert_SS(rst![SC05004])
        If DCount("[FinRef]", "[BD-Equipments]", "[FinRef] Like '" & Convert_SS(rst![SC05002]) & "'") = 0 Then
            BID = 0
            
            ZZ = InStr(1, rst![SC05004], " ")
            If ZZ = 0 Then
                EXIST_BNBR = BNBR
            Else
                EXIST_BNBR = MID(rst![SC05004], 1, (ZZ - 1))
            End If
            
            If Nz(EXIST_BNBR, "") <> "" Then Call FIND_BID(EXIST_BNBR)

            
            If BID <> 0 Then
            
                rst1.AddNew
                rst1![EquipmentID] = DMax("[EquipmentID]", "BD-Equipments", "") + 1
                If Nz(rst![SC05004], "") <> "" Then rst1![BuildingID] = BID
                If Nz(rst![SC05002], "") <> "" Then rst1![Equipment Tag] = Left(rst![SC05002], 50)
                If Nz(rst![SC05001], "") <> "" Then
                    If MID(rst![SC05001], 1, 2) = "ZZ" Then
                        LL = Len(rst![SC05001])
                        TMP_MODEL = MID(rst![SC05001], 3, (LL - 2))
                        rst1![Model] = Left(TMP_MODEL, 100)
                      Else
                        TMP_MODEL = Left(rst![SC05001], 100)
                        rst1![Model] = Left(TMP_MODEL, 100)
                      End If
                End If
                If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                    If Nz(rst![SC05044], "") <> "" Then rst1![Serial Number] = Left(rst![SC05044], 100)
                Else
                    If Nz(rst![SC05002], "") <> "" Then rst1![Serial Number] = Left(rst![SC05002], 100)
                End If
                If Nz(rst![SC05002], "") <> "" Then rst1![FinRef] = Left(rst![SC05002], 100)
                If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                    If Nz(rst![SC05020], "") <> "" Then rst1![Sales Order #] = Left(rst![SC05020], 100)
                    If Nz(rst![SC05018], "") <> "" Then rst1![Special Notes] = Left(rst![SC05018], 250)
                    If Nz(rst![SC05018], "") <> "" Then rst1![EqFamily] = Left(rst![SC05003], 250)
                    If Nz(rst![SC05016], "") <> "" Then rst1![Comdate] = rst![SC05016]
                    If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                    If Nz(rst![SC05014], "") <> "" Then rst1![ShipDate] = rst![SC05014]
                Else
                    If Nz(rst![SC05006], "") <> "" Then rst1![Comdate] = rst![SC05006]
                    If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                    If Nz(rst![SC05006], "") <> "" Then rst1![ShipDate] = rst![SC05006]
                End If
                If Nz(rst![SC05001], "") <> "" Then
                    Eqt = MID(TMP_MODEL, 1, 3) & "*"
                    If DCount("[Equipment Type Description]", "Equipment Type", "[Equipment Type]=" & "'" & Eqt & "'") > 0 Then
                        rst1![Equipment Type] = Eqt
                        rst1![DFSorNDFS] = -1
                    Else
                        rst1![Equipment Type] = Left(Eqt, 3)
                        rst1![DFSorNDFS] = 0
                    End If
                End If
                    
                rst1![RecordedDate] = Date
                rst1![UpdatedDate] = Date
                rst1![Creator] = Xstr
                rst1.Update
            Else
' Check Customer file for matching then add it as Building then add equipment

                CID = 0
                If Nz(EXIST_BNBR, "") <> "" Then Call FIND_CID(EXIST_BNBR)
                If CID > 0 Then
                    source = "SELECT * FROM [BD-Customers] WHERE [CustomerID] = " & CID
                    Set rst2 = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
                    
                    Set rst3 = dbs.OpenRecordset("BD-Buildings", dbOpenDynaset, dbSeeChanges)
                    rst3.AddNew
                        rst3![BuildingID] = DMax("[BuildingID]", "BD-Buildings", "") + 1
                        TMP_BUIL = rst2![CustomerName]
                        i = Dupl_CheckI("BD-Buildings", "BuildingName", rst2![CustomerName], "BuildingID", rst1![BuildingID])
                        If i > 0 Then
                            TMP_BUIL = TMP_BUIL & "-" & i
                        End If
                        If Nz(rst2![CustomerName], "") <> "" Then rst3![BuildingName] = TMP_BUIL
                        If Nz(rst2![CustomerNumber], "") <> "" Then rst3![BuildingNumber] = Left(rst2![CustomerNumber], 50)
                        If Nz(rst2![Address], "") <> "" Then rst3![Address] = Left(rst2![Address], 250)
                        If Nz(rst2![PostalCode], "") <> "" Then rst3![PostalCode] = Left(rst2![PostalCode], 20)
                        If Nz(rst2![City], "") <> "" Then rst3![City] = Left(rst2![City], 100)
                        If Nz(rst2![PhoneNumber], "") <> "" Then rst3![PhoneNumber] = Left(rst2![PhoneNumber], 30)
                        If Nz(rst2![FaxNumber], "") <> "" Then rst3![FaxNumber] = Left(rst2![FaxNumber], 30)
                        If Nz(rst2![CustomerID], "") <> "" Then rst3![CustomerID] = rst2![CustomerID]
                        If Nz(rst2![State], "") <> "" Then rst3![State] = Left(rst2![State], 50)
                        If Nz(rst2![NationalAcc], "") <> "" Then rst3![NationalAcc] = Left(rst2![NationalAcc], 100)
                        rst3![RecordedDate] = Date
                        rst3![UpdatedDate] = Date
                        rst3![Creator] = Xstr & "-Customer"


                    rst3.Update
                    rst2.Close
                    rst3.Close
                End If
                BID = 0
                If Nz(EXIST_BNBR, "") <> "" Then Call FIND_BID(EXIST_BNBR)
                If BID <> 0 Then
                    rst1.AddNew
                    rst1![EquipmentID] = DMax("[EquipmentID]", "BD-Equipments", "") + 1
                    If Nz(rst![SC05004], "") <> "" Then rst1![BuildingID] = BID
                    If Nz(rst![SC05002], "") <> "" Then rst1![Equipment Tag] = Left(rst![SC05002], 50)
                    If Nz(rst![SC05001], "") <> "" Then
                        If MID(rst![SC05001], 1, 2) = "ZZ" Then
                            LL = Len(rst![SC05001])
                            TMP_MODEL = MID(rst![SC05001], 3, (LL - 2))
                            rst1![Model] = Left(TMP_MODEL, 100)
                          Else
                            TMP_MODEL = Left(rst![SC05001], 100)
                            rst1![Model] = Left(TMP_MODEL, 100)
                          End If
                    End If
                    If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                        If Nz(rst![SC05044], "") <> "" Then rst1![Serial Number] = Left(rst![SC05044], 100)
                    Else
                        If Nz(rst![SC05002], "") <> "" Then rst1![Serial Number] = Left(rst![SC05002], 100)
                    End If
                    If Nz(rst![SC05002], "") <> "" Then rst1![FinRef] = Left(rst![SC05002], 100)
                    If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                        If Nz(rst![SC05020], "") <> "" Then rst1![Sales Order #] = Left(rst![SC05020], 100)
                        If Nz(rst![SC05018], "") <> "" Then rst1![Special Notes] = Left(rst![SC05018], 250)
                        If Nz(rst![SC05018], "") <> "" Then rst1![EqFamily] = Left(rst![SC05003], 250)
                        If Nz(rst![SC05016], "") <> "" Then rst1![Comdate] = rst![SC05016]
                        If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                        If Nz(rst![SC05014], "") <> "" Then rst1![ShipDate] = rst![SC05014]
                    Else
                        If Nz(rst![SC05006], "") <> "" Then rst1![Comdate] = rst![SC05006]
                        If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                        If Nz(rst![SC05006], "") <> "" Then rst1![ShipDate] = rst![SC05006]
                    End If
                    If Nz(rst![SC05001], "") <> "" Then
                        Eqt = MID(TMP_MODEL, 1, 3) & "*"
                        If DCount("[Equipment Type Description]", "Equipment Type", "[Equipment Type]=" & "'" & Eqt & "'") > 0 Then
                            rst1![Equipment Type] = Eqt
                            rst1![DFSorNDFS] = -1
                        Else
                            rst1![Equipment Type] = Left(Eqt, 3)
                            rst1![DFSorNDFS] = 0
                        End If
                    End If
                        
                    rst1![RecordedDate] = Date
                    rst1![UpdatedDate] = Date
                    rst1![Creator] = Xstr
                    rst1.Update
                End If
            End If
            rst.Delete
        Else
            rst.Edit
            rst![ENTRY_TYPE] = "U"
            rst.Update
        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising New Equipment From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set rst3 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_EQUI_SYNC_I = False
    CloseProgress

End Function

Function SCALA_EQUI_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim rst2 As Recordset
    Dim rst3 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim Eqt As String
    Dim TMP_MODEL As String
    Dim TMP_BUIL As String
    Dim LL As Integer
    Dim i As Integer
    Dim FN As Long
    Dim ENBR As String
    Dim EXIST_ENBR As String
    Dim ZZ As Integer
    
    FN = DLookup("[FirstNumber]", "General Office Info")
    
    On Error GoTo endd
    SCALA_EQUI_SYNC_U = True
        
    Set dbs = CurrentDb
    
    Call Call_prog("")
    source = "SELECT * FROM [Synch02] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF

        ENBR = Convert_SS(rst![SC05002])
        source1 = "SELECT * FROM [BD-Equipments] WHERE [FinRef] Like '" & ENBR & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        If rst1.RecordCount = 0 Then
            ZZ = InStr(1, rst![SC05002], " ")
            If ZZ = 0 Then
                EXIST_ENBR = ENBR
            Else
                EXIST_ENBR = MID(rst![SC05002], 1, (ZZ - 1))
            End If
            rst1.Close
            source1 = "SELECT * FROM [BD-Equipments] WHERE [FinRef] Like '" & EXIST_ENBR & "'"
            Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                rst1.Edit
                rst1![FinRef] = ENBR
                rst1.Update
            End If
        End If
        rst1.Close
        source1 = "SELECT * FROM [BD-Equipments] WHERE [FinRef] Like '" & ENBR & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        
        If rst1.RecordCount > 0 Then
            BID = 0
            Call FIND_BID(CStr(rst![SC05004]))
            rst1.Edit
            If Nz(rst![SC05004], "") <> "" Then rst1![BuildingID] = BID
            If Nz(rst![SC05002], "") <> "" Then rst1![Equipment Tag] = Left(rst![SC05002], 50)
            If Nz(rst![SC05001], "") <> "" Then
                If MID(rst![SC05001], 1, 2) = "ZZ" Then
                    LL = Len(rst![SC05001])
                    TMP_MODEL = MID(rst![SC05001], 3, (LL - 2))
                    rst1![Model] = Left(TMP_MODEL, 100)
                Else
                    TMP_MODEL = Left(rst![SC05001], 100)
                    rst1![Model] = Left(TMP_MODEL, 100)
                End If
            End If
            If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SC05044], "") <> "" Then rst1![Serial Number] = Left(rst![SC05044], 100)
            Else
                If Nz(rst![SC05002], "") <> "" Then rst1![Serial Number] = Left(rst![SC05002], 100)
            End If
            If Nz(rst![SC05002], "") <> "" Then rst1![FinRef] = Left(rst![SC05002], 100)
            If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                If Nz(rst![SC05020], "") <> "" Then rst1![Sales Order #] = Left(rst![SC05020], 100)
                If Nz(rst![SC05018], "") <> "" Then rst1![Special Notes] = Left(rst![SC05018], 250)
                If Nz(rst![SC05018], "") <> "" Then rst1![EqFamily] = Left(rst![SC05003], 250)
                If Nz(rst![SC05016], "") <> "" Then rst1![Comdate] = rst![SC05016]
                If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                If Nz(rst![SC05014], "") <> "" Then rst1![ShipDate] = rst![SC05014]
            Else
                If FN = 44000000 Or FN = 128000000 Then
    ' do not do this for SPANISH scala implementation
                Else
                    If Nz(rst![SC05006], "") <> "" Then rst1![Comdate] = rst![SC05006]
                    If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                    If Nz(rst![SC05006], "") <> "" Then rst1![ShipDate] = rst![SC05006]
                End If
            End If
            If Nz(rst![SC05001], "") <> "" Then
                Eqt = MID(TMP_MODEL, 1, 3) & "*"
                If DCount("[Equipment Type Description]", "Equipment Type", "[Equipment Type]=" & "'" & Eqt & "'") > 0 Then
                    rst1![Equipment Type] = Eqt
                    rst1![DFSorNDFS] = -1
                Else
                    rst1![Equipment Type] = Left(Eqt, 3)
                    rst1![DFSorNDFS] = 0
                End If
            End If
            rst1![UpdatedDate] = Date
            rst1.Update
            BN = 0
            Call FIND_BNO(rst1![BuildingID])
            If rst![SC05004] <> BN Then
' Building has changed.  Do necessary changes
            If BID > 0 Then Call Correct_FNB(rst1![EquipmentID], BID)
                
            End If
        Else

' Equipment does not exist in DFSD it must be added (SAME CODE AS EQUIMENT ADD BELOW)
            BID = 0
            If Nz(rst![SC05004], "") <> "" Then Call FIND_BID(CStr(rst![SC05004]))
                If BID <> 0 Then
                    rst1.AddNew
                    rst1![EquipmentID] = DMax("[EquipmentID]", "BD-Equipments", "") + 1
                    If Nz(rst![SC05004], "") <> "" Then rst1![BuildingID] = BID
                    If Nz(rst![SC05002], "") <> "" Then rst1![Equipment Tag] = Left(rst![SC05002], 50)
                    If Nz(rst![SC05001], "") <> "" Then
                    If MID(rst![SC05001], 1, 2) = "ZZ" Then
                        LL = Len(rst![SC05001])
                        TMP_MODEL = MID(rst![SC05001], 3, (LL - 2))
                        rst1![Model] = Left(TMP_MODEL, 100)
                    Else
                        TMP_MODEL = Left(rst![SC05001], 100)
                        rst1![Model] = Left(TMP_MODEL, 100)
                    End If
                End If
                If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                    If Nz(rst![SC05044], "") <> "" Then rst1![Serial Number] = Left(rst![SC05044], 100)
                Else
                    If Nz(rst![SC05002], "") <> "" Then rst1![Serial Number] = Left(rst![SC05002], 100)
                End If
                If Nz(rst![SC05002], "") <> "" Then rst1![FinRef] = Left(rst![SC05002], 100)
                If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                    If Nz(rst![SC05020], "") <> "" Then rst1![Sales Order #] = Left(rst![SC05020], 100)
                    If Nz(rst![SC05018], "") <> "" Then rst1![Special Notes] = Left(rst![SC05018], 250)
                    If Nz(rst![SC05018], "") <> "" Then rst1![EqFamily] = Left(rst![SC05003], 250)
                    If Nz(rst![SC05016], "") <> "" Then rst1![Comdate] = rst![SC05016]
                    If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                    If Nz(rst![SC05014], "") <> "" Then rst1![ShipDate] = rst![SC05014]
                Else
                    If Nz(rst![SC05006], "") <> "" Then rst1![Comdate] = rst![SC05006]
                    If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                    If Nz(rst![SC05006], "") <> "" Then rst1![ShipDate] = rst![SC05006]
                End If
                If Nz(rst![SC05001], "") <> "" Then
                    Eqt = MID(TMP_MODEL, 1, 3) & "*"
                    If DCount("[Equipment Type Description]", "Equipment Type", "[Equipment Type]=" & "'" & Eqt & "'") > 0 Then
                        rst1![Equipment Type] = Eqt
                        rst1![DFSorNDFS] = -1
                    Else
                        rst1![Equipment Type] = Left(Eqt, 3)
                        rst1![DFSorNDFS] = 0
                    End If
                End If
                    
                rst1![RecordedDate] = Date
                rst1![UpdatedDate] = Date
                rst1![Creator] = Xstr
                rst1.Update
            Else
' Check Customer file for matching then add it as Building then add equipment
                CID = 0
                If Nz(rst![SC05004], "") <> "" Then Call FIND_CID(CStr(rst![SC05004]))
                If CID > 0 Then
                    source = "SELECT * FROM [BD-Customers] WHERE [CustomerID] = " & CID
                    Set rst2 = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
                    
                    Set rst3 = dbs.OpenRecordset("BD-Buildings", dbOpenDynaset, dbSeeChanges)
                    rst3.AddNew
                    rst3![BuildingID] = DMax("[BuildingID]", "BD-Buildings", "") + 1
                    TMP_BUIL = rst2![CustomerName]
                    i = Dupl_CheckI("BD-Buildings", "BuildingName", rst2![CustomerName], "BuildingID", rst3![BuildingID])
'OLD CODE NEED TO CHECK                    I = Dupl_CheckI("BD-Buildings", "BuildingName", rst2![CustomerName], "BuildingID", rst1![BuildingID])
                    If i > 0 Then
                        TMP_BUIL = TMP_BUIL & "-" & i
                    End If
                    If Nz(rst2![CustomerName], "") <> "" Then rst3![BuildingName] = TMP_BUIL
                    If Nz(rst2![CustomerNumber], "") <> "" Then rst3![BuildingNumber] = Left(rst2![CustomerNumber], 50)
                    If Nz(rst2![Address], "") <> "" Then rst3![Address] = Left(rst2![Address], 250)
                    If Nz(rst2![PostalCode], "") <> "" Then rst3![PostalCode] = Left(rst2![PostalCode], 20)
                    If Nz(rst2![City], "") <> "" Then rst3![City] = Left(rst2![City], 100)
                    If Nz(rst2![PhoneNumber], "") <> "" Then rst3![PhoneNumber] = Left(rst2![PhoneNumber], 30)
                    If Nz(rst2![FaxNumber], "") <> "" Then rst3![FaxNumber] = Left(rst2![FaxNumber], 30)
                    If Nz(rst2![CustomerID], "") <> "" Then rst3![CustomerID] = rst2![CustomerID]
                    If Nz(rst2![State], "") <> "" Then rst3![State] = Left(rst2![State], 50)
                    If Nz(rst2![NationalAcc], "") <> "" Then rst3![NationalAcc] = Left(rst2![NationalAcc], 100)
                    rst3![RecordedDate] = Date
                    rst3![UpdatedDate] = Date
                    rst3![Creator] = Xstr & "-Customer"
                    rst3.Update
                    rst2.Close
                    rst3.Close
                End If
                BID = 0
                If Nz(rst![SC05004], "") <> "" Then Call FIND_BID(CStr(rst![SC05004]))
                If BID <> 0 Then
                    rst1.AddNew
                    rst1![EquipmentID] = DMax("[EquipmentID]", "BD-Equipments", "") + 1
                    If Nz(rst![SC05004], "") <> "" Then rst1![BuildingID] = BID
                    If Nz(rst![SC05002], "") <> "" Then rst1![Equipment Tag] = Left(rst![SC05002], 50)
                    If Nz(rst![SC05001], "") <> "" Then
                        If MID(rst![SC05001], 1, 2) = "ZZ" Then
                            LL = Len(rst![SC05001])
                            TMP_MODEL = MID(rst![SC05001], 3, (LL - 2))
                            rst1![Model] = Left(TMP_MODEL, 100)
                          Else
                            TMP_MODEL = Left(rst![SC05001], 100)
                            rst1![Model] = Left(TMP_MODEL, 100)
                          End If
                    End If
                    If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                        If Nz(rst![SC05044], "") <> "" Then rst1![Serial Number] = Left(rst![SC05044], 100)
                    Else
                        If Nz(rst![SC05002], "") <> "" Then rst1![Serial Number] = Left(rst![SC05002], 100)
                    End If
                    If Nz(rst![SC05002], "") <> "" Then rst1![FinRef] = Left(rst![SC05002], 100)
                    If FN = 142000000 Or FN = 156000000 Or FN = 158000000 Then
                        If Nz(rst![SC05020], "") <> "" Then rst1![Sales Order #] = Left(rst![SC05020], 100)
                        If Nz(rst![SC05018], "") <> "" Then rst1![Special Notes] = Left(rst![SC05018], 250)
                        If Nz(rst![SC05018], "") <> "" Then rst1![EqFamily] = Left(rst![SC05003], 250)
                        If Nz(rst![SC05016], "") <> "" Then rst1![Comdate] = rst![SC05016]
                        If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                        If Nz(rst![SC05014], "") <> "" Then rst1![ShipDate] = rst![SC05014]
                    Else
                        If Nz(rst![SC05006], "") <> "" Then rst1![Comdate] = rst![SC05006]
                        If Nz(rst![SC05007], "") <> "" Then rst1![Warranty] = rst![SC05007]
                        If Nz(rst![SC05006], "") <> "" Then rst1![ShipDate] = rst![SC05006]
                    End If
                    If Nz(rst![SC05001], "") <> "" Then
                        Eqt = MID(TMP_MODEL, 1, 3) & "*"
                        If DCount("[Equipment Type Description]", "Equipment Type", "[Equipment Type]=" & "'" & Eqt & "'") > 0 Then
                            rst1![Equipment Type] = Eqt
                            rst1![DFSorNDFS] = -1
                        Else
                            rst1![Equipment Type] = Left(Eqt, 3)
                            rst1![DFSorNDFS] = 0
                        End If
                    End If
                        
                    rst1![RecordedDate] = Date
                    rst1![UpdatedDate] = Date
                    rst1![Creator] = Xstr
                    rst1.Update
                End If
            End If
        End If
        rst1.Close
        rst.Delete
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Equipment From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
        
    Wend
    
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set rst3 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_EQUI_SYNC_U = False
    CloseProgress

End Function
Function SCALA_TECH_SYNC_I() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim KK As Long
    Dim FL As String
    Dim LL As String
    Dim Okay As Boolean
    Dim i As Integer

    On Error GoTo endd
    SCALA_TECH_SYNC_I = True
        
    Set dbs = CurrentDb
    
    Call Call_prog("")

    source = "SELECT * FROM [Synch01] WHERE [ENTRY_TYPE] = 'I' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "Technicians"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        If DCount("[FinRef]", "[Technicians]", "[FinRef] Like '" & Convert_SS(rst![HR01001]) & "'") = 0 Then
           rst1.AddNew
           If DCount("[TechnicianID]", "Technicians") > 0 Then
               rst1![TechnicianID] = DMax("[TechnicianID]", "Technicians", "") + 1
           Else
               rst1![[TechnicianID]] = DLookup("[FirstNumber]", "General Office Info")
           End If
           If Nz(rst![HR01001], "") <> "" Then rst1![FinRef] = Left(rst![HR01001], 100)
           If Nz(rst![HR01002], "") <> "" Then
               KK = InStr(1, rst![HR01002], " ")
               rst1![TechFirstName] = Left(rst![HR01002], KK - 1)
               rst1![TechLastName] = MID(rst![HR01002], KK + 1, Len(rst![HR01002]) - KK)
               FL = UCase(Left(rst1![TechFirstName], 1))
               LL = UCase(Left(rst1![TechLastName], 1))
               If (Asc(FL) < 65 Or Asc(FL) > 90) Then FL = "T"
               If (Asc(LL) < 65 Or Asc(LL) > 90) Then LL = "A"
           Else
               rst1![TechFirstName] = Create_Msg(Forms![MENU-NEW]![Text118], 453)
               rst1![TechLastName] = Create_Msg(Forms![MENU-NEW]![Text118], 452)
               FL = "T"
               LL = "A"
           End If
               
           Okay = False
                   
            Do
                i = Dupl_CheckT1("Technicians", "TechCode", FL & LL & "1", "TechnicianID", rst1![TechnicianID])
                If i > 0 Then
                    If i < 10 Then
                        rst1![TechCode] = FL & LL & i
                        Okay = True
                    Else
                        LL = Chr(Asc(LL) + 1)
                        If Asc(LL) > 91 Then
                            LL = "A"
                            FL = Chr(Asc(FL) + 1)
                            If Asc(FL) > 91 Then
                                FL = "A"
                            End If
                        End If
                    End If
                Else
                    rst1![TechCode] = FL & LL & "1"
                    Okay = True
                End If
            Loop Until Okay = True
                   
            If Nz(rst![HR01003], "") <> "" Then rst1![Address] = Left(rst![HR01003], 200)
            If Nz(rst![HR01004], "") <> "" Then rst1![City] = Left(rst![HR01004], 50)
    '       If Nz(rst![HR01005], "") <> "" Then rst1![PostalCode] = Left(rst![HR01005], 20)
    '       If Nz(rst![HR01006], "") <> "" Then rst1![State] = Left(rst![HR01007], 20)
            If Nz(rst![HR01007], "") <> "" Then rst1![PhoneNumber] = Left(rst![HR01007], 30)
            If Nz(rst![HR01008], "") <> "" Then rst1![MobileNumber] = Left(rst![HR01008], 30)
            rst1![Team] = "A"
            rst1.Update
            rst.Delete
        Else
            rst.Edit
            rst![ENTRY_TYPE] = "U"
            rst.Update
        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising New Technicians From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
        
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_TECH_SYNC_I = False
    CloseProgress

End Function

Function SCALA_TECH_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim KK As Long
    Dim FL As String
    Dim LL As String
    Dim Okay As Boolean
    Dim i As Integer
    
    On Error GoTo endd
    SCALA_TECH_SYNC_U = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")

    source = "SELECT * FROM [Synch01] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        source1 = "SELECT * FROM [Technicians] WHERE [FinRef] Like '" & Convert_SS(Left(rst![HR01001], 100)) & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        If rst1.RecordCount > 0 Then
            rst1.Edit
            If Nz(rst![HR01002], "") <> "" Then
                KK = InStr(1, rst![HR01002], " ")
                rst1![TechFirstName] = Left(rst![HR01002], KK - 1)
                rst1![TechLastName] = MID(rst![HR01002], KK + 1, Len(rst![HR01002]) - KK)
            Else
                rst1![TechFirstName] = Create_Msg(Forms![MENU-NEW]![Text118], 453)
                rst1![TechLastName] = Create_Msg(Forms![MENU-NEW]![Text118], 452)
            End If
            If Nz(rst![HR01003], "") <> "" Then rst1![Address] = Left(rst![HR01003], 200)
            If Nz(rst![HR01004], "") <> "" Then rst1![City] = Left(rst![HR01004], 50)
'           If Nz(rst![HR01005], "") <> "" Then rst1![PostalCode] = Left(rst![HR01005], 20)
'          If Nz(rst![HR01006], "") <> "" Then rst1![State] = Left(rst![HR01007], 20)
            If Nz(rst![HR01007], "") <> "" Then rst1![PhoneNumber] = Left(rst![HR01007], 30)
            If Nz(rst![HR01008], "") <> "" Then rst1![MobileNumber] = Left(rst![HR01008], 30)
            rst1.Update
        Else
' Technician does not exist in DFSD it must be added
           rst1.AddNew
           If DCount("[TechnicianID]", "Technicians") > 0 Then
               rst1![TechnicianID] = DMax("[TechnicianID]", "Technicians", "") + 1
           Else
               rst1![[TechnicianID]] = DLookup("[FirstNumber]", "General Office Info")
           End If
           If Nz(rst![HR01001], "") <> "" Then rst1![FinRef] = Left(rst![HR01001], 100)
           If Nz(rst![HR01002], "") <> "" Then
               KK = InStr(1, rst![HR01002], " ")
               rst1![TechFirstName] = Left(rst![HR01002], KK - 1)
               rst1![TechLastName] = MID(rst![HR01002], KK + 1, Len(rst![HR01002]) - KK)
               FL = UCase(Left(rst1![TechFirstName], 1))
               LL = UCase(Left(rst1![TechLastName], 1))
               If (Asc(FL) < 65 Or Asc(FL) > 90) Then FL = "T"
               If (Asc(LL) < 65 Or Asc(LL) > 90) Then LL = "A"
           Else
               rst1![TechFirstName] = Create_Msg(Forms![MENU-NEW]![Text118], 453)
               rst1![TechLastName] = Create_Msg(Forms![MENU-NEW]![Text118], 452)
               FL = "T"
               LL = "A"
           End If
               
           Okay = False
                   
            Do
                i = Dupl_CheckT1("Technicians", "TechCode", FL & LL & "1", "TechnicianID", rst1![TechnicianID])
                If i > 0 Then
                    If i < 10 Then
                        rst1![TechCode] = FL & LL & i
                        Okay = True
                    Else
                        LL = Chr(Asc(LL) + 1)
                        If Asc(LL) > 91 Then
                            LL = "A"
                            FL = Chr(Asc(FL) + 1)
                            If Asc(FL) > 91 Then
                                FL = "A"
                            End If
                        End If
                    End If
                Else
                    rst1![TechCode] = FL & LL & "1"
                    Okay = True
                End If
            Loop Until Okay = True
                   
            If Nz(rst![HR01003], "") <> "" Then rst1![Address] = Left(rst![HR01003], 200)
            If Nz(rst![HR01004], "") <> "" Then rst1![City] = Left(rst![HR01004], 50)
    '       If Nz(rst![HR01005], "") <> "" Then rst1![PostalCode] = Left(rst![HR01005], 20)
    '       If Nz(rst![HR01006], "") <> "" Then rst1![State] = Left(rst![HR01007], 20)
            If Nz(rst![HR01007], "") <> "" Then rst1![PhoneNumber] = Left(rst![HR01007], 30)
            If Nz(rst![HR01008], "") <> "" Then rst1![MobileNumber] = Left(rst![HR01008], 30)
            rst1![Team] = "A"
            rst1.Update
        
        End If
        rst1.Close
        rst.Delete
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Technicians From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
       
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_TECH_SYNC_U = False
    CloseProgress

End Function

Function SCALA_PART_SYNC_I() As Boolean

    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_CUST As String
    Dim PCStr As String
    Dim PCpos As Integer
    Dim NewPC As String
    Dim DP As String
    Dim FN As Long
    
    FN = DLookup("[FirstNumber]", "General Office Info")

    On Error GoTo endd
    SCALA_PART_SYNC_I = True

    Set dbs = CurrentDb

    Call Call_prog("")
    source = "SELECT * FROM [Synch04] WHERE [ENTRY_TYPE] = 'I' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Parts List"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If

    If FN = 58000000 Or FN = 60000000 Then
        DoCmd.SetWarnings False
        DoCmd.OpenQuery "Part Synch"
        DoCmd.SetWarnings True
    End If

    DP = Format(CDbl(1), "#0.00")
    DP = MID(DP, 2, 1)
    While Not rst.EOF
        'If rst![SC01124] = "999999" Then
            If DCount("[PartNumber]", "[BD-Parts List]", "[PartNumber] = '" & rst![SC01001] & "'") = 0 Then
                rst1.AddNew
                If DCount("[PartID]", "BD-Parts List") > 0 Then
                    If DMax("[PartID]", "BD-Parts List", "") > 0 Then
                        rst1![PartID] = DMax("[PartID]", "BD-Parts List", "") + 1
                    Else
                        rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
                    End If
                Else
                    rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
                End If

                If Nz(rst![SC01001], "") <> "" Then rst1![PartNumber] = Left(rst![SC01001], 50)
                If Nz(rst![SC01002], "") <> "" Then rst1![PartDescription] = Left((rst![SC01002] & " " & rst![SC01003]), 250)
                If Nz(rst![SC01055], "") <> "" And Nz(rst![SC01140], "") <> "" Then
                    PCStr = CStr(rst![SC01055])
                    PCpos = InStr(1, PCStr, ".")
                    If PCpos = 0 Then
                        NewPC = PCStr
                    Else
                        NewPC = MID(PCStr, 1, PCpos - 1)
                        NewPC = NewPC & DP & MID(PCStr, PCpos + 1, Len(PCStr) - PCpos)
'                    If MID(NewPC, 1, 1) = DP Then NewPC = MID(NewPC, 2, (Len(NewPC) - 1))
                    End If
                    If rst![SC01140] > 0 Then
                        rst1![PartCost] = CSng(NewPC) / CSng(rst![SC01140])
                    Else
                        rst1![PartCost] = CSng(NewPC)
                    End If
                End If
                If Nz(rst![SC01134], "") <> "" Then
                    If rst![SC01134] = 0 Then rst1![PartUnit] = "Nr"
                    If rst![SC01134] = 1 Then rst1![PartUnit] = "kg"
                    If rst![SC01134] = 2 Then rst1![PartUnit] = "l"
                    If rst![SC01134] = 3 Then rst1![PartUnit] = "m"
                    If rst![SC01134] = 4 Then rst1![PartUnit] = "mc"
                    If rst![SC01134] = 5 Then rst1![PartUnit] = "km"
                    If rst![SC01134] = 6 Then rst1![PartUnit] = "h"
                    If rst![SC01134] = 7 Then rst1![PartUnit] = "gg"
                    If rst![SC01134] = 8 Then rst1![PartUnit] = "mq"
                    
' add more here later if required

                Else
                    If rst![SC01134] = 0 Then rst1![PartUnit] = "Nr"
                
                End If
                If Nz(rst![SC01010], "") <> "" Then
                    If rst![SC01010] = " " Then
                        rst1![Stock] = 0
                    Else
                        If IsNumeric(rst![SC01010]) Then
                            rst1![Stock] = rst![SC01010]
                        Else
                            rst1![Stock] = 0
                        End If
                    End If
                End If
                If Nz(rst![SC01062], "") <> "" Then rst1![LeadTime] = rst![SC01062]
        
        
                PCStr = CStr(rst![SC01004])
                PCpos = InStr(1, PCStr, ".")
                If PCpos = 0 Then
                    NewPC = PCStr
                Else
                    NewPC = MID(PCStr, 1, PCpos - 1)
                    NewPC = NewPC & DP & MID(PCStr, PCpos + 1, Len(PCStr) - PCpos)
                End If
        
                If FN = 146000000 Or FN = 143000000 Or FN = 154000000 Then 'UAE and india
                Else
                    If Nz(rst![SC01004], "") <> "" Then rst1![SC] = CSng(NewPC)
                End If
                
                rst1![UpdatedOn] = Date
                rst1![UpdatedBy] = Xstr
                rst1.Update
                rst.Delete
            Else
                rst.Edit
                rst![ENTRY_TYPE] = "U"
                rst.Update
            End If
        'Else
        '    rst.Delete
        'End If

        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising new Parts From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend

    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_PART_SYNC_I = False
    CloseProgress
End Function

Function SCALA_PART_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_CUST As String
    Dim PCStr As String
    Dim PCpos As Integer
    Dim NewPC As String
    Dim DP As String
    Dim FN As Long
    
    FN = DLookup("[FirstNumber]", "General Office Info")
    
    On Error GoTo endd
    SCALA_PART_SYNC_U = True

    Set dbs = CurrentDb

    Call Call_prog("")
    source = "SELECT * FROM [Synch04] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    If FN = 58000000 Or FN = 60000000 Then
        DoCmd.SetWarnings False
        DoCmd.OpenQuery "Part Synch"
        DoCmd.SetWarnings True
    End If
    
    DP = Format(CDbl(1), "#0.00")
    DP = MID(DP, 2, 1)
    While Not rst.EOF
        source1 = "SELECT * FROM [BD-Parts List] WHERE [PartNumber] Like '" & Convert_SS(rst![SC01001]) & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        If rst1.RecordCount > 0 Then
            rst1.Edit
            If Nz(rst![SC01001], "") <> "" Then rst1![PartNumber] = Left(rst![SC01001], 50)
            If Nz(rst![SC01002], "") <> "" Then rst1![PartDescription] = Left((rst![SC01002] & " " & rst![SC01003]), 250)
            If Nz(rst![SC01055], "") <> "" And Nz(rst![SC01140], "") <> "" Then

                PCStr = CStr(rst![SC01055])
                PCpos = InStr(1, PCStr, ".")
                If PCpos = 0 Then
                    NewPC = PCStr
                Else
                    NewPC = MID(PCStr, 1, PCpos - 1)
                    NewPC = NewPC & DP & MID(PCStr, PCpos + 1, Len(PCStr) - PCpos)
                End If
'                If MID(NewPC, 1, 1) = DP Then NewPC = MID(NewPC, 2, (Len(NewPC) - 1))
                If rst![SC01055] > 0 Then
                    If rst![SC01140] > 0 Then
                        rst1![PartCost] = CSng(NewPC) / CSng(rst![SC01140])
                    Else
                        rst1![PartCost] = CSng(NewPC)
                    End If
                End If
            End If
            If Nz(rst![SC01062], "") <> "" Then rst1![LeadTime] = rst![SC01062]
                
            PCStr = CStr(rst![SC01004])
            PCpos = InStr(1, PCStr, ".")
            If PCpos = 0 Then
                NewPC = PCStr
            Else
                NewPC = MID(PCStr, 1, PCpos - 1)
                NewPC = NewPC & DP & MID(PCStr, PCpos + 1, Len(PCStr) - PCpos)
            End If
            
            If FN = 146000000 Or FN = 143000000 Or FN = 154000000 Then
            Else
                If Nz(rst![SC01004], "") <> "" Then rst1![SC] = CSng(NewPC)
            End If
            rst1![UpdatedOn] = Date
            rst1![UpdatedBy] = Xstr
            rst1.Update
        Else
'Part does not exist in DFSD it must be added (SAME CODE AS PART ADD BELOW)
            If DCount("[PartNumber]", "[BD-Parts List]", "[PartNumber] = '" & rst![SC01001] & "'") = 0 Then
                rst1.AddNew
                If DCount("[PartID]", "BD-Parts List") > 0 Then
                    If DMax("[PartID]", "BD-Parts List", "") > 0 Then
                        rst1![PartID] = DMax("[PartID]", "BD-Parts List", "") + 1
                    Else
                        rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
                    End If
                Else
                    rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
                End If
                If Nz(rst![SC01001], "") <> "" Then rst1![PartNumber] = Left(rst![SC01001], 50)
                If Nz(rst![SC01002], "") <> "" Then rst1![PartDescription] = Left((rst![SC01002] & " " & rst![SC01003]), 250)
                If Nz(rst![SC01055], "") <> "" And Nz(rst![SC01140], "") <> "" Then
                    PCStr = CStr(rst![SC01055])
                    PCpos = InStr(1, PCStr, ".")
                    If PCpos = 0 Then
                        NewPC = PCStr
                    Else
                        NewPC = MID(PCStr, 1, PCpos - 1)
                        NewPC = NewPC & DP & MID(PCStr, PCpos + 1, Len(PCStr) - PCpos)
                    End If
'                    If MID(NewPC, 1, 1) = DP Then NewPC = MID(NewPC, 2, (Len(NewPC) - 1))
                    If rst![SC01140] > 0 Then
                        rst1![PartCost] = CSng(NewPC) / CSng(rst![SC01140])
                    Else
                        rst1![PartCost] = CSng(NewPC)
                    End If
                End If
                If Nz(rst![SC01134], "") <> "" Then
                    If rst![SC01134] = 0 Then rst1![PartUnit] = "Nr"
                    If rst![SC01134] = 1 Then rst1![PartUnit] = "kg"
                    If rst![SC01134] = 2 Then rst1![PartUnit] = "l"
                    If rst![SC01134] = 3 Then rst1![PartUnit] = "m"
                    If rst![SC01134] = 4 Then rst1![PartUnit] = "mc"
                    If rst![SC01134] = 5 Then rst1![PartUnit] = "km"
                    If rst![SC01134] = 6 Then rst1![PartUnit] = "h"
                    If rst![SC01134] = 7 Then rst1![PartUnit] = "gg"
                    If rst![SC01134] = 8 Then rst1![PartUnit] = "mq"
                    
' add more here later if required

                Else
                    If rst![SC01134] = 0 Then rst1![PartUnit] = "Nr"
                
                End If
                If Nz(rst![SC01010], "") <> "" Then
                    If rst![SC01010] = " " Then
                        rst1![Stock] = 0
                    Else
                        If IsNumeric(rst![SC01010]) Then
                            rst1![Stock] = rst![SC01010]
                        Else
                            rst1![Stock] = 0
                        End If
                    End If
                End If
                If Nz(rst![SC01062], "") <> "" Then rst1![LeadTime] = rst![SC01062]
                
                
                PCStr = CStr(rst![SC01004])
                PCpos = InStr(1, PCStr, ".")
                If PCpos = 0 Then
                    NewPC = PCStr
                Else
                    NewPC = MID(PCStr, 1, PCpos - 1)
                    NewPC = NewPC & DP & MID(PCStr, PCpos + 1, Len(PCStr) - PCpos)
                End If
                
                If FN = 146000000 Or FN = 143000000 Or FN = 154000000 Then
                Else
                    If Nz(rst![SC01004], "") <> "" Then rst1![SC] = CSng(NewPC)
                End If
                
                rst1![UpdatedOn] = Date
                rst1![UpdatedBy] = Xstr
                rst1.Update
            End If
        End If
        rst1.Close
        rst.Delete
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Parts From Scala into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend

    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_PART_SYNC_U = False
    CloseProgress

End Function

Function RunSynch() As Boolean
    Dim dbs As Database
    Dim rstSS As Recordset
    Dim strSql As String
    Dim LOCATION_Synch As String
    Dim retVal As Double
    Dim Path As String
    Dim LL As String
    Dim LK As String
    Dim Integrate As Integer
    Dim SNameInteg As String
    Dim OfficeFlag As String
    
    LOCATION_Synch = Forms![MENU-NEW]![Text121]
    Set dbs = CurrentDb

    strSql = "SELECT * FROM [TRN] WHERE [FirstNumber] = " & DLookup("[FirstNumber]", "General Office Info")
    Set rstSS = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If rstSS.RecordCount > 0 Then
        SNameInteg = rstSS![DBName]
        Integrate = rstSS![DBID]
        OfficeFlag = rstSS![Office]
    End If
    rstSS.Close

    
On Error GoTo endd

Select Case Integrate
    Case 1
        Open LOCATION_Synch & "\Settings.ini" For Input As #1
            Do While Not EOF(1)
                Input #1, LL
                If Left(LL, 15) = "LastExportDate=" Then Exit Do
            Loop
        Close #1
        Path = LOCATION_Synch & "\SCALA_DFSD_interface.exe"
        retVal = Shell(Path, vbMinimizedNoFocus)
        
        Do
            Call Delay(45)
            
            Open LOCATION_Synch & "\Settings.ini" For Input As #1
                Do While Not EOF(1)
                    Input #1, LK
                    If Left(LL, 15) = "LastExportDate=" Then Exit Do
                Loop
            Close #1
        Loop While LL = LK
    Case 3
        'run code to create text file from Online
    End Select
   
    Call ITG(Integrate, LOCATION_Synch, OfficeFlag, SNameInteg)
    
    RunSynch = True
    Set rstSS = Nothing
    Set dbs = Nothing
    Exit Function
endd:
    RunSynch = False
    MsgBox "Not able to do, either not connected to network or necessary files are available"
End Function
Function Dupl_CheckI(TN As String, FN As String, NN As String, CKF As String, IDD As Long) As Integer
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim NK As String
    Dim i As Integer
    
    i = 0
    Dupl_CheckI = 0
    NK = NN

If InStr(1, NK, "|") Then
    MsgBox (NK)
    Exit Function
End If

    
    Set dbs = CurrentDb
    While i = 0
        strSql = "SELECT * FROM [" & TN & "] WHERE [" & FN & "] Like '" & Convert_SS(NK) & "' And [" & CKF & "] <> " & IDD
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        If rst.RecordCount > 0 Then
            Dupl_CheckI = Dupl_CheckI + 1
            NK = NN & "-" & Dupl_CheckI
        Else
            i = 1
        End If
        rst.Close
    Wend
    Set rst = Nothing
    Set dbs = Nothing
End Function
Function Dupl_CheckT1(TN As String, FN As String, NN As String, CKF As String, IDD As Long) As Integer
    Dim dbs As Database
    Dim rst As Recordset
    Dim strSql As String
    Dim NK As String
    Dim i As Integer
    Dim IK As Integer
    
    i = 0
    Dupl_CheckT1 = 0
    NK = NN
    
    Set dbs = CurrentDb
    
    While i = 0
        strSql = "SELECT * FROM [" & TN & "] WHERE [" & FN & "] Like '" & Convert_SS(NK) & "' And [" & CKF & "] <> " & IDD
        Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        
        If rst.RecordCount > 0 Then
            Dupl_CheckT1 = Dupl_CheckT1 + 1
            NK = Left(NN, 2) & Dupl_CheckT1
        Else
            i = 1
        End If
        rst.Close
        If Dupl_CheckT1 = 10 Then
            i = 1
        End If
    Wend
    Set rst = Nothing
    Set dbs = Nothing
End Function



Private Function Correct_Eq_Table() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim SerN(100000) As String
    Dim BuiN(100000) As String
    Dim Count As Integer
    Dim i As Integer
         
    On Error GoTo endd
    Correct_Eq_Table = True
    Set dbs = CurrentDb

' CHANGE TABLE NAME HERE
    source = "SELECT * FROM [Synch02] ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    rst.MoveLast
    If rst.RecordCount > 0 Then
        rst.MoveFirst
        i = 0
        While Not rst.EOF
            If Nz(rst![SC05004], "") <> "" Then
                i = i + 1
                SerN(i) = rst![SC05002]
                BuiN(i) = rst![SC05004]
            End If
            rst.MoveNext
        Wend
        Count = i
        rst.MoveFirst
        While Not rst.EOF
            If IsNull(rst![SC05004]) Then
                For i = 1 To Count
                    If SerN(i) = rst![SC05002] Then
                        rst.Edit
                           rst![SC05004] = BuiN(i)
                        rst.Update
                        Exit For
                    End If
                Next i
            End If
            rst.MoveNext
        Wend
    End If
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    Exit Function
endd:
    Correct_Eq_Table = False

End Function

' MEXICO SYNCHRONISATION

Function SIMP_CUST_SYNC() As Boolean
    
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim rst2 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_CUST As String
    Dim CHECK As Variant
    Dim TMP_ADDRESS As String
    
    On Error GoTo endd
    SIMP_CUST_SYNC = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")
    Call UpdateProgress(1, "Synchronising Customers From SIMP into DFSD")

    source = "Synch03"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Customers"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If

    While Not rst.EOF
        CHECK = ""
        If IsNull(rst![Clave_Clie]) Then GoTo SKIPTHISONE
        CHECK = DLookup("[CustomerNumber]", "BD-Customers", "[CustomerNumber] Like '" & Convert_SS(rst![Clave_Clie]) & "'")
        
        If IsNull(CHECK) Then
            rst1.AddNew
            rst1![CustomerID] = DMax("[CustomerID]", "BD-Customers", "") + 1
            TMP_CUST = rst![Nombre_Clie]
            i = Dupl_CheckI("BD-Customers", "CustomerName", rst![Nombre_Clie], "CustomerID", rst1![CustomerID])
            If i > 0 Then
                TMP_CUST = TMP_CUST & "-" & i
            End If
            If Nz(rst![Nombre_Clie], "") <> "" Then rst1![CustomerName] = TMP_CUST
            If Nz(rst![Clave_Clie], "") <> "" Then rst1![CustomerNumber] = Left(rst![Clave_Clie], 50)
            If MID((rst![colonia] & " " & rst![delegacion]), 1, 250) = "" Then
                TMP_ADDRESS = "?"
            Else
                TMP_ADDRESS = rst![colonia] & " " & rst![delegacion]
            End If
            rst1![Address] = MID(TMP_ADDRESS, 1, 250)
            If Nz(rst![Ciudad], "") <> "" Then rst1![City] = rst![Ciudad]
            If Nz(rst![colonia], "") <> "" Then rst1![State] = Left(rst![Estado], 20)
            If Nz(rst![CodigoPostal], "") <> "" Then rst1![PostalCode] = Left(rst![CodigoPostal], 20)
            If Nz(rst![Telefono1], "") <> "" Then rst1![PhoneNumber] = Left(rst![Telefono1], 30)
            If Nz(rst![Fax], "") <> "" Then rst1![FaxNumber] = Left(rst![Fax], 30)
            rst1![RecordedDate] = Date
            rst1![UpdatedDate] = Date
            rst1![Creator] = Xstr
            rst1.Update
        Else
            source1 = "SELECT * FROM [BD-Customers] WHERE [CustomerNumber] Like '" & Convert_SS(rst![Clave_Clie]) & "'"
            Set rst2 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
            If rst2.RecordCount > 0 Then
                rst2.Edit
                TMP_CUST = rst![Nombre_Clie]
                i = Dupl_CheckI("BD-Customers", "CustomerName", rst![Clave_Clie], "CustomerID", rst1![CustomerID])
                If i > 0 Then
                    TMP_CUST = TMP_CUST & "-" & i
                End If
                If Nz(rst![Nombre_Clie], "") <> "" Then rst2![CustomerName] = TMP_CUST
                If Nz(rst![Clave_Clie], "") <> "" Then rst2![CustomerNumber] = Left(rst![Clave_Clie], 50)
                If MID((rst![colonia] & " " & rst![delegacion]), 1, 250) = "" Then
                    TMP_ADDRESS = "?"
                Else
                    TMP_ADDRESS = rst![colonia] & " " & rst![delegacion]
                End If
                rst2![Address] = MID(TMP_ADDRESS, 1, 250)
                If Nz(rst![Ciudad], "") <> "" Then rst2![City] = rst![Ciudad]
                If Nz(rst![colonia], "") <> "" Then rst2![State] = Left(rst![Estado], 20)
                If Nz(rst![CodigoPostal], "") <> "" Then rst2![PostalCode] = Left(rst![CodigoPostal], 20)
                If Nz(rst![Telefono1], "") <> "" Then rst2![PhoneNumber] = Left(rst![Telefono1], 30)
                If Nz(rst![Fax], "") <> "" Then rst2![FaxNumber] = Left(rst![Fax], 30)
                rst2![UpdatedDate] = Date
                rst2.Update
            End If
            rst2.Close
        End If
SKIPTHISONE:
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising Customers From SIMP into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SIMP_CUST_SYNC = False
    CloseProgress
End Function

Function SIMP_PART_SYNC() As Boolean
    
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim rst2 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_CUST As String
    Dim CHECK As Variant
    Dim TMP_ADDRESS As String
    
    On Error GoTo endd
    SIMP_PART_SYNC = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")
    Call UpdateProgress(1, "Synchronising Parts From SIMP into DFSD")

    source = "Synch04"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Parts List"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If

    While Not rst.EOF
        CHECK = ""
        If IsNull(rst![Clave_Art]) Then GoTo SKIPTHISONE
        CHECK = DLookup("[PartNumber]", "BD-Parts List", "[PartNumber] Like '" & Convert_SS(rst![Clave_Art]) & "'")
        
        If IsNull(CHECK) Then
            rst1.AddNew
            If Nz(rst![Clave_Art], "") <> "" Then rst1![PartNumber] = Left(rst![Clave_Art], 50)
            If Nz(rst![Descripcion], "") <> "" Then rst1![PartDescription] = Left(rst![Descripcion], 250)
            If Nz(rst![PrecioVenta], "") <> "" Then rst1![PartCost] = rst![PrecioVenta]
            rst1![UpdatedBy] = Xstr
            rst1![UpdatedOn] = Date
            rst1.Update
        Else
            source1 = "SELECT * FROM [BD-Parts List] WHERE [PartNumber] Like '" & Convert_SS(rst![Clave_Art]) & "'"
            Set rst2 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
            If rst2.RecordCount > 0 Then
                rst2.Edit
                If Nz(rst![Clave_Art], "") <> "" Then rst2![PartNumber] = Left(rst![Clave_Art], 50)
                If Nz(rst![Descripcion], "") <> "" Then rst2![PartDescription] = Left(rst![Descripcion], 250)
                If Nz(rst![PrecioVenta], "") <> "" Then rst2![PartCost] = rst![PrecioVenta]
                rst2![UpdatedBy] = Xstr
                rst2![UpdatedOn] = rst![Fecha_ult_modificacion]
                
                
                rst2.Update
            End If
            rst2.Close
        End If
SKIPTHISONE:
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising Parts From SIMP into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SIMP_PART_SYNC = False
    CloseProgress
End Function

Function Correct_FNB(EqID As Long, BUID As Long)
Dim dbs As Database
Dim rst As Recordset
Dim rst1 As Recordset
Dim rst2 As Recordset
Dim strSql As String
Dim CNtext As String
Dim AAtext As String
Dim AddCheck As String
Dim CityCheck As String
Dim StateCheck As String
Dim PCodeCheck As String
Dim PNumber As String
Dim Svisor As String
Dim MpX As Long
Dim MpY As Long
Dim MpID As Long
Dim BName As String
Dim BNumber As String


Set dbs = CurrentDb

'get info from BD-Buildings
                
strSql = "SELECT * FROM [BD-Buildings] WHERE [BuildingID]= " & BUID
Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
    If Nz(rst![MapX], "") <> "" Then MpX = rst![MapX]
    If Nz(rst![MapY], "") <> "" Then MpY = rst![MapY]
    If Nz(rst![MapID], "") <> "" Then MpID = rst![MapID]
    If Nz(rst![BuildingName], "") <> "" Then BName = rst![BuildingName]
    If Nz(rst![BuildingNumber], "") <> "" Then BNumber = rst![BuildingNumber]
    If Nz(rst![ContactFirstName], "") <> "" Then
        If Nz(rst![ContactLastName], "") <> "" Then
            CNtext = rst![ContactFirstName] & " / " & rst![ContactLastName]
        Else
            CNtext = rst![ContactFirstName]
        End If
    Else
        If Nz(rst![ContactLastName], "") <> "" Then
            CNtext = rst![ContactLastName]
        Else
            CNtext = "-"
        End If
    End If
    AddCheck = "-"
    CityCheck = "-"
    StateCheck = "-"
    PCodeCheck = "-"
    PNumber = "-"
    Svisor = "-"
    If Nz(rst![Address], "") <> "" Then AddCheck = rst![Address]
    If Nz(rst![City], "") <> "" Then CityCheck = rst![City]
    If Nz(rst![State], "") <> "" Then StateCheck = rst![State]
    If Nz(rst![PostalCode], "") <> "" Then PCodeCheck = rst![PostalCode]
    AAtext = AddCheck & ", " & CityCheck & ", " & StateCheck & ", " & PCodeCheck
    If Nz(rst![PhoneNumber], "") <> "" Then PNumber = rst![PhoneNumber]
    If Nz(rst![Supervisor], "") <> "" Then Svisor = rst![Supervisor]
                
rst.Close

'Finding reference to the Service Selection ID via SS4

    strSql = "SELECT * FROM [Service Selection 4] WHERE [EquipmentID] =" & EqID
    Set rst = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
        While Not rst.EOF
            
' fix Service Selection Table
            strSql = "SELECT * FROM [Service Selection] WHERE [Service Selection] =" & rst![Service Selection]
            Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            While Not rst1.EOF
                rst1.Edit
                rst1![BuildingID] = BUID
                rst1.Update
                rst1.MoveNext
            Wend
            rst1.Close
            
' fix Service Selection 5 Table
            strSql = "SELECT * FROM [Service Selection 5] WHERE [Service Selection] =" & rst![Service Selection]
            Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            While Not rst1.EOF
                rst1.Edit
                rst1![BuildingID] = BUID
                rst1![BuildingName] = BName
                rst1![BuildingNumber] = BNumber
                rst1![MapX] = MpX
                rst1![MapY] = MpY
                rst1![CN] = CNtext
                rst1![AA] = AAtext
                rst1![Supervisor] = Svisor
                rst1![PhoneNumber] = PNumber
                rst1.Update
                
                rst1.MoveNext
            Wend
            rst1.Close
           
' fix Service Selection 6 Table
            strSql = "SELECT * FROM [Service Selection 6] WHERE [Service Selection] =" & rst![Service Selection]
            Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            While Not rst1.EOF
                rst1.Edit
                rst1![BuildingID] = BUID
                rst1![BuildingName] = BName
                rst1![BuildingNumber] = BNumber
                rst1![MapX] = MpX
                rst1![MapY] = MpY
                rst1![CN] = CNtext
                rst1![AA] = AAtext
                rst1![Supervisor] = Svisor
                rst1![PhoneNumber] = PNumber
                rst1.Update
                rst1.MoveNext
            Wend
            rst1.Close
        
' fix Tag Table
            strSql = "SELECT * FROM [Service Selection 2] WHERE [Service Selection] =" & rst![Service Selection]
            Set rst1 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                strSql = "SELECT * FROM [Tag] WHERE [TagID] = '" & rst1![Tag] & "'"
                Set rst2 = dbs.OpenRecordset(strSql, dbOpenDynaset, dbSeeChanges)
                    While Not rst2.EOF
                        rst2.Edit
                        rst2![BuildingID] = BUID
                        rst2.Update
                        rst2.MoveNext
                    Wend
                rst2.Close
            End If
            rst1.Close
            rst.MoveNext
        Wend
        rst.Close

                
'        If C1 = CL Then
'            Call UpdateProgress(100 * (C / Count), "Fixing moved Equipment")
'            C1 = 0
'        End If
'        C = C + 1
'        C1 = C1 + 1
    CloseProgress
    Set rst = Nothing
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set dbs = Nothing
End Function

Function fix_dash()

    Dim dbs As Database
    Dim rst As Recordset
    Dim LL As Integer
    Dim POS As Integer
    Dim FS As String
    Dim BS As String
    Dim NS As String
    Dim i As Integer

    Set dbs = CurrentDb
    
    Set rst = dbs.OpenRecordset("Fix18", dbOpenDynaset, dbSeeChanges)
    
    While Not rst.EOF
        LL = Len(rst![BuildingName])
        POS = InStr(1, rst![BuildingName], "�")
        FS = MID(rst![BuildingName], 1, POS - 1)
        BS = MID(rst![BuildingName], POS + 1, LL)
        NS = FS & "'" & BS
        i = Dupl_CheckI("BD-Buildings", "BuildingName", NS, "BuildingID", rst![BuildingID])
        If i > 0 Then
            NS = NS & "-" & i
        End If
        rst.Edit
        rst![BuildingName] = NS
        rst.Update
        rst.MoveNext
    Wend
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    
End Function

Function SAS_CUST_SYNC_I() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim rst2 As Recordset
    Dim source2 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_CUST As String
    Dim FN As Long
    
    FN = DLookup("[FirstNumber]", "General Office Info")
        
    On Error GoTo endd
    SAS_CUST_SYNC_I = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")

    source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'I' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Customers"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    While Not rst.EOF
        If DCount("[CustomerNumber]", "[BD-Customers]", "[CustomerNumber] Like '" & Convert_SS(rst![CUST_NBR]) & "'") = 0 Then
            rst1.AddNew
            If DCount("[CustomerID]", "BD-Customers") > 0 Then
                If DMax("[CustomerID]", "BD-Customers", "") > 0 Then
                    rst1![CustomerID] = DMax("[CustomerID]", "BD-Customers", "") + 1
                Else
                    rst1![CustomerID] = DLookup("[FirstNumber]", "General Office Info")
                End If
            Else
                rst1![CustomerID] = DLookup("[FirstNumber]", "General Office Info")
            End If
                
            TMP_CUST = rst![CUST_NAME]
            i = Dupl_CheckI("BD-Customers", "CustomerName", rst![CUST_NAME], "CustomerID", rst1![CustomerID])
            If i > 0 Then
                TMP_CUST = TMP_CUST & "-" & i
            End If
            If Nz(rst![CUST_NAME], "") <> "" Then rst1![CustomerName] = TMP_CUST
            If Nz(rst![CUST_NBR], "") <> "" Then rst1![CustomerNumber] = Left(rst![CUST_NBR], 50)
            If Nz(rst![CUST_CATG], "") <> "" Then rst1![Customer Segment] = Left(rst![CUST_CATG], 50)
            If Nz(rst![CUST_CATG], "") <> "" Then rst1![CustomerLevel] = Left(rst![CUST_CATG], 50)
            source2 = "SELECT * FROM [Synch04] WHERE [CUST_NBR] = '" & rst![CUST_NBR] & "'"
            Set rst2 = dbs.OpenRecordset(source2, dbOpenDynaset, dbSeeChanges)
            If rst2.RecordCount > 0 Then
                While Not rst2.EOF
                    If Nz(rst2![ADDR_1], "") <> "" Then rst1![Address] = Left(rst2![ADDR_1], 250)
                    If Nz(rst2![ADDR_2], "") <> "" Then rst1![Address] = Left(rst1![Address] & " " & rst2![ADDR_2], 250)
                    If Nz(rst2![ADDR_3], "") <> "" Then rst1![Address] = Left(rst1![Address] & " " & rst2![ADDR_3], 250)
                    If Nz(rst2![City], "") <> "" Then rst1![City] = rst2![City]
                    If Nz(rst2![COUNTRY_CD], "") <> "" Then rst1![State] = rst2![COUNTRY_CD]
                    If Nz(rst2![POST_CD], "") <> "" Then rst1![PostalCode] = rst2![POST_CD]
                    If Nz(rst2![CONTACT_PHONE], "") <> "" Then rst1![PhoneNumber] = rst2![CONTACT_PHONE]
                    If Nz(rst2![CONTACT_FAX], "") <> "" Then rst1![FaxNumber] = rst2![CONTACT_FAX]
                    If Nz(rst2![PAYMENT_MODE], "") <> "" Then rst1![PayTerm] = rst2![PAYMENT_MODE]
                    rst2.Delete
                    rst2.MoveNext
                Wend
            End If
            rst2.Close
            rst1![RecordedDate] = Date
            rst1![UpdatedDate] = Date
            rst1![Creator] = Xstr
            rst1.Update
            rst.Delete
        Else
            rst.Edit
            rst![ENTRY_TYPE] = "U"
            rst.Update
        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising new Customers From SAS into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SAS_CUST_SYNC_I = False
    CloseProgress
End Function

Function SAS_CUST_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim rst2 As Recordset
    Dim source2 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_CUST As String
    Dim FN As Long
    
    FN = DLookup("[FirstNumber]", "General Office Info")
        
    On Error GoTo endd
    SAS_CUST_SYNC_U = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")

    source = "SELECT * FROM [Synch03] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    While Not rst.EOF
        
        source1 = "SELECT * FROM [BD-Customers] WHERE [CustomerNumber] Like '" & Convert_SS(rst![CUST_NBR]) & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        If rst1.RecordCount > 0 Then
            rst1.Edit
            TMP_CUST = rst![CUST_NAME]
            i = Dupl_CheckI("BD-Customers", "CustomerName", rst![CUST_NAME], "CustomerID", rst1![CustomerID])
            If i > 0 Then
                TMP_CUST = TMP_CUST & "-" & i
            End If
            If Nz(rst![CUST_NAME], "") <> "" Then rst1![CustomerName] = TMP_CUST
            If Nz(rst![CUST_NBR], "") <> "" Then rst1![CustomerNumber] = Left(rst![CUST_NBR], 50)
            If Nz(rst![CUST_CATG], "") <> "" Then rst1![Customer Segment] = Left(rst![CUST_CATG], 50)
            If Nz(rst![CUST_CATG], "") <> "" Then rst1![CustomerLevel] = Left(rst![CUST_CATG], 50)
            source2 = "SELECT * FROM [Synch04] WHERE [CUST_NBR] = '" & rst![CUST_NBR] & "'"
            Set rst2 = dbs.OpenRecordset(source2, dbOpenDynaset, dbSeeChanges)
            If rst2.RecordCount > 0 Then
                While Not rst2.EOF
                    If Nz(rst2![ADDR_1], "") <> "" Then rst1![Address] = Left(rst2![ADDR_1], 250)
                    If Nz(rst2![ADDR_2], "") <> "" Then rst1![Address] = Left(rst1![Address] & " " & rst2![ADDR_2], 250)
                    If Nz(rst2![ADDR_3], "") <> "" Then rst1![Address] = Left(rst1![Address] & " " & rst2![ADDR_3], 250)
                    If Nz(rst2![City], "") <> "" Then rst1![City] = rst2![City]
                    If Nz(rst2![COUNTRY_CD], "") <> "" Then rst1![State] = rst2![COUNTRY_CD]
                    If Nz(rst2![POST_CD], "") <> "" Then rst1![PostalCode] = rst2![POST_CD]
                    If Nz(rst2![CONTACT_PHONE], "") <> "" Then rst1![PhoneNumber] = rst2![CONTACT_PHONE]
                    If Nz(rst2![CONTACT_FAX], "") <> "" Then rst1![FaxNumber] = rst2![CONTACT_FAX]
                    If Nz(rst2![PAYMENT_MODE], "") <> "" Then rst1![PayTerm] = rst2![PAYMENT_MODE]
                    rst2.Delete
                    rst2.MoveNext
                Wend
            End If
            rst2.Close
            rst1![UpdatedDate] = Date
            rst1![Creator] = Xstr & "-up"
            rst1.Update
            rst.Delete
        Else
'customer does not exist same code as inserting customer below
            rst1.AddNew
            If DCount("[CustomerID]", "BD-Customers") > 0 Then
                If DMax("[CustomerID]", "BD-Customers", "") > 0 Then
                    rst1![CustomerID] = DMax("[CustomerID]", "BD-Customers", "") + 1
                Else
                    rst1![CustomerID] = DLookup("[FirstNumber]", "General Office Info")
                End If
            Else
                rst1![CustomerID] = DLookup("[FirstNumber]", "General Office Info")
            End If
                
            TMP_CUST = rst![CUST_NAME]
            i = Dupl_CheckI("BD-Customers", "CustomerName", rst![CUST_NAME], "CustomerID", rst1![CustomerID])
            If i > 0 Then
                TMP_CUST = TMP_CUST & "-" & i
            End If
            If Nz(rst![CUST_NAME], "") <> "" Then rst1![CustomerName] = TMP_CUST
            If Nz(rst![CUST_NBR], "") <> "" Then rst1![CustomerNumber] = Left(rst![CUST_NBR], 50)
            If Nz(rst![CUST_CATG], "") <> "" Then rst1![Customer Segment] = Left(rst![CUST_CATG], 50)
            If Nz(rst![CUST_CATG], "") <> "" Then rst1![CustomerLevel] = Left(rst![CUST_CATG], 50)
            
            source2 = "SELECT * FROM [Synch04] WHERE [CUST_NBR] = '" & rst![CUST_NBR] & "'"
            Set rst2 = dbs.OpenRecordset(source2, dbOpenDynaset, dbSeeChanges)
            If rst2.RecordCount > 0 Then
                While Not rst2.EOF
                    If Nz(rst2![ADDR_1], "") <> "" Then rst1![Address] = Left(rst2![ADDR_1], 250)
                    If Nz(rst2![ADDR_2], "") <> "" Then rst1![Address] = Left(rst1![Address] & " " & rst2![ADDR_2], 250)
                    If Nz(rst2![ADDR_3], "") <> "" Then rst1![Address] = Left(rst1![Address] & " " & rst2![ADDR_3], 250)
                    If Nz(rst2![City], "") <> "" Then rst1![City] = rst2![City]
                    If Nz(rst2![COUNTRY_CD], "") <> "" Then rst1![State] = rst2![COUNTRY_CD]
                    If Nz(rst2![POST_CD], "") <> "" Then rst1![PostalCode] = rst2![POST_CD]
                    If Nz(rst2![CONTACT_PHONE], "") <> "" Then rst1![PhoneNumber] = rst2![CONTACT_PHONE]
                    If Nz(rst2![CONTACT_FAX], "") <> "" Then rst1![FaxNumber] = rst2![CONTACT_FAX]
                    If Nz(rst2![PAYMENT_MODE], "") <> "" Then rst1![PayTerm] = rst2![PAYMENT_MODE]
                    rst2.Delete
                    rst2.MoveNext
                Wend
            End If
            rst2.Close
            rst1![RecordedDate] = Date
            rst1![UpdatedDate] = Date
            rst1![Creator] = Xstr
            rst1.Update
            rst.Delete

        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Customers From SAS into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
        rst1.Close
    Wend
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set rst2 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SAS_CUST_SYNC_U = False
    CloseProgress
End Function

Function SAS_BUIL_SYNC_I() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_BUIL As String
    Dim FN As Long
    Dim OFF1 As String
    Dim OFF2 As String
    Dim OFF3 As String
    Dim OFF4 As String
    Dim OFF5 As String
    Dim OFF6 As String
    
    FN = DLookup("[FirstNumber]", "General Office Info")
    OFF1 = DLookup("[OFFICE]", "TRN", "[FirstNumber] = " & FN)
    
    OFF2 = "N/A"
    OFF3 = "N/A"
    OFF4 = "N/A"
    OFF5 = "N/A"
    OFF6 = "N/A"
    If OFF1 = "LYO" Then OFF2 = "STR"
    If OFF1 = "BOR" Then
        OFF2 = "ORL"
        OFF3 = "LIL"
        OFF4 = "NAN"
        OFF5 = "ROU"
    End If
    If OFF1 = "MAR" Then
        OFF2 = "MON"
        OFF3 = "TOU"
    End If
    
    On Error GoTo endd
    SAS_BUIL_SYNC_I = True
        
    Set dbs = CurrentDb
    
    Call Call_prog("")

    source = "SELECT * FROM [Synch01] WHERE [ENTRY_TYPE] = 'I' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Buildings"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        If DCount("[BuildingNumber]", "[BD-Buildings]", "[BuildingNumber] Like '" & Convert_SS(rst![WORKSITE_NBR]) & "'") = 0 Then
             If rst![OFFICE_CD] = OFF1 Or rst![OFFICE_CD] = OFF2 Or rst![OFFICE_CD] = OFF3 Or rst![OFFICE_CD] = OFF4 Or rst![OFFICE_CD] = OFF5 Then
                rst1.AddNew
                If DCount("[BuildingID]", "BD-Buildings") > 0 Then
                    rst1![BuildingID] = DMax("[BuildingID]", "BD-Buildings", "") + 1
                Else
                    rst1![BuildingID] = DLookup("[FirstNumber]", "General Office Info")
                End If
                TMP_BUIL = rst![ADDR_1]
                i = Dupl_CheckI("BD-Buildings", "BuildingName", rst![ADDR_1], "BuildingID", rst1![BuildingID])
                If i > 0 Then
                    TMP_BUIL = TMP_BUIL & "-" & i
                End If
                If Nz(rst![ADDR_1], "") <> "" Then rst1![BuildingName] = TMP_BUIL
                If Nz(rst![WORKSITE_NBR], "") <> "" Then rst1![BuildingNumber] = rst![WORKSITE_NBR]
                If Nz(rst![ADDR_2], "") <> "" Then rst1![Address] = Left(rst![ADDR_2], 250)
                If Nz(rst![ADDR_3], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![ADDR_3]), 250)
                If Nz(rst![City], "") <> "" Then rst1![City] = rst![City]
                If Nz(rst![POST_CD], "") <> "" Then rst1![PostalCode] = rst![POST_CD]
                If Nz(rst![CONTACT_PHONE], "") <> "" Then rst1![PhoneNumber] = rst![CONTACT_PHONE]
                If Nz(rst![CONTACT_FAX], "") <> "" Then rst1![FaxNumber] = rst![CONTACT_FAX]
                If Nz(rst![CONTACT_NAME], "") <> "" Then rst1![ContactFirstName] = rst![CONTACT_NAME]
                If Nz(rst![COUNTRY_CD], "") <> "" Then rst1![State] = rst![COUNTRY_CD]
                If Nz(rst![COUNTRY_CD], "") <> "" Then rst1![State] = rst![COUNTRY_CD]
                If Nz(rst![CLASS_CD], "") <> "" Then rst1![Segment] = rst![CLASS_CD]
                If rst![OFFICE_CD] = "TOU" Then
                    rst1![OfficeID] = 3
                ElseIf rst![OFFICE_CD] = "LIL" Then
                    rst1![OfficeID] = 1
                ElseIf rst![OFFICE_CD] = "NAN" Then
                    rst1![OfficeID] = 2
                ElseIf rst![OFFICE_CD] = "BOR" Then
                    rst1![OfficeID] = 4
                ElseIf rst![OFFICE_CD] = "PAR" Then
                    rst1![OfficeID] = 5
                ElseIf rst![OFFICE_CD] = "ROU" Then
                    rst1![OfficeID] = 6
                ElseIf rst![OFFICE_CD] = "LYO" Then
                    rst1![OfficeID] = 7
                ElseIf rst![OFFICE_CD] = "STR" Then
                    rst1![OfficeID] = 8
                ElseIf rst![OFFICE_CD] = "MON" Then
                    rst1![OfficeID] = 9
                Else
                End If
                rst1![RecordedDate] = Date
                rst1![UpdatedDate] = Date
                rst1![Creator] = Xstr
                rst1.Update
                rst.Delete
             Else
                rst.Delete
             End If
        Else
            rst.Edit
            rst![ENTRY_TYPE] = "U"
            rst.Update
        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising new Buildngs From SAS into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SAS_BUIL_SYNC_I = False
    CloseProgress

End Function

Function SAS_BUIL_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    Dim TMP_BUIL As String
    Dim AddCheck As String
    Dim CityCheck As String
    Dim StateCheck As String
    Dim PCodeCheck As String
    Dim PNumber As String
    Dim AAtext As String
    Dim FN As Long
    Dim OFF1 As String
    Dim OFF2 As String
    Dim OFF3 As String
    Dim OFF4 As String
    Dim OFF5 As String
    Dim OFF6 As String
    
    FN = DLookup("[FirstNumber]", "General Office Info")
    OFF1 = DLookup("[OFFICE]", "TRN", "[FirstNumber] = " & FN)
    
    OFF2 = "N/A"
    OFF3 = "N/A"
    OFF4 = "N/A"
    OFF5 = "N/A"
    OFF6 = "N/A"
    If OFF1 = "LYO" Then OFF2 = "STR"
    If OFF1 = "BOR" Then
        OFF2 = "ORL"
        OFF3 = "LIL"
        OFF4 = "NAN"
        OFF5 = "ROU"
    End If
    If OFF1 = "MAR" Then
        OFF2 = "MON"
        OFF3 = "TOU"
    End If

    On Error GoTo endd
    SAS_BUIL_SYNC_U = True
    
    Set dbs = CurrentDb
    
    Call Call_prog("")

    source = "SELECT * FROM [Synch01] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        If rst![OFFICE_CD] = OFF1 Or rst![OFFICE_CD] = OFF2 Or rst![OFFICE_CD] = OFF3 Or rst![OFFICE_CD] = OFF4 Or rst![OFFICE_CD] = OFF5 Then
            source1 = "SELECT * FROM [BD-Buildings] WHERE [BuildingNumber] Like '" & Convert_SS(rst![WORKSITE_NBR]) & "'"
            Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
            If rst1.RecordCount > 0 Then
                rst1.Edit
                TMP_BUIL = rst![ADDR_1]
                i = Dupl_CheckI("BD-Buildings", "BuildingName", rst![ADDR_1], "BuildingID", rst1![BuildingID])
                If i > 0 Then
                    TMP_BUIL = TMP_BUIL & "-" & i
                End If
                If Nz(rst![ADDR_1], "") <> "" Then rst1![BuildingName] = TMP_BUIL
                If Nz(rst![WORKSITE_NBR], "") <> "" Then rst1![BuildingNumber] = rst![WORKSITE_NBR]
                If Nz(rst![ADDR_2], "") <> "" Then rst1![Address] = Left(rst![ADDR_2], 250)
                If Nz(rst![ADDR_3], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![ADDR_3]), 250)
                If Nz(rst![City], "") <> "" Then rst1![City] = rst![City]
                If Nz(rst![POST_CD], "") <> "" Then rst1![PostalCode] = rst![POST_CD]
                If Nz(rst![CONTACT_PHONE], "") <> "" Then rst1![PhoneNumber] = rst![CONTACT_PHONE]
                If Nz(rst![CONTACT_FAX], "") <> "" Then rst1![FaxNumber] = rst![CONTACT_FAX]
                If Nz(rst![CONTACT_NAME], "") <> "" Then rst1![ContactFirstName] = rst![CONTACT_NAME]
                If Nz(rst![COUNTRY_CD], "") <> "" Then rst1![State] = rst![COUNTRY_CD]
                If Nz(rst![COUNTRY_CD], "") <> "" Then rst1![State] = rst![COUNTRY_CD]
                If Nz(rst![CLASS_CD], "") <> "" Then rst1![Segment] = rst![CLASS_CD]
                rst1![UpdatedDate] = Date
                rst1![Creator] = Xstr & "-up"
                rst1.Update
            Else
' Building does not exist in DFSD it must be added (SAME CODES AS BUILDING ADD BELOW)
                rst1.AddNew
                If DCount("[BuildingID]", "BD-Buildings") > 0 Then
                    rst1![BuildingID] = DMax("[BuildingID]", "BD-Buildings", "") + 1
                Else
                    rst1![BuildingID] = DLookup("[FirstNumber]", "General Office Info")
                End If
                TMP_BUIL = rst![ADDR_1]
                i = Dupl_CheckI("BD-Buildings", "BuildingName", rst![ADDR_1], "BuildingID", rst1![BuildingID])
                If i > 0 Then
                    TMP_BUIL = TMP_BUIL & "-" & i
                End If
                If Nz(rst![ADDR_1], "") <> "" Then rst1![BuildingName] = TMP_BUIL
                If Nz(rst![WORKSITE_NBR], "") <> "" Then rst1![BuildingNumber] = rst![WORKSITE_NBR]
                If Nz(rst![ADDR_2], "") <> "" Then rst1![Address] = Left(rst![ADDR_2], 250)
                If Nz(rst![ADDR_3], "") <> "" Then rst1![Address] = Left((rst1![Address] & " " & rst![ADDR_3]), 250)
                If Nz(rst![City], "") <> "" Then rst1![City] = rst![City]
                If Nz(rst![POST_CD], "") <> "" Then rst1![PostalCode] = rst![POST_CD]
                If Nz(rst![CONTACT_PHONE], "") <> "" Then rst1![PhoneNumber] = rst![CONTACT_PHONE]
                If Nz(rst![CONTACT_FAX], "") <> "" Then rst1![FaxNumber] = rst![CONTACT_FAX]
                If Nz(rst![CONTACT_NAME], "") <> "" Then rst1![ContactFirstName] = rst![CONTACT_NAME]
                If Nz(rst![COUNTRY_CD], "") <> "" Then rst1![State] = rst![COUNTRY_CD]
                If Nz(rst![COUNTRY_CD], "") <> "" Then rst1![State] = rst![COUNTRY_CD]
                If Nz(rst![CLASS_CD], "") <> "" Then rst1![Segment] = rst![CLASS_CD]
                If rst![OFFICE_CD] = "TOU" Then
                    rst1![OfficeID] = 3
                ElseIf rst![OFFICE_CD] = "LIL" Then
                    rst1![OfficeID] = 1
                ElseIf rst![OFFICE_CD] = "NAN" Then
                    rst1![OfficeID] = 2
                ElseIf rst![OFFICE_CD] = "BOR" Then
                    rst1![OfficeID] = 4
                ElseIf rst![OFFICE_CD] = "PAR" Then
                    rst1![OfficeID] = 5
                ElseIf rst![OFFICE_CD] = "ROU" Then
                    rst1![OfficeID] = 6
                ElseIf rst![OFFICE_CD] = "LYO" Then
                    rst1![OfficeID] = 7
                ElseIf rst![OFFICE_CD] = "STR" Then
                    rst1![OfficeID] = 8
                ElseIf rst![OFFICE_CD] = "MON" Then
                    rst1![OfficeID] = 9
                End If
                rst1![RecordedDate] = Date
                rst1![UpdatedDate] = Date
                rst1![Creator] = Xstr
                rst1.Update
                rst.Delete
            End If
            rst1.Close
        Else
            rst.Delete
        End If
        
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Buildings From SAS into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    rst.Close
    CloseProgress
    
' UPDATE SERVICE SELECTION 5 TABLE
   Call Call_prog("")
   Call UpdateProgress(1, "Updating changed Buildings From Scala into DFSD Service Backlog")

    source = "SELECT * FROM [Synch01] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    While Not rst.EOF
        source1 = "SELECT * FROM [Service Selection 5] WHERE [BuildingNumber] Like '" & Convert_SS(rst![WORKSITE_NBR]) & "'"
        Set rst1 = dbs.OpenRecordset(source1)
        While Not rst1.EOF
            rst1.Edit
            If Nz(rst![ADDR_1], "") <> "" Then rst1![BuildingName] = rst![ADDR_1]
            AddCheck = "-"
            CityCheck = "-"
            StateCheck = "-"
            PCodeCheck = "-"
            PNumber = "-"
            If Nz(rst![ADDR_2], "") <> "" Then AddCheck = rst![ADDR_2]
            If Nz(rst![ADDR_3], "") <> "" Then AddCheck = AddCheck & " " & rst![ADDR_3]
            If Nz(rst![City], "") <> "" Then CityCheck = rst![City]
            If Nz(rst![POST_CD], "") <> "" Then PCodeCheck = rst![POST_CD]

            AAtext = AddCheck & ", " & CityCheck & ", " & StateCheck & ", " & PCodeCheck
            rst1![AA] = Left(AAtext, 250)
            If Nz(rst![CONTACT_PHONE], "") <> "" Then PNumber = rst![CONTACT_PHONE]
            rst1![PhoneNumber] = Left(PNumber, 30)
            rst1.Update
            rst1.MoveNext
        Wend
        rst1.Close
        rst.Delete
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Buildings From SAS into DFSD SB")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SAS_BUIL_SYNC_U = False
    CloseProgress

End Function

Function SAS_EQUI_SYNC_I() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim Eqt As String
    Dim i As Integer
    Dim TMP_BUIL As String
    Dim TMP_MODEL As String
    Dim LL As Integer
    Dim CHECK As Integer
    
    On Error GoTo endd
    SAS_EQUI_SYNC_I = True
        
    Set dbs = CurrentDb
    Call Call_prog("")

    source = "SELECT * FROM [Synch02] WHERE [ENTRY_TYPE] = 'I' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Equipments"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    While Not rst.EOF
        CHECK = 0
        If IsNull(rst![SERIAL_CD]) Then
            CHECK = DCount("[SASID]", "[BD-Equipments]", "[SASID] Like '" & Convert_SS((rst![WORKSITE_NBR]) & rst![ITEM_SEQ]) & "'")
        Else
            CHECK = DCount("[FinRef]", "[BD-Equipments]", "[FinRef] Like '" & Convert_SS(rst![SERIAL_CD]) & "'")
        End If
        If CHECK = 0 Then
            BID = 0
            If Nz(rst![WORKSITE_NBR], "") <> "" Then Call FIND_BID(CStr(rst![WORKSITE_NBR]))
            If BID <> 0 Then
                rst1.AddNew
                rst1![EquipmentID] = DMax("[EquipmentID]", "BD-Equipments", "") + 1
                If Nz(rst![WORKSITE_NBR], "") <> "" Then rst1![BuildingID] = BID
                If Nz(rst![SERIAL_CD], "") <> "" Then rst1![Equipment Tag] = Left(rst![SERIAL_CD], 50)
'xxxxxx
                If Nz(rst![MATL], "") <> "" Then rst1![Equipment Tag] = MID(rst1![Equipment Tag] & " " & Left(rst![MATL], 10), 1, 50)
                If Nz(rst![FITTER_NAME], "") <> "" Then rst1![Maintenance] = Left(rst![FITTER_NAME], 50)
                If Nz(rst![WORKSITE_NBR], "") <> "" Then rst1![Location] = Left(rst![WORKSITE_NBR], 50)
'xxxxxxx
                If Nz(rst![MATL], "") <> "" Then
                    TMP_MODEL = Left(rst![MATL], 100)
                    rst1![Model] = Left(TMP_MODEL, 100)
                End If
                If Nz(rst![SERIAL_CD], "") <> "" Then rst1![Serial Number] = Left(rst![SERIAL_CD], 100)
                If Nz(rst![SERIAL_CD], "") <> "" Then rst1![FinRef] = Left(rst![SERIAL_CD], 100)
                If Nz(rst![ORDER_NBR], "") <> "" Then rst1![Sales Order #] = Left(rst![ORDER_NBR], 100)
                If Nz(rst![ORDER_START_DT], "") <> "" Then rst1![Comdate] = rst![ORDER_START_DT]
                If Nz(rst![END_WARRANTY_DT], "") <> "" Then rst1![Warranty] = rst![END_WARRANTY_DT]
                If Nz(rst![ORDER_SHIP_DT], "") <> "" Then rst1![ShipDate] = rst![ORDER_SHIP_DT]
                If Nz(rst![REFRIG_CD], "") <> "" Then rst1![Special Notes] = Left(rst![REFRIG_CD], 100)
                
                If Nz(rst![MATL], "") <> "" Then
                    Eqt = MID(TMP_MODEL, 1, 3) & "*"
                    If DCount("[Equipment Type Description]", "Equipment Type", "[Equipment Type]=" & "'" & Eqt & "'") > 0 Then
                        rst1![Equipment Type] = Eqt
                        rst1![DFSorNDFS] = -1
                    Else
                        rst1![Equipment Type] = Left(Eqt, 3)
                        rst1![DFSorNDFS] = 0
                    End If
                End If
                rst1![SASID] = rst![WORKSITE_NBR] & rst![ITEM_SEQ]
                        
                rst1![RecordedDate] = Date
                rst1![UpdatedDate] = Date
                rst1![Creator] = Xstr
                rst1.Update
            Else
    ' NOT EQUIPMENT FROM THIS LOCATION DELETE IT
            End If
            rst.Delete
        Else
            rst.Edit
            rst![ENTRY_TYPE] = "U"
            rst.Update
        End If
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising New Equipment From SAS into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    
    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SAS_EQUI_SYNC_I = False
    CloseProgress

End Function

Function SAS_EQUI_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim Eqt As String
    Dim TMP_MODEL As String
    Dim TMP_BUIL As String
    Dim LL As Integer
    Dim i As Integer
    Dim CHECK As Integer
    
    On Error GoTo endd
    SAS_EQUI_SYNC_U = True
        
    Set dbs = CurrentDb
    
    Call Call_prog("")

    source = "SELECT * FROM [Synch02] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
            
    While Not rst.EOF
        CHECK = 0
        If IsNull(rst![SERIAL_CD]) Then
            source1 = "SELECT * FROM [BD-Equipments] WHERE [SASID] Like '" & Convert_SS((rst![WORKSITE_NBR]) & rst![ITEM_SEQ]) & "'"
        Else
            source1 = "SELECT * FROM [BD-Equipments] WHERE [FinRef] Like '" & Convert_SS(rst![SERIAL_CD]) & "'"
        End If
        
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        If rst1.RecordCount > 0 Then
            BID = 0
            Call FIND_BID(CStr(rst![WORKSITE_NBR]))
            rst1.Edit
            If Nz(rst![WORKSITE_NBR], "") <> "" Then rst1![BuildingID] = BID
            If Nz(rst![SERIAL_CD], "") <> "" Then rst1![Equipment Tag] = Left(rst![SERIAL_CD], 50)
            If Nz(rst![MATL], "") <> "" Then rst1![Equipment Tag] = MID(rst1![Equipment Tag] & " " & Left(rst![MATL], 10), 1, 50)
            If Nz(rst![FITTER_NAME], "") <> "" Then rst1![Maintenance] = Left(rst![FITTER_NAME], 50)
            If Nz(rst![WORKSITE_NBR], "") <> "" Then rst1![Location] = Left(rst![WORKSITE_NBR], 50)
                
            If Nz(rst![MATL], "") <> "" Then
                TMP_MODEL = Left(rst![MATL], 100)
                rst1![Model] = Left(TMP_MODEL, 100)
            End If
            If Nz(rst![SERIAL_CD], "") <> "" Then rst1![Serial Number] = Left(rst![SERIAL_CD], 100)
            If Nz(rst![ORDER_NBR], "") <> "" Then rst1![Sales Order #] = Left(rst![ORDER_NBR], 100)
            If Nz(rst![ORDER_START_DT], "") <> "" Then rst1![Comdate] = rst![ORDER_START_DT]
            If Nz(rst![END_WARRANTY_DT], "") <> "" Then rst1![Warranty] = rst![END_WARRANTY_DT]
            If Nz(rst![ORDER_SHIP_DT], "") <> "" Then rst1![ShipDate] = rst![ORDER_SHIP_DT]
            If Nz(rst![REFRIG_CD], "") <> "" Then rst1![Special Notes] = Left(rst![REFRIG_CD], 250)
            If Nz(rst![MATL], "") <> "" Then
                Eqt = MID(TMP_MODEL, 1, 3) & "*"
                If DCount("[Equipment Type Description]", "Equipment Type", "[Equipment Type]=" & "'" & Eqt & "'") > 0 Then
                    rst1![Equipment Type] = Eqt
                    rst1![DFSorNDFS] = -1
                Else
                    rst1![Equipment Type] = Left(Eqt, 3)
                    rst1![DFSorNDFS] = 0
                End If
            End If
            rst1![SASID] = rst![WORKSITE_NBR] & rst![ITEM_SEQ]
            rst1![UpdatedDate] = Date
            rst1.Update
            BN = 0
            Call FIND_BNO(rst1![BuildingID])
            If rst![WORKSITE_NBR] <> BN Then
    ' Building has changed.  Do necessary changes
            If BID > 0 Then Call Correct_FNB(rst1![EquipmentID], BID)
                    
            End If
        Else
    ' Equipment does not exist in DFSD it must be added (SAME CODE AS EQUIMENT ADD BELOW)
            BID = 0
            If Nz(rst![WORKSITE_NBR], "") <> "" Then Call FIND_BID(CStr(rst![WORKSITE_NBR]))
            If BID <> 0 Then
                rst1.AddNew
                rst1![EquipmentID] = DMax("[EquipmentID]", "BD-Equipments", "") + 1
                If Nz(rst![WORKSITE_NBR], "") <> "" Then rst1![BuildingID] = BID
                If Nz(rst![SERIAL_CD], "") <> "" Then rst1![Equipment Tag] = Left(rst![SERIAL_CD], 50)
                If Nz(rst![MATL], "") <> "" Then
                    TMP_MODEL = Left(rst![MATL], 100)
                    rst1![Model] = Left(TMP_MODEL, 100)
                End If
                If Nz(rst![SERIAL_CD], "") <> "" Then rst1![Serial Number] = Left(rst![SERIAL_CD], 100)
                If Nz(rst![SERIAL_CD], "") <> "" Then rst1![FinRef] = Left(rst![SERIAL_CD], 100)
                If Nz(rst![ORDER_NBR], "") <> "" Then rst1![Sales Order #] = Left(rst![ORDER_NBR], 100)
                If Nz(rst![ORDER_START_DT], "") <> "" Then rst1![Comdate] = rst![ORDER_START_DT]
                If Nz(rst![END_WARRANTY_DT], "") <> "" Then rst1![Warranty] = rst![END_WARRANTY_DT]
                If Nz(rst![ORDER_SHIP_DT], "") <> "" Then rst1![ShipDate] = rst![ORDER_SHIP_DT]
                If Nz(rst![REFRIG_CD], "") <> "" Then rst1![Special Notes] = Left(rst![REFRIG_CD], 100)
                If Nz(rst![MATL], "") <> "" Then
                    Eqt = MID(TMP_MODEL, 1, 3) & "*"
                    If DCount("[Equipment Type Description]", "Equipment Type", "[Equipment Type]=" & "'" & Eqt & "'") > 0 Then
                        rst1![Equipment Type] = Eqt
                        rst1![DFSorNDFS] = -1
                    Else
                        rst1![Equipment Type] = Left(Eqt, 3)
                        rst1![DFSorNDFS] = 0
                    End If
                End If
                rst1![SASID] = rst![WORKSITE_NBR] & rst![ITEM_SEQ]
                rst1![RecordedDate] = Date
                rst1![UpdatedDate] = Date
                rst1![Creator] = Xstr
                rst1.Update
            Else
                    
            End If
        End If
        rst1.Close
        rst.Delete
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Equipment From SAS into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend
    
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SAS_EQUI_SYNC_U = False
    CloseProgress

End Function


Function SAS_PART_SYNC_I() As Boolean

    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer

    On Error GoTo endd
    SAS_PART_SYNC_I = True

    Set dbs = CurrentDb

    Call Call_prog("")

    source = "SELECT * FROM [Synch05] WHERE [ENTRY_TYPE] = 'I' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    source1 = "BD-Parts List"
    Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    While Not rst.EOF
        If DCount("[PartNumber]", "[BD-Parts List]", "[PartNumber] = '" & rst![PART_NBR] & "'") = 0 Then
            rst1.AddNew
            If DCount("[PartID]", "BD-Parts List") > 0 Then
                If DMax("[PartID]", "BD-Parts List", "") > 0 Then
                    rst1![PartID] = DMax("[PartID]", "BD-Parts List", "") + 1
                Else
                    rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
                End If
            Else
                rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
            End If

            If Nz(rst![PART_NBR], "") <> "" Then rst1![PartNumber] = Left(rst![PART_NBR], 50)
            If Nz(rst![PART_DESCR], "") <> "" Then rst1![PartDescription] = Left(rst![PART_DESCR], 250)
            If Nz(rst![STOCK_PRICE], "") <> "" Then rst1![PartCost] = rst![STOCK_PRICE]
            If Nz(rst![PRICE_LIST], "") <> "" Then rst1![SC] = rst![PRICE_LIST]
            If Nz(rst![PART_TYPE], "") <> "" Then rst1![PartFamily] = rst![PART_TYPE]
            If Nz(rst![PART_CLASS], "") <> "" Then rst1![PartFamily1] = rst![PART_CLASS]
        
            rst1![PartUnit] = "Nr"
            rst1![UpdatedOn] = Date
            rst1![UpdatedBy] = Xstr
            rst1.Update
            rst.Delete
        Else
            rst.Edit
            rst![ENTRY_TYPE] = "U"
            rst.Update
        End If

        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Synchronising new Parts From SAS into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend

    rst.Close
    rst1.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SAS_PART_SYNC_I = False
    CloseProgress
End Function

Function SAS_PART_SYNC_U() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim i As Integer
    
    On Error GoTo endd
    SAS_PART_SYNC_U = True

    Set dbs = CurrentDb

    Call Call_prog("")

    source = "SELECT * FROM [Synch05] WHERE [ENTRY_TYPE] = 'U' ORDER BY [ENTRY_DATE]"
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    While Not rst.EOF
        source1 = "SELECT * FROM [BD-Parts List] WHERE [PartNumber] Like '" & Convert_SS(rst![PART_NBR]) & "'"
        Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
        If rst1.RecordCount > 0 Then
            rst1.Edit
            If Nz(rst![PART_NBR], "") <> "" Then rst1![PartNumber] = Left(rst![PART_NBR], 50)
            If Nz(rst![PART_DESCR], "") <> "" Then rst1![PartDescription] = Left(rst![PART_DESCR], 250)
            If Nz(rst![STOCK_PRICE], "") <> "" Then rst1![PartCost] = rst![STOCK_PRICE]
            If Nz(rst![PRICE_LIST], "") <> "" Then rst1![SC] = rst![PRICE_LIST]
            If Nz(rst![PART_TYPE], "") <> "" Then rst1![PartFamily] = rst![PART_TYPE]
            If Nz(rst![PART_CLASS], "") <> "" Then rst1![PartFamily1] = rst![PART_CLASS]
            
            rst1![UpdatedOn] = Date
            rst1![UpdatedBy] = Xstr
            rst1.Update
        Else
'Part does not exist in DFSD it must be added (SAME CODE AS PART ADD BELOW)
           rst1.AddNew
            If DCount("[PartID]", "BD-Parts List") > 0 Then
                If DMax("[PartID]", "BD-Parts List", "") > 0 Then
                    rst1![PartID] = DMax("[PartID]", "BD-Parts List", "") + 1
                Else
                    rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
                End If
            Else
                rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
            End If

            If Nz(rst![PART_NBR], "") <> "" Then rst1![PartNumber] = Left(rst![PART_NBR], 50)
            If Nz(rst![PART_DESCR], "") <> "" Then rst1![PartDescription] = Left(rst![PART_DESCR], 250)
            If Nz(rst![STOCK_PRICE], "") <> "" Then rst1![PartCost] = rst![STOCK_PRICE]
            If Nz(rst![PRICE_LIST], "") <> "" Then rst1![SC] = rst![PRICE_LIST]
            If Nz(rst![PART_TYPE], "") <> "" Then rst1![PartFamily] = rst![PART_TYPE]
            If Nz(rst![PART_CLASS], "") <> "" Then rst1![PartFamily1] = rst![PART_CLASS]
            
            rst1![PartUnit] = "Nr"
            rst1![UpdatedOn] = Date
            rst1![UpdatedBy] = Xstr
            rst1.Update
        End If
        rst1.Close
        rst.Delete
        rst.MoveNext
        If C1 = CL Then
            Call UpdateProgress(100 * (C / Count), "Updating changed Parts From SAS into DFSD")
            C1 = 0
        End If
        C = C + 1
        C1 = C1 + 1
    Wend

    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SAS_PART_SYNC_U = False
    CloseProgress

End Function

'MODIFIED BY JOSE 18TH JUNE 2011
Function SCALA_PART_SYNC_M() As Boolean
    Dim dbs As Database
    Dim rst As Recordset
    Dim source As String
    Dim rst1 As Recordset
    Dim source1 As String
    Dim C As Single
    Dim C1 As Single
    Dim CL As Single
    Dim Count As Single
    Dim FN As Long
        
    FN = DLookup("[FirstNumber]", "General Office Info")
    On Error GoTo endd
    SCALA_PART_SYNC_M = True

    DoCmd.SetWarnings False
    DoCmd.OpenQuery "Mercury-DFSD Synch"
    DoCmd.SetWarnings True
    Set dbs = CurrentDb

    Call Call_prog("")
    If FN = 24000000 Or FN = 46000000 Or FN = 48000000 Or FN = 50000000 Or FN = 52000000 Or FN = 54000000 Or FN = 56000000 Or FN = 62000000 Then
        source = "SELECT [STOCKCODE], [AVAILABLE_QTY], [SELL_IND],[REPLACE_PART], [AVAILABLE_QTY] FROM [Synch06] WHERE [AVAILABLE_QTY] IS NOT NULL"
    ElseIf FN = 126000000 Or FN = 130000000 Or FN = 150000000 Then
         source = "SELECT [STOCKCODE], [PART_CATEG], [PART_CLASS], [SELL_IND],[REPLACE_PART], [AVAILABLE_QTY], [TRANSFER_PRICE], [BOOK_PRICE],[DESCRIPTION1] FROM [Synch06] WHERE [PART_CATEG] IS NOT NULL OR [PART_CLASS] IS NOT NULL OR [AVAILABLE_QTY] IS NOT NULL"
    Else
        source = "SELECT [STOCKCODE], [PART_CATEG], [PART_CLASS], [SELL_IND],[REPLACE_PART], [AVAILABLE_QTY] FROM [Synch06] WHERE [PART_CATEG] IS NOT NULL OR [PART_CLASS] IS NOT NULL OR [AVAILABLE_QTY] IS NOT NULL"
    End If
    
   
    
    Set rst = dbs.OpenRecordset(source, dbOpenDynaset, dbSeeChanges)
    If rst.RecordCount > 0 Then
        rst.MoveLast
        rst.MoveFirst
        Count = rst.RecordCount
    Else
        Count = 1
    End If
    C = 1
    C1 = 1
    If Count < 100 Then
        CL = 1
    Else
        CL = Int(Count / 100)
    End If
    
    
    If FN = 126000000 Or FN = 130000000 Or FN = 150000000 Then
        ' Portugal and Greece Mercury to DFSD inteface, different to others as we skip intermediate step through the financial system
            While Not rst.EOF
                source1 = "SELECT * FROM [BD-Parts List] WHERE [PartNumber] Like '" & Convert_SS(rst![STOCKCODE]) & "'"
                Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
                If rst1.RecordCount > 0 Then
                    'part exists
                    rst1.Edit
                    If Nz(rst![PART_CATEG], "") <> "" Then rst1![PartFamily] = Left(rst![PART_CATEG], 250)
                    If Nz(rst![PART_CLASS], "") <> "" Then rst1![PartFamily1] = Left(rst![PART_CLASS], 250)
                    If Nz(rst![AVAILABLE_QTY], "") <> "" Then rst1![Central_Stock] = rst![AVAILABLE_QTY]
                    If Nz(rst![SELL_IND], "") <> "" Then rst1![OBS] = rst![SELL_IND]
                    If Nz(rst![REPLACE_PART], "") <> "" Then rst1![PartNumberR] = rst![REPLACE_PART]
                    If Nz(rst![TRANSFER_PRICE], "") <> "" Then rst1![PartCost] = rst![TRANSFER_PRICE]
                    
                    rst1![UpdatedOn] = Date
                    rst1![UpdatedBy] = "Mercury-DFSD Updated"
                    rst1.Update
                Else
                    'part needs to be added
                    If rst![SELL_IND] = "OBS" And rst![AVAILABLE_QTY] <= 0 Then
                    Else
                        rst1.AddNew
                        If DCount("[PartID]", "BD-Parts List") > 0 Then
                            If DMax("[PartID]", "BD-Parts List", "") > 0 Then
                                rst1![PartID] = DMax("[PartID]", "BD-Parts List", "") + 1
                            Else
                                rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
                            End If
                        Else
                            rst1![PartID] = DLookup("[FirstNumber]", "General Office Info")
                        End If
                        
                        If Nz(rst![STOCKCODE], "") <> "" Then rst1![PartNumber] = Left(rst![STOCKCODE], 50)
                        If Nz(rst![DESCRIPTION1], "") <> "" Then rst1![PartDescription] = Left(rst![DESCRIPTION1], 250)
                        If Nz(rst![PART_CATEG], "") <> "" Then rst1![PartFamily] = Left(rst![PART_CATEG], 250)
                        If Nz(rst![PART_CLASS], "") <> "" Then rst1![PartFamily1] = Left(rst![PART_CLASS], 250)
                        If Nz(rst![AVAILABLE_QTY], "") <> "" Then rst1![Central_Stock] = rst![AVAILABLE_QTY]
                        If Nz(rst![SELL_IND], "") <> "" Then rst1![OBS] = rst![SELL_IND]
                        If Nz(rst![REPLACE_PART], "") <> "" Then rst1![PartNumberR] = rst![REPLACE_PART]
                        If Nz(rst![TRANSFER_PRICE], "") <> "" Then rst1![PartCost] = rst![TRANSFER_PRICE]
                        If Nz(rst![BOOK_PRICE], "") <> "" Then rst1![SC] = rst![BOOK_PRICE]
                        
                        rst1![UpdatedOn] = Date
                        rst1![UpdatedBy] = "Mercury-DFSD Added"
                        
                        rst1.Update
                        
                    End If
                End If
                rst1.Close
                rst.Delete
                rst.MoveNext
                If C1 = CL Then
                    Call UpdateProgress(100 * (C / Count), "Synchronising  -  Mercury to DFSD")
                    C1 = 0
                End If
                C = C + 1
                C1 = C1 + 1
            Wend
        
    
    Else
        'only update available quantity for certain offices
        If FN = 24000000 Or FN = 46000000 Or FN = 48000000 Or FN = 50000000 Or FN = 52000000 Or FN = 54000000 Or FN = 56000000 Or FN = 62000000 Then
            While Not rst.EOF
                source1 = "SELECT * FROM [BD-Parts List] WHERE [PartNumber] Like '" & Convert_SS(rst![STOCKCODE]) & "'"
                Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
                If rst1.RecordCount > 0 Then
                    rst1.Edit
                    If Nz(rst![AVAILABLE_QTY], "") <> "" Then rst1![Central_Stock] = rst![AVAILABLE_QTY]
                    If Nz(rst![SELL_IND], "") <> "" Then rst1![OBS] = rst![SELL_IND]
                    If Nz(rst![REPLACE_PART], "") <> "" Then rst1![PartNumberR] = rst![REPLACE_PART]
                    
                    rst1![UpdatedOn] = Date
                    rst1![UpdatedBy] = Xstr
                    rst1.Update
                End If
                rst1.Close
                rst.Delete
                rst.MoveNext
                If C1 = CL Then
                    Call UpdateProgress(100 * (C / Count), "Synchronising  -  Mercury to DFSD")
                    C1 = 0
                End If
                C = C + 1
                C1 = C1 + 1
            Wend
        'update part category & class and available quantity for all other offices
        Else
            While Not rst.EOF
                source1 = "SELECT * FROM [BD-Parts List] WHERE [PartNumber] Like '" & Convert_SS(rst![STOCKCODE]) & "'"
                Set rst1 = dbs.OpenRecordset(source1, dbOpenDynaset, dbSeeChanges)
                If rst1.RecordCount > 0 Then
                    rst1.Edit
                    If Nz(rst![PART_CATEG], "") <> "" Then rst1![PartFamily] = Left(rst![PART_CATEG], 250)
                    If Nz(rst![PART_CLASS], "") <> "" Then rst1![PartFamily1] = Left(rst![PART_CLASS], 250)
                    If Nz(rst![AVAILABLE_QTY], "") <> "" Then rst1![Central_Stock] = rst![AVAILABLE_QTY]
                    If Nz(rst![SELL_IND], "") <> "" Then rst1![OBS] = rst![SELL_IND]
                    If Nz(rst![REPLACE_PART], "") <> "" Then rst1![PartNumberR] = rst![REPLACE_PART]
                    rst1![UpdatedOn] = Date
                    rst1![UpdatedBy] = "Mercury-DFSD"
                    rst1.Update
                End If
                rst1.Close
                rst.Delete
                rst.MoveNext
                If C1 = CL Then
                    Call UpdateProgress(100 * (C / Count), "Synchronising  -  Mercury to DFSD")
                    C1 = 0
                End If
                C = C + 1
                C1 = C1 + 1
            Wend
        End If
    End If
        
    rst.Close
    Set rst = Nothing
    Set rst1 = Nothing
    Set dbs = Nothing
    CloseProgress
    Exit Function
endd:
    SCALA_PART_SYNC_M = False
    CloseProgress

End Function

